package org.envoy.sqlserver.functionaltests;

import org.envoy.expression.*;

/**
 * Created by thomas on 7/20/14.
 */
public abstract class AbstractAddressableEntityExpression<T> extends AbstractEntityExpression<T> {
    private ContactInformationExpression contactInformation = new ContactInformationExpression(this);


    public AbstractAddressableEntityExpression(Class<? extends T> entityClass) {
        super(entityClass);
    }


    public ContactInformationExpression getContactInformation() {
        return contactInformation;
    }
}
