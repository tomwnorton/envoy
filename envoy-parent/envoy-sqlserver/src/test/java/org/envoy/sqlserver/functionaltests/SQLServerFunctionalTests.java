package org.envoy.sqlserver.functionaltests;

import org.envoy.query.grouping.*;
import org.envoy.sqlserver.*;
import org.junit.*;
import org.mockito.*;

import javax.sql.*;
import java.io.*;
import java.sql.*;
import java.util.*;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

/**
 * Created with IntelliJ IDEA. User: thomas Date: 1/9/13 Time: 6:59 PM To change
 * this template use File | Settings | File Templates.
 */
public class SQLServerFunctionalTests {
    static SQLServerDatabaseMetaData metaData;


    @Before
    public void setUp() {
        metaData = new SQLServerDatabaseMetaData();
    }


    @Test
    public void test_that_the_sql_looks_right() throws SQLException, IOException {
        //--Arrange
        final DataSource dataSource = mock(DataSource.class);
        final Connection connection = mock(Connection.class);
        final PreparedStatement ps = mock(PreparedStatement.class);
        final ResultSet rs = mock(ResultSet.class);

        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(ps);
        when(ps.executeQuery()).thenReturn(rs);

        when(rs.next()).thenReturn(true, true, true, true, false);
        when(rs.getObject("User_0_ID")).thenReturn(1, 2, 3, 9);
        when(rs.getString("User_0_FirstName")).thenReturn("Bob", "John", null);
        when(rs.getString("User_0_LastName")).thenReturn("Smith", "Jones", null);
        when(rs.getString("User_0_Zip")).thenReturn(null, null, "12345", null);
        when(rs.getString("User_0_UserName")).thenReturn("foo", "bar", null, "hello");
        when(rs.getBoolean("User_0_Active")).thenReturn(true, false, true, true);

        metaData.setDataSource(dataSource);

        //--Act
        final UserExpression user = new UserExpression();
        final List<User> actual = metaData.getQueryBuilder()
                .from(user)
                .where(user.isActive())
                .list();

        //--Assert
        Assert.assertEquals(4, actual.size());

        int index = 0;
        Assert.assertEquals(new Integer(1), actual.get(index).getId());
        Assert.assertEquals("foo", actual.get(index).getUserName());
        Assert.assertEquals("Bob Smith", actual.get(index).getContactInformation().getName().getFirstNameLastName());
        Assert.assertTrue(actual.get(index).isActive());

        index++;
        Assert.assertEquals(new Integer(2), actual.get(index).getId());
        Assert.assertEquals("bar", actual.get(index).getUserName());
        Assert.assertEquals("John Jones", actual.get(index).getContactInformation().getName().getFirstNameLastName());
        Assert.assertFalse(actual.get(index).isActive());

        index++;
        Assert.assertEquals(new Integer(3), actual.get(index).getId());
        Assert.assertEquals(null, actual.get(index).getUserName());
        Assert.assertEquals("12345", actual.get(index).getContactInformation().getPostalCode());
        Assert.assertNull(actual.get(index).getContactInformation().getName());
        Assert.assertTrue(actual.get(index).isActive());

        index++;
        Assert.assertEquals(new Integer(9), actual.get(index).getId());
        Assert.assertEquals("hello", actual.get(index).getUserName());
        Assert.assertNull(actual.get(index).getContactInformation());
        Assert.assertTrue(actual.get(index).isActive());

        final ArgumentCaptor<String> arg = ArgumentCaptor.forClass(String.class);
        verify(connection).prepareStatement(arg.capture());
        assertThat(arg.getValue(), org.hamcrest.Matchers.startsWith("SELECT "));
        assertThat(arg.getValue(), org.hamcrest.Matchers.endsWith(" FROM [User] AS User_0 WHERE User_0.Active = (1)"));
        int positionOfFromClause = arg.getValue().indexOf(" FROM");
        String actualSelectListClause = arg.getValue().substring("SELECT ".length(), positionOfFromClause);
        String[] actualSelectList = actualSelectListClause.split(", ");
        assertThat(
                Arrays.asList(actualSelectList), containsInAnyOrder(
                        "User_0.ID AS User_0_ID",
                        "User_0.UserName AS User_0_UserName",
                        "User_0.FirstName AS User_0_FirstName",
                        "User_0.LastName AS User_0_LastName",
                        "User_0.Address1 AS User_0_Address1",
                        "User_0.Address2 AS User_0_Address2",
                        "User_0.City AS User_0_City",
                        "User_0.[State] AS User_0_State",
                        "User_0.Zip AS User_0_Zip",
                        "User_0.Country AS User_0_Country",
                        "User_0.Active AS User_0_Active"));
    }


    @Test
    public void test_that_the_sql_looks_right_for_custom_select() throws SQLException, IOException {
        //--Arrange
        final DataSource dataSource = mock(DataSource.class);
        final Connection connection = mock(Connection.class);
        final PreparedStatement ps = mock(PreparedStatement.class);
        final ResultSet rs = mock(ResultSet.class);

        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(ps);
        when(ps.executeQuery()).thenReturn(rs);

        when(rs.next()).thenReturn(true, true, true, true, false);
        when(rs.getObject("User_0_ID")).thenReturn(1, 2, 3, 9);
        when(rs.getString("User_0_FirstName")).thenReturn("Bob", "John", null);
        when(rs.getString("User_0_LastName")).thenReturn("Smith", "Jones", null);
        when(rs.getString("User_0_Zip")).thenReturn(null, null, "12345", null);
        when(rs.getString("User_0_UserName")).thenReturn("foo", "bar", null, "hello");
        when(rs.getBoolean("User_0_Active")).thenReturn(true, false, true, true);

        metaData.setDataSource(dataSource);

        //--Act
        final UserExpression user = new UserExpression();
        final List<Integer> actual = metaData.getQueryBuilder()
                .from(user)
                .aggregate(user.getId())
                .as(
                        new ColumnAggregator1<Integer, Integer>() {
                            @Override
                            public Integer aggregate(final Integer id) {
                                return id;
                            }
                        })
                .list();

        //--Assert
        Assert.assertEquals(4, actual.size());

        int index = 0;
        assertEquals(1, actual.get(index++).intValue());
        assertEquals(2, actual.get(index++).intValue());
        assertEquals(3, actual.get(index++).intValue());
        assertEquals(9, actual.get(index++).intValue());

        final ArgumentCaptor<String> arg = ArgumentCaptor.forClass(String.class);
        verify(connection).prepareStatement(arg.capture());
        Assert.assertEquals("SELECT User_0.ID AS User_0_ID FROM [User] AS User_0", arg.getValue());
    }
}
