package org.envoy;

import org.mockito.*;

public class IsNotEqual<T> extends ArgumentMatcher<T> {
    private final T notExpected;


    public IsNotEqual(final T notExpected) {
        this.notExpected = notExpected;
    }


    @Override
    public boolean matches(final Object argument) {
        return !notExpected.equals(argument);
    }
}
