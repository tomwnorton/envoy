package org.envoy.sqlserver;

import org.envoy.exceptions.*;

class ExceptionTransformerImpl implements ExceptionTransformer {
    @Override
    public EnvoyException transform(final Exception e) {
        return new EnvoyException("Could not execute query", e);
    }
}
