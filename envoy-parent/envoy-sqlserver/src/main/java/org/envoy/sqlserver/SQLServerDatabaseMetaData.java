package org.envoy.sqlserver;

import org.envoy.*;
import org.envoy.expression.*;
import org.envoy.query.*;

import java.sql.*;

public class SQLServerDatabaseMetaData extends AbstractSqlDatabaseMetaData {
    public SQLServerDatabaseMetaData() {
        super(new ExceptionTransformerImpl());
    }


    @Override
    public <EntityType> QueryResolver<EntityType, Connection> getQueryResolver() {
        return new BasicSqlQueryResolver<EntityType>();
    }


    @Override
    public SqlExpressionVisitor createExpressionVisitor() {
        return new ExpressionVisitorImpl(this);
    }


    @Override
    public String getStartForDatabaseObjectEscape() {
        return "[";
    }


    @Override
    public String getEndForDatabaseObjectEscape() {
        return "]";
    }
}
