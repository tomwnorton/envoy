package org.envoy.collection;

import org.apache.commons.lang3.*;

import java.util.*;

public class Tuple implements Comparable<Tuple> {
    private final List<? extends Comparable<?>> elements;


    public Tuple(final List<? extends Comparable<?>> elements) {
        this.elements = elements;
    }


    @Override
    public int compareTo(final Tuple rhs) {
        final int lhsSize = size();
        final int rhsSize = rhs.size();
        final int max = ObjectUtils.max(lhsSize, rhsSize);

        for (int i = 0; i < max; i++) {
            if (lhsSize == i) {
                return -1;
            }

            if (rhsSize == i) {
                return 1;
            }

            final int temp = get(i).compareTo(rhs.get(i));
            if (temp != 0) {
                return temp;
            }
        }

        return 0;
    }


    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof Tuple)) {
            return false;
        }

        return compareTo((Tuple) obj) == 0;
    }


    @SuppressWarnings("unchecked")
    public Comparable<Comparable<?>> get(final int index) {
        return (Comparable<Comparable<?>>) elements.get(index);
    }


    public int size() {
        return elements.size();
    }
}
