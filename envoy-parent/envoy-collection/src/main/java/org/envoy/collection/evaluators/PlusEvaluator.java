package org.envoy.collection.evaluators;

import java.math.*;

public class PlusEvaluator extends NumericBinaryEvaluator {
    public PlusEvaluator(final Evaluator lhs, final Evaluator rhs, final EvaluatorFactory evaluatorFactory) {
        super(lhs, rhs, evaluatorFactory);
    }


    @Override
    protected Byte calculate(final Byte lhs, final Byte rhs) {
        return (byte) (lhs + rhs);
    }


    @Override
    protected Short calculate(final Short lhs, final Short rhs) {
        return (short) (lhs + rhs);
    }


    @Override
    protected Integer calculate(final Integer lhs, final Integer rhs) {
        return lhs + rhs;
    }


    @Override
    protected Long calculate(final Long lhs, final Long rhs) {
        return lhs + rhs;
    }


    @Override
    protected Float calculate(final Float lhs, final Float rhs) {
        return lhs + rhs;
    }


    @Override
    protected Double calculate(final Double lhs, final Double rhs) {
        return lhs + rhs;
    }


    @Override
    protected BigInteger calculate(final BigInteger lhs, final BigInteger rhs) {
        return lhs.add(rhs);
    }


    @Override
    protected BigDecimal calculate(final BigDecimal lhs, final BigDecimal rhs) {
        return lhs.add(rhs);
    }
}
