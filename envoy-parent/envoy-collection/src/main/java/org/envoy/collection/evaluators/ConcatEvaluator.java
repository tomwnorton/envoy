package org.envoy.collection.evaluators;

public class ConcatEvaluator extends AbstractBinaryEvaluator<String> {
    public ConcatEvaluator(final Evaluator lhs, final Evaluator rhs, final EvaluatorFactory evaluatorFactory) {
        super(lhs, rhs, null, evaluatorFactory);
    }


    @Override
    protected String onEvaluate(final String lhs, final String rhs) {
        if (lhs == null || rhs == null) {
            return null;
        }
        return lhs + rhs;
    }
}
