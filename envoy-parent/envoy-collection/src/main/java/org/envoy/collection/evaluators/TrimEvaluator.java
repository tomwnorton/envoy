package org.envoy.collection.evaluators;

public class TrimEvaluator extends AbstractUnaryEvaluator<String> {
    public TrimEvaluator(final Evaluator operand) {
        super(operand);
    }


    @Override
    protected String onEvaluate(final Object entity, final String operand) {
        return operand.trim();
    }
}
