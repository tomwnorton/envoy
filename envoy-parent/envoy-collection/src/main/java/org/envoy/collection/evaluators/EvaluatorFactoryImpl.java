package org.envoy.collection.evaluators;

import org.envoy.expression.*;

import java.util.*;

public class EvaluatorFactoryImpl implements EvaluatorFactory {
    @Override
    public ValueEvaluator createValueEvaluator(final Object value) {
        return new ValueEvaluator(value);
    }


    @Override
    public NullEvaluator createNullEvaluator() {
        return new NullEvaluator();
    }


    @Override
    public EqualsEvaluator createEqualsEvaluator(final Evaluator lhs, final Evaluator rhs) {
        return new EqualsEvaluator(lhs, rhs, this);
    }


    @Override
    public NotEqualsEvaluator createNotEqualsEvaluator(final Evaluator lhs, final Evaluator rhs) {
        return new NotEqualsEvaluator(lhs, rhs, this);
    }


    @Override
    public OrEvaluator createOrEvaluator(final Evaluator lhs, final Evaluator rhs) {
        return new OrEvaluator(lhs, rhs);
    }


    @Override
    public AndEvaluator createAndEvaluator(final Evaluator lhs, final Evaluator rhs) {
        return new AndEvaluator(lhs, rhs);
    }


    @Override
    public FieldEvaluator createFieldEvaluator(final FieldExpression fieldExpr) {
        return new FieldEvaluator(fieldExpr);
    }


    @Override
    public LessThanEvaluator createLessThanEvaluator(final Evaluator lhs, final Evaluator rhs) {
        return new LessThanEvaluator(lhs, rhs, this);
    }


    @Override
    public ValueCastor createValueCastor(final Object lhs, final Object rhs) {
        return new ValueCastorImpl(lhs, rhs);
    }


    @Override
    public LessThanEqualEvaluator createLessThanEqualEvaluator(
            final Evaluator lhs,
            final Evaluator rhs) {
        return new LessThanEqualEvaluator(lhs, rhs, this);
    }


    @Override
    public GreaterThanEvaluator createGreaterThanEvaluator(final Evaluator lhs, final Evaluator rhs) {
        return new GreaterThanEvaluator(lhs, rhs, this);
    }


    @Override
    public GreaterThanEqualEvaluator createGreaterThanEqualEvaluator(final Evaluator lhs, final Evaluator rhs) {
        return new GreaterThanEqualEvaluator(lhs, rhs, this);
    }


    @Override
    public ConvertToStringEvaluator createConvertToStringEvaluator(final Evaluator evaluator) {
        return new ConvertToStringEvaluator(evaluator);
    }


    @Override
    public PlusEvaluator createPlusEvaluator(final Evaluator lhs, final Evaluator rhs) {
        return new PlusEvaluator(lhs, rhs, this);
    }


    @Override
    public MinusEvaluator createMinusEvaluator(final Evaluator lhs, final Evaluator rhs) {
        return new MinusEvaluator(lhs, rhs, this);
    }


    @Override
    public TimesEvaluator createTimesEvaluator(final Evaluator lhs, final Evaluator rhs) {
        return new TimesEvaluator(lhs, rhs, this);
    }


    @Override
    public DividedByEvaluator createDividedByEvaluator(final Evaluator lhs, final Evaluator rhs) {
        return new DividedByEvaluator(lhs, rhs, this);
    }


    @Override
    public ModulusEvaluator createModulusEvaluator(final Evaluator lhs, final Evaluator rhs) {
        return new ModulusEvaluator(lhs, rhs, this);
    }


    @Override
    public CoalesceEvaluator createCoalesceEvaluator(final List<Evaluator> evals) {
        return new CoalesceEvaluator(evals);
    }


    @Override
    public <ResultType extends Number> NegativeEvaluator<ResultType> createNegativeEvaluator(final Evaluator operand) {
        return new NegativeEvaluator<ResultType>(operand);
    }


    @Override
    public AndEvaluator createBetweenEvaluator(final Evaluator lhs, final Evaluator lowerBound, final Evaluator upperBound) {
        final GreaterThanEqualEvaluator greaterThanEqualEvaluator = this.createGreaterThanEqualEvaluator(lhs, lowerBound);
        final LessThanEqualEvaluator lessThanEqualEvaluator = this.createLessThanEqualEvaluator(lhs, upperBound);
        return this.createAndEvaluator(greaterThanEqualEvaluator, lessThanEqualEvaluator);
    }


    @Override
    public TrimEvaluator createTrimEvaluator(final Evaluator operand) {
        return new TrimEvaluator(operand);
    }


    @Override
    public LengthEvaluator createLengthEvaluator(final Evaluator operand) {
        return new LengthEvaluator(operand);
    }


    @Override
    public LowerCaseEvaluator createLowerCaseEvaluator(final Evaluator operand) {
        return new LowerCaseEvaluator(operand);
    }


    @Override
    public UpperCaseEvaluator createUpperCaseEvaluator(final Evaluator operand) {
        return new UpperCaseEvaluator(operand);
    }


    @Override
    public ConvertStringToIntegerEvaluator createConvertStringToIntegerEvaluator(final Evaluator operand) {
        return new ConvertStringToIntegerEvaluator(operand);
    }


    @Override
    public ConcatEvaluator createConcatEvaluator(final Evaluator lhs, final Evaluator rhs) {
        return new ConcatEvaluator(lhs, rhs, this);
    }


    @Override
    public ContainsEvaluator createContainsEvaluator(final Evaluator lhs, final Evaluator rhs) {
        return new ContainsEvaluator(lhs, rhs, this);
    }


    @Override
    public StartsWithEvaluator createStartsWithEvaluator(final Evaluator lhs, final Evaluator rhs) {
        return new StartsWithEvaluator(lhs, rhs, this);
    }


    @Override
    public EndsWithEvaluator createEndsWithEvaluator(final Evaluator lhs, final Evaluator rhs) {
        return new EndsWithEvaluator(lhs, rhs, this);
    }


    @Override
    public InEvaluator createInEvaluator(final Evaluator lhs, final Collection<Object> values) {
        return new InEvaluator(lhs, values);
    }
}
