package org.envoy.collection.evaluators;

public class LessThanEvaluator extends AbstractBinaryEvaluator<Comparable<Object>> {
    public LessThanEvaluator(final Evaluator lhs, final Evaluator rhs, final EvaluatorFactory evaluatorFactory) {
        super(lhs, rhs, false, evaluatorFactory);
    }


    @Override
    protected Boolean onEvaluate(final Comparable<Object> lhs, final Comparable<Object> rhs) {
        return lhs != null && rhs != null && lhs.compareTo(rhs) < 0;
    }
}
