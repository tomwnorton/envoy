package org.envoy.collection.evaluators;

import java.math.*;

public class NegativeEvaluator<ResultType extends Number> extends AbstractUnaryEvaluator<Number> {
    public NegativeEvaluator(final Evaluator operand) {
        super(operand);
    }


    @SuppressWarnings("unchecked")
    @Override
    protected ResultType onEvaluate(final Object entity, final Number operand) {
        final Number operandResult = (Number) operand;

        if (operandResult instanceof Byte) {
            return (ResultType) ((Object) (byte) (-operandResult.byteValue()));
        } else if (operandResult instanceof Short) {
            return (ResultType) ((Object) (short) (-operandResult.shortValue()));
        } else if (operandResult instanceof Integer) {
            return (ResultType) ((Object) (-operandResult.intValue()));
        } else if (operandResult instanceof Long) {
            return (ResultType) ((Object) (-operandResult.longValue()));
        } else if (operandResult instanceof Float) {
            return (ResultType) ((Object) (-operandResult.floatValue()));
        } else if (operandResult instanceof Double) {
            return (ResultType) ((Object) (-operandResult.doubleValue()));
        } else if (operandResult instanceof BigInteger) {
            return (ResultType) (((BigInteger) operandResult).negate());
        } else {
            return (ResultType) (((BigDecimal) operandResult).negate());
        }
    }
}
