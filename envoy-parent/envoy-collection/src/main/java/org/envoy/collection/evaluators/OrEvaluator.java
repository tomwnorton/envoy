package org.envoy.collection.evaluators;

public class OrEvaluator implements Evaluator {
    private final Evaluator lhs;
    private final Evaluator rhs;


    public OrEvaluator(final Evaluator lhs, final Evaluator rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }


    @Override
    public Object evaluate(final Object entity) {
        return ((Boolean) lhs.evaluate(entity)) || ((Boolean) rhs.evaluate(entity));
    }
}
