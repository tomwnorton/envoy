package org.envoy.collection.evaluators;

/**
 * This class has to behave just like a SQL plugin would: The SQL plugins treat:
 * <p/>
 * <code>
 * entity.getField().equalTo(null)
 * </code>
 * <p/>
 * as
 * <p/>
 * <code>
 * entity.field IS NULL
 * </code>
 * <p/>
 * so this class has to do the same. No NullPointerExceptions allowed.
 *
 * @author thomas
 */
public class EqualsEvaluator extends AbstractBinaryEvaluator<Object> {
    public EqualsEvaluator(final Evaluator lhs, final Evaluator rhs, final EvaluatorFactory evaluatorFactory) {
        super(lhs, rhs, false, evaluatorFactory);
    }


    @Override
    protected Boolean onEvaluate(final Object lhs, final Object rhs) {
        boolean result = false;

        if (lhs != null) {
            result = lhs.equals(rhs);
        } else if (lhs == null && rhs == null) {
            result = true;
        }

        return result;
    }


    @Override
    protected boolean useDefaultValue(final boolean isLhsNull, final boolean isRhsNull) {
        return isLhsNull && !isRhsNull || !isLhsNull && isRhsNull;
    }
}
