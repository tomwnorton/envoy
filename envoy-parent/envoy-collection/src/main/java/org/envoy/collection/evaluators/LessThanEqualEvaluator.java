package org.envoy.collection.evaluators;

public class LessThanEqualEvaluator extends AbstractBinaryEvaluator<Comparable<Object>> {
    public LessThanEqualEvaluator(final Evaluator lhs, final Evaluator rhs, final EvaluatorFactory factory) {
        super(lhs, rhs, false, factory);
    }


    @Override
    protected Boolean onEvaluate(final Comparable<Object> lhs, final Comparable<Object> rhs) {
        return lhs != null && rhs != null && lhs.compareTo(rhs) <= 0;
    }
}
