package org.envoy.collection.evaluators;

import java.util.*;

public class CoalesceEvaluator implements Evaluator {
    private final List<Evaluator> evals;


    public CoalesceEvaluator(final List<Evaluator> evals) {
        this.evals = evals;
    }


    @Override
    public Object evaluate(final Object entity) {
        for (final Evaluator evaluator : evals) {
            final Object result = evaluator.evaluate(entity);
            if (result != null) {
                return result;
            }
        }

        return null;
    }
}
