package org.envoy.collection.evaluators;

public class StartsWithEvaluator extends AbstractBinaryEvaluator<String> {
    public StartsWithEvaluator(final Evaluator lhs, final Evaluator rhs, final EvaluatorFactory evaluatorFactory) {
        super(lhs, rhs, false, evaluatorFactory);
    }


    @Override
    protected Object onEvaluate(final String lhs, final String rhs) {
        return lhs.startsWith(rhs);
    }
}
