package org.envoy.collection;

import org.envoy.DatabaseMetaData;
import org.envoy.expression.*;
import org.envoy.query.*;
import org.envoy.query.grouping.*;

import java.sql.*;
import java.util.*;

class QueryResolverImpl<ResultType> implements QueryResolver<ResultType, AutoCloseable> {
    @Override
    @SuppressWarnings("unchecked")
    public List<ResultType> resolve(
            final DatabaseMetaData metaData,
            final SelectQueryModel<?, ResultType> sqlQuery,
            final AutoCloseable connection)
            throws SQLException {
        final CollectionDatabaseMetaData<?> collectionMetaData = (CollectionDatabaseMetaData<?>) metaData;
        final ArrayList<ResultType> results = new ArrayList<ResultType>();

        final ExpressionVisitorImpl visitor = collectionMetaData.createExpressionVisitor();
        if (sqlQuery.getWhereFilter() != null) {
            sqlQuery.getWhereFilter().accept(visitor);
        }

        for (final Object element : collectionMetaData.getCollection()) {
            if (sqlQuery.getWhereFilter() == null || visitor.isMatch(element)) {
                if (sqlQuery.getAggregatorWrapper() instanceof AggregatorWrapperForEntity) {
                    results.add((ResultType) element);
                } else {
                    final ArrayList<Object> values = new ArrayList<>();
                    for (final Expression columnExpr : sqlQuery.getSelectList()) {
                        final ExpressionVisitorImpl columnVisitor = collectionMetaData.createExpressionVisitor();
                        columnExpr.accept(columnVisitor);
                        values.add(columnVisitor.peek().evaluate(element));
                    }

                    results.add(sqlQuery.getAggregatorWrapper().aggregate(values));
                }
            }
        }

        return results;
    }
}
