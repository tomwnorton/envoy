package org.envoy.collection.evaluators;

/**
 * Calling these classes {@code Evaluators} so that no one confuses them for
 * {@code Expressions}
 *
 * @param <ResultType>
 * @author thomas
 */
public interface Evaluator {
    public Object evaluate(Object entity);
}
