package org.envoy.collection;

import org.envoy.collection.evaluators.*;
import org.envoy.expression.*;
import org.envoy.query.*;

import java.util.*;

public class ExpressionVisitorImpl implements ExpressionVisitor {
    private final EvaluatorFactory evaluatorFactory;
    private final Stack<Evaluator> stack;


    public ExpressionVisitorImpl(final EvaluatorFactory evaluatorFactory) {
        this.evaluatorFactory = evaluatorFactory;
        stack = new Stack<Evaluator>();
    }


    Evaluator peek() {
        return stack.peek();
    }


    void push(final Evaluator evaluator) {
        stack.push(evaluator);
    }


    @Override
    public void visitEquals(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        rhs.accept(this);

        final Evaluator rhsEvaluator = stack.pop();
        final Evaluator lhsEvaluator = stack.pop();

        stack.push(evaluatorFactory.createEqualsEvaluator(lhsEvaluator, rhsEvaluator));
    }


    @Override
    public void visitParameter(final Object value) {
        stack.push(evaluatorFactory.createValueEvaluator(value));
    }


    @Override
    public void visitNotEquals(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        rhs.accept(this);

        final Evaluator rhsEvaluator = stack.pop();
        final Evaluator lhsEvaluator = stack.pop();

        stack.push(evaluatorFactory.createNotEqualsEvaluator(lhsEvaluator, rhsEvaluator));
    }


    @Override
    public void visitAnd(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        rhs.accept(this);

        final Evaluator rhsEvaluator = stack.pop();
        final Evaluator lhsEvaluator = stack.pop();

        stack.push(evaluatorFactory.createAndEvaluator(lhsEvaluator, rhsEvaluator));
    }


    @Override
    public void visitOr(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        rhs.accept(this);

        final Evaluator rhsEvaluator = stack.pop();
        final Evaluator lhsEvaluator = stack.pop();

        stack.push(evaluatorFactory.createOrEvaluator(lhsEvaluator, rhsEvaluator));
    }


    @Override
    public void visitNot(final Expression expr) {
        expr.accept(this);
        final Evaluator evaluator = stack.pop();
        final Evaluator notEvaluator = new Evaluator() {
            @Override
            public Boolean evaluate(final Object entity) {
                return !((Boolean) evaluator.evaluate(entity));
            }
        };
        stack.push(notEvaluator);
    }


    @SuppressWarnings("unchecked")
    @Override
    public <T> void visitIn(final Expression expr, final Collection<T> collection) {
        expr.accept(this);
        final Evaluator lhs = stack.pop();
        stack.push(evaluatorFactory.createInEvaluator(lhs, (Collection<Object>) collection));
    }


    @Override
    public void visitField(final FieldExpression fieldExpr) {
        stack.push(evaluatorFactory.createFieldEvaluator(fieldExpr));
    }


    @Override
    public <T> void visitNull(final Expression expr) {
        expr.accept(this);

        final Evaluator lhs = stack.pop();
        final Evaluator rhs = evaluatorFactory.createNullEvaluator();

        stack.push(evaluatorFactory.createEqualsEvaluator(lhs, rhs));
    }


    @Override
    public <T> void visitNotNull(final Expression expr) {
        expr.accept(this);

        final Evaluator lhs = stack.pop();
        final Evaluator rhs = evaluatorFactory.createNullEvaluator();

        stack.push(evaluatorFactory.createNotEqualsEvaluator(lhs, rhs));
    }


    @Override
    public void visitTrue() {
        stack.push(evaluatorFactory.createValueEvaluator(true));
    }


    @Override
    public void visitFalse() {
        stack.push(evaluatorFactory.createValueEvaluator(false));
    }


    public boolean isMatch(final Object entity) {
        final Evaluator evaluator = stack.peek();
        return (Boolean) evaluator.evaluate(entity);
    }


    @Override
    public void visitLessThan(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        rhs.accept(this);

        final Evaluator rhsEvaluator = stack.pop();
        final Evaluator lhsEvaluator = stack.pop();

        stack.push(evaluatorFactory.createLessThanEvaluator(lhsEvaluator, rhsEvaluator));
    }


    @Override
    public void visitLessThanEqual(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        rhs.accept(this);

        final Evaluator rhsEvaluator = stack.pop();
        final Evaluator lhsEvaluator = stack.pop();

        stack.push(evaluatorFactory.createLessThanEqualEvaluator(lhsEvaluator, rhsEvaluator));
    }


    @Override
    public void visitGreaterThan(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        rhs.accept(this);

        final Evaluator rhsEvaluator = stack.pop();
        final Evaluator lhsEvaluator = stack.pop();

        stack.push(evaluatorFactory.createGreaterThanEvaluator(lhsEvaluator, rhsEvaluator));
    }


    @Override
    public void visitGreaterThanEqual(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        rhs.accept(this);

        final Evaluator rhsEvaluator = stack.pop();
        final Evaluator lhsEvaluator = stack.pop();

        stack.push(evaluatorFactory.createGreaterThanEqualEvaluator(lhsEvaluator, rhsEvaluator));
    }


    @Override
    public void visitConvertToString(final Expression expression) {
        expression.accept(this);
        final Evaluator evaluator = stack.pop();
        stack.push(evaluatorFactory.createConvertToStringEvaluator(evaluator));
    }


    @Override
    public void visitPlus(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        rhs.accept(this);

        final Evaluator rhsEvaluator = stack.pop();
        final Evaluator lhsEvaluator = stack.pop();

        stack.push(evaluatorFactory.createPlusEvaluator(lhsEvaluator, rhsEvaluator));
    }


    @Override
    public void visitMinus(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        rhs.accept(this);

        final Evaluator rhsEvaluator = stack.pop();
        final Evaluator lhsEvaluator = stack.pop();

        stack.push(evaluatorFactory.createMinusEvaluator(lhsEvaluator, rhsEvaluator));
    }


    @Override
    public void visitTimes(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        rhs.accept(this);

        final Evaluator rhsEvaluator = stack.pop();
        final Evaluator lhsEvaluator = stack.pop();

        stack.push(evaluatorFactory.createTimesEvaluator(lhsEvaluator, rhsEvaluator));
    }


    @Override
    public void visitDividedBy(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        rhs.accept(this);

        final Evaluator rhsEvaluator = stack.pop();
        final Evaluator lhsEvaluator = stack.pop();

        stack.push(evaluatorFactory.createDividedByEvaluator(lhsEvaluator, rhsEvaluator));
    }


    @Override
    public void visitModulus(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        rhs.accept(this);

        final Evaluator rhsEvaluator = stack.pop();
        final Evaluator lhsEvaluator = stack.pop();

        stack.push(evaluatorFactory.createModulusEvaluator(lhsEvaluator, rhsEvaluator));
    }


    @Override
    public void visitCoalesce(final List<? extends Expression> exprs) {
        final ArrayList<Evaluator> evals = new ArrayList<Evaluator>();
        for (final Expression expr : exprs) {
            expr.accept(this);
            evals.add(stack.pop());
        }

        stack.push(evaluatorFactory.createCoalesceEvaluator(evals));
    }


    @Override
    public void visitNegative(final Expression expr) {
        expr.accept(this);
        final Evaluator operand = stack.pop();
        stack.push(evaluatorFactory.createNegativeEvaluator(operand));
    }


    @Override
    public void visitBetween(final Expression lhs, final Expression lowerBound, final Expression upperBound) {
        lhs.accept(this);
        lowerBound.accept(this);
        upperBound.accept(this);

        final Evaluator upperBoundEval = stack.pop();
        final Evaluator lowerBoundEval = stack.pop();
        final Evaluator lhsEval = stack.pop();

        stack.push(evaluatorFactory.createBetweenEvaluator(lhsEval, lowerBoundEval, upperBoundEval));
    }


    @Override
    public void visitTrim(final Expression expr) {
        expr.accept(this);
        final Evaluator operand = stack.pop();
        stack.push(evaluatorFactory.createTrimEvaluator(operand));
    }


    @Override
    public void visitLength(final Expression expr) {
        expr.accept(this);
        final Evaluator operand = stack.pop();
        stack.push(evaluatorFactory.createLengthEvaluator(operand));
    }


    @Override
    public void visitLowerCase(final Expression expr) {
        expr.accept(this);
        final Evaluator operand = stack.pop();
        stack.push(evaluatorFactory.createLowerCaseEvaluator(operand));
    }


    @Override
    public void visitUpperCase(final Expression expr) {
        expr.accept(this);
        final Evaluator operand = stack.pop();
        stack.push(evaluatorFactory.createUpperCaseEvaluator(operand));
    }


    @Override
    public void visitConvertStringToInteger(final Expression expr) {
        expr.accept(this);
        final Evaluator operand = stack.pop();
        stack.push(evaluatorFactory.createConvertStringToIntegerEvaluator(operand));
    }


    @Override
    public void visitConcat(final Expression lhsExpr, final Expression rhsExpr) {
        lhsExpr.accept(this);
        rhsExpr.accept(this);

        final Evaluator rhs = stack.pop();
        final Evaluator lhs = stack.pop();

        stack.push(evaluatorFactory.createConcatEvaluator(lhs, rhs));
    }


    @Override
    public void visitContains(final Expression source, final Expression pattern) {
        source.accept(this);
        pattern.accept(this);

        final Evaluator patternEval = stack.pop();
        final Evaluator sourceEval = stack.pop();

        stack.push(evaluatorFactory.createContainsEvaluator(sourceEval, patternEval));
    }


    @Override
    public void visitStartsWith(final Expression source, final Expression pattern) {
        source.accept(this);
        pattern.accept(this);

        final Evaluator patternEval = stack.pop();
        final Evaluator sourceEval = stack.pop();

        stack.push(evaluatorFactory.createStartsWithEvaluator(sourceEval, patternEval));
    }


    @Override
    public void visitEndsWith(final Expression source, final Expression pattern) {
        source.accept(this);
        pattern.accept(this);

        final Evaluator patternEval = stack.pop();
        final Evaluator sourceEval = stack.pop();

        stack.push(evaluatorFactory.createEndsWithEvaluator(sourceEval, patternEval));
    }


    @Override
    public void visitSelectList(final List<? extends Expression> expressions) {
    }


    @Override
    public void visitEntity(final EntityExpression<?> expr) {
    }


    @Override
    public void visitSelect(
            final SelectListExpression selectListExpr,
            final EntityExpression<?> fromExpr,
            final BooleanExpression whereExpr) {
    }


    @Override
    public void visitCount(final Expression expr) {
        // TODO Auto-generated method stub

    }

    @Override
    public void visitInsert(InsertModel insertModel) {

    }

    @Override
    public void visitUpdate(UpdateModel updateModel) {

    }
}
