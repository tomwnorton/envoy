package org.envoy.collection.evaluators;

public class ValueEvaluator implements Evaluator {
    private final Object value;


    public ValueEvaluator(final Object value) {
        this.value = value;
    }


    @Override
    public Object evaluate(final Object entity) {
        return value;
    }
}
