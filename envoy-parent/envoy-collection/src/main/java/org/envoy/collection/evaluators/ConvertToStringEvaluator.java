package org.envoy.collection.evaluators;

public class ConvertToStringEvaluator implements Evaluator {
    private final Evaluator evaluator;


    public ConvertToStringEvaluator(final Evaluator evaluator) {
        this.evaluator = evaluator;
    }


    @Override
    public Object evaluate(final Object entity) {
        final Object temp = evaluator.evaluate(entity);
        return temp == null ? null : temp.toString();
    }
}
