package org.envoy.collection.evaluators;

public class NotEqualsEvaluator extends AbstractBinaryEvaluator<Object> {
    public NotEqualsEvaluator(final Evaluator lhs, final Evaluator rhs, final EvaluatorFactory evaluatorFactory) {
        super(lhs, rhs, true, evaluatorFactory);
    }


    @Override
    protected Boolean onEvaluate(final Object lhs, final Object rhs) {
        boolean result = false;

        if (lhs != null) {
            result = !lhs.equals(rhs);
        } else if (rhs != null) {
            result = !rhs.equals(lhs);
        }

        return result;
    }


    @Override
    protected boolean useDefaultValue(final boolean isLhsNull, final boolean isRhsNull) {
        return isLhsNull && !isRhsNull || !isLhsNull && isRhsNull;
    }
}
