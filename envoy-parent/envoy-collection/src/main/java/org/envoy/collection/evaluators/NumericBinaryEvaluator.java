package org.envoy.collection.evaluators;

import java.math.*;

public abstract class NumericBinaryEvaluator extends AbstractBinaryEvaluator<Number> {
    public NumericBinaryEvaluator(final Evaluator lhs, final Evaluator rhs, final EvaluatorFactory evaluatorFactory) {
        super(lhs, rhs, null, evaluatorFactory);
    }


    protected abstract Byte calculate(Byte lhs, Byte rhs);


    protected abstract Short calculate(Short lhs, Short rhs);


    protected abstract Integer calculate(Integer lhs, Integer rhs);


    protected abstract Long calculate(Long lhs, Long rhs);


    protected abstract Float calculate(Float lhs, Float rhs);


    protected abstract Double calculate(Double lhs, Double rhs);


    protected abstract BigInteger calculate(BigInteger lhs, BigInteger rhs);


    protected abstract BigDecimal calculate(BigDecimal lhs, BigDecimal rhs);


    @Override
    protected final Number onEvaluate(final Number lhs, final Number rhs) {
        if (lhs == null || rhs == null) {
            return null;
        } else if (lhs instanceof Byte) {
            return this.calculate((Byte) lhs, (Byte) rhs);
        } else if (lhs instanceof Short) {
            return this.calculate((Short) lhs, (Short) rhs);
        } else if (lhs instanceof Integer) {
            return this.calculate((Integer) lhs, (Integer) rhs);
        } else if (lhs instanceof Long) {
            return this.calculate((Long) lhs, (Long) rhs);
        } else if (lhs instanceof Float) {
            return this.calculate((Float) lhs, (Float) rhs);
        } else if (lhs instanceof Double) {
            return this.calculate((Double) lhs, (Double) rhs);
        } else if (lhs instanceof BigInteger) {
            return this.calculate((BigInteger) lhs, (BigInteger) rhs);
        } else {
            return this.calculate((BigDecimal) lhs, (BigDecimal) rhs);
        }
    }
}
