package org.envoy.collection.evaluators;

public abstract class AbstractBinaryEvaluator<OperandBaseType> implements Evaluator {
    private final Evaluator lhs;
    private final Evaluator rhs;
    private final Object defaultValue;
    private final EvaluatorFactory evaluatorFactory;


    protected abstract Object onEvaluate(OperandBaseType lhs, OperandBaseType rhs);


    public AbstractBinaryEvaluator(
            final Evaluator lhs,
            final Evaluator rhs,
            final Object defaultValue,
            final EvaluatorFactory evaluatorFactory) {
        this.lhs = lhs;
        this.rhs = rhs;
        this.defaultValue = defaultValue;
        this.evaluatorFactory = evaluatorFactory;
    }


    @SuppressWarnings("unchecked")
    @Override
    public Object evaluate(final Object entity) {
        final ValueCastor valueCastor = evaluatorFactory.createValueCastor(lhs.evaluate(entity), rhs.evaluate(entity));
        final OperandBaseType lhs = (OperandBaseType) valueCastor.getLhs();
        final OperandBaseType rhs = (OperandBaseType) valueCastor.getRhs();

        if (this.useDefaultValue(lhs == null, rhs == null)) {
            return defaultValue;
        }

        return onEvaluate(lhs, rhs);
    }


    protected boolean useDefaultValue(final boolean isLhsNull, final boolean isRhsNull) {
        return isLhsNull || isRhsNull;
    }
}
