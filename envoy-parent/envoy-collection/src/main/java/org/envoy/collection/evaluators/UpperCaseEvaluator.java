package org.envoy.collection.evaluators;

public class UpperCaseEvaluator extends AbstractUnaryEvaluator<String> {
    public UpperCaseEvaluator(final Evaluator operand) {
        super(operand);
    }


    @Override
    protected Object onEvaluate(final Object entity, final String operandValue) {
        return operandValue.toUpperCase();
    }
}
