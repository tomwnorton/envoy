package org.envoy.collection.evaluators;

public abstract class AbstractUnaryEvaluator<OperandType> implements Evaluator {
    private final Evaluator operand;


    protected abstract Object onEvaluate(Object entity, OperandType operandValue);


    public AbstractUnaryEvaluator(final Evaluator operand) {
        this.operand = operand;
    }


    @SuppressWarnings("unchecked")
    @Override
    public Object evaluate(final Object entity) {
        final Object operandValue = operand.evaluate(entity);
        return operandValue == null
               ? null
               : onEvaluate(entity, (OperandType) operandValue);
    }
}
