package org.envoy.collection.evaluators;

import org.envoy.expression.*;

import java.util.*;

public interface EvaluatorFactory {
    public ValueEvaluator createValueEvaluator(Object value);


    public NullEvaluator createNullEvaluator();


    public EqualsEvaluator createEqualsEvaluator(Evaluator lhs, Evaluator rhs);


    public NotEqualsEvaluator createNotEqualsEvaluator(Evaluator lhs, Evaluator rhs);


    public OrEvaluator createOrEvaluator(Evaluator lhs, Evaluator rhs);


    public AndEvaluator createAndEvaluator(Evaluator lhs, Evaluator rhs);


    public FieldEvaluator createFieldEvaluator(FieldExpression fieldExpr);


    public LessThanEvaluator createLessThanEvaluator(Evaluator lhs, Evaluator rhs);


    public ValueCastor createValueCastor(Object lhs, Object rhs);


    public LessThanEqualEvaluator createLessThanEqualEvaluator(Evaluator lhs, Evaluator rhs);


    public GreaterThanEvaluator createGreaterThanEvaluator(Evaluator lhs, Evaluator rhs);


    public GreaterThanEqualEvaluator createGreaterThanEqualEvaluator(Evaluator lhs, Evaluator rhs);


    public ConvertToStringEvaluator createConvertToStringEvaluator(Evaluator evaluator);


    public PlusEvaluator createPlusEvaluator(Evaluator lhs, Evaluator rhs);


    public MinusEvaluator createMinusEvaluator(Evaluator lhs, Evaluator rhs);


    public TimesEvaluator createTimesEvaluator(Evaluator lhs, Evaluator rhs);


    public DividedByEvaluator createDividedByEvaluator(Evaluator lhs, Evaluator rhs);


    public ModulusEvaluator createModulusEvaluator(Evaluator lhs, Evaluator rhs);


    public CoalesceEvaluator createCoalesceEvaluator(List<Evaluator> evals);


    public <ResultType extends Number> NegativeEvaluator<ResultType> createNegativeEvaluator(Evaluator operand);


    public AndEvaluator createBetweenEvaluator(Evaluator lhs, Evaluator lowerBound, Evaluator upperBound);


    public TrimEvaluator createTrimEvaluator(Evaluator operand);


    public LengthEvaluator createLengthEvaluator(Evaluator operand);


    public LowerCaseEvaluator createLowerCaseEvaluator(Evaluator operand);


    public UpperCaseEvaluator createUpperCaseEvaluator(Evaluator operand);


    public ConvertStringToIntegerEvaluator createConvertStringToIntegerEvaluator(Evaluator operand);


    public ConcatEvaluator createConcatEvaluator(Evaluator lhs, Evaluator rhs);


    public ContainsEvaluator createContainsEvaluator(Evaluator lhs, Evaluator rhs);


    public StartsWithEvaluator createStartsWithEvaluator(Evaluator lhs, Evaluator rhs);


    public EndsWithEvaluator createEndsWithEvaluator(Evaluator lhs, Evaluator rhs);


    public InEvaluator createInEvaluator(Evaluator lhs, Collection<Object> values);
}
