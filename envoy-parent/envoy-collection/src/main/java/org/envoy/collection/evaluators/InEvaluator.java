package org.envoy.collection.evaluators;

import java.util.*;

public class InEvaluator implements Evaluator {
    private final Evaluator lhs;
    private final Collection<Object> values;


    public InEvaluator(final Evaluator lhs, final Collection<Object> values) {
        this.lhs = lhs;
        this.values = values;
    }


    @Override
    public Object evaluate(final Object entity) {
        return values.contains(lhs.evaluate(entity));
    }
}
