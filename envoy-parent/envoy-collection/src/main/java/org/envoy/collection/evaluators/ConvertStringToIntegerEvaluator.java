package org.envoy.collection.evaluators;

public class ConvertStringToIntegerEvaluator extends AbstractUnaryEvaluator<String> {
    public ConvertStringToIntegerEvaluator(final Evaluator operand) {
        super(operand);
    }


    @Override
    protected Object onEvaluate(final Object entity, final String operandValue) {
        return Long.valueOf(operandValue);
    }
}
