package org.envoy.collection;

import org.envoy.*;
import org.envoy.collection.evaluators.*;
import org.envoy.query.*;

import java.io.*;
import java.util.*;

public class CollectionDatabaseMetaData<ElementType> extends AbstractDatabaseMetaData<AutoCloseable> {
    private final Collection<ElementType> collection;


    public CollectionDatabaseMetaData(final Collection<ElementType> collection) {
        super(null);
        this.collection = collection;
    }


    @Override
    public <EntityType> QueryResolver<EntityType, AutoCloseable> getQueryResolver() {
        return new QueryResolverImpl<EntityType>();
    }


    Collection<ElementType> getCollection() {
        return collection;
    }


    public ExpressionVisitorImpl createExpressionVisitor() {
        return new ExpressionVisitorImpl(new EvaluatorFactoryImpl());
    }


    @Override
    public AutoCloseable createConnection() {
        return new Closeable() {
            @Override
            public void close() throws IOException {
            }
        };
    }

    @Override
    public InsertResolver getInsertResolver() {
        return null;
    }

    @Override
    public UpdateResolver getUpdateResolver() {
        return null;
    }
}
