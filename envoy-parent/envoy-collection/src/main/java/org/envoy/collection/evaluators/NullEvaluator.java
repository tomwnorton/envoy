package org.envoy.collection.evaluators;

public class NullEvaluator implements Evaluator {
    @Override
    public Object evaluate(final Object entity) {
        return null;
    }
}
