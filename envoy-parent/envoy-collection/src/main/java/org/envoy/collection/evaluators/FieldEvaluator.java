package org.envoy.collection.evaluators;

import org.envoy.expression.*;

public class FieldEvaluator implements Evaluator {
    private final FieldExpression fieldExpr;


    public FieldEvaluator(final FieldExpression fieldExpr) {
        this.fieldExpr = fieldExpr;
    }


    @Override
    public Object evaluate(final Object entity) {
        return fieldExpr.getFromRootEntity(entity);
    }
}
