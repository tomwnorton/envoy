package org.envoy.collection.evaluators;

public class ReducingEvaluator implements Evaluator {
    //--Mutable code makes me want to hurl, but I can't think of any better way to do this :(
    private Object previousValue;
    private final Evaluator evaluator;
    private final Reducer reducer;


    public ReducingEvaluator(final Evaluator evaluator, final Reducer reducer) {
        this.previousValue = reducer.initialValue();
        this.evaluator = evaluator;
        this.reducer = reducer;
    }


    @Override
    public Object evaluate(final Object entity) {
        final Object newValue = reducer.reduce(previousValue, entity, evaluator);
        previousValue = newValue;
        return newValue;
    }
}
