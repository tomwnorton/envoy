package org.envoy.collection.evaluators;

public class LengthEvaluator extends AbstractUnaryEvaluator<String> {
    public LengthEvaluator(final Evaluator operand) {
        super(operand);
    }


    @Override
    protected Integer onEvaluate(final Object entity, final String operand) {
        return operand.length();
    }
}
