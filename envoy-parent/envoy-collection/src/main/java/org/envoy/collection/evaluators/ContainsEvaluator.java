package org.envoy.collection.evaluators;

public class ContainsEvaluator extends AbstractBinaryEvaluator<String> {
    public ContainsEvaluator(final Evaluator lhs, final Evaluator rhs, final EvaluatorFactory evaluatorFactory) {
        super(lhs, rhs, false, evaluatorFactory);
    }


    @Override
    protected Object onEvaluate(final String lhs, final String rhs) {
        return lhs.contains(rhs);
    }
}
