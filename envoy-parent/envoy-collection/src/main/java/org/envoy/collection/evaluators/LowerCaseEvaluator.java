package org.envoy.collection.evaluators;

public class LowerCaseEvaluator extends AbstractUnaryEvaluator<String> {
    public LowerCaseEvaluator(final Evaluator operand) {
        super(operand);
    }


    @Override
    protected String onEvaluate(final Object entity, final String operandValue) {
        return operandValue.toLowerCase();
    }
}
