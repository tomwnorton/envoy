package org.envoy.collection.evaluators;

public class CountReducer implements Reducer {
    @Override
    public Object reduce(final Object previousResult, final Object entity, final Evaluator evaluator) {
        return (Integer) previousResult + 1;
    }


    @Override
    public Object initialValue() {
        return 0;
    }
}
