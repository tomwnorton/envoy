/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.envoy.collection.evaluators;

import java.math.*;

/**
 * Makes performing operations on <code>Comparable</code>s possible.
 * <p/>
 * If the operands are not Numbers then this just returns the operands unchanged.
 * If the operands are Numbers, however, then things get complicated.  In Java, different
 * Number types can't be compared against each other (<code>new Integer(5).equals(new Long(5))</code>
 * returns <code>false</code>).  So we have to manually code Numeric promotion ourselves:
 * <ul>
 * <li>If one or both operands are BigDecimals, then both operands are returned as BigDecimals.</li>
 * <li>If one or both operands are BigIntegers, then both operands are returned as BigIntegers,
 * except when one of the operands is a Float or a Double, at which point both are returned
 * as BigDecimals, so that the precision is preserved.</li>
 * <li>If one or both operands are Doubles, then both operands are returned as Doubles.</li>
 * <li>If one or both operands are Floats, then both operands are returned as Floats.</li>
 * <li>If one or both operands are Longs, then both operands are returned as Longs.</li>
 * <li>If one or both operands are Integers, then both operands are returned as Integers.</li>
 * <li>If one or both operands are Shorts, then both operands are returned as Shorts.</li>
 * <li>If both operands are Bytes, then both operands are returned as Bytes.</li>
 * </ul>
 *
 * @author thomas
 */
class ValueCastorImpl implements ValueCastor {
    private Object lhs;
    private Object rhs;


    public ValueCastorImpl(final Object lhs, final Object rhs) {
        if (lhs != null && rhs != null) {
            if (lhs instanceof BigDecimal
                || rhs instanceof BigDecimal
                || lhs instanceof BigInteger && rhs instanceof Double
                || lhs instanceof BigInteger && rhs instanceof Float
                || lhs instanceof Double && rhs instanceof BigInteger
                || lhs instanceof Float && rhs instanceof BigInteger) {
                this.lhs = toBigDecimal((Number) lhs);
                this.rhs = toBigDecimal((Number) rhs);
            } else if (lhs instanceof BigInteger || rhs instanceof BigInteger) {
                this.lhs = new BigInteger(String.valueOf(lhs));
                this.rhs = new BigInteger(String.valueOf(rhs));
            } else if (lhs instanceof Double || rhs instanceof Double) {
                this.lhs = new Double(((Number) lhs).doubleValue());
                this.rhs = new Double(((Number) rhs).doubleValue());
            } else if (lhs instanceof Float || rhs instanceof Float) {
                this.lhs = new Float(((Number) lhs).floatValue());
                this.rhs = new Float(((Number) rhs).floatValue());
            } else if (lhs instanceof Long || rhs instanceof Long) {
                this.lhs = new Long(((Number) lhs).longValue());
                this.rhs = new Long(((Number) rhs).longValue());
            } else if (lhs instanceof Integer || rhs instanceof Integer) {
                this.lhs = new Integer(((Number) lhs).intValue());
                this.rhs = new Integer(((Number) rhs).intValue());
            } else if (lhs instanceof Short || rhs instanceof Short) {
                this.lhs = new Short(((Number) lhs).shortValue());
                this.rhs = new Short(((Number) rhs).shortValue());
            } else {
                this.lhs = lhs;
                this.rhs = rhs;
            }
        } else {
            this.lhs = lhs;
            this.rhs = rhs;
        }
    }


    @Override
    public Object getLhs() {
        return lhs;
    }


    @Override
    public Object getRhs() {
        return rhs;
    }


    private BigDecimal toBigDecimal(final Number number) {
        if (number instanceof BigDecimal) {
            return (BigDecimal) number;
        } else if (number instanceof BigInteger) {
            return new BigDecimal((BigInteger) number);
        } else {
            return new BigDecimal(number.doubleValue());
        }
    }
}
