package org.envoy.collection.evaluators;

public interface Reducer {
    public Object reduce(Object previousResult, Object entity, Evaluator evaluator);


    public Object initialValue();
}
