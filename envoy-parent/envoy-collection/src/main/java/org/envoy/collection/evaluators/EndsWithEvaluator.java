package org.envoy.collection.evaluators;

public class EndsWithEvaluator extends AbstractBinaryEvaluator<String> {
    public EndsWithEvaluator(final Evaluator lhs, final Evaluator rhs, final EvaluatorFactory evaluatorFactory) {
        super(lhs, rhs, false, evaluatorFactory);
    }


    @Override
    protected Object onEvaluate(final String lhs, final String rhs) {
        return lhs.endsWith(rhs);
    }
}
