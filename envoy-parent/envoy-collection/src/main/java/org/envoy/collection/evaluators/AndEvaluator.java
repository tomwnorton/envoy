package org.envoy.collection.evaluators;

public class AndEvaluator implements Evaluator {
    private final Evaluator lhs;
    private final Evaluator rhs;


    public AndEvaluator(final Evaluator lhs, final Evaluator rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }


    @Override
    public Object evaluate(final Object entity) {
        return ((Boolean) lhs.evaluate(entity)) && ((Boolean) rhs.evaluate(entity));
    }
}
