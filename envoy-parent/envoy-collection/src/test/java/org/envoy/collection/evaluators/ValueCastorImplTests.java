/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.envoy.collection.evaluators;

import org.junit.*;

import java.math.*;

/**
 * @author thomas
 */
public class ValueCastorImplTests {
    @Test
    public void test_that_both_operand_properties_are_Longs_when_the_entered_lhs_is_a_Long_3_and_the_entered_rhs_is_an_Integer_5() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(new Long(3L), new Integer(5));

        //--Assert
        Assert.assertEquals(new Long(3L), underTest.getLhs());
        Assert.assertEquals(new Long(5L), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_Longs_when_the_entered_lhs_is_a_Long_91_and_the_entered_rhs_is_an_Integer_22() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(new Long(91L), new Integer(22));

        //--Assert
        Assert.assertEquals(new Long(91L), underTest.getLhs());
        Assert.assertEquals(new Long(22L), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_Longs_when_the_entered_lhs_is_an_Integer_3_and_the_entered_rhs_is_an_Long_5() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(new Integer(3), new Long(5L));

        //--Assert
        Assert.assertEquals(new Long(3L), underTest.getLhs());
        Assert.assertEquals(new Long(5L), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_Integers_when_the_entered_lhs_is_an_Integer_3_and_the_entered_rhs_is_a_Short_5() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(new Integer(3), new Short((short) 5));

        //--Assert
        Assert.assertEquals(new Integer(3), underTest.getLhs());
        Assert.assertEquals(new Integer(5), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_Integers_when_the_entered_lhs_is_an_Integer_22_and_the_entered_rhs_is_a_Short_91() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(new Integer(22), new Short((short) 91));

        //--Assert
        Assert.assertEquals(new Integer(22), underTest.getLhs());
        Assert.assertEquals(new Integer(91), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_Integers_when_the_entered_lhs_is_a_Short_3_and_the_entered_rhs_is_an_Integer_5() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(new Short((short) 3), new Integer(5));

        //--Assert
        Assert.assertEquals(new Integer(3), underTest.getLhs());
        Assert.assertEquals(new Integer(5), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_Longs_when_the_entered_lhs_is_a_Short_3_and_the_entered_rhs_is_an_Long_5() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(new Short((short) 3), new Long(5L));

        //--Assert
        Assert.assertEquals(new Long(3L), underTest.getLhs());
        Assert.assertEquals(new Long(5L), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_Shorts_when_the_entered_lhs_is_a_Short_3_and_the_entered_rhs_is_a_Byte_5() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(new Short((short) 3), new Byte((byte) 5));

        //--Assert
        Assert.assertEquals(new Short((short) 3), underTest.getLhs());
        Assert.assertEquals(new Short((short) 5), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_Shorts_when_the_entered_lhs_is_a_Short_22_and_the_entered_rhs_is_a_Byte_91() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(new Short((short) 22), new Byte((byte) 91));

        //--Assert
        Assert.assertEquals(new Short((short) 22), underTest.getLhs());
        Assert.assertEquals(new Short((short) 91), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_Shorts_when_the_entered_lhs_is_a_Byte_3_and_the_entered_rhs_is_a_Short_5() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(new Byte((byte) 3), new Short((short) 5));

        //--Assert
        Assert.assertEquals(new Short((short) 3), underTest.getLhs());
        Assert.assertEquals(new Short((short) 5), underTest.getRhs());
    }


    @Test
    public void test_that_both_operands_are_the_same_as_the_entered_parameters_when_both_parameters_are_Bytes() {
        //--Arrange
        final Byte lhs = new Byte((byte) 3);
        final Byte rhs = new Byte((byte) 5);
        final ValueCastorImpl underTest = new ValueCastorImpl(lhs, rhs);

        //--Assert
        Assert.assertSame(lhs, underTest.getLhs());
        Assert.assertSame(rhs, underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_Floats_when_the_entered_lhs_is_a_Float_3_and_the_entered_rhs_is_a_Long_5() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(new Float(3), new Long(5));

        //--Assert
        Assert.assertEquals(new Float(3), underTest.getLhs());
        Assert.assertEquals(new Float(5), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_Floats_when_the_entered_lhs_is_a_Float_22_and_the_entered_rhs_is_a_Long_91() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(new Float(22), new Long(91));

        //--Assert
        Assert.assertEquals(new Float(22), underTest.getLhs());
        Assert.assertEquals(new Float(91), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_Floats_when_the_entered_lhs_is_a_Long_3_and_the_entered_rhs_is_a_Float_5() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(new Long(3), new Float(5));

        //--Assert
        Assert.assertEquals(new Float(3), underTest.getLhs());
        Assert.assertEquals(new Float(5), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_Doubles_when_the_entered_lhs_is_a_Double_3_and_the_entered_rhs_is_a_Float_5() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(new Double(3), new Float(5));

        //--Assert
        Assert.assertEquals(new Double(3), underTest.getLhs());
        Assert.assertEquals(new Double(5), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_Doubles_when_the_entered_lhs_is_a_Double_22_and_the_entered_rhs_is_a_Float_91() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(new Double(22), new Float(91));

        //--Assert
        Assert.assertEquals(new Double(22), underTest.getLhs());
        Assert.assertEquals(new Double(91), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_Doubles_when_the_entered_lhs_is_a_Float_3_and_the_entered_rhs_is_a_Double_5() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(new Float(3), new Double(5));

        //--Assert
        Assert.assertEquals(new Double(3), underTest.getLhs());
        Assert.assertEquals(new Double(5), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_BigDecimals_when_the_entered_lhs_is_a_BigDecimal_3_and_the_entered_rhs_is_a_Double_5() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(new BigDecimal(3), new Double(5));

        //--Assert
        Assert.assertEquals(new BigDecimal(3), underTest.getLhs());
        Assert.assertEquals(new BigDecimal(5), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_BigDecimals_when_the_entered_lhs_is_a_BigDecimal_22_and_the_entered_rhs_is_a_Double_91() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(new BigDecimal(22), new Double(91));

        //--Assert
        Assert.assertEquals(new BigDecimal(22), underTest.getLhs());
        Assert.assertEquals(new BigDecimal(91), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_BigDecimals_when_the_entered_lhs_is_a_BigDecimal_3_and_the_entered_rhs_is_a_Float_5() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(new BigDecimal(3), new Float(5));

        //--Assert
        Assert.assertEquals(new BigDecimal(3), underTest.getLhs());
        Assert.assertEquals(new BigDecimal(5), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_BigDecimals_when_the_entered_lhs_is_a_BigDecimal_3_and_the_entered_rhs_is_a_BigInteger() {
        //--Arrange
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 200; ++i) {
            sb.append("Z");
        }
        final BigInteger rhs = new BigInteger(sb.toString(), 36);
        final ValueCastorImpl underTest = new ValueCastorImpl(new BigDecimal(3), rhs);

        //--Assert
        Assert.assertEquals(new BigDecimal(3), underTest.getLhs());
        Assert.assertEquals(new BigDecimal(rhs), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_BigDecimals_when_the_entered_lhs_is_a_BigDecimal_3_and_the_entered_rhs_is_a_BigDecimal() {
        //--Arrange
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 400; ++i) {
            sb.append("Z");
        }
        final BigInteger unscaledValue = new BigInteger(sb.toString(), 36);
        final BigDecimal rhs = new BigDecimal(unscaledValue, -20);
        final ValueCastorImpl underTest = new ValueCastorImpl(new BigDecimal(3), rhs);

        //--Assert
        Assert.assertEquals(new BigDecimal(3), underTest.getLhs());
        Assert.assertEquals(rhs, underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_BigDecimals_when_the_entered_lhs_is_a_Double_3_and_the_entered_rhs_is_a_BigDecimal_5() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(new Double(3), new BigDecimal(5));

        //--Assert
        Assert.assertEquals(new BigDecimal(3), underTest.getLhs());
        Assert.assertEquals(new BigDecimal(5), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_BigDecimals_when_the_entered_lhs_is_a_BigInteger_3_and_the_entered_rhs_is_a_BigDecimal_5() {
        //--Arrange
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 200; ++i) {
            sb.append("Z");
        }
        final BigInteger lhs = new BigInteger(sb.toString(), 36);
        final ValueCastorImpl underTest = new ValueCastorImpl(lhs, new BigDecimal(5));

        //--Assert
        Assert.assertEquals(new BigDecimal(lhs), underTest.getLhs());
        Assert.assertEquals(new BigDecimal(5), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_BigDecimals_when_the_entered_lhs_is_a_huge_BigDecimal_and_the_entered_rhs_is_a_BigDecimal_5() {
        //--Arrange
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 400; ++i) {
            sb.append("Z");
        }
        final BigInteger unscaledValue = new BigInteger(sb.toString(), 36);
        final BigDecimal lhs = new BigDecimal(unscaledValue, -20);
        final ValueCastorImpl underTest = new ValueCastorImpl(lhs, new BigDecimal(5));

        //--Assert
        Assert.assertEquals(lhs, underTest.getLhs());
        Assert.assertEquals(new BigDecimal(5), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_BigIntegers_when_the_entered_lhs_is_a_huge_BigInteger_and_the_entered_rhs_is_a_Long_5() {
        //--Arrange
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 200; ++i) {
            sb.append("Z");
        }
        final BigInteger lhs = new BigInteger(sb.toString(), 36);
        final ValueCastorImpl underTest = new ValueCastorImpl(lhs, new Long(5));

        //--Assert
        Assert.assertEquals(lhs, underTest.getLhs());
        Assert.assertEquals(new BigInteger("5"), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_BigIntegers_when_the_entered_lhs_is_a_Long_and_the_entered_rhs_is_a_huge_BigInteger() {
        //--Arrange
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 200; ++i) {
            sb.append("Z");
        }
        final BigInteger rhs = new BigInteger(sb.toString(), 36);
        final ValueCastorImpl underTest = new ValueCastorImpl(new Long(5), rhs);

        //--Assert
        Assert.assertEquals(new BigInteger("5"), underTest.getLhs());
        Assert.assertEquals(rhs, underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_BigDecimals_when_the_entered_lhs_is_a_BigInteger_and_the_entered_rhs_is_a_Double() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(new BigInteger("3"), new Double(5));

        //--Assert
        Assert.assertEquals(new BigDecimal("3"), underTest.getLhs());
        Assert.assertEquals(new BigDecimal("5"), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_BigDecimals_when_the_entered_lhs_is_a_BigInteger_and_the_entered_rhs_is_a_Float() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(new BigInteger("3"), new Float(5));

        //--Assert
        Assert.assertEquals(new BigDecimal("3"), underTest.getLhs());
        Assert.assertEquals(new BigDecimal("5"), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_BigDecimals_when_the_entered_lhs_is_a_Double_and_the_entered_rhs_is_a_BigInteger() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(new Double(3), new BigInteger("5"));

        //--Assert
        Assert.assertEquals(new BigDecimal("3"), underTest.getLhs());
        Assert.assertEquals(new BigDecimal("5"), underTest.getRhs());
    }


    @Test
    public void test_that_both_operand_properties_are_BigDecimals_when_the_entered_lhs_is_a_Float_and_the_entered_rhs_is_a_BigInteger() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(new Float(3), new BigInteger("5"));

        //--Assert
        Assert.assertEquals(new BigDecimal("3"), underTest.getLhs());
        Assert.assertEquals(new BigDecimal("5"), underTest.getRhs());
    }


    @Test
    public void test_that_no_transformation_is_done_when_the_lhs_is_an_Integer_and_the_rhs_is_null() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(new Integer(4), null);

        //--Assert
        Assert.assertEquals(new Integer(4), underTest.getLhs());
        Assert.assertNull(underTest.getRhs());
    }


    @Test
    public void test_that_no_transformation_is_done_when_the_lhs_is_null_and_the_rhs_is_an_Integer() {
        //--Arrange
        final ValueCastorImpl underTest = new ValueCastorImpl(null, new Integer(4));

        //--Assert
        Assert.assertNull(underTest.getLhs());
        Assert.assertEquals(new Integer(4), underTest.getRhs());
    }
}
