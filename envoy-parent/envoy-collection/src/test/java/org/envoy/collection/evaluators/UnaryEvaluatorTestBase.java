package org.envoy.collection.evaluators;

import org.junit.*;
import org.mockito.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public abstract class UnaryEvaluatorTestBase {
    @Mock
    protected Evaluator operand;

    @Mock
    protected Object entity;

    protected Evaluator underTest;


    protected abstract Evaluator createTestEvaluator(Evaluator operand);


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        underTest = createTestEvaluator(operand);
    }


    @Test
    public void test_that_evaluate_returns_null_when_the_operand_evaluates_to_null() {
        //--Arrange
        when(operand.evaluate(same(entity))).thenReturn(null);

        //--Assert
        Assert.assertNull(underTest.evaluate(entity));
    }
}
