package org.envoy.collection.evaluators;

import org.junit.*;
import org.mockito.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public abstract class AbstractBinaryEvaluatorTestBase {
    @Mock
    private EvaluatorFactory evaluatorFactory;

    @Mock
    private Evaluator lhs;

    @Mock
    private Evaluator rhs;

    @Mock
    private Object lhsResult;

    @Mock
    private Object rhsResult;

    @Mock
    protected ValueCastor valueCastor;

    @Mock
    protected Object entity;

    protected Evaluator underTest;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        underTest = this.factory(this.lhs, this.rhs, this.evaluatorFactory);

        when(lhs.evaluate(same(entity))).thenReturn(lhsResult);
        when(rhs.evaluate(same(entity))).thenReturn(rhsResult);
        when(evaluatorFactory.createValueCastor(same(lhsResult), same(rhsResult))).thenReturn(valueCastor);
    }


    protected abstract Evaluator factory(Evaluator lhs, Evaluator rhs, EvaluatorFactory evaluatorFactory);
}
