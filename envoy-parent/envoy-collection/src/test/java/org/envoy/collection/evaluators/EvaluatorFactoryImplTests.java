package org.envoy.collection.evaluators;

import org.envoy.expression.*;
import org.junit.*;
import org.junit.runner.*;
import org.powermock.core.classloader.annotations.*;
import org.powermock.modules.junit4.*;

import java.util.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(EvaluatorFactoryImpl.class)
public class EvaluatorFactoryImplTests {
    private EvaluatorFactoryImpl underTest;


    @Before
    public void setUp() {
        underTest = new EvaluatorFactoryImpl();
    }


    @Test
    public void test_that_createValueEvaluator_returns_the_correct_evaluator_for_three() throws Exception {
        //--Arrange
        final ValueEvaluator expected = mock(ValueEvaluator.class);
        whenNew(ValueEvaluator.class).withArguments(3).thenReturn(expected);

        //--Act
        final ValueEvaluator actual = underTest.createValueEvaluator(3);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createNullEvaluator_returns_the_correct_evaluator() throws Exception {
        //--Arrange
        final NullEvaluator expected = mock(NullEvaluator.class);
        whenNew(NullEvaluator.class).withNoArguments().thenReturn(expected);

        //--Act
        final NullEvaluator actual = underTest.createNullEvaluator();

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createEqualsEvaluator_returns_the_correct_evaluator() throws Exception {
        //--Arrange
        final Evaluator lhs = mock(Evaluator.class);
        final Evaluator rhs = mock(Evaluator.class);
        final EqualsEvaluator expected = mock(EqualsEvaluator.class);
        whenNew(EqualsEvaluator.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Act
        final EqualsEvaluator actual = underTest.createEqualsEvaluator(lhs, rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createNotEqualsEvaluator_returns_the_correct_evaluator() throws Exception {
        //--Arrange
        final Evaluator lhs = mock(Evaluator.class);
        final Evaluator rhs = mock(Evaluator.class);
        final NotEqualsEvaluator expected = mock(NotEqualsEvaluator.class);
        whenNew(NotEqualsEvaluator.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Act
        final NotEqualsEvaluator actual = underTest.createNotEqualsEvaluator(lhs, rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createOrEvaluator_returns_the_correct_evaluator() throws Exception {
        //--Arrange
        final Evaluator lhs = mock(Evaluator.class);
        final Evaluator rhs = mock(Evaluator.class);
        final OrEvaluator expected = mock(OrEvaluator.class);
        whenNew(OrEvaluator.class).withArguments(same(lhs), same(rhs)).thenReturn(expected);

        //--Act
        final OrEvaluator actual = underTest.createOrEvaluator(lhs, rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createAndEvaluator_returns_the_correct_evaluator() throws Exception {
        //--Arrange
        final Evaluator lhs = mock(Evaluator.class);
        final Evaluator rhs = mock(Evaluator.class);
        final AndEvaluator expected = mock(AndEvaluator.class);
        whenNew(AndEvaluator.class).withArguments(same(lhs), same(rhs)).thenReturn(expected);

        //--Act
        final AndEvaluator actual = underTest.createAndEvaluator(lhs, rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_createFieldEvaluator_returns_the_correct_evaluator() throws Exception {
        //--Arrange
        final FieldExpression fieldExpr = mock(FieldExpression.class);
        final FieldEvaluator expected = mock(FieldEvaluator.class);
        whenNew(FieldEvaluator.class).withArguments(same(fieldExpr)).thenReturn(expected);

        //--Act
        final FieldEvaluator actual = underTest.createFieldEvaluator(fieldExpr);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createLessThanEvaluator_returns_the_correct_evaluator() throws Exception {
        //--Arrange
        final Evaluator lhs = mock(Evaluator.class);
        final Evaluator rhs = mock(Evaluator.class);
        final LessThanEvaluator expected = mock(LessThanEvaluator.class);
        whenNew(LessThanEvaluator.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Act
        final LessThanEvaluator actual = underTest.createLessThanEvaluator(lhs, rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_createValueCastor_returns_the_correct_ValueCastor() throws Exception {
        //--Arrange
        final Comparable<Object> lhs = mock(Comparable.class);
        final Comparable<Object> rhs = mock(Comparable.class);
        final ValueCastorImpl expected = mock(ValueCastorImpl.class);
        whenNew(ValueCastorImpl.class).withArguments(same(lhs), same(rhs)).thenReturn(expected);

        //--Act
        final ValueCastor actual = underTest.createValueCastor(lhs, rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createLessThanEqualEvaluator_returns_the_correct_evaluator() throws Exception {
        //--Arrange
        final Evaluator lhs = mock(Evaluator.class);
        final Evaluator rhs = mock(Evaluator.class);
        final LessThanEqualEvaluator expected = mock(LessThanEqualEvaluator.class);
        whenNew(LessThanEqualEvaluator.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Act
        final LessThanEqualEvaluator actual = underTest.createLessThanEqualEvaluator(lhs, rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createGreaterThanEvaluator_returns_the_correct_evaluator() throws Exception {
        //--Arrange
        final Evaluator lhs = mock(Evaluator.class);
        final Evaluator rhs = mock(Evaluator.class);
        final GreaterThanEvaluator expected = mock(GreaterThanEvaluator.class);
        whenNew(GreaterThanEvaluator.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Act
        final GreaterThanEvaluator actual = underTest.createGreaterThanEvaluator(lhs, rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createGreaterThanEqualEvaluator_returns_the_correct_evaluator() throws Exception {
        //--Arrange
        final Evaluator lhs = mock(Evaluator.class);
        final Evaluator rhs = mock(Evaluator.class);
        final GreaterThanEqualEvaluator expected = mock(GreaterThanEqualEvaluator.class);
        whenNew(GreaterThanEqualEvaluator.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Act
        final GreaterThanEqualEvaluator actual = underTest.createGreaterThanEqualEvaluator(lhs, rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createConvertToStringEvaluator_returns_the_correct_evaluator() throws Exception {
        //--Arrange
        final Evaluator lhs = mock(Evaluator.class);
        final ConvertToStringEvaluator expected = mock(ConvertToStringEvaluator.class);
        whenNew(ConvertToStringEvaluator.class).withArguments(same(lhs)).thenReturn(expected);

        //--Act
        final ConvertToStringEvaluator actual = underTest.createConvertToStringEvaluator(lhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createPlusEvaluator_returns_the_correct_evaluator() throws Exception {
        //--Arrange
        final Evaluator lhs = mock(Evaluator.class);
        final Evaluator rhs = mock(Evaluator.class);
        final PlusEvaluator expected = new PlusEvaluator(null, null, null);
        whenNew(PlusEvaluator.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Act
        final Evaluator actual = underTest.createPlusEvaluator(lhs, rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createMinusEvaluator_returns_the_correct_evaluator() throws Exception {
        //--Arrange
        final Evaluator lhs = mock(Evaluator.class);
        final Evaluator rhs = mock(Evaluator.class);
        final MinusEvaluator expected = new MinusEvaluator(null, null, null);
        whenNew(MinusEvaluator.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Act
        final Evaluator actual = underTest.createMinusEvaluator(lhs, rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createTimesEvaluator_returns_the_correct_evaluator() throws Exception {
        //--Arrange
        final Evaluator lhs = mock(Evaluator.class);
        final Evaluator rhs = mock(Evaluator.class);
        final TimesEvaluator expected = new TimesEvaluator(null, null, null);
        whenNew(TimesEvaluator.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Act
        final Evaluator actual = underTest.createTimesEvaluator(lhs, rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createDividedByEvaluator_returns_the_correct_evaluator() throws Exception {
        //--Arrange
        final Evaluator lhs = mock(Evaluator.class);
        final Evaluator rhs = mock(Evaluator.class);
        final DividedByEvaluator expected = new DividedByEvaluator(null, null, null);
        whenNew(DividedByEvaluator.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Act
        final Evaluator actual = underTest.createDividedByEvaluator(lhs, rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createModulusEvaluator_returns_the_correct_evaluator() throws Exception {
        //--Arrange
        final Evaluator lhs = mock(Evaluator.class);
        final Evaluator rhs = mock(Evaluator.class);
        final ModulusEvaluator expected = new ModulusEvaluator(null, null, null);
        whenNew(ModulusEvaluator.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Act
        final Evaluator actual = underTest.createModulusEvaluator(lhs, rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createCoalesceEvaluator_returns_the_correct_evaluator() throws Exception {
        //--Arrange
        final List<Evaluator> evals = new ArrayList<Evaluator>();
        final CoalesceEvaluator expected = new CoalesceEvaluator(null);
        whenNew(CoalesceEvaluator.class).withArguments(same(evals)).thenReturn(expected);

        //--Act
        final Evaluator actual = underTest.createCoalesceEvaluator(evals);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createNegativeEvaluator_returns_the_correct_evaluator() throws Exception {
        //--Arrange
        final Evaluator operand = mock(Evaluator.class);
        final NegativeEvaluator<Number> expected = new NegativeEvaluator<Number>(null);
        whenNew(NegativeEvaluator.class).withArguments(same(operand)).thenReturn(expected);

        //--Act
        final Evaluator actual = underTest.<Number>createNegativeEvaluator(operand);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createBetweenEvaluator_returns_an_AndEvaluator_built_with_the_correct_LessThanEqualEvaluator_and_GreaterThanEqualEvaluator()
            throws Exception {
        //--Arrange
        final Evaluator lhs = mock(Evaluator.class);
        final Evaluator lowerBound = mock(Evaluator.class);
        final Evaluator upperBound = mock(Evaluator.class);

        final GreaterThanEqualEvaluator greaterThanEqualEvaluator = new GreaterThanEqualEvaluator(null, null, null);
        final LessThanEqualEvaluator lessThanEqualEvaluator = new LessThanEqualEvaluator(null, null, null);
        final AndEvaluator andEvaluator = new AndEvaluator(null, null);

        whenNew(GreaterThanEqualEvaluator.class).withArguments(same(lhs), same(lowerBound), same(underTest)).thenReturn(
                greaterThanEqualEvaluator);
        whenNew(LessThanEqualEvaluator.class).withArguments(same(lhs), same(upperBound), same(underTest)).thenReturn(
                lessThanEqualEvaluator);
        whenNew(AndEvaluator.class).withArguments(same(greaterThanEqualEvaluator), same(lessThanEqualEvaluator)).thenReturn(
                andEvaluator);

        //--Act
        final Evaluator actual = underTest.createBetweenEvaluator(lhs, lowerBound, upperBound);

        //--Assert
        Assert.assertSame(andEvaluator, actual);
    }


    @Test
    public void test_that_createTrimEvaluator_returns_the_correct_TrimEvaluator() throws Exception {
        //--Arrange
        final Evaluator operand = mock(Evaluator.class);
        final TrimEvaluator expected = new TrimEvaluator(null);
        whenNew(TrimEvaluator.class).withArguments(same(operand)).thenReturn(expected);

        //--Act
        final TrimEvaluator actual = underTest.createTrimEvaluator(operand);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createLengthEvaluator_returns_the_correct_LengthEvaluator() throws Exception {
        //--Arrange
        final Evaluator operand = mock(Evaluator.class);
        final LengthEvaluator expected = new LengthEvaluator(null);
        whenNew(LengthEvaluator.class).withArguments(same(operand)).thenReturn(expected);

        //--Act
        final LengthEvaluator actual = underTest.createLengthEvaluator(operand);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createLowerCaseEvaluator_returns_the_correct_LowerCaseEvaluator() throws Exception {
        //--Arrange
        final Evaluator operand = mock(Evaluator.class);
        final LowerCaseEvaluator expected = new LowerCaseEvaluator(null);
        whenNew(LowerCaseEvaluator.class).withArguments(same(operand)).thenReturn(expected);

        //--Act
        final LowerCaseEvaluator actual = underTest.createLowerCaseEvaluator(operand);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createUpperCaseEvaluator_returns_the_correct_UpperCaseEvaluator() throws Exception {
        //--Arrange
        final Evaluator operand = mock(Evaluator.class);
        final UpperCaseEvaluator expected = new UpperCaseEvaluator(null);
        whenNew(UpperCaseEvaluator.class).withArguments(same(operand)).thenReturn(expected);

        //--Act
        final UpperCaseEvaluator actual = underTest.createUpperCaseEvaluator(operand);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createConvertStringToIntegerEvaluator_returns_the_correct_ConvertStringToIntegerEvaluator()
            throws Exception {
        //--Arrange
        final Evaluator operand = mock(Evaluator.class);
        final ConvertStringToIntegerEvaluator expected = new ConvertStringToIntegerEvaluator(null);
        whenNew(ConvertStringToIntegerEvaluator.class).withArguments(same(operand)).thenReturn(expected);

        //--Act
        final ConvertStringToIntegerEvaluator actual = underTest.createConvertStringToIntegerEvaluator(operand);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createConcatEvaluator_returns_the_correct_ConcatEvaluator() throws Exception {
        //--Arrange
        final Evaluator lhs = mock(Evaluator.class);
        final Evaluator rhs = mock(Evaluator.class);
        final ConcatEvaluator expected = new ConcatEvaluator(null, null, null);
        whenNew(ConcatEvaluator.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Act
        final ConcatEvaluator actual = underTest.createConcatEvaluator(lhs, rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createContainsEvaluator_returns_the_correct_ContainsEvaluator() throws Exception {
        //--Arrange
        final Evaluator lhs = mock(Evaluator.class);
        final Evaluator rhs = mock(Evaluator.class);
        final ContainsEvaluator expected = new ContainsEvaluator(null, null, null);
        whenNew(ContainsEvaluator.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Act
        final ContainsEvaluator actual = underTest.createContainsEvaluator(lhs, rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createStartsWithEvaluator_returns_the_correct_StartsWithEvaluator() throws Exception {
        //--Arrange
        final Evaluator lhs = mock(Evaluator.class);
        final Evaluator rhs = mock(Evaluator.class);
        final StartsWithEvaluator expected = new StartsWithEvaluator(null, null, null);
        whenNew(StartsWithEvaluator.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Act
        final StartsWithEvaluator actual = underTest.createStartsWithEvaluator(lhs, rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createEndsWithEvaluator_returns_the_correct_EndsWithEvaluator() throws Exception {
        //--Arrange
        final Evaluator lhs = mock(Evaluator.class);
        final Evaluator rhs = mock(Evaluator.class);
        final EndsWithEvaluator expected = new EndsWithEvaluator(null, null, null);
        whenNew(EndsWithEvaluator.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Act
        final EndsWithEvaluator actual = underTest.createEndsWithEvaluator(lhs, rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_createInEvaluator_returns_the_correct_InEvaluator() throws Exception {
        //--Arrange
        final Evaluator lhs = mock(Evaluator.class);
        final Collection<Object> values = new ArrayList<Object>();
        final InEvaluator expected = new InEvaluator(null, null);
        whenNew(InEvaluator.class).withArguments(same(lhs), same(values)).thenReturn(expected);

        //--Act
        final InEvaluator actual = underTest.createInEvaluator(lhs, values);

        //--Assert
        Assert.assertSame(expected, actual);
    }
}
