package org.envoy.collection.evaluators;

import org.junit.runner.*;
import org.junit.runners.*;
import org.junit.runners.Parameterized.*;

import java.util.*;

@RunWith(Parameterized.class)
public class PlusEvaluatorTests extends NumericBinaryEvaluatorTestBase {
    public PlusEvaluatorTests(final Number lhs, final Number rhs, final Number expected) {
        super(lhs, rhs, expected);
    }


    @Parameters
    public static Collection<Object[]> data() {
        final Object[][] data = new Object[][]{{2, 3, 5}, {7, 10, 17}};
        return Arrays.asList(data);
    }


    @Override
    protected Evaluator factory(final Evaluator lhs, final Evaluator rhs, final EvaluatorFactory evaluatorFactory) {
        return new PlusEvaluator(lhs, rhs, evaluatorFactory);
    }
}
