package org.envoy.collection.evaluators;

import org.junit.*;

import static org.mockito.Mockito.*;

public abstract class AbstractBinaryEvaluatorWithDefaultValueTestBase extends AbstractBinaryEvaluatorTestBase {
    private final Object defaultValue;


    public AbstractBinaryEvaluatorWithDefaultValueTestBase(final Object defaultValue) {
        this.defaultValue = defaultValue;
    }


    @Test
    public void test_that_the_default_value_is_returned_when_lhs_is_null() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn(null);

        //--Assert
        Assert.assertEquals(defaultValue, underTest.evaluate(entity));
    }


    @Test
    public void test_that_the_default_value_is_returned_when_rhs_is_null() {
        //--Arrange
        when(valueCastor.getRhs()).thenReturn(null);

        //--Assert
        Assert.assertEquals(defaultValue, underTest.evaluate(entity));
    }
}
