package org.envoy.collection.evaluators;

import org.junit.*;

import static org.mockito.Mockito.*;

public class GreaterThanEvaluatorTests extends AbstractBinaryEvaluatorTestBase {
    @Override
    protected GreaterThanEvaluator factory(final Evaluator lhs, final Evaluator rhs, final EvaluatorFactory evaluatorFactory) {
        return new GreaterThanEvaluator(lhs, rhs, evaluatorFactory);
    }


    @Test
    public void test_that_evaluate_returns_true_when_lhs_is_5_and_rhs_is_3() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn(5);
        when(valueCastor.getRhs()).thenReturn(3);

        //--Assert
        Assert.assertEquals(true, underTest.evaluate(entity));
    }


    @Test
    public void test_that_evaluate_returns_false_when_lhs_is_3_and_rhs_is_5() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn(3);
        when(valueCastor.getRhs()).thenReturn(5);

        //--Assert
        Assert.assertEquals(false, underTest.evaluate(entity));
    }


    @Test
    public void test_that_evaluate_returns_false_when_lhs_is_null() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn(null);
        when(valueCastor.getRhs()).thenReturn(3);

        //--Assert
        Assert.assertEquals(false, underTest.evaluate(entity));
    }


    @Test
    public void test_that_evaluate_returns_false_when_rhs_is_null() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn(5);
        when(valueCastor.getRhs()).thenReturn(null);

        //--Assert
        Assert.assertEquals(false, underTest.evaluate(entity));
    }
}
