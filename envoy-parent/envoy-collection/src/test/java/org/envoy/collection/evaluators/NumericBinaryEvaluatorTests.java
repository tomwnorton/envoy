package org.envoy.collection.evaluators;

import org.junit.*;

import java.math.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class NumericBinaryEvaluatorTests extends AbstractBinaryEvaluatorTestBase {
    private static Byte BYTE_WAS_RETURNED = 1;
    private static Short SHORT_WAS_RETURNED = 2;
    private static Integer INT_WAS_RETURNED = 3;
    private static Long LONG_WAS_RETURNED = 4L;
    private static Float FLOAT_WAS_RETURNED = 5.0F;
    private static Double DOUBLE_WAS_RETURNED = 6.0D;
    private static BigInteger BIG_INTEGER_WAS_RETURNED = new BigInteger("7");
    private static BigDecimal BIG_DECIMAL_WAS_RETURNED = new BigDecimal("8.0");

    private class UnderTest extends NumericBinaryEvaluator {
        public UnderTest(final Evaluator lhs, final Evaluator rhs, final EvaluatorFactory evaluatorFactory) {
            super(lhs, rhs, evaluatorFactory);
        }


        @Override
        protected Byte calculate(final Byte lhs, final Byte rhs) {
            return BYTE_WAS_RETURNED;
        }


        @Override
        protected Short calculate(final Short lhs, final Short rhs) {
            return SHORT_WAS_RETURNED;
        }


        @Override
        protected Integer calculate(final Integer lhs, final Integer rhs) {
            return INT_WAS_RETURNED;
        }


        @Override
        protected Long calculate(final Long lhs, final Long rhs) {
            return LONG_WAS_RETURNED;
        }


        @Override
        protected Float calculate(final Float lhs, final Float rhs) {
            return FLOAT_WAS_RETURNED;
        }


        @Override
        protected Double calculate(final Double lhs, final Double rhs) {
            return DOUBLE_WAS_RETURNED;
        }


        @Override
        protected BigInteger calculate(final BigInteger lhs, final BigInteger rhs) {
            return BIG_INTEGER_WAS_RETURNED;
        }


        @Override
        protected BigDecimal calculate(final BigDecimal lhs, final BigDecimal rhs) {
            return BIG_DECIMAL_WAS_RETURNED;
        }
    }


    @Override
    protected Evaluator factory(final Evaluator lhs, final Evaluator rhs, final EvaluatorFactory evaluatorFactory) {
        return spy(new UnderTest(lhs, rhs, evaluatorFactory));
    }


    @Test
    public void test_that_the_calculate_for_Bytes_is_used_when_both_arguments_are_bytes() {
        //--Arrange
        final Byte lhs = new Byte((byte) 3);
        final Byte rhs = new Byte((byte) 19);

        when(valueCastor.getLhs()).thenReturn(lhs);
        when(valueCastor.getRhs()).thenReturn(rhs);

        //--Assert
        Assert.assertEquals(BYTE_WAS_RETURNED, underTest.evaluate(entity));
        verify((UnderTest) underTest).calculate(same(lhs), same(rhs));
    }


    @Test
    public void test_that_the_calculate_for_Shorts_is_used_when_both_arguments_are_shorts() {
        //--Arrange
        final Short lhs = new Short((short) 3);
        final Short rhs = new Short((short) 19);

        when(valueCastor.getLhs()).thenReturn(lhs);
        when(valueCastor.getRhs()).thenReturn(rhs);

        //--Assert
        Assert.assertEquals(SHORT_WAS_RETURNED, underTest.evaluate(entity));
        verify((UnderTest) underTest).calculate(same(lhs), same(rhs));
    }


    @Test
    public void test_that_the_calculate_for_Integers_is_used_when_both_arguments_are_ints() {
        //--Arrange
        final Integer lhs = new Integer(3);
        final Integer rhs = new Integer(19);

        when(valueCastor.getLhs()).thenReturn(lhs);
        when(valueCastor.getRhs()).thenReturn(rhs);

        //--Assert
        Assert.assertEquals(INT_WAS_RETURNED, underTest.evaluate(entity));
        verify((UnderTest) underTest).calculate(same(lhs), same(rhs));
    }


    @Test
    public void test_that_the_calculate_for_Longs_is_used_when_both_arguments_are_longs() {
        //--Arrange
        final Long lhs = new Long(3);
        final Long rhs = new Long(19);

        when(valueCastor.getLhs()).thenReturn(lhs);
        when(valueCastor.getRhs()).thenReturn(rhs);

        //--Assert
        Assert.assertEquals(LONG_WAS_RETURNED, underTest.evaluate(entity));
        verify((UnderTest) underTest).calculate(same(lhs), same(rhs));
    }


    @Test
    public void test_that_the_calculate_for_Floats_is_used_when_both_arguments_are_floats() {
        //--Arrange
        final Float lhs = new Float(3);
        final Float rhs = new Float(19);

        when(valueCastor.getLhs()).thenReturn(lhs);
        when(valueCastor.getRhs()).thenReturn(rhs);

        //--Assert
        Assert.assertEquals(FLOAT_WAS_RETURNED, underTest.evaluate(entity));
        verify((UnderTest) underTest).calculate(same(lhs), same(rhs));
    }


    @Test
    public void test_that_the_calculate_for_Doubles_is_used_when_both_arguments_are_doubles() {
        //--Arrange
        final Double lhs = new Double(3);
        final Double rhs = new Double(19);

        when(valueCastor.getLhs()).thenReturn(lhs);
        when(valueCastor.getRhs()).thenReturn(rhs);

        //--Assert
        Assert.assertEquals(DOUBLE_WAS_RETURNED, underTest.evaluate(entity));
        verify((UnderTest) underTest).calculate(same(lhs), same(rhs));
    }


    @Test
    public void test_that_the_calculate_for_BigIntegers_is_used_when_both_arguments_are_BigIntegers() {
        //--Arrange
        final BigInteger lhs = new BigInteger("3");
        final BigInteger rhs = new BigInteger("19");

        when(valueCastor.getLhs()).thenReturn(lhs);
        when(valueCastor.getRhs()).thenReturn(rhs);

        //--Assert
        Assert.assertEquals(BIG_INTEGER_WAS_RETURNED, underTest.evaluate(entity));
        verify((UnderTest) underTest).calculate(same(lhs), same(rhs));
    }


    @Test
    public void test_that_the_calculate_for_BigDecimals_is_used_when_both_arguments_are_BigDecimals() {
        //--Arrange
        final BigDecimal lhs = new BigDecimal("3");
        final BigDecimal rhs = new BigDecimal("19");

        when(valueCastor.getLhs()).thenReturn(lhs);
        when(valueCastor.getRhs()).thenReturn(rhs);

        //--Assert
        Assert.assertEquals(BIG_DECIMAL_WAS_RETURNED, underTest.evaluate(entity));
        verify((UnderTest) underTest).calculate(same(lhs), same(rhs));
    }


    @Test
    public void test_that_null_is_returned_when_lhs_is_null() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn(null);
        when(valueCastor.getRhs()).thenReturn(12);

        //--Assert
        Assert.assertNull(underTest.evaluate(entity));
    }


    @Test
    public void test_that_null_is_returned_when_rhs_is_null() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn(34);
        when(valueCastor.getRhs()).thenReturn(null);

        //--Assert
        Assert.assertNull(underTest.evaluate(entity));
    }
}
