package org.envoy.collection.evaluators;

import org.junit.*;

import static org.mockito.Mockito.*;

public class ConcatEvaluatorTests extends AbstractBinaryEvaluatorTestBase {
    @Override
    protected Evaluator factory(final Evaluator lhs, final Evaluator rhs, final EvaluatorFactory evaluatorFactory) {
        return new ConcatEvaluator(lhs, rhs, evaluatorFactory);
    }


    @Test
    public void test_that_evaluate_returns_helloworld_for_the_operands_hello_and_world() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn("hello");
        when(valueCastor.getRhs()).thenReturn("world");

        //--Assert
        Assert.assertEquals("helloworld", underTest.evaluate(entity));
    }


    @Test
    public void test_that_evaluate_returns_goodbad_for_the_operands_good_and_bad() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn("good");
        when(valueCastor.getRhs()).thenReturn("bad");

        //--Assert
        Assert.assertEquals("goodbad", underTest.evaluate(entity));
    }


    @Test
    public void test_that_evaluate_returns_null_for_the_lhs_is_null() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn(null);
        when(valueCastor.getRhs()).thenReturn("world");

        //--Assert
        Assert.assertNull(underTest.evaluate(entity));
    }


    @Test
    public void test_that_evaluate_returns_null_for_the_rhs_is_null() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn("hello");
        when(valueCastor.getRhs()).thenReturn(null);

        //--Assert
        Assert.assertNull(underTest.evaluate(entity));
    }
}
