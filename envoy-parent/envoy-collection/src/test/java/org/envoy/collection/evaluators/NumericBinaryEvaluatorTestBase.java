package org.envoy.collection.evaluators;

import org.junit.*;

import java.math.*;

import static org.mockito.Mockito.*;

public abstract class NumericBinaryEvaluatorTestBase extends AbstractBinaryEvaluatorTestBase {
    private final Number givenLhs;
    private final Number givenRhs;
    private final Number expected;


    public NumericBinaryEvaluatorTestBase(final Number lhs, final Number rhs, final Number expected) {
        givenLhs = lhs;
        givenRhs = rhs;
        this.expected = expected;
    }


    @Test
    public void test_that_calculate_for_bytes_works() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn(givenLhs.byteValue());
        when(valueCastor.getRhs()).thenReturn(givenRhs.byteValue());

        //--Assert
        Assert.assertEquals(expected.byteValue(), underTest.evaluate(entity));
    }


    @Test
    public void test_that_calculate_for_shorts_works() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn(givenLhs.shortValue());
        when(valueCastor.getRhs()).thenReturn(givenRhs.shortValue());

        //--Assert
        Assert.assertEquals(expected.shortValue(), underTest.evaluate(entity));
    }


    @Test
    public void test_that_calculate_for_ints_works() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn(givenLhs.intValue());
        when(valueCastor.getRhs()).thenReturn(givenRhs.intValue());

        //--Assert
        Assert.assertEquals(expected.intValue(), underTest.evaluate(entity));
    }


    @Test
    public void test_that_calculate_for_longs_works() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn(givenLhs.longValue());
        when(valueCastor.getRhs()).thenReturn(givenRhs.longValue());

        //--Assert
        Assert.assertEquals(expected.longValue(), underTest.evaluate(entity));
    }


    @Test
    public void test_that_calculate_for_floats_works() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn(givenLhs.floatValue());
        when(valueCastor.getRhs()).thenReturn(givenRhs.floatValue());

        //--Assert
        Assert.assertEquals(expected.floatValue(), underTest.evaluate(entity));
    }


    @Test
    public void test_that_calculate_for_doubles_works() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn(givenLhs.doubleValue());
        when(valueCastor.getRhs()).thenReturn(givenRhs.doubleValue());

        //--Assert
        Assert.assertEquals(expected.doubleValue(), underTest.evaluate(entity));
    }


    @Test
    public void test_that_calculate_for_BigIntegers_works() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn(new BigInteger(givenLhs.toString()));
        when(valueCastor.getRhs()).thenReturn(new BigInteger(givenRhs.toString()));

        //--Assert
        Assert.assertEquals(new BigInteger(expected.toString()), underTest.evaluate(entity));
    }


    @Test
    public void test_that_calculate_for_BigDecimals_works() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn(new BigDecimal(givenLhs.toString()));
        when(valueCastor.getRhs()).thenReturn(new BigDecimal(givenRhs.toString()));

        //--Assert
        Assert.assertEquals(new BigDecimal(expected.toString()), underTest.evaluate(entity));
    }
}
