package org.envoy.collection;

import org.envoy.collection.evaluators.*;
import org.envoy.expression.*;
import org.envoy.query.*;
import org.envoy.query.grouping.*;
import org.junit.*;
import org.mockito.invocation.*;
import org.mockito.stubbing.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class QueryResolverImplTests {
    private class TestEntity implements Serializable {
        private static final long serialVersionUID = -7245821079675513412L;
    }

    private class TestEntityExpression extends AbstractEntityExpression<TestEntity> {
        public TestEntityExpression() {
            super(TestEntity.class);
        }
    }


    @Test
    public void test_that_the_entire_array_is_returned_when_the_expression_entityClass_is_the_same_as_the_array_element_entity_class_and_there_are_no_other_conditions_for_the_query()
            throws SQLException {
        //--Arrange
        final Collection<TestEntity> collection = Arrays.asList(new TestEntity(), new TestEntity(), new TestEntity());
        final CollectionDatabaseMetaData<TestEntity> metaData = new CollectionDatabaseMetaData<TestEntity>(collection);

        final TestEntityExpression expression = new TestEntityExpression();
        final SelectQueryModel<TestEntity, TestEntity> sqlQuery = new SelectQueryModel<>();
        sqlQuery.setAggregatorWrapper(new AggregatorWrapperForEntity<TestEntity>(null, null, null));
        sqlQuery.setFrom(expression);

        final QueryResolverImpl<TestEntity> underTest = new QueryResolverImpl<QueryResolverImplTests.TestEntity>();

        //--Act
        final List<TestEntity> actual = underTest.resolve(metaData, sqlQuery, null);

        //--Assert
        Assert.assertEquals(collection, actual);
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_only_the_desired_elements_are_returned_when_there_is_a_where_condition() throws SQLException {
        //--Arrange
        final TestEntity firstEntity = mock(TestEntity.class);
        final TestEntity secondEntity = mock(TestEntity.class);
        final TestEntity thirdEntity = mock(TestEntity.class);
        final TestEntity fourthEntity = mock(TestEntity.class);
        final Collection<TestEntity> collection = Arrays.asList(firstEntity, secondEntity, thirdEntity, fourthEntity);
        final Collection<TestEntity> expectedCollection = Arrays.asList(firstEntity, thirdEntity, fourthEntity);

        final ExpressionVisitorImpl visitor = mock(ExpressionVisitorImpl.class);
        when(visitor.isMatch(same(firstEntity))).thenReturn(true);
        when(visitor.isMatch(same(secondEntity))).thenReturn(false);
        when(visitor.isMatch(same(thirdEntity))).thenReturn(true);
        when(visitor.isMatch(same(fourthEntity))).thenReturn(true);

        final CollectionDatabaseMetaData<TestEntity> metaData = mock(CollectionDatabaseMetaData.class);
        when(metaData.getCollection()).thenReturn(collection);
        when(metaData.createExpressionVisitor()).thenReturn(visitor);

        final TestEntityExpression expression = new TestEntityExpression();
        final SelectQueryModel<TestEntity, TestEntity> sqlQuery = new SelectQueryModel<>();
        sqlQuery.setAggregatorWrapper(new AggregatorWrapperForEntity<TestEntity>(null, null, null));
        sqlQuery.setFrom(expression);
        sqlQuery.setWhereFilter(mock(BooleanExpression.class));

        final QueryResolverImpl<TestEntity> underTest = new QueryResolverImpl<QueryResolverImplTests.TestEntity>();

        //--Act
        final List<TestEntity> actual = underTest.resolve(metaData, sqlQuery, null);

        //--Assert
        verify(sqlQuery.getWhereFilter()).accept(same(visitor));
        Assert.assertEquals(expectedCollection, actual);
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_aggregator_is_used_when_we_want_just_one_specific_string_column_returned() throws SQLException {
        //--Arrange
        final TestEntity firstEntity = new TestEntity();
        final TestEntity secondEntity = new TestEntity();

        final Evaluator evaluator = mock(Evaluator.class);
        when(evaluator.evaluate(same(firstEntity))).thenReturn("hello");
        when(evaluator.evaluate(same(secondEntity))).thenReturn("world");

        final CollectionDatabaseMetaData<TestEntity> metaData = mock(CollectionDatabaseMetaData.class);
        when(metaData.getCollection()).thenReturn(Arrays.asList(firstEntity, secondEntity));
        //--Need to return a new visitor each time because the visitor has state.
        //  This is the same behavior as the real object.
        doAnswer(
                new Answer<ExpressionVisitorImpl>() {
                    @Override
                    public ExpressionVisitorImpl answer(final InvocationOnMock invocation) throws Throwable {
                        return new ExpressionVisitorImpl(new EvaluatorFactoryImpl());
                    }
                }).when(metaData).createExpressionVisitor();

        final Expression wantedExpr = mock(Expression.class);
        doAnswer(
                new Answer<Void>() {
                    @Override
                    public Void answer(final InvocationOnMock invocation) throws Throwable {
                        final ExpressionVisitorImpl visitor = (ExpressionVisitorImpl) invocation.getArguments()[0];
                        visitor.push(evaluator);
                        return null;
                    }
                }).when(wantedExpr).accept(any(ExpressionVisitor.class));

        final AggregatorWrapper<String> aggregatorWrapper = mock(AggregatorWrapper.class);
        when(aggregatorWrapper.aggregate(Arrays.<Object>asList("hello"))).thenReturn("hello");
        when(aggregatorWrapper.aggregate(Arrays.<Object>asList("world"))).thenReturn("world");

        final SelectQueryModel<TestEntity, String> sqlQuery = new SelectQueryModel<>();
        sqlQuery.setFrom(new TestEntityExpression());
        sqlQuery.setSelectList(Arrays.asList(wantedExpr));
        sqlQuery.setAggregatorWrapper(aggregatorWrapper);

        final QueryResolverImpl<String> underTest = new QueryResolverImpl<>();

        //--Act
        final List<String> actual = underTest.resolve(metaData, sqlQuery, null);

        //--Assert
        Assert.assertEquals(Arrays.asList("hello", "world"), actual);
    }
}
