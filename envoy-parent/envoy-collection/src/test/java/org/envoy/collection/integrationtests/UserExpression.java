package org.envoy.collection.integrationtests;

import org.envoy.expression.*;

public class UserExpression extends AbstractEntityExpression<User> {
    private final IntegerFieldExpression id = new IntegerFieldExpression(this);
    private final BooleanFieldExpression active = new BooleanFieldExpression(this);


    public UserExpression() {
        super(User.class);
    }


    public IntegerFieldExpression getId() {
        return id;
    }


    public BooleanFieldExpression isActive() {
        return active;
    }
}
