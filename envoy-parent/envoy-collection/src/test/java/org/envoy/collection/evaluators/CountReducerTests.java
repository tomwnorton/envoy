package org.envoy.collection.evaluators;

import org.junit.*;
import org.mockito.*;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class CountReducerTests {
    private Object entity;
    private CountReducer underTest;

    @Mock
    private Evaluator evaluator;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        underTest = new CountReducer();
        entity = new Object();
    }


    @Test
    public void test_that_reduce_returns_1_when_previousResult_is_0() {
        //--Act
        final int actual = (Integer) underTest.reduce(0, entity, evaluator);

        //--Assert
        assertThat(actual, is(1));
    }


    @Test
    public void test_that_reduce_returns_5_when_previousResult_is_4() {
        //--Act
        final int actual = (Integer) underTest.reduce(4, entity, evaluator);

        //--Assert
        assertThat(actual, is(5));
    }


    @Test
    public void test_that_initialValue_returns_0() {
        assertThat((Integer) underTest.initialValue(), is(0));
    }
}
