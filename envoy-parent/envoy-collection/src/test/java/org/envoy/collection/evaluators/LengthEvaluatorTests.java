package org.envoy.collection.evaluators;

import org.junit.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class LengthEvaluatorTests extends UnaryEvaluatorTestBase {
    @Override
    protected Evaluator createTestEvaluator(final Evaluator operand) {
        return new LengthEvaluator(operand);
    }


    @Test
    public void test_that_evaluate_returns_5_for_hello() {
        //--Arrange
        when(operand.evaluate(same(entity))).thenReturn("hello");

        //--Assert
        Assert.assertEquals(new Integer(5), underTest.evaluate(entity));
    }


    @Test
    public void test_that_evaluate_returns_7_for_goodbye() {
        //--Arrange
        when(operand.evaluate(same(entity))).thenReturn("goodbye");

        //--Assert
        Assert.assertEquals(new Integer(7), underTest.evaluate(entity));
    }
}
