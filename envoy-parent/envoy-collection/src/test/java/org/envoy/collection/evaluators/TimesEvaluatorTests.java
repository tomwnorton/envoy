package org.envoy.collection.evaluators;

import org.junit.runner.*;
import org.junit.runners.*;
import org.junit.runners.Parameterized.*;

import java.util.*;

@RunWith(Parameterized.class)
public class TimesEvaluatorTests extends NumericBinaryEvaluatorTestBase {
    public TimesEvaluatorTests(final Number lhs, final Number rhs, final Number expected) {
        super(lhs, rhs, expected);
    }


    @Parameters
    public static Collection<Object[]> getData() {
        final Object[][] data = new Object[][]{{2, 2, 4}, {3, 9, 27}};
        return Arrays.asList(data);
    }


    @Override
    protected Evaluator factory(final Evaluator lhs, final Evaluator rhs, final EvaluatorFactory evaluatorFactory) {
        return new TimesEvaluator(lhs, rhs, evaluatorFactory);
    }
}
