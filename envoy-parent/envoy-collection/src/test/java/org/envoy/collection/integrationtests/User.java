package org.envoy.collection.integrationtests;

import org.envoy.*;

import java.io.*;


public class User implements Serializable {
    private static final long serialVersionUID = -5908267686253716314L;

    private Property<Integer> id = new Property<>();
    private Property<Boolean> active = new Property<>();


    public Boolean isActive() {
        return active.getValue();
    }


    public void setActive(final Boolean active) {
        this.active.setValue(active);
    }


    public Integer getId() {
        return id.getValue();
    }


    public void setId(final Integer id) {
        this.id.setValue(id);
    }
}
