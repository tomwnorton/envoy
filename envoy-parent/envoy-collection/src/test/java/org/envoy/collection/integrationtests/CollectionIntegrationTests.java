package org.envoy.collection.integrationtests;

import org.envoy.collection.*;
import org.envoy.query.grouping.*;
import org.junit.*;

import java.util.*;

public class CollectionIntegrationTests {
    private UserBuilder userBuilder;


    @Before
    public void setUp() {
        this.userBuilder = new UserBuilder();
    }


    @Test
    public void filter_users_by_active_flag() {
        //--Arrange
        final List<User> users = new ArrayList<User>();

        users.add(new User());
        users.get(0).setActive(true);

        users.add(new User());
        users.add(new User());
        users.add(new User());

        users.add(new User());
        users.get(4).setActive(true);

        users.add(new User());
        users.get(5).setActive(true);

        users.add(new User());

        users.add(new User());
        users.get(7).setActive(true);

        final List<User> expected = new ArrayList<User>();
        expected.add(users.get(0));
        expected.add(users.get(4));
        expected.add(users.get(5));
        expected.add(users.get(7));

        //--Act
        final CollectionDatabaseMetaData<User> metaData = new CollectionDatabaseMetaData<User>(users);
        final UserExpression user = new UserExpression();
        // @formatter:off
		final List<User> actual = metaData.getQueryBuilder()
				.from(user)
				.where(user.isActive())
				.list();
		// @formatter:on

        //--Assert
        Assert.assertEquals(expected, actual);
    }


    @Test
    public void test_that_user_ids_can_be_returned() {
        //--Arrange
        final List<User> users = new ArrayList<User>();

        users.add(userBuilder.setId(5).get());
        users.add(userBuilder.setId(12).get());
        users.add(userBuilder.setId(17).get());
        users.add(userBuilder.setId(3).get());
        users.add(userBuilder.setId(11).get());

        final List<Integer> expected = new ArrayList<Integer>();
        for (final User user : users) {
            expected.add(user.getId());
        }

        //--Act
        final CollectionDatabaseMetaData<User> metaData = new CollectionDatabaseMetaData<User>(users);
        final UserExpression user = new UserExpression();
        // @formatter:off
        final List<Integer> actual = metaData.getQueryBuilder()
                .from(user)
                .aggregate(user.getId())
                .as(new ColumnAggregator1<Integer, Integer>()
                {
                    @Override
                    public Integer aggregate(final Integer id)
                    {
                        return id;
                    }
                })
                .list();
        // @formatter:on

        //--Assert
        Assert.assertEquals(expected, actual);
    }
}
