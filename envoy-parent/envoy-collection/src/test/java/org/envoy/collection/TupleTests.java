package org.envoy.collection;

import org.junit.*;

import java.util.*;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class TupleTests {
    @Test
    public void test_that_compareTo_returns_negative_when_lhs_is_empty_and_rhs_contains_a_single_element() {
        //--Arrange
        final Tuple lhs = new Tuple(new ArrayList<Comparable<?>>());
        final Tuple rhs = new Tuple(Arrays.<Comparable<?>>asList(mock(Comparable.class)));

        //--Act
        final int actual = lhs.compareTo(rhs);

        //--Assert
        assertThat(actual, lessThan(0));
    }


    @Test
    public void test_that_compareTo_returns_zero_when_lhs_and_rhs_are_empty() {
        //--Arrange
        final Tuple lhs = new Tuple(new ArrayList<Comparable<?>>());
        final Tuple rhs = new Tuple(new ArrayList<Comparable<?>>());

        //--Act
        final int actual = lhs.compareTo(rhs);

        //--Assert
        assertThat(actual, is(0));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_compareTo_returns_positive_when_the_first_element_of_lhs_is_greater_than_the_first_element_of_rhs() {
        //--Arrange
        final Comparable<Comparable<?>> lhsFirst = mock(Comparable.class);
        final Comparable<Comparable<?>> rhsFirst = mock(Comparable.class);
        when(lhsFirst.compareTo(same(rhsFirst))).thenReturn(1);

        final Tuple lhs = new Tuple(Arrays.asList(lhsFirst));
        final Tuple rhs = new Tuple(Arrays.asList(rhsFirst));

        //--Act
        final int actual = lhs.compareTo(rhs);

        //--Assert
        assertThat(actual, greaterThan(0));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_compareTo_returns_positive_when_the_first_element_of_lhs_has_one_element_and_rhs_has_no_elements() {
        //--Arrange
        final Comparable<Comparable<?>> lhsFirst = mock(Comparable.class);
        final Comparable<Comparable<?>> rhsFirst = mock(Comparable.class);
        when(lhsFirst.compareTo(same(rhsFirst))).thenReturn(1);

        final Tuple lhs = new Tuple(Arrays.asList(lhsFirst));
        final Tuple rhs = new Tuple(new ArrayList<Comparable<?>>());

        //--Act
        final int actual = lhs.compareTo(rhs);

        //--Assert
        assertThat(actual, greaterThan(0));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_compareTo_returns_negative_when_the_first_element_of_lhs_is_less_than_the_first_element_of_rhs() {
        //--Arrange
        final Comparable<Comparable<?>> lhsFirst = mock(Comparable.class);
        final Comparable<Comparable<?>> rhsFirst = mock(Comparable.class);
        when(lhsFirst.compareTo(same(rhsFirst))).thenReturn(-1);

        final Tuple lhs = new Tuple(Arrays.asList(lhsFirst));
        final Tuple rhs = new Tuple(Arrays.asList(rhsFirst));

        //--Act
        final int actual = lhs.compareTo(rhs);

        //--Assert
        assertThat(actual, lessThan(0));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_compareTo_returns_negative_when_the_first_elements_of_lhs_and_rhs_are_equal_but_lhs_has_only_one_element_whereas_rhs_has_two() {
        //--Arrange
        final Comparable<Comparable<?>> lhsFirst = mock(Comparable.class);
        final Comparable<Comparable<?>> rhsFirst = mock(Comparable.class);
        final Comparable<Comparable<?>> rhsSecond = mock(Comparable.class);
        when(lhsFirst.compareTo(same(rhsFirst))).thenReturn(0);

        final Tuple lhs = new Tuple(Arrays.asList(lhsFirst));
        final Tuple rhs = new Tuple(Arrays.asList(rhsFirst, rhsSecond));

        //--Act
        final int actual = lhs.compareTo(rhs);

        //--Assert
        assertThat(actual, lessThan(0));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_compareTo_returns_positive_when_the_first_elements_of_lhs_and_rhs_are_equal_and_the_second_lhs_element_is_greter_than_the_second_rhs_element() {
        //--Arrange
        final Comparable<Comparable<?>> lhsFirst = mock(Comparable.class);
        final Comparable<Comparable<?>> lhsSecond = mock(Comparable.class);
        final Comparable<Comparable<?>> rhsFirst = mock(Comparable.class);
        final Comparable<Comparable<?>> rhsSecond = mock(Comparable.class);
        when(lhsFirst.compareTo(same(rhsFirst))).thenReturn(0);
        when(lhsSecond.compareTo(same(rhsSecond))).thenReturn(1);

        final Tuple lhs = new Tuple(Arrays.asList(lhsFirst, lhsSecond));
        final Tuple rhs = new Tuple(Arrays.asList(rhsFirst, rhsSecond));

        //--Act
        final int actual = lhs.compareTo(rhs);

        //--Assert
        assertThat(actual, greaterThan(0));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_compareTo_returns_positive_when_the_first_elements_of_lhs_and_rhs_are_equal_but_lhs_has_two_elements_whereas_rhs_has_only_one() {
        //--Arrange
        final Comparable<Comparable<?>> lhsFirst = mock(Comparable.class);
        final Comparable<Comparable<?>> lhsSecond = mock(Comparable.class);
        final Comparable<Comparable<?>> rhsFirst = mock(Comparable.class);
        when(lhsFirst.compareTo(same(rhsFirst))).thenReturn(0);

        final Tuple lhs = new Tuple(Arrays.asList(lhsFirst, lhsSecond));
        final Tuple rhs = new Tuple(Arrays.asList(rhsFirst));

        //--Act
        final int actual = lhs.compareTo(rhs);

        //--Assert
        assertThat(actual, greaterThan(0));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_compareTo_returns_negative_when_the_first_elements_of_lhs_and_rhs_are_equal_and_the_second_lhs_element_is_less_than_the_second_rhs_element() {
        //--Arrange
        final Comparable<Comparable<?>> lhsFirst = mock(Comparable.class);
        final Comparable<Comparable<?>> lhsSecond = mock(Comparable.class);
        final Comparable<Comparable<?>> rhsFirst = mock(Comparable.class);
        final Comparable<Comparable<?>> rhsSecond = mock(Comparable.class);
        when(lhsFirst.compareTo(same(rhsFirst))).thenReturn(0);
        when(lhsSecond.compareTo(same(rhsSecond))).thenReturn(-1);

        final Tuple lhs = new Tuple(Arrays.asList(lhsFirst, lhsSecond));
        final Tuple rhs = new Tuple(Arrays.asList(rhsFirst, rhsSecond));

        //--Act
        final int actual = lhs.compareTo(rhs);

        //--Assert
        assertThat(actual, lessThan(0));
    }
}
