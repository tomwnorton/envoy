package org.envoy.collection.evaluators;

import org.junit.*;

import static org.mockito.Mockito.*;

public class LessThanEvaluatorTests extends AbstractBinaryEvaluatorTestBase {
    @Override
    protected Evaluator factory(final Evaluator lhs, final Evaluator rhs, final EvaluatorFactory evaluatorFactory) {
        return new LessThanEvaluator(lhs, rhs, evaluatorFactory);
    }


    @Test
    public void test_that_evaluate_returns_true_when_lhs_is_2_and_rhs_is_9() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn((2));
        when(valueCastor.getRhs()).thenReturn((9));

        //--Assert
        Assert.assertEquals(true, underTest.evaluate(entity));
    }


    @Test
    public void test_that_evaluate_returns_false_when_lhs_is_16_and_rhs_is_11() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn((16));
        when(valueCastor.getRhs()).thenReturn((11));

        //--Assert
        Assert.assertEquals(false, underTest.evaluate(entity));
    }


    @Test
    public void test_that_evaluator_returns_false_when_lhs_is_null() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn((null));
        when(valueCastor.getRhs()).thenReturn((11));

        //--Assert
        Assert.assertEquals(false, underTest.evaluate(entity));
    }


    @Test
    public void test_that_evaluate_returns_false_when_rhs_is_null() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn((16));
        when(valueCastor.getRhs()).thenReturn((null));

        //--Assert
        Assert.assertEquals(false, underTest.evaluate(entity));
    }
}
