package org.envoy.collection.evaluators;

import org.junit.*;

import java.math.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class NegativeEvaluatorTests extends UnaryEvaluatorTestBase {
    @Override
    protected Evaluator createTestEvaluator(final Evaluator operand) {
        return new NegativeEvaluator<Number>(operand);
    }


    @Test
    public void test_that_negative_7_byte_is_evaluated_correctly() {
        //--Arrange
        when(operand.evaluate(same(entity))).thenReturn((byte) 7);

        //--Assert
        Assert.assertEquals((byte) (-7), underTest.evaluate(entity));
    }


    @Test
    public void test_that_negative_7_short_is_evaluated_correctly() {
        //--Arrange
        when(operand.evaluate(same(entity))).thenReturn((short) 7);

        //--Assert
        Assert.assertEquals((short) (-7), underTest.evaluate(entity));
    }


    @Test
    public void test_that_negative_7_is_evaluated_correctly() {
        //--Arrange
        when(operand.evaluate(same(entity))).thenReturn(7);

        //--Assert
        Assert.assertEquals((-7), underTest.evaluate(entity));
    }


    @Test
    public void test_that_negative_7_trillion_is_evaluated_correctly() {
        //--Arrange
        when(operand.evaluate(same(entity))).thenReturn(7000000000000L);

        //--Assert
        Assert.assertEquals((-7000000000000L), underTest.evaluate(entity));
    }


    @Test
    public void test_that_negative_7_float_is_evaluated_correctly() {
        //--Arrange
        when(operand.evaluate(same(entity))).thenReturn(7.0F);

        //--Assert
        Assert.assertEquals(-7.0F, underTest.evaluate(entity));
    }


    @Test
    public void test_that_negative_7_point_113_double_is_evaluated_correctly() {
        //--Arrange
        when(operand.evaluate(same(entity))).thenReturn(7.113D);

        //--Assert
        Assert.assertEquals(-7.113D, underTest.evaluate(entity));
    }


    @Test
    public void test_that_negative_7_BigInteger_is_evaluated_correctly() {
        //--Arrange
        when(operand.evaluate(same(entity))).thenReturn(new BigInteger("7"));

        //--Assert
        Assert.assertEquals(new BigInteger("-7"), underTest.evaluate(entity));
    }


    @Test
    public void test_that_negative_7_BigDecimal_is_evaluated_correctly() {
        //--Arrange
        when(operand.evaluate(same(entity))).thenReturn(new BigDecimal("7"));

        //--Assert
        Assert.assertEquals(new BigDecimal("-7"), underTest.evaluate(entity));
    }
}
