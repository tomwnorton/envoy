package org.envoy.collection.evaluators;

import org.junit.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class UpperCaseEvaluatorTests extends UnaryEvaluatorTestBase {
    @Override
    protected Evaluator createTestEvaluator(final Evaluator operand) {
        return new UpperCaseEvaluator(operand);
    }


    @Test
    public void test_that_evaluate_returns_HELLO_when_hello_is_given() {
        //--Arrange
        when(operand.evaluate(same(entity))).thenReturn("hello");

        //--Assert
        Assert.assertEquals("HELLO", underTest.evaluate(entity));
    }


    @Test
    public void test_that_evaluate_returns_WORLD_when_world_is_given() {
        //--Arrange
        when(operand.evaluate(same(entity))).thenReturn("world");

        //--Assert
        Assert.assertEquals("WORLD", underTest.evaluate(entity));
    }
}
