package org.envoy.collection.evaluators;

import org.junit.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class ConvertStringToIntegerEvaluatorTests extends UnaryEvaluatorTestBase {
    @Override
    protected Evaluator createTestEvaluator(final Evaluator operand) {
        return new ConvertStringToIntegerEvaluator(operand);
    }


    @Test
    public void test_that_evaluate_returns_the_long_12_when_the_string_12_is_given() {
        //--Arrange
        when(operand.evaluate(same(entity))).thenReturn("12");

        //--Assert
        Assert.assertEquals(new Long(12), underTest.evaluate(entity));
    }


    @Test
    public void test_that_evaluate_returns_the_long_435_when_the_string_435_is_given() {
        //--Arrange
        when(operand.evaluate(same(entity))).thenReturn("435");

        //--Assert
        Assert.assertEquals(new Long(435), underTest.evaluate(entity));
    }
}
