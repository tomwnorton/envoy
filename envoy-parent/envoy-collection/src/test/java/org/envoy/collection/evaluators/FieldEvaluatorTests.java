package org.envoy.collection.evaluators;

import org.envoy.expression.*;
import org.junit.*;

import java.io.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class FieldEvaluatorTests {
    @SuppressWarnings("unchecked")
    @Test
    public void test_that_evaluate_gets_the_field_value() {
        //--Arrange
        final Serializable entity = mock(Serializable.class);
        final Serializable field = mock(Serializable.class);
        final FieldExpression fieldExpr = mock(FieldExpression.class);
        when(fieldExpr.getFromRootEntity(same(entity))).thenReturn(field);

        final FieldEvaluator underTest = new FieldEvaluator(fieldExpr);

        //--Assert
        Assert.assertSame(field, underTest.evaluate(entity));
    }
}
