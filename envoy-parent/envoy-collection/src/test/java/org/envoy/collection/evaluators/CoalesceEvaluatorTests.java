package org.envoy.collection.evaluators;

import org.junit.*;

import java.io.*;
import java.util.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class CoalesceEvaluatorTests {
    @Test
    public void test_that_the_evaluation_of_the_first_element_is_returned_when_the_evaluation_is_not_null() {
        //--Arrange
        final Evaluator firstEval = mock(Evaluator.class);
        final Evaluator secondEval = mock(Evaluator.class);
        final Evaluator thirdEval = mock(Evaluator.class);

        final Serializable first = mock(Serializable.class);
        final Serializable second = mock(Serializable.class);
        final Serializable third = mock(Serializable.class);
        final Serializable entity = mock(Serializable.class);

        when(firstEval.evaluate(same(entity))).thenReturn(first);
        when(secondEval.evaluate(same(entity))).thenReturn(second);
        when(thirdEval.evaluate(same(entity))).thenReturn(third);

        final CoalesceEvaluator underTest =
                new CoalesceEvaluator(Arrays.asList(firstEval, secondEval, thirdEval));

        //--Assert
        Assert.assertSame(first, underTest.evaluate(entity));
    }


    @Test
    public void test_that_the_evaluation_of_the_second_element_is_returned_when_the_evaluation_is_null_but_the_second_evaluation_is_not_null() {
        //--Arrange
        final Evaluator firstEval = mock(Evaluator.class);
        final Evaluator secondEval = mock(Evaluator.class);
        final Evaluator thirdEval = mock(Evaluator.class);

        final Serializable second = mock(Serializable.class);
        final Serializable third = mock(Serializable.class);
        final Serializable entity = mock(Serializable.class);

        when(firstEval.evaluate(same(entity))).thenReturn(null);
        when(secondEval.evaluate(same(entity))).thenReturn(second);
        when(thirdEval.evaluate(same(entity))).thenReturn(third);

        final CoalesceEvaluator underTest =
                new CoalesceEvaluator(Arrays.asList(firstEval, secondEval, thirdEval));

        //--Assert
        Assert.assertSame(second, underTest.evaluate(entity));
    }


    @Test
    public void test_that_null_is_returned_when_there_are_no_evaluators() {
        //--Arrange
        final Serializable entity = mock(Serializable.class);

        final CoalesceEvaluator underTest = new CoalesceEvaluator(new ArrayList<Evaluator>());

        //--Assert
        Assert.assertNull(underTest.evaluate(entity));
    }
}
