package org.envoy.collection.evaluators;

import org.junit.*;

import static org.mockito.Mockito.*;

public class ContainsEvaluatorTests extends AbstractBinaryEvaluatorWithDefaultValueTestBase {
    public ContainsEvaluatorTests() {
        super(false);
    }


    @Override
    protected Evaluator factory(final Evaluator lhs, final Evaluator rhs, final EvaluatorFactory evaluatorFactory) {
        return new ContainsEvaluator(lhs, rhs, evaluatorFactory);
    }


    @Test
    public void test_that_evaluate_returns_true_when_hello_world_contains_llo() {
        //--Arrange
        when(this.valueCastor.getLhs()).thenReturn("hello world");
        when(this.valueCastor.getRhs()).thenReturn("llo");

        //--Assert
        Assert.assertEquals(Boolean.TRUE, this.underTest.evaluate(this.entity));
    }


    @Test
    public void test_that_evaluate_returns_false_when_germany_does_not_contains_berlin() {
        //--Arrange
        when(this.valueCastor.getLhs()).thenReturn("germany");
        when(this.valueCastor.getRhs()).thenReturn("berlin");

        //--Assert
        Assert.assertEquals(Boolean.FALSE, this.underTest.evaluate(this.entity));
    }
}
