package org.envoy.collection.evaluators;

import org.junit.*;

import java.io.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class AndEvaluatorTests {
    @Test
    public void test_that_true_and_true_evaluates_to_true() {
        //--Arrange
        final Serializable entity = mock(Serializable.class);

        final Evaluator lhs = mock(Evaluator.class);
        when(lhs.evaluate(same(entity))).thenReturn(true);

        final Evaluator rhs = mock(Evaluator.class);
        when(rhs.evaluate(same(entity))).thenReturn(true);

        final AndEvaluator underTest = new AndEvaluator(lhs, rhs);

        //--Assert
        Assert.assertEquals(true, underTest.evaluate(entity));
    }


    @Test
    public void test_that_false_and_false_evaluates_to_false() {
        //--Arrange
        final Serializable entity = mock(Serializable.class);

        final Evaluator lhs = mock(Evaluator.class);
        when(lhs.evaluate(same(entity))).thenReturn(false);

        final Evaluator rhs = mock(Evaluator.class);
        when(rhs.evaluate(same(entity))).thenReturn(false);

        final AndEvaluator underTest = new AndEvaluator(lhs, rhs);

        //--Assert
        Assert.assertEquals(false, underTest.evaluate(entity));
    }


    @Test
    public void test_that_true_and_false_evaluates_to_false() {
        //--Arrange
        final Serializable entity = mock(Serializable.class);

        final Evaluator lhs = mock(Evaluator.class);
        when(lhs.evaluate(same(entity))).thenReturn(true);

        final Evaluator rhs = mock(Evaluator.class);
        when(rhs.evaluate(same(entity))).thenReturn(false);

        final AndEvaluator underTest = new AndEvaluator(lhs, rhs);

        //--Assert
        Assert.assertEquals(false, underTest.evaluate(entity));
    }


    @Test
    public void test_that_false_and_true_evaluates_to_false() {
        //--Arrange
        final Serializable entity = mock(Serializable.class);

        final Evaluator lhs = mock(Evaluator.class);
        when(lhs.evaluate(same(entity))).thenReturn(false);

        final Evaluator rhs = mock(Evaluator.class);
        when(rhs.evaluate(same(entity))).thenReturn(true);

        final AndEvaluator underTest = new AndEvaluator(lhs, rhs);

        //--Assert
        Assert.assertEquals(false, underTest.evaluate(entity));
    }
}
