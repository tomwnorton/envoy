package org.envoy.collection.evaluators;

import org.junit.*;

import static org.mockito.Mockito.*;

public class EqualsEvaluatorTests extends AbstractBinaryEvaluatorTestBase {
    @Test
    public void test_that_evaluate_returns_true_when_lhs_wraps_hello_and_rhs_wraps_hello() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn("hello");
        when(valueCastor.getRhs()).thenReturn("hello");

        //--Assert
        Assert.assertEquals(true, underTest.evaluate(entity));
    }


    @Test
    public void test_that_evaluate_returns_false_when_lhs_wraps_hello_and_rhs_wraps_world() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn("hello");
        when(valueCastor.getRhs()).thenReturn("world");

        //--Assert
        Assert.assertEquals(false, underTest.evaluate(entity));
    }


    @Test
    public void test_that_evaluate_returns_false_when_lhs_wraps_null_and_rhs_wraps_world() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn(null);
        when(valueCastor.getRhs()).thenReturn("world");

        //--Assert
        Assert.assertEquals(false, underTest.evaluate(entity));
    }


    @Test
    public void test_that_evaluate_returns_false_when_lhs_wraps_hello_and_rhs_wraps_null() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn("hello");
        when(valueCastor.getRhs()).thenReturn(null);

        //--Assert
        Assert.assertEquals(false, underTest.evaluate(entity));
    }


    @Test
    public void test_that_evaluate_returns_true_when_lhs_wraps_null_and_rhs_wraps_null() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn(null);
        when(valueCastor.getRhs()).thenReturn(null);

        //--Assert
        Assert.assertEquals(true, underTest.evaluate(entity));
    }


    @Override
    protected Evaluator factory(final Evaluator lhs, final Evaluator rhs, final EvaluatorFactory evaluatorFactory) {
        return new EqualsEvaluator(lhs, rhs, evaluatorFactory);
    }
}
