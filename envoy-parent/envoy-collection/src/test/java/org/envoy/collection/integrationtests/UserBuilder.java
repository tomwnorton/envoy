package org.envoy.collection.integrationtests;

public class UserBuilder {
    private int id;
    private boolean active;


    public UserBuilder setId(final int id) {
        this.id = id;
        return this;
    }


    public UserBuilder setActive(final boolean active) {
        this.active = active;
        return this;
    }


    public User get() {
        final User result = new User();
        result.setId(id);
        result.setActive(active);
        return result;
    }
}
