package org.envoy.collection.evaluators;

import org.junit.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class TrimEvaluatorTests extends UnaryEvaluatorTestBase {
    @Override
    protected Evaluator createTestEvaluator(final Evaluator operand) {
        return new TrimEvaluator(operand);
    }


    @Test
    public void test_that_onEvaluate_trims_the_string_hello_world_when_that_string_is_surrounded_by_whitespace() {
        //--Arrange
        when(operand.evaluate(same(entity))).thenReturn("   \t  \n\rhello world\t\t   \t ");

        //--Act

        //--Assert
        Assert.assertEquals("hello world", underTest.evaluate(entity));
    }


    @Test
    public void test_that_onEvaluate_trims_the_string_good_morning_to_all_when_that_string_is_surrounded_by_whitespace() {
        //--Arrange
        when(operand.evaluate(same(entity))).thenReturn("   good morning to all ");

        //--Act

        //--Assert
        Assert.assertEquals("good morning to all", underTest.evaluate(entity));
    }
}
