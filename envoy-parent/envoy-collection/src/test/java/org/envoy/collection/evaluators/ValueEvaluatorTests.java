package org.envoy.collection.evaluators;

import org.junit.*;

public class ValueEvaluatorTests {
    @Test
    public void test_that_evaluate_returns_three_when_three_needs_to_be_evaluated() {
        //--Arrange
        final ValueEvaluator underTest = new ValueEvaluator(3);

        //--Assert
        Assert.assertEquals(Integer.valueOf(3), underTest.evaluate(null));
    }


    @Test
    public void test_that_evaluate_returns_hello_when_hello_needs_to_be_evaluated() {
        //--Arrange
        final ValueEvaluator underTest = new ValueEvaluator("hello");

        //--Assert
        Assert.assertEquals("hello", underTest.evaluate(null));
    }
}
