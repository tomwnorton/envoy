package org.envoy.collection.evaluators;

import org.junit.*;
import org.junit.runner.*;
import org.junit.runners.*;
import org.junit.runners.Parameterized.*;

import java.math.*;
import java.util.*;

import static org.mockito.Mockito.*;

@RunWith(Parameterized.class)
public class ModulusEvaluatorTests extends AbstractBinaryEvaluatorTestBase {
    private final Number givenLhs;
    private final Number givenRhs;
    private final Number expected;


    public ModulusEvaluatorTests(final Number lhs, final Number rhs, final Number expected) {
        givenLhs = lhs;
        givenRhs = rhs;
        this.expected = expected;
    }


    @Override
    protected Evaluator factory(final Evaluator lhs, final Evaluator rhs, final EvaluatorFactory evaluatorFactory) {
        return new ModulusEvaluator(lhs, rhs, evaluatorFactory);
    }


    @Parameters
    public static Collection<Object[]> getData() {
        final Object[][] data = new Object[][]{{27, 4, 3}, {39, 7, 4}};
        return Arrays.asList(data);
    }


    @Test
    public void test_that_calculate_for_bytes_works() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn(givenLhs.byteValue());
        when(valueCastor.getRhs()).thenReturn(givenRhs.byteValue());

        //--Assert
        Assert.assertEquals(expected.byteValue(), underTest.evaluate(entity));
    }


    @Test
    public void test_that_calculate_for_shorts_works() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn(givenLhs.shortValue());
        when(valueCastor.getRhs()).thenReturn(givenRhs.shortValue());

        //--Assert
        Assert.assertEquals(expected.shortValue(), underTest.evaluate(entity));
    }


    @Test
    public void test_that_calculate_for_ints_works() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn(givenLhs.intValue());
        when(valueCastor.getRhs()).thenReturn(givenRhs.intValue());

        //--Assert
        Assert.assertEquals(expected.intValue(), underTest.evaluate(entity));
    }


    @Test
    public void test_that_calculate_for_longs_works() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn(givenLhs.longValue());
        when(valueCastor.getRhs()).thenReturn(givenRhs.longValue());

        //--Assert
        Assert.assertEquals(expected.longValue(), underTest.evaluate(entity));
    }


    @Test
    public void test_that_calculate_for_BigIntegers_works() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn(new BigInteger(givenLhs.toString()));
        when(valueCastor.getRhs()).thenReturn(new BigInteger(givenRhs.toString()));

        //--Assert
        Assert.assertEquals(new BigInteger(expected.toString()), underTest.evaluate(entity));
    }
}
