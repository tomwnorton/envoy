package org.envoy.collection.evaluators;

import org.junit.*;

import static org.mockito.Mockito.*;

public class StartsWithEvaluatorTests extends AbstractBinaryEvaluatorWithDefaultValueTestBase {
    public StartsWithEvaluatorTests() {
        super(false);
    }


    @Override
    protected Evaluator factory(final Evaluator lhs, final Evaluator rhs, final EvaluatorFactory evaluatorFactory) {
        return new StartsWithEvaluator(lhs, rhs, evaluatorFactory);
    }


    @Test
    public void test_that_evaluate_returns_true_when_henchman_starts_with_hench() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn("henchman");
        when(valueCastor.getRhs()).thenReturn("hench");

        //--Assert
        Assert.assertEquals(true, underTest.evaluate(entity));
    }


    @Test
    public void test_that_evaluate_returns_false_when_henchman_does_not_start_with_enchm() {
        //--Arrange
        when(valueCastor.getLhs()).thenReturn("henchman");
        when(valueCastor.getRhs()).thenReturn("enchm");

        //--Assert
        Assert.assertEquals(false, underTest.evaluate(entity));
    }
}