package org.envoy.collection.evaluators;

import org.junit.*;
import org.mockito.*;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class ReducingEvaluatorTests {
    @Mock
    private Evaluator evaluator;

    @Mock
    private Reducer reducer;

    private final Object initialValue = new Object();
    private final Object entity = new Object();
    private ReducingEvaluator underTest;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(reducer.initialValue()).thenReturn(initialValue);

        underTest = new ReducingEvaluator(evaluator, reducer);
    }


    @Test
    public void test_that_reduce_is_called_with_the_initialValue_when_called_for_the_first_time() {
        //--Arrange
        final Object expected = new Object();
        when(reducer.reduce(same(initialValue), same(entity), same(evaluator))).thenReturn(expected);

        //--Act
        final Object actual = underTest.evaluate(entity);

        //--Assert
        assertThat(actual, is(sameInstance(expected)));
    }


    @Test
    public void test_that_reduce_is_called_with_the_value_first_returned_when_called_for_the_second_time() {
        //--Arrange
        final Object first = new Object();
        final Object expected = new Object();
        when(reducer.reduce(same(initialValue), same(entity), same(evaluator))).thenReturn(first);
        when(reducer.reduce(same(first), same(entity), same(evaluator))).thenReturn(expected);

        //--Act
        underTest.evaluate(entity);
        final Object actual = underTest.evaluate(entity);

        //--Assert
        assertThat(actual, is(sameInstance(expected)));
    }
}
