package org.envoy.collection.evaluators;

import org.junit.*;
import org.mockito.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class ConvertToStringEvaluatorTests {
    @Mock
    private Evaluator evaluator;

    @Mock
    private Object entity;

    private ConvertToStringEvaluator underTest;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        underTest = new ConvertToStringEvaluator(evaluator);
    }


    @Test
    public void test_that_evaluate_converts_the_int_9814_to__the_equivalent_string() {
        //--Arrange
        when(evaluator.evaluate(same(entity))).thenReturn(9814);

        //--Assert
        Assert.assertEquals("9814", underTest.evaluate(entity));
    }


    @Test
    public void test_that_evaluate_converts_the_int_35_to__the_equivalent_string() {
        //--Arrange
        when(evaluator.evaluate(same(entity))).thenReturn(35);

        //--Assert
        Assert.assertEquals("35", underTest.evaluate(entity));
    }


    @Test
    public void test_that_evaluate_null_when_the_given_evaluator_returns_null() {
        //--Arrange
        when(evaluator.evaluate(same(entity))).thenReturn(null);

        //--Assert
        Assert.assertNull(underTest.evaluate(entity));
    }
}
