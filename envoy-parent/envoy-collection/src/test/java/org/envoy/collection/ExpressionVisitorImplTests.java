package org.envoy.collection;

import org.envoy.collection.evaluators.*;
import org.envoy.expression.*;
import org.junit.*;
import org.mockito.*;
import org.mockito.invocation.*;
import org.mockito.stubbing.*;

import java.io.*;
import java.util.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class ExpressionVisitorImplTests {
    @Mock
    private EvaluatorFactory evaluatorFactory;

    private ExpressionVisitorImpl underTest;


    private static Stubber doAccept(final Evaluator expected) {
        return doAnswer(
                new Answer<Void>() {
                    @Override
                    public Void answer(final InvocationOnMock invocation) throws Throwable {
                        final ExpressionVisitorImpl visitor = (ExpressionVisitorImpl) invocation.getArguments()[0];
                        visitor.push(expected);
                        return null;
                    }
                });
    }


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        underTest = new ExpressionVisitorImpl(evaluatorFactory);
    }


    @Test
    public void test_that_isMatch_returns_true_when_all_that_is_to_be_processed_is_the_parameter_true() {
        //--Arrange
        final ValueEvaluator parameter = mock(ValueEvaluator.class);
        when(evaluatorFactory.createValueEvaluator(true)).thenReturn(parameter);
        when(parameter.evaluate(null)).thenReturn(true);

        //--Act
        underTest.visitParameter(true);

        //--Assert
        Assert.assertSame(parameter, underTest.peek());
    }


    @Test
    public void test_that_isMatch_returns_false_when_all_that_is_to_be_processed_is_the_parameter_false() {
        //--Arrange
        final ValueEvaluator parameter = mock(ValueEvaluator.class);
        when(evaluatorFactory.createValueEvaluator(false)).thenReturn(parameter);
        when(parameter.evaluate(null)).thenReturn(false);

        //--Act
        underTest.visitParameter(false);

        //--Assert
        Assert.assertSame(parameter, underTest.peek());
    }


    @Test
    public void test_that_visitTrue_resolves_to_true() {
        //--Arrange
        final ValueEvaluator trueConstant = mock(ValueEvaluator.class);
        when(evaluatorFactory.createValueEvaluator(true)).thenReturn(trueConstant);
        when(trueConstant.evaluate(null)).thenReturn(true);

        //--Act
        underTest.visitTrue();

        //--Assert
        Assert.assertSame(trueConstant, underTest.peek());
    }


    @Test
    public void test_that_visitFalse_resolves_to_false() {
        //--Arrange
        final ValueEvaluator falseConstant = mock(ValueEvaluator.class);
        when(evaluatorFactory.createValueEvaluator(false)).thenReturn(falseConstant);
        when(falseConstant.evaluate(null)).thenReturn(false);

        //--Act
        underTest.visitFalse();

        //--Assert
        Assert.assertSame(falseConstant, underTest.peek());
    }


    @Test
    public void test_that_visitEquals_resolves_to_true_when_the_EqualsExpression_evaluates_as_thus() {
        //--Arrange
        final Expression lhsExpr = mock(Expression.class);
        final Evaluator lhs = mock(Evaluator.class);
        doAccept(lhs).when(lhsExpr).accept(same(underTest));

        final Expression rhsExpr = mock(Expression.class);
        final Evaluator rhs = mock(Evaluator.class);
        doAccept(rhs).when(rhsExpr).accept(same(underTest));

        final EqualsEvaluator expected = new EqualsEvaluator(null, null, null);
        when(evaluatorFactory.createEqualsEvaluator(same(lhs), same(rhs))).thenReturn(expected);

        //--Act
        underTest.visitEquals(lhsExpr, rhsExpr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitNull_resolves_to_true_when_the_EqualsExpression_evaluates_as_thus() {
        //--Arrange
        final Expression lhsExpr = mock(Expression.class);
        final Evaluator lhs = mock(Evaluator.class);
        doAccept(lhs).when(lhsExpr).accept(same(underTest));

        final NullEvaluator rhs = new NullEvaluator();
        when(evaluatorFactory.createNullEvaluator()).thenReturn(rhs);

        final EqualsEvaluator expected = new EqualsEvaluator(null, null, null);
        when(evaluatorFactory.createEqualsEvaluator(same(lhs), same(rhs))).thenReturn(expected);

        //--Act
        underTest.visitNull(lhsExpr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitNotEquals_resolves_to_true_when_the_NotEqualsExpression_evaluates_as_thus() {
        //--Arrange
        final Expression lhsExpr = mock(Expression.class);
        final Evaluator lhs = mock(Evaluator.class);
        doAccept(lhs).when(lhsExpr).accept(same(underTest));

        final Expression rhsExpr = mock(Expression.class);
        final Evaluator rhs = mock(Evaluator.class);
        doAccept(rhs).when(rhsExpr).accept(same(underTest));

        final NotEqualsEvaluator expected = new NotEqualsEvaluator(null, null, null);
        when(evaluatorFactory.createNotEqualsEvaluator(same(lhs), same(rhs))).thenReturn(expected);

        //--Act
        underTest.visitNotEquals(lhsExpr, rhsExpr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitNotNull_resolves_to_true_when_the_NotEqualsExpression_evaluates_as_thus() {
        //--Arrange
        final Expression lhsExpr = mock(Expression.class);
        final Evaluator lhs = mock(Evaluator.class);
        doAccept(lhs).when(lhsExpr).accept(same(underTest));

        final NullEvaluator rhs = new NullEvaluator();
        when(evaluatorFactory.createNullEvaluator()).thenReturn(rhs);

        final NotEqualsEvaluator expected = new NotEqualsEvaluator(null, null, null);
        when(evaluatorFactory.createNotEqualsEvaluator(same(lhs), same(rhs))).thenReturn(expected);

        //--Act
        underTest.visitNotNull(lhsExpr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitNot_resolves_to_true_when_false_is_given() {
        //--Arrange
        final Serializable entity = mock(Serializable.class);

        final Expression operandExpr = mock(Expression.class);
        final Evaluator operand = mock(Evaluator.class);
        doAccept(operand).when(operandExpr).accept(same(underTest));
        when(operand.evaluate(same(entity))).thenReturn(false);

        //--Act
        underTest.visitNot(operandExpr);

        //--Assert
        Assert.assertTrue(underTest.isMatch(entity));
    }


    @Test
    public void test_that_visitNot_resolves_to_false_when_true_is_given() {
        //--Arrange
        final Serializable entity = mock(Serializable.class);

        final Expression operandExpr = mock(Expression.class);
        final Evaluator operand = mock(Evaluator.class);
        doAccept(operand).when(operandExpr).accept(same(underTest));
        when(operand.evaluate(same(entity))).thenReturn(true);

        //--Act
        underTest.visitNot(operandExpr);

        //--Assert
        Assert.assertFalse(underTest.isMatch(entity));
    }


    @Test
    public void test_that_visitOr_resolves_to_true_when_the_OrExpression_evaluates_as_thus() {
        //--Arrange
        final Expression lhsExpr = mock(Expression.class);
        final Evaluator lhs = mock(Evaluator.class);
        doAccept(lhs).when(lhsExpr).accept(same(underTest));

        final Expression rhsExpr = mock(Expression.class);
        final Evaluator rhs = mock(Evaluator.class);
        doAccept(rhs).when(rhsExpr).accept(same(underTest));

        final OrEvaluator expected = new OrEvaluator(null, null);
        when(evaluatorFactory.createOrEvaluator(same(lhs), same(rhs))).thenReturn(expected);

        //--Act
        underTest.visitOr(lhsExpr, rhsExpr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitAnd_resolves_to_true_when_the_AndExpression_evaluates_as_thus() {
        //--Arrange
        final Expression lhsExpr = mock(Expression.class);
        final Evaluator lhs = mock(Evaluator.class);
        doAccept(lhs).when(lhsExpr).accept(same(underTest));

        final Expression rhsExpr = mock(Expression.class);
        final Evaluator rhs = mock(Evaluator.class);
        doAccept(rhs).when(rhsExpr).accept(same(underTest));

        final AndEvaluator expected = new AndEvaluator(null, null);
        when(evaluatorFactory.createAndEvaluator(same(lhs), same(rhs))).thenReturn(expected);

        //--Act
        underTest.visitAnd(lhsExpr, rhsExpr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_visitField_resolves_to_the_field_value() {
        //--Arrange
        final GenericFieldExpression<Serializable> fieldExpr = mock(GenericFieldExpression.class);
        final FieldEvaluator fieldEvaluator = new FieldEvaluator(null);
        when(evaluatorFactory.createFieldEvaluator(same(fieldExpr))).thenReturn(fieldEvaluator);

        //--Act
        underTest.visitField(fieldExpr);

        //--Assert
        Assert.assertSame(fieldEvaluator, underTest.peek());
    }


    @Test
    public void test_that_visitLessThan_generates_the_correct_LessThanEvaluator() {
        //--Arrange
        final Expression lhsExpr = mock(Expression.class);
        final Evaluator lhs = mock(Evaluator.class);
        doAccept(lhs).when(lhsExpr).accept(same(underTest));

        final Expression rhsExpr = mock(Expression.class);
        final Evaluator rhs = mock(Evaluator.class);
        doAccept(rhs).when(rhsExpr).accept(same(underTest));

        final LessThanEvaluator expected = new LessThanEvaluator(null, null, null);
        when(evaluatorFactory.createLessThanEvaluator(same(lhs), same(rhs))).thenReturn(expected);

        //--Act
        underTest.visitLessThan(lhsExpr, rhsExpr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitLessThanEqual_generates_the_correct_LessThanEqualEvaluator() {
        //--Arrange
        final Expression lhsExpr = mock(Expression.class);
        final Evaluator lhs = mock(Evaluator.class);
        doAccept(lhs).when(lhsExpr).accept(same(underTest));

        final Expression rhsExpr = mock(Expression.class);
        final Evaluator rhs = mock(Evaluator.class);
        doAccept(rhs).when(rhsExpr).accept(same(underTest));

        final LessThanEqualEvaluator expected = new LessThanEqualEvaluator(null, null, null);
        when(evaluatorFactory.createLessThanEqualEvaluator(same(lhs), same(rhs))).thenReturn(expected);

        //--Act
        underTest.visitLessThanEqual(lhsExpr, rhsExpr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitGreaterThan_generates_the_correct_GreaterThanEvaluator() {
        //--Arrange
        final Expression lhsExpr = mock(Expression.class);
        final Evaluator lhs = mock(Evaluator.class);
        doAccept(lhs).when(lhsExpr).accept(same(underTest));

        final Expression rhsExpr = mock(Expression.class);
        final Evaluator rhs = mock(Evaluator.class);
        doAccept(rhs).when(rhsExpr).accept(same(underTest));

        final GreaterThanEvaluator expected = new GreaterThanEvaluator(null, null, null);
        when(evaluatorFactory.createGreaterThanEvaluator(same(lhs), same(rhs))).thenReturn(expected);

        //--Act
        underTest.visitGreaterThan(lhsExpr, rhsExpr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitGreaterThanEqual_generates_the_correct_GreaterThanEqualEvaluator() {
        //--Arrange
        final Expression lhsExpr = mock(Expression.class);
        final Evaluator lhs = mock(Evaluator.class);
        doAccept(lhs).when(lhsExpr).accept(same(underTest));

        final Expression rhsExpr = mock(Expression.class);
        final Evaluator rhs = mock(Evaluator.class);
        doAccept(rhs).when(rhsExpr).accept(same(underTest));

        final GreaterThanEqualEvaluator expected = new GreaterThanEqualEvaluator(null, null, null);
        when(evaluatorFactory.createGreaterThanEqualEvaluator(same(lhs), same(rhs))).thenReturn(expected);

        //--Act
        underTest.visitGreaterThanEqual(lhsExpr, rhsExpr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitConvertToString_generates_the_correct_ConvertToStringEvaluator() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        final Evaluator evaluator = mock(Evaluator.class);
        doAccept(evaluator).when(expr).accept(same(underTest));

        final ConvertToStringEvaluator expected = new ConvertToStringEvaluator(null);
        when(evaluatorFactory.createConvertToStringEvaluator(same(evaluator))).thenReturn(expected);

        //--Act
        underTest.visitConvertToString(expr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitPlus_generates_the_correct_PlusEvaluator() {
        //--Arrange
        final Expression lhsExpr = mock(Expression.class);
        final Evaluator lhs = mock(Evaluator.class);
        doAccept(lhs).when(lhsExpr).accept(same(underTest));

        final Expression rhsExpr = mock(Expression.class);
        final Evaluator rhs = mock(Evaluator.class);
        doAccept(rhs).when(rhsExpr).accept(same(underTest));

        final PlusEvaluator expected = new PlusEvaluator(null, null, null);
        when(evaluatorFactory.createPlusEvaluator(same(lhs), same(rhs))).thenReturn(expected);

        //--Act
        underTest.visitPlus(lhsExpr, rhsExpr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitMinus_generates_the_correct_MinusEvaluator() {
        //--Arrange
        final Expression lhsExpr = mock(Expression.class);
        final Evaluator lhs = mock(Evaluator.class);
        doAccept(lhs).when(lhsExpr).accept(same(underTest));

        final Expression rhsExpr = mock(Expression.class);
        final Evaluator rhs = mock(Evaluator.class);
        doAccept(rhs).when(rhsExpr).accept(same(underTest));

        final MinusEvaluator expected = new MinusEvaluator(null, null, null);
        when(evaluatorFactory.createMinusEvaluator(same(lhs), same(rhs))).thenReturn(expected);

        //--Act
        underTest.visitMinus(lhsExpr, rhsExpr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitTimes_generates_the_correct_TimesEvaluator() {
        //--Arrange
        final Expression lhsExpr = mock(Expression.class);
        final Evaluator lhs = mock(Evaluator.class);
        doAccept(lhs).when(lhsExpr).accept(same(underTest));

        final Expression rhsExpr = mock(Expression.class);
        final Evaluator rhs = mock(Evaluator.class);
        doAccept(rhs).when(rhsExpr).accept(same(underTest));

        final TimesEvaluator expected = new TimesEvaluator(null, null, null);
        when(evaluatorFactory.createTimesEvaluator(same(lhs), same(rhs))).thenReturn(expected);

        //--Act
        underTest.visitTimes(lhsExpr, rhsExpr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitDividedBy_generates_the_correct_DividedByEvaluator() {
        //--Arrange
        final Expression lhsExpr = mock(Expression.class);
        final Evaluator lhs = mock(Evaluator.class);
        doAccept(lhs).when(lhsExpr).accept(same(underTest));

        final Expression rhsExpr = mock(Expression.class);
        final Evaluator rhs = mock(Evaluator.class);
        doAccept(rhs).when(rhsExpr).accept(same(underTest));

        final DividedByEvaluator expected = new DividedByEvaluator(null, null, null);
        when(evaluatorFactory.createDividedByEvaluator(same(lhs), same(rhs))).thenReturn(expected);

        //--Act
        underTest.visitDividedBy(lhsExpr, rhsExpr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitModulus_generates_the_correct_ModulusEvaluator() {
        //--Arrange
        final Expression lhsExpr = mock(Expression.class);
        final Evaluator lhs = mock(Evaluator.class);
        doAccept(lhs).when(lhsExpr).accept(same(underTest));

        final Expression rhsExpr = mock(Expression.class);
        final Evaluator rhs = mock(Evaluator.class);
        doAccept(rhs).when(rhsExpr).accept(same(underTest));

        final ModulusEvaluator expected = new ModulusEvaluator(null, null, null);
        when(evaluatorFactory.createModulusEvaluator(same(lhs), same(rhs))).thenReturn(expected);

        //--Act
        underTest.visitModulus(lhsExpr, rhsExpr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitCoalesce_generates_the_correct_CoalesceEvaluator_when_there_are_three_expressions() {
        //--Arrange
        final Expression firstExpr = mock(Expression.class);
        final Evaluator firstEvaluator = mock(Evaluator.class);
        doAccept(firstEvaluator).when(firstExpr).accept(same(underTest));

        final Expression secondExpr = mock(Expression.class);
        final Evaluator secondEvaluator = mock(Evaluator.class);
        doAccept(secondEvaluator).when(secondExpr).accept(same(underTest));

        final Expression thirdExpr = mock(Expression.class);
        final Evaluator thirdEvaluator = mock(Evaluator.class);
        doAccept(thirdEvaluator).when(thirdExpr).accept(same(underTest));

        final CoalesceEvaluator expected = new CoalesceEvaluator(null);
        when(
                evaluatorFactory.createCoalesceEvaluator(
                        Arrays.asList(
                                firstEvaluator,
                                secondEvaluator,
                                thirdEvaluator)))
                .thenReturn(expected);

        //--Act
        underTest.visitCoalesce(Arrays.<Expression>asList(firstExpr, secondExpr, thirdExpr));

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitNegative_generates_the_correct_NegativeEvaluator() {
        //--Arrange
        final Expression operandExpr = mock(Expression.class);
        final Evaluator operand = mock(Evaluator.class);
        doAccept(operand).when(operandExpr).accept(same(underTest));

        final NegativeEvaluator<Integer> expected = new NegativeEvaluator<Integer>(null);
        when(evaluatorFactory.<Integer>createNegativeEvaluator(same(operand))).thenReturn(expected);

        //--Act
        underTest.visitNegative(operandExpr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitBetween_generates_the_correct_AndEvaluator() {
        //--Arrange
        final Expression lhsExpr = mock(Expression.class);
        final Evaluator lhs = mock(Evaluator.class);
        doAccept(lhs).when(lhsExpr).accept(same(underTest));

        final Expression lowerBoundExpr = mock(Expression.class);
        final Evaluator lowerBound = mock(Evaluator.class);
        doAccept(lowerBound).when(lowerBoundExpr).accept(same(underTest));

        final Expression upperBoundExpr = mock(Expression.class);
        final Evaluator upperBound = mock(Evaluator.class);
        doAccept(upperBound).when(upperBoundExpr).accept(same(underTest));

        final AndEvaluator expected = new AndEvaluator(null, null);
        when(evaluatorFactory.createBetweenEvaluator(same(lhs), same(lowerBound), same(upperBound))).thenReturn(expected);

        //--Act
        underTest.visitBetween(lhsExpr, lowerBoundExpr, upperBoundExpr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitTrim_generates_the_correct_TrimEvaluator() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        final Evaluator operand = mock(Evaluator.class);
        doAccept(operand).when(expr).accept(same(underTest));

        final TrimEvaluator expected = new TrimEvaluator(null);
        when(evaluatorFactory.createTrimEvaluator(same(operand))).thenReturn(expected);

        //--Act
        underTest.visitTrim(expr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitLength_generates_the_correct_LengthEvaluator() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        final Evaluator operand = mock(Evaluator.class);
        doAccept(operand).when(expr).accept(same(underTest));

        final LengthEvaluator expected = new LengthEvaluator(null);
        when(evaluatorFactory.createLengthEvaluator(same(operand))).thenReturn(expected);

        //--Act
        underTest.visitLength(expr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitLowerCase_generates_the_correct_LowerCaseEvaluator() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        final Evaluator operand = mock(Evaluator.class);
        doAccept(operand).when(expr).accept(same(underTest));

        final LowerCaseEvaluator expected = new LowerCaseEvaluator(null);
        when(evaluatorFactory.createLowerCaseEvaluator(same(operand))).thenReturn(expected);

        //--Act
        underTest.visitLowerCase(expr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitUpperCase_generates_the_correct_UpperCaseEvaluator() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        final Evaluator operand = mock(Evaluator.class);
        doAccept(operand).when(expr).accept(same(underTest));

        final UpperCaseEvaluator expected = new UpperCaseEvaluator(null);
        when(evaluatorFactory.createUpperCaseEvaluator(same(operand))).thenReturn(expected);

        //--Act
        underTest.visitUpperCase(expr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitConvertStringToInteger_generates_the_correct_ConvertStringToIntegerEvaluator() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        final Evaluator operand = mock(Evaluator.class);
        doAccept(operand).when(expr).accept(same(underTest));

        final ConvertStringToIntegerEvaluator expected = new ConvertStringToIntegerEvaluator(null);
        when(evaluatorFactory.createConvertStringToIntegerEvaluator(same(operand))).thenReturn(expected);

        //--Act
        underTest.visitConvertStringToInteger(expr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitConcat_generates_the_correct_ConcatEvaluator() {
        //--Arrange
        final Expression lhsExpr = mock(Expression.class);
        final Evaluator lhs = mock(Evaluator.class);
        doAccept(lhs).when(lhsExpr).accept(underTest);

        final Expression rhsExpr = mock(Expression.class);
        final Evaluator rhs = mock(Evaluator.class);
        doAccept(rhs).when(rhsExpr).accept(underTest);

        final ConcatEvaluator expected = new ConcatEvaluator(null, null, null);
        when(evaluatorFactory.createConcatEvaluator(same(lhs), same(rhs))).thenReturn(expected);

        //--Act
        underTest.visitConcat(lhsExpr, rhsExpr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitContains_generates_the_correct_ContainsEvaluator() {
        //--Arrange
        final Expression lhsExpr = mock(Expression.class);
        final Evaluator lhs = mock(Evaluator.class);
        doAccept(lhs).when(lhsExpr).accept(underTest);

        final Expression rhsExpr = mock(Expression.class);
        final Evaluator rhs = mock(Evaluator.class);
        doAccept(rhs).when(rhsExpr).accept(underTest);

        final ContainsEvaluator expected = new ContainsEvaluator(null, null, null);
        when(evaluatorFactory.createContainsEvaluator(same(lhs), same(rhs))).thenReturn(expected);

        //--Act
        underTest.visitContains(lhsExpr, rhsExpr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitStartsWith_generates_the_correct_StartsWithEvaluator() {
        //--Arrange
        final Expression lhsExpr = mock(Expression.class);
        final Evaluator lhs = mock(Evaluator.class);
        doAccept(lhs).when(lhsExpr).accept(underTest);

        final Expression rhsExpr = mock(Expression.class);
        final Evaluator rhs = mock(Evaluator.class);
        doAccept(rhs).when(rhsExpr).accept(underTest);

        final StartsWithEvaluator expected = new StartsWithEvaluator(null, null, null);
        when(evaluatorFactory.createStartsWithEvaluator(same(lhs), same(rhs))).thenReturn(expected);

        //--Act
        underTest.visitStartsWith(lhsExpr, rhsExpr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitEndsWith_generates_the_correct_EndsWithEvaluator() {
        //--Arrange
        final Expression lhsExpr = mock(Expression.class);
        final Evaluator lhs = mock(Evaluator.class);
        doAccept(lhs).when(lhsExpr).accept(underTest);

        final Expression rhsExpr = mock(Expression.class);
        final Evaluator rhs = mock(Evaluator.class);
        doAccept(rhs).when(rhsExpr).accept(underTest);

        final EndsWithEvaluator expected = new EndsWithEvaluator(null, null, null);
        when(evaluatorFactory.createEndsWithEvaluator(same(lhs), same(rhs))).thenReturn(expected);

        //--Act
        underTest.visitEndsWith(lhsExpr, rhsExpr);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }


    @Test
    public void test_that_visitIn_generates_the_correct_InEvaluator() {
        //--Arrange
        final Expression lhsExpr = mock(Expression.class);
        final Evaluator lhs = mock(Evaluator.class);
        doAccept(lhs).when(lhsExpr).accept(underTest);

        final Collection<Object> values = new ArrayList<Object>();

        final InEvaluator expected = new InEvaluator(null, null);
        when(evaluatorFactory.createInEvaluator(same(lhs), same(values))).thenReturn(expected);

        //--Act
        underTest.visitIn(lhsExpr, values);

        //--Assert
        Assert.assertSame(expected, underTest.peek());
    }
}
