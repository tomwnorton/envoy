package org.envoy.collection.evaluators;

import org.junit.*;

import java.util.*;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class InEvaluatorTests {
    @Test
    public void test_that_evaluate_returns_true_when_lhs_is_in_the_collection() {
        //--Arrange
        final ArrayList<Object> list = new ArrayList<>();
        list.add(new Object());
        list.add(new Object());
        list.add(new Object());

        final Object expected = list.get(1);
        final Object entity = new Object();

        final Evaluator lhs = mock(Evaluator.class);
        when(lhs.evaluate(same(entity))).thenReturn(expected);

        final InEvaluator underTest = new InEvaluator(lhs, list);

        //--Assert
        assertThat((Boolean) underTest.evaluate(entity), is(true));
    }


    @Test
    public void test_that_evaluate_returns_false_when_lhs_is_not_in_the_collection() {
        //--Arrange
        final ArrayList<Object> list = new ArrayList<>();
        list.add(new Object());
        list.add(new Object());
        list.add(new Object());

        final Object expected = new Object();
        final Object entity = new Object();

        final Evaluator lhs = mock(Evaluator.class);
        when(lhs.evaluate(same(entity))).thenReturn(expected);

        final InEvaluator underTest = new InEvaluator(lhs, list);

        //--Assert
        assertThat((Boolean) underTest.evaluate(entity), is(false));
    }
}
