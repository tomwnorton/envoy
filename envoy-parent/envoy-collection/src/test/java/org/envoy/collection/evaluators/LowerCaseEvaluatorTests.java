package org.envoy.collection.evaluators;

import org.junit.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class LowerCaseEvaluatorTests extends UnaryEvaluatorTestBase {
    @Override
    protected Evaluator createTestEvaluator(final Evaluator operand) {
        return new LowerCaseEvaluator(operand);
    }


    @Test
    public void test_that_evaluate_returns_hello_when_HELLO_is_given() {
        //--Arrange
        when(operand.evaluate(same(entity))).thenReturn("HELLO");

        //--Assert
        Assert.assertEquals("hello", underTest.evaluate(entity));
    }


    @Test
    public void test_that_evaluate_returns_world_when_WORLD_is_given() {
        //--Arrange
        when(operand.evaluate(same(entity))).thenReturn("WORLD");

        //--Assert
        Assert.assertEquals("world", underTest.evaluate(entity));
    }
}
