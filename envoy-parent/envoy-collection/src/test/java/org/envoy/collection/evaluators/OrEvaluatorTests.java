package org.envoy.collection.evaluators;

import org.junit.*;

import java.io.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class OrEvaluatorTests {
    @Test
    public void test_that_true_or_true_is_true() {
        //--Arrange
        final Serializable entity = mock(Serializable.class);

        final Evaluator lhs = mock(Evaluator.class);
        when(lhs.evaluate(same(entity))).thenReturn(true);

        final Evaluator rhs = mock(Evaluator.class);
        when(rhs.evaluate(same(entity))).thenReturn(true);

        final OrEvaluator underTest = new OrEvaluator(lhs, rhs);

        //--Assert
        Assert.assertEquals(true, underTest.evaluate(entity));
    }


    @Test
    public void test_that_false_or_false_is_false() {
        //--Arrange
        final Serializable entity = mock(Serializable.class);

        final Evaluator lhs = mock(Evaluator.class);
        when(lhs.evaluate(same(entity))).thenReturn(false);

        final Evaluator rhs = mock(Evaluator.class);
        when(rhs.evaluate(same(entity))).thenReturn(false);

        final OrEvaluator underTest = new OrEvaluator(lhs, rhs);

        //--Assert
        Assert.assertEquals(false, underTest.evaluate(entity));
    }


    @Test
    public void test_that_true_or_false_is_true() {
        //--Arrange
        final Serializable entity = mock(Serializable.class);

        final Evaluator lhs = mock(Evaluator.class);
        when(lhs.evaluate(same(entity))).thenReturn(true);

        final Evaluator rhs = mock(Evaluator.class);
        when(rhs.evaluate(same(entity))).thenReturn(false);

        final OrEvaluator underTest = new OrEvaluator(lhs, rhs);

        //--Assert
        Assert.assertEquals(true, underTest.evaluate(entity));
    }


    @Test
    public void test_that_false_or_true_is_true() {
        //--Arrange
        final Serializable entity = mock(Serializable.class);

        final Evaluator lhs = mock(Evaluator.class);
        when(lhs.evaluate(same(entity))).thenReturn(false);

        final Evaluator rhs = mock(Evaluator.class);
        when(rhs.evaluate(same(entity))).thenReturn(true);

        final OrEvaluator underTest = new OrEvaluator(lhs, rhs);

        //--Assert
        Assert.assertEquals(true, underTest.evaluate(entity));
    }
}
