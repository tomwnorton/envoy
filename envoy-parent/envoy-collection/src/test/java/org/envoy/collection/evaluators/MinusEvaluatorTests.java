package org.envoy.collection.evaluators;

import org.junit.runner.*;
import org.junit.runners.*;
import org.junit.runners.Parameterized.*;

import java.util.*;

@RunWith(Parameterized.class)
public class MinusEvaluatorTests extends NumericBinaryEvaluatorTestBase {
    public MinusEvaluatorTests(final Number lhs, final Number rhs, final Number expected) {
        super(lhs, rhs, expected);
    }


    @Parameters
    public static Collection<Object[]> getData() {
        final Object[][] data = new Object[][]{{4, 2, 2}, {17, 12, 5}};
        return Arrays.asList(data);
    }


    @Override
    protected Evaluator factory(final Evaluator lhs, final Evaluator rhs, final EvaluatorFactory evaluatorFactory) {
        return new MinusEvaluator(lhs, rhs, evaluatorFactory);
    }
}
