package org.envoy.derby.functionaltests;

import org.envoy.*;

import java.io.*;

public class Name implements Serializable {
    private static final long serialVersionUID = -6537395622680287197L;

    private Property<String> firstName = new Property<>();
    private Property<String> lastName = new Property<>();


    public String getFirstNameLastName() {
        return firstName.getValue() + " " + lastName.getValue();
    }


    public String getLastNameFirstName() {
        return lastName.getValue() + ", " + firstName.getValue();
    }


    public String getFirstName() {
        return firstName.getValue();
    }


    public void setFirstName(final String firstName) {
        this.firstName.setValue(firstName);
    }


    public String getLastName() {
        return lastName.getValue();
    }


    public void setLastName(final String lastName) {
        this.lastName.setValue(lastName);
    }
}
