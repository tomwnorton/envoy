package org.envoy.derby.functionaltests;

import org.envoy.expression.*;

public class ContactInformationExpression extends AbstractComponentExpression<ContactInformation> {
    private final NameExpression name = new NameExpression(this);
    private final StringFieldExpression addressLine1 = new StringFieldExpression(this);
    private final StringFieldExpression addressLine2 = new StringFieldExpression(this);
    private final StringFieldExpression city = new StringFieldExpression(this);
    private final StringFieldExpression state = new StringFieldExpression(this);
    private final StringFieldExpression postalCode = new StringFieldExpression(this);
    private final StringFieldExpression countryCode = new StringFieldExpression(this);


    public ContactInformationExpression(ModelExpression parent) {
        super(parent, ContactInformation.class);
    }


    public NameExpression getName() {
        return name;
    }


    public StringFieldExpression getAddressLine1() {
        return addressLine1;
    }


    public StringFieldExpression getAddressLine2() {
        return addressLine2;
    }


    public StringFieldExpression getCity() {
        return city;
    }


    public StringFieldExpression getState() {
        return state;
    }


    public StringFieldExpression getPostalCode() {
        return postalCode;
    }


    public StringFieldExpression getCountryCode() {
        return countryCode;
    }
}
