package org.envoy.derby.functionaltests;

import com.mchange.v2.c3p0.*;
import org.envoy.derby.*;
import org.envoy.query.grouping.*;
import org.junit.*;

import java.beans.*;
import java.sql.*;
import java.util.*;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

/**
 * Created by thomas on 8/25/14.
 */
public class DerbyIntegrationTests {
    private ComboPooledDataSource dataSource;
    private Connection connection;
    static DerbyDatabaseMetaData metaData;

    @Before
    public void setUp() throws PropertyVetoException, SQLException {
        if (dataSource == null) {
            dataSource = new ComboPooledDataSource();
            dataSource.setDriverClass("org.apache.derby.jdbc.EmbeddedDriver");
            dataSource.setJdbcUrl("jdbc:derby:memory:sample;create=true");
        }
        connection = dataSource.getConnection();
        metaData = new DerbyDatabaseMetaData();
        metaData.setDataSource(dataSource);

        runQuery(
                "CREATE TABLE \"User\" (ID INT PRIMARY KEY, UserName VARCHAR(50), Active INT NOT NULL, FirstName VARCHAR(50), "
                + "LastName VARCHAR(50), Address1 VARCHAR(50), Address2 VARCHAR(50), City VARCHAR(50), " +
                "\"State\" VARCHAR(50), Zip VARCHAR(10), Country VARCHAR(2))");
        runQuery(
                "CREATE TABLE People (ID INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), " +
                "FullName VARCHAR(50))");
    }

    private void runQuery(String sql) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.execute();
        }
    }

    @After
    public void tearDown() throws SQLException {
        if (connection != null) {
            runQuery("DROP TABLE \"User\"");
            runQuery("DROP TABLE People");
            connection.close();
        }
    }


    @Test
    public void test_that_sql_works_for_select_of_active_elements() throws SQLException {
        //--Arrange
        final String INSERT_SQL = "INSERT INTO \"User\" (ID, UserName, Active, FirstName, LastName, Zip) "
                                  + "VALUES (?, ?, ?, ?, ?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(INSERT_SQL)) {
            ps.setInt(1, 1);
            ps.setString(2, "foo");
            ps.setBoolean(3, true);
            ps.setString(4, "Bob");
            ps.setString(5, "Smith");
            ps.setString(6, null);
            ps.executeUpdate();
            ps.clearParameters();

            ps.setInt(1, 2);
            ps.setString(2, "bar");
            ps.setBoolean(3, false);
            ps.setString(4, "John");
            ps.setString(5, "Jones");
            ps.setString(6, null);
            ps.executeUpdate();
            ps.clearParameters();

            ps.setInt(1, 3);
            ps.setString(2, null);
            ps.setBoolean(3, true);
            ps.setString(4, null);
            ps.setString(5, null);
            ps.setString(6, "12345");
            ps.executeUpdate();
            ps.clearParameters();

            ps.setInt(1, 9);
            ps.setString(2, "hello");
            ps.setBoolean(3, true);
            ps.setString(4, null);
            ps.setString(5, null);
            ps.setString(6, null);
            ps.executeUpdate();
        }

        //--Act
        UserExpression user = new UserExpression();
        List<User> actual = metaData.getQueryBuilder()
                .from(user)
                .where(user.isActive())
                .list();

        //--Assert
        assertThat(actual, hasSize(3));

        assertThat(actual.get(0).getId(), is(1));
        assertThat(actual.get(0).getUserName(), is("foo"));
        assertThat(actual.get(0).isActive(), is(true));
        assertThat(actual.get(0).getContactInformation().getName().getFirstName(), is("Bob"));
        assertThat(actual.get(0).getContactInformation().getName().getLastName(), is("Smith"));
        assertThat(actual.get(0).getContactInformation().getAddressLine1(), is((String) null));
        assertThat(actual.get(0).getContactInformation().getAddressLine2(), is((String) null));
        assertThat(actual.get(0).getContactInformation().getCity(), is((String) null));
        assertThat(actual.get(0).getContactInformation().getState(), is((String) null));
        assertThat(actual.get(0).getContactInformation().getPostalCode(), is((String) null));
        assertThat(actual.get(0).getContactInformation().getCountryCode(), is((String) null));

        assertThat(actual.get(1).getId(), is(3));
        assertThat(actual.get(1).getUserName(), is((String) null));
        assertThat(actual.get(1).isActive(), is(true));
        assertThat(actual.get(1).getContactInformation().getName(), is((Name) null));
        assertThat(actual.get(1).getContactInformation().getAddressLine1(), is((String) null));
        assertThat(actual.get(1).getContactInformation().getAddressLine2(), is((String) null));
        assertThat(actual.get(1).getContactInformation().getCity(), is((String) null));
        assertThat(actual.get(1).getContactInformation().getState(), is((String) null));
        assertThat(actual.get(1).getContactInformation().getPostalCode(), is("12345"));
        assertThat(actual.get(1).getContactInformation().getCountryCode(), is((String) null));

        assertThat(actual.get(2).getId(), is(9));
        assertThat(actual.get(2).getUserName(), is("hello"));
        assertThat(actual.get(2).isActive(), is(true));
        assertThat(actual.get(2).getContactInformation(), is((ContactInformation) null));
    }


    @Test
    public void test_that_sql_works_for_select_of_ids() throws SQLException {
        //--Arrange
        final String INSERT_SQL = "INSERT INTO \"User\" (ID, UserName, Active, FirstName, LastName, Zip) "
                                  + "VALUES (?, ?, ?, ?, ?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(INSERT_SQL)) {
            ps.setInt(1, 1);
            ps.setString(2, "foo");
            ps.setBoolean(3, true);
            ps.setString(4, "Bob");
            ps.setString(5, "Smith");
            ps.setString(6, null);
            ps.executeUpdate();
            ps.clearParameters();

            ps.setInt(1, 2);
            ps.setString(2, "bar");
            ps.setBoolean(3, false);
            ps.setString(4, "John");
            ps.setString(5, "Jones");
            ps.setString(6, null);
            ps.executeUpdate();
            ps.clearParameters();

            ps.setInt(1, 3);
            ps.setString(2, null);
            ps.setBoolean(3, true);
            ps.setString(4, null);
            ps.setString(5, null);
            ps.setString(6, "12345");
            ps.executeUpdate();
            ps.clearParameters();

            ps.setInt(1, 9);
            ps.setString(2, "hello");
            ps.setBoolean(3, true);
            ps.setString(4, null);
            ps.setString(5, null);
            ps.setString(6, null);
            ps.executeUpdate();
        }

        DerbyDatabaseMetaData metaData = new DerbyDatabaseMetaData();
        metaData.setDataSource(dataSource);

        //--Act
        UserExpression user = new UserExpression();
        List<Integer> actual = metaData.getQueryBuilder()
                .from(user)
                .where(user.getId().greaterThan(1))
                .aggregate(user.getId())
                .as(
                        new ColumnAggregator1<Integer, Integer>() {
                            @Override
                            public Integer aggregate(Integer id) {
                                return id;
                            }
                        })
                .list();

        //--Assert
        assertThat(actual, contains(2, 3, 9));
    }

    @Test
    public void test_that_insert_works_for_manually_assigned_primary_keys() {
        //--Arrange
        User johnDoe = new User();
        johnDoe.setId(10);
        johnDoe.setActive(true);
        johnDoe.setUserName("test");
        johnDoe.setContactInformation(new ContactInformation());
        johnDoe.getContactInformation().setName(new Name());
        johnDoe.getContactInformation().getName().setFirstName("John");
        johnDoe.getContactInformation().getName().setLastName("Doe");
        johnDoe.getContactInformation().setCountryCode("US");

        //--Act
        johnDoe.save();

        //--Assert
        UserExpression user = new UserExpression();
        User theUser = metaData.getQueryBuilder()
                .from(user)
                .where(user.getId().equalTo(10))
                .single();
        assertThat(theUser, is(not((User) null)));
    }

    @Test
    public void test_that_insert_works_for_auto_generated_primary_keys() {
        //--Arrange
        Person janeSmith = new Person();
        janeSmith.setFullName("Jane Smith");

        //--Act
        janeSmith.save();

        //--Assert
        PersonExpression person = new PersonExpression();
        Person actual = metaData.getQueryBuilder()
                .from(person)
                .where(person.getId().equalTo(1))
                .single();
        assertThat(actual.getFullName(), is(janeSmith.getFullName()));
    }
}
