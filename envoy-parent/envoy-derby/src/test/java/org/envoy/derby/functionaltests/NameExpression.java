package org.envoy.derby.functionaltests;

import org.envoy.expression.*;

public class NameExpression extends AbstractComponentExpression<Name> {
    private final StringFieldExpression firstName = new StringFieldExpression(this);
    private final StringFieldExpression lastName = new StringFieldExpression(this);


    public NameExpression(ModelExpression parent) {
        super(parent, Name.class);
    }


    public StringExpression getFirstNameLastName() {
        return firstName.concat(" ").concat(lastName);
    }


    public StringExpression getLastNameFirstName() {
        return lastName.concat(", ").concat(firstName);
    }


    public StringFieldExpression getFirstName() {
        return firstName;
    }


    public StringFieldExpression getLastName() {
        return lastName;
    }
}
