package org.envoy.derby.functionaltests;

import org.envoy.expression.*;

/**
 * Created by thomas on 9/25/14.
 */
public class PersonExpression extends AbstractEntityExpression<Person> {
    private IntegerFieldExpression id = new IntegerFieldExpression(this, "ID").primaryKey().autoIncrement();
    private StringFieldExpression fullName = new StringFieldExpression(this, "FullName");

    public PersonExpression() {
        super(Person.class);
        setTableName("People");
    }

    public IntegerFieldExpression getId() {
        return id;
    }

    public StringFieldExpression getFullName() {
        return fullName;
    }
}
