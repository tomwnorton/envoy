package org.envoy.derby.functionaltests;

import org.envoy.*;

import java.io.*;

public class ContactInformation implements Serializable {
    private static final long serialVersionUID = 2499165165907992142L;

    private Property<Name> name = new Property<>();
    private Property<String> addressLine1 = new Property<>();
    private Property<String> addressLine2 = new Property<>();
    private Property<String> city = new Property<>();
    private Property<String> state = new Property<>();
    private Property<String> postalCode = new Property<>();
    private Property<String> countryCode = new Property<>();


    public Name getName() {
        return name.getValue();
    }


    public void setName(final Name name) {
        this.name.setValue(name);
    }


    public String getAddressLine1() {
        return addressLine1.getValue();
    }


    public void setAddressLine1(final String addressLine1) {
        this.addressLine1.setValue(addressLine1);
    }


    public String getAddressLine2() {
        return addressLine2.getValue();
    }


    public void setAddressLine2(final String addressLine2) {
        this.addressLine2.setValue(addressLine2);
    }


    public String getCity() {
        return city.getValue();
    }


    public void setCity(final String city) {
        this.city.setValue(city);
    }


    public String getState() {
        return state.getValue();
    }


    public void setState(final String state) {
        this.state.setValue(state);
    }


    public String getPostalCode() {
        return postalCode.getValue();
    }


    public void setPostalCode(final String postalCode) {
        this.postalCode.setValue(postalCode);
    }


    public String getCountryCode() {
        return countryCode.getValue();
    }


    public void setCountryCode(final String countryCode) {
        this.countryCode.setValue(countryCode);
    }
}
