package org.envoy.derby.functionaltests;

import org.envoy.*;
import org.envoy.expression.*;

/**
 * Created by thomas on 7/20/14.
 */
public abstract class AbstractAddressableEntity<EntityType extends AbstractAddressableEntity<EntityType>> extends
        AbstractEntity<EntityType> {
    private Property<ContactInformation> contactInformation = new Property<>();

    public AbstractAddressableEntity(
            Class<? extends EntityExpression<EntityType>> expressionClass,
            DatabaseMetaData<? extends AutoCloseable> metaData) {
        super(expressionClass, metaData);
    }


    public ContactInformation getContactInformation() {
        return contactInformation.getValue();
    }


    public void setContactInformation(ContactInformation contactInformation) {
        this.contactInformation.setValue(contactInformation);
    }
}
