package org.envoy.derby.functionaltests;

import org.envoy.*;

/**
 * Created by thomas on 9/25/14.
 */
public class Person extends AbstractEntity<Person> {
    private Integer id;
    private String fullName;

    public Person() {
        super(PersonExpression.class, DerbyIntegrationTests.metaData);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
