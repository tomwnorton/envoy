package org.envoy.derby.functionaltests;

import org.envoy.*;

import java.io.*;

/**
 * Created with IntelliJ IDEA. User: thomas Date: 1/9/13 Time: 6:51 PM To change this template use File | Settings |
 * File Templates.
 */
public class User extends AbstractAddressableEntity implements Serializable {
    private static final long serialVersionUID = -7184510810951442407L;

    private Property<Integer> id = new Property<>();
    private Property<String> userName = new Property<>();
    private Property<Boolean> active = new Property<>();


    public User() {
        super(UserExpression.class, DerbyIntegrationTests.metaData);
    }


    public Integer getId() {
        return id.getValue();
    }


    public void setId(final Integer id) {
        this.id.setValue(id);
    }


    public String getUserName() {
        return userName.getValue();
    }


    public void setUserName(final String userName) {
        this.userName.setValue(userName);
    }


    public Boolean isActive() {
        return active.getValue();
    }


    public void setActive(final Boolean active) {
        this.active.setValue(active);
    }
}
