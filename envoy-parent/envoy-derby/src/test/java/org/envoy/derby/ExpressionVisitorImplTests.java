package org.envoy.derby;

import org.apache.commons.lang3.tuple.*;
import org.envoy.*;
import org.envoy.expression.*;
import org.envoy.query.*;
import org.junit.*;
import org.mockito.*;
import org.mockito.invocation.*;
import org.mockito.stubbing.*;

import java.io.*;
import java.util.*;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

/**
 * Created by thomas on 7/21/14.
 */
public class ExpressionVisitorImplTests {
    @Mock
    private SqlDatabaseMetaData metaData;

    @Mock
    private FieldAliasCache fieldAliasCache;

    private ExpressionVisitorImpl underTest;


    private static Stubber doAccept(final String sql) {
        return doAnswer(
                new Answer<Void>() {
                    @Override
                    public Void answer(final InvocationOnMock invocation) throws Throwable {
                        final ExpressionVisitorImpl visitor = (ExpressionVisitorImpl) invocation.getArguments()[0];
                        visitor.getQuery().append(sql);
                        return null;
                    }
                });
    }


    private static Stubber doParameter(final Object parameter) {
        return doAnswer(
                new Answer<Void>() {
                    @Override
                    public Void answer(final InvocationOnMock invocation) throws Throwable {
                        final ExpressionVisitorImpl visitor = (ExpressionVisitorImpl) invocation.getArguments()[0];
                        visitor.visitParameter(parameter);
                        return null;
                    }
                });
    }


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        underTest = new ExpressionVisitorImpl(metaData);
        underTest.setParameters(new ArrayList<>());
        underTest.setQuery(new StringBuilder());
        underTest.setFieldAliasCache(fieldAliasCache);
    }


    @Test
    public void test_that_visitEquals_generates_the_correct_sql_for_the_equals_expression_a_equals_b() {
        //--Arrange
        final Expression lhs = Mockito.mock(Expression.class);
        doAccept("a").when(lhs).accept(underTest);

        final Expression rhs = Mockito.mock(Expression.class);
        doAccept("b").when(rhs).accept(underTest);

        //--Act
        underTest.visitEquals(lhs, rhs);

        //--Assert
        Assert.assertEquals("a = (b)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitEquals_generates_the_correct_sql_for_the_equals_expression_x_equals_y() {
        //--Arrange
        final Expression lhs = Mockito.mock(Expression.class);
        doAccept("x").when(lhs).accept(underTest);

        final Expression rhs = Mockito.mock(Expression.class);
        doAccept("y").when(rhs).accept(underTest);

        //--Act
        underTest.visitEquals(lhs, rhs);

        //--Assert
        Assert.assertEquals("x = (y)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitNotEquals_generates_the_correct_sql_for_the_not_equals_expression_a_not_equals_b() {
        //--Arrange
        final Expression lhs = Mockito.mock(Expression.class);
        doAccept("a").when(lhs).accept(underTest);

        final Expression rhs = Mockito.mock(Expression.class);
        doAccept("b").when(rhs).accept(underTest);

        //--Act
        underTest.visitNotEquals(lhs, rhs);

        //--Assert
        Assert.assertEquals("a <> (b)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitNotEquals_generates_the_correct_sql_for_the_not_equals_expression_x_not_equals_y() {
        //--Arrange
        final Expression lhs = Mockito.mock(Expression.class);
        doAccept("x").when(lhs).accept(underTest);

        final Expression rhs = Mockito.mock(Expression.class);
        doAccept("y").when(rhs).accept(underTest);

        //--Act
        underTest.visitNotEquals(lhs, rhs);

        //--Assert
        Assert.assertEquals("x <> (y)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitAnd_generates_the_correct_sql_for_the_and_expression_a_and_b() {
        //--Arrange
        final BooleanExpression lhs = Mockito.mock(BooleanExpression.class);
        doAccept("a").when(lhs).accept(underTest);

        final BooleanExpression rhs = Mockito.mock(BooleanExpression.class);
        doAccept("b").when(rhs).accept(underTest);

        //--Act
        underTest.visitAnd(lhs, rhs);

        //--Assert
        Assert.assertEquals("a AND (b)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitAnd_generates_the_correct_sql_for_the_and_expression_x_and_y() {
        //--Arrange
        final BooleanExpression lhs = Mockito.mock(BooleanExpression.class);
        doAccept("x").when(lhs).accept(underTest);

        final BooleanExpression rhs = Mockito.mock(BooleanExpression.class);
        doAccept("y").when(rhs).accept(underTest);

        //--Act
        underTest.visitAnd(lhs, rhs);

        //--Assert
        Assert.assertEquals("x AND (y)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitOr_generates_the_correct_sql_for_the_or_expression_a_or_b() {
        //--Arrange
        final BooleanExpression lhs = Mockito.mock(BooleanExpression.class);
        doAccept("a").when(lhs).accept(underTest);

        final BooleanExpression rhs = Mockito.mock(BooleanExpression.class);
        doAccept("b").when(rhs).accept(underTest);

        //--Act
        underTest.visitOr(lhs, rhs);

        //--Assert
        Assert.assertEquals("a OR (b)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitOr_generates_the_correct_sql_for_the_or_expression_x_or_y() {
        //--Arrange
        final BooleanExpression lhs = Mockito.mock(BooleanExpression.class);
        doAccept("x").when(lhs).accept(underTest);

        final BooleanExpression rhs = Mockito.mock(BooleanExpression.class);
        doAccept("y").when(rhs).accept(underTest);

        //--Act
        underTest.visitOr(lhs, rhs);

        //--Assert
        Assert.assertEquals("x OR (y)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitOr_generates_the_correct_sql_for_not_a() {
        //--Arrange
        final BooleanExpression lhs = Mockito.mock(BooleanExpression.class);
        doAccept("a").when(lhs).accept(underTest);

        //--Act
        underTest.visitNot(lhs);

        //--Assert
        Assert.assertEquals("NOT (a)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitOr_generates_the_correct_sql_for_not_b() {
        //--Arrange
        final BooleanExpression lhs = Mockito.mock(BooleanExpression.class);
        doAccept("b").when(lhs).accept(underTest);

        //--Act
        underTest.visitNot(lhs);

        //--Assert
        Assert.assertEquals("NOT (b)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitIn_generates_the_correct_sql_for_id_in_1_2_17_6() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        doAccept("id").when(expr).accept(underTest);

        //--Act
        underTest.visitIn(expr, Arrays.asList(1, 2, 17, 6));

        //--Assert
        Assert.assertEquals("id IN (?, ?, ?, ?)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitIn_generates_the_parameters_for_id_in_1_2_17_6() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        doAccept("id").when(expr).accept(underTest);

        //--Act
        underTest.visitIn(expr, Arrays.asList(1, 2, 17, 6));

        //--Assert
        Assert.assertEquals(Arrays.<Serializable>asList(1, 2, 17, 6), underTest.getParameters());
    }


    @Test
    public void test_that_visitIn_generates_the_correct_sql_for_status_in_on_off() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        doAccept("status").when(expr).accept(underTest);

        //--Act
        underTest.visitIn(expr, Arrays.asList("on", "off"));

        //--Assert
        Assert.assertEquals("status IN (?, ?)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitIn_generates_the_correct_parameters_for_status_in_on_off() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        doAccept("status").when(expr).accept(underTest);

        //--Act
        underTest.visitIn(expr, Arrays.asList("on", "off"));

        //--Assert
        Assert.assertEquals(Arrays.<Serializable>asList("on", "off"), underTest.getParameters());
    }


    @Test
    public void test_that_visitParameter_generates_the_question_mark_sql() {
        //--Act
        underTest.visitParameter(7);

        //--Assert
        Assert.assertEquals("?", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitParameter_generates_the_correct_parameter_for_7() {
        //--Act
        underTest.visitParameter(7);

        //--Assert
        Assert.assertEquals(Arrays.<Serializable>asList(7), underTest.getParameters());
    }


    @Test
    public void test_that_visitParameter_generates_the_correct_parameter_for_hello() {
        //--Act
        underTest.visitParameter("hello");

        //--Assert
        Assert.assertEquals(Arrays.<Serializable>asList("hello"), underTest.getParameters());
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_visitField_generates_the_correct_string_for_table_alias_BOX_and_field_name_ISCUBE() {
        //--Arrange
        final GenericFieldExpression<Serializable> fieldExpr = mock(GenericFieldExpression.class);
        final EntityExpression<Object> entityExpr = mock(EntityExpression.class);
        when((EntityExpression<Object>) fieldExpr.getEntityExpression()).thenReturn(entityExpr);
        when(entityExpr.getTableName()).thenReturn("BOX");
        when(fieldAliasCache.getFieldAlias(same(entityExpr))).thenReturn("BOX_3");
        when(fieldExpr.getFieldName()).thenReturn("ISCUBE");

        when(metaData.getEscapedObjectName("BOX_3")).thenReturn("\"BOX_3\"");
        when(metaData.getEscapedObjectName("ISCUBE")).thenReturn("\"ISCUBE\"");

        underTest.visitField(fieldExpr);

        //--Assert
        Assert.assertEquals("\"BOX_3\".\"ISCUBE\"", underTest.getQuery().toString());
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_visitField_generates_the_correct_string_for_table_alias_ORDER_and_field_name_ACTIVE() {
        //--Arrange
        final GenericFieldExpression<Serializable> fieldExpr = mock(GenericFieldExpression.class);
        final EntityExpression<Object> entityExpr = mock(EntityExpression.class);
        when((EntityExpression<Object>) fieldExpr.getEntityExpression()).thenReturn(entityExpr);
        when(entityExpr.getTableName()).thenReturn("ORDER");
        when(fieldAliasCache.getFieldAlias(same(entityExpr))).thenReturn("ORDER_10");
        when(fieldExpr.getFieldName()).thenReturn("ACTIVE");

        when(metaData.getEscapedObjectName("ORDER_10")).thenReturn("[ORDER_10]");
        when(metaData.getEscapedObjectName("ACTIVE")).thenReturn("[ACTIVE]");

        underTest.visitField(fieldExpr);

        //--Assert
        Assert.assertEquals("[ORDER_10].[ACTIVE]", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitNull_generates_the_correct_string_for_the_expression_hello() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        doAccept("hello").when(expr).accept(underTest);

        //--Act
        underTest.visitNull(expr);

        //--Assert
        Assert.assertEquals("hello IS NULL", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitNull_generates_the_correct_string_for_the_expression_world() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        doAccept("world").when(expr).accept(underTest);

        //--Act
        underTest.visitNull(expr);

        //--Assert
        Assert.assertEquals("world IS NULL", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitNotNull_generates_the_correct_string_for_the_expression_hello() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        doAccept("hello").when(expr).accept(underTest);

        //--Act
        underTest.visitNotNull(expr);

        //--Assert
        Assert.assertEquals("hello IS NOT NULL", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitNotNull_generates_the_correct_string_for_the_expression_world() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        doAccept("world").when(expr).accept(underTest);

        //--Act
        underTest.visitNotNull(expr);

        //--Assert
        Assert.assertEquals("world IS NOT NULL", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitTrue_generates_the_sql_for_TRUE() {
        //--Act
        underTest.visitTrue();

        //--Assert
        Assert.assertEquals("1", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitFalse_generates_the_sql_for_FALSE() {
        //--Act
        underTest.visitFalse();

        //--Assert
        Assert.assertEquals("0", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitLessThan_generates_the_sql_for_2_less_than_3() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);

        doAccept("2").when(lhs).accept(underTest);
        doAccept("3").when(rhs).accept(underTest);

        //--Act
        underTest.visitLessThan(lhs, rhs);

        //--Assert
        Assert.assertEquals("2 < (3)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitLessThan_generates_the_sql_for_5_less_than_9() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);

        doAccept("5").when(lhs).accept(underTest);
        doAccept("9").when(rhs).accept(underTest);

        //--Act
        underTest.visitLessThan(lhs, rhs);

        //--Assert
        Assert.assertEquals("5 < (9)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitLessThanEqual_generates_the_sql_5_less_than_equal_9() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);

        doAccept("5").when(lhs).accept(underTest);
        doAccept("9").when(rhs).accept(underTest);

        //--Act
        underTest.visitLessThanEqual(lhs, rhs);

        //--Assert
        Assert.assertEquals("5 <= (9)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitLessThanEqual_generates_the_sql_1_less_than_equal_2() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);

        doAccept("1").when(lhs).accept(underTest);
        doAccept("2").when(rhs).accept(underTest);

        //--Act
        underTest.visitLessThanEqual(lhs, rhs);

        //--Assert
        Assert.assertEquals("1 <= (2)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitGreaterThan_generates_the_sql_for_2_less_than_3() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);

        doAccept("2").when(lhs).accept(underTest);
        doAccept("3").when(rhs).accept(underTest);

        //--Act
        underTest.visitGreaterThan(lhs, rhs);

        //--Assert
        Assert.assertEquals("2 > (3)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitGreaterThan_generates_the_sql_for_5_less_than_9() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);

        doAccept("5").when(lhs).accept(underTest);
        doAccept("9").when(rhs).accept(underTest);

        //--Act
        underTest.visitGreaterThan(lhs, rhs);

        //--Assert
        Assert.assertEquals("5 > (9)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitGreaterThanEqual_generates_the_sql_for_2_less_than_3() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);

        doAccept("2").when(lhs).accept(underTest);
        doAccept("3").when(rhs).accept(underTest);

        //--Act
        underTest.visitGreaterThanEqual(lhs, rhs);

        //--Assert
        Assert.assertEquals("2 >= (3)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitGreaterThanEqual_generates_the_sql_for_5_less_than_9() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);

        doAccept("5").when(lhs).accept(underTest);
        doAccept("9").when(rhs).accept(underTest);

        //--Act
        underTest.visitGreaterThanEqual(lhs, rhs);

        //--Assert
        Assert.assertEquals("5 >= (9)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitConvertToString_writes_the_sql_to_convert_the_float_column_cost_to_a_string() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        doAccept("cost").when(expr).accept(underTest);

        //--Act
        underTest.visitConvertToString(expr);

        //--Assert
        Assert.assertEquals("STR(cost)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitConvertToString_writes_the_sql_to_convert_the_int_column_quantity_to_a_string() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        doAccept("quantity").when(expr).accept(underTest);

        //--Act
        underTest.visitConvertToString(expr);

        //--Assert
        Assert.assertEquals("STR(quantity)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitPlus_generates_the_sql_for_2_less_than_3() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);

        doAccept("2").when(lhs).accept(underTest);
        doAccept("3").when(rhs).accept(underTest);

        //--Act
        underTest.visitPlus(lhs, rhs);

        //--Assert
        Assert.assertEquals("2 + (3)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitPlus_generates_the_sql_for_5_less_than_9() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);

        doAccept("5").when(lhs).accept(underTest);
        doAccept("9").when(rhs).accept(underTest);

        //--Act
        underTest.visitPlus(lhs, rhs);

        //--Assert
        Assert.assertEquals("5 + (9)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitMinus_generates_the_sql_for_2_less_than_3() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);

        doAccept("2").when(lhs).accept(underTest);
        doAccept("3").when(rhs).accept(underTest);

        //--Act
        underTest.visitMinus(lhs, rhs);

        //--Assert
        Assert.assertEquals("2 - (3)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitMinus_generates_the_sql_for_5_less_than_9() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);

        doAccept("5").when(lhs).accept(underTest);
        doAccept("9").when(rhs).accept(underTest);

        //--Act
        underTest.visitMinus(lhs, rhs);

        //--Assert
        Assert.assertEquals("5 - (9)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitTimes_generates_the_sql_for_2_less_than_3() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);

        doAccept("2").when(lhs).accept(underTest);
        doAccept("3").when(rhs).accept(underTest);

        //--Act
        underTest.visitTimes(lhs, rhs);

        //--Assert
        Assert.assertEquals("2 * (3)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitTimes_generates_the_sql_for_5_less_than_9() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);

        doAccept("5").when(lhs).accept(underTest);
        doAccept("9").when(rhs).accept(underTest);

        //--Act
        underTest.visitTimes(lhs, rhs);

        //--Assert
        Assert.assertEquals("5 * (9)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitDividedBy_generates_the_sql_for_2_less_than_3() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);

        doAccept("2").when(lhs).accept(underTest);
        doAccept("3").when(rhs).accept(underTest);

        //--Act
        underTest.visitDividedBy(lhs, rhs);

        //--Assert
        Assert.assertEquals("2 / (3)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitDividedBy_generates_the_sql_for_5_less_than_9() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);

        doAccept("5").when(lhs).accept(underTest);
        doAccept("9").when(rhs).accept(underTest);

        //--Act
        underTest.visitDividedBy(lhs, rhs);

        //--Assert
        Assert.assertEquals("5 / (9)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitModulus_generates_the_sql_for_2_less_than_3() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);

        doAccept("2").when(lhs).accept(underTest);
        doAccept("3").when(rhs).accept(underTest);

        //--Act
        underTest.visitModulus(lhs, rhs);

        //--Assert
        Assert.assertEquals("2 MOD (3)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitModulus_generates_the_sql_for_5_less_than_9() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);

        doAccept("5").when(lhs).accept(underTest);
        doAccept("9").when(rhs).accept(underTest);

        //--Act
        underTest.visitModulus(lhs, rhs);

        //--Assert
        Assert.assertEquals("5 MOD (9)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitCoalesce_generates_the_sql_for_coalesce_first_second_third() {
        //--Arrange
        final Expression firstExpr = mock(Expression.class);
        final Expression secondExpr = mock(Expression.class);
        final Expression thirdExpr = mock(Expression.class);

        doAccept("first").when(firstExpr).accept(underTest);
        doAccept("second").when(secondExpr).accept(underTest);
        doAccept("third").when(thirdExpr).accept(underTest);

        //--Act
        underTest.visitCoalesce(Arrays.<Expression>asList(firstExpr, secondExpr, thirdExpr));

        //--Assert
        Assert.assertEquals("COALESCE(first, second, third)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitCoalesce_generates_the_sql_for_coalesce_id_97() {
        //--Arrange
        final Expression firstExpr = mock(Expression.class);
        final Expression secondExpr = mock(Expression.class);

        doAccept("id").when(firstExpr).accept(underTest);
        doAccept("97").when(secondExpr).accept(underTest);

        //--Act
        underTest.visitCoalesce(Arrays.<Expression>asList(firstExpr, secondExpr));

        //--Assert
        Assert.assertEquals("COALESCE(id, 97)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitNegative_generates_the_sql_for_negative_7() {
        //--Arrange
        final Expression operand = mock(Expression.class);
        doAccept("7").when(operand).accept(underTest);

        //--Act
        underTest.visitNegative(operand);

        //--Assert
        Assert.assertEquals("-7", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitNegative_generates_the_sql_for_negative_82() {
        //--Arrange
        final Expression operand = mock(Expression.class);
        doAccept("82").when(operand).accept(underTest);

        //--Act
        underTest.visitNegative(operand);

        //--Assert
        Assert.assertEquals("-82", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitBetween_generates_the_sql_for_x_between_96_and_103() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression lowerBound = mock(Expression.class);
        final Expression upperBound = mock(Expression.class);

        doAccept("x").when(lhs).accept(underTest);
        doAccept("96").when(lowerBound).accept(underTest);
        doAccept("103").when(upperBound).accept(underTest);

        //--Act
        underTest.visitBetween(lhs, lowerBound, upperBound);

        //--Assert
        Assert.assertEquals("x BETWEEN 96 AND 103", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitBetween_generates_the_sql_for_y_between_2_and_5() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression lowerBound = mock(Expression.class);
        final Expression upperBound = mock(Expression.class);

        doAccept("y").when(lhs).accept(underTest);
        doAccept("2").when(lowerBound).accept(underTest);
        doAccept("5").when(upperBound).accept(underTest);

        //--Act
        underTest.visitBetween(lhs, lowerBound, upperBound);

        //--Assert
        Assert.assertEquals("y BETWEEN 2 AND 5", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitTrim_returns_LTRIM_RTRIM_hello_for_the_expression_hello() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        doAccept("hello").when(expr).accept(underTest);

        //--Act
        underTest.visitTrim(expr);

        //--Assert
        Assert.assertEquals("LTRIM(RTRIM(hello))", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitTrim_returns_LTRIM_RTRIM_world_for_the_expression_world() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        doAccept("world").when(expr).accept(underTest);

        //--Act
        underTest.visitTrim(expr);

        //--Assert
        Assert.assertEquals("LTRIM(RTRIM(world))", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitLength_returns_LEN_hello_for_the_expression_hello() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        doAccept("hello").when(expr).accept(underTest);

        //--Act
        underTest.visitLength(expr);

        //--Assert
        Assert.assertEquals("LEN(hello)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitLength_returns_LEN_world_for_the_expression_world() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        doAccept("world").when(expr).accept(underTest);

        //--Act
        underTest.visitLength(expr);

        //--Assert
        Assert.assertEquals("LEN(world)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitLowerCase_returns_LOWER_hello_for_the_expression_hello() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        doAccept("hello").when(expr).accept(underTest);

        //--Act
        underTest.visitLowerCase(expr);

        //--Assert
        Assert.assertEquals("LOWER(hello)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitLowerCase_returns_LOWER_world_for_the_expression_world() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        doAccept("world").when(expr).accept(underTest);

        //--Act
        underTest.visitLowerCase(expr);

        //--Assert
        Assert.assertEquals("LOWER(world)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitUpperCase_returns_Upper_hello_for_the_expression_hello() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        doAccept("hello").when(expr).accept(underTest);

        //--Act
        underTest.visitUpperCase(expr);

        //--Assert
        Assert.assertEquals("UPPER(hello)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitUpperCase_returns_Upper_world_for_the_expression_world() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        doAccept("world").when(expr).accept(underTest);

        //--Act
        underTest.visitUpperCase(expr);

        //--Assert
        Assert.assertEquals("UPPER(world)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitConvertStringToInteger_returns_CAST_hello_AS_BIGINT_for_the_expression_hello() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        doAccept("hello").when(expr).accept(underTest);

        //--Act
        underTest.visitConvertStringToInteger(expr);

        //--Assert
        Assert.assertEquals("CAST(hello AS BIGINT)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitConvertStringToInteger_returns_CAST_world_AS_BIGINT_for_the_expression_world() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        doAccept("world").when(expr).accept(underTest);

        //--Act
        underTest.visitConvertStringToInteger(expr);

        //--Assert
        Assert.assertEquals("CAST(world AS BIGINT)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitConcat_returns_hello_plus_world_for_the_operands_hello_and_world() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        doAccept("hello").when(lhs).accept(underTest);

        final Expression rhs = mock(Expression.class);
        doAccept("world").when(rhs).accept(underTest);

        //--Act
        underTest.visitConcat(lhs, rhs);

        //--Assert
        Assert.assertEquals("hello || world", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitConcat_returns_good_plus_bad_for_the_operands_good_and_bad() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        doAccept("good").when(lhs).accept(underTest);

        final Expression rhs = mock(Expression.class);
        doAccept("bad").when(rhs).accept(underTest);

        //--Act
        underTest.visitConcat(lhs, rhs);

        //--Assert
        Assert.assertEquals("good || bad", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitContains_writes_the_correct_like_expression_for_good_contains_bad_with_bad_properly_escaped() {
        //--Arrange
        final Expression source = mock(Expression.class);
        doAccept("good").when(source).accept(underTest);

        final Expression pattern = mock(Expression.class);
        doAccept("bad").when(pattern).accept(underTest);

        //--Act
        underTest.visitContains(source, pattern);

        //--Assert
        Assert.assertEquals(
                "good LIKE '%' || REPLACE(REPLACE(REPLACE(bad, '\\', '\\\\'), '%', '\\%'), '_', '\\_') || '%'",
                underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitContains_writes_the_correct_like_expression_for_hello_contains_world_with_world_properly_escaped() {
        //--Arrange
        final Expression source = mock(Expression.class);
        doAccept("hello").when(source).accept(underTest);

        final Expression pattern = mock(Expression.class);
        doAccept("world").when(pattern).accept(underTest);

        //--Act
        underTest.visitContains(source, pattern);

        //--Assert
        Assert.assertEquals(
                "hello LIKE '%' || REPLACE(REPLACE(REPLACE(world, '\\', '\\\\'), '%', '\\%'), '_', '\\_') || '%'",
                underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitContains_writes_the_correct_like_expression_for_hello_contains_parameter_with_the_parameter_not_escaped() {
        //--Arrange
        final Expression source = mock(Expression.class);
        doAccept("hello").when(source).accept(underTest);

        final Expression pattern = mock(Expression.class);
        doParameter("world").when(pattern).accept(underTest);

        //--Act
        underTest.visitContains(source, pattern);

        //--Assert
        Assert.assertEquals("hello LIKE ?", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitContains_writes_the_correct_like_expression_for_world_contains_parameter_with_the_parameter_not_escaped() {
        //--Arrange
        final Expression source = mock(Expression.class);
        doAccept("world").when(source).accept(underTest);

        final Expression pattern = mock(Expression.class);
        doParameter("world").when(pattern).accept(underTest);

        //--Act
        underTest.visitContains(source, pattern);

        //--Assert
        Assert.assertEquals("world LIKE ?", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitContains_preserves_any_expressions_prior_to_the_like_clause() {
        //--Arrange
        final Expression source = mock(Expression.class);
        doAccept("hello").when(source).accept(underTest);

        final Expression pattern = mock(Expression.class);
        doParameter("world").when(pattern).accept(underTest);

        underTest.getQuery().append("infinity ");

        //--Act
        underTest.visitContains(source, pattern);

        //--Assert
        Assert.assertEquals("infinity hello LIKE ?", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitContains_writes_the_correct_like_expression_for_hello_contains_parameter_when_parameter_has_a_backslash_in_it() {
        //--Arrange
        final Expression source = mock(Expression.class);
        doAccept("hello").when(source).accept(underTest);

        final Expression pattern = mock(Expression.class);
        doParameter("something\\cool").when(pattern).accept(underTest);

        //--Act
        underTest.visitContains(source, pattern);

        //--Assert
        Assert.assertEquals("%something\\\\cool%", underTest.getParameters().get(0));
    }


    @Test
    public void test_that_visitContains_writes_the_correct_like_expression_for_hello_contains_parameter_when_parameter_has_a_percent_in_it() {
        //--Arrange
        final Expression source = mock(Expression.class);
        doAccept("hello").when(source).accept(underTest);

        final Expression pattern = mock(Expression.class);
        doParameter("97%").when(pattern).accept(underTest);

        //--Act
        underTest.visitContains(source, pattern);

        //--Assert
        Assert.assertEquals("%97\\%%", underTest.getParameters().get(0));
    }


    @Test
    public void test_that_visitContains_writes_the_correct_like_expression_for_hello_contains_parameter_when_parameter_has_an_underscore_in_it() {
        //--Arrange
        final Expression source = mock(Expression.class);
        doAccept("hello").when(source).accept(underTest);

        final Expression pattern = mock(Expression.class);
        doParameter("good_bad_ugly").when(pattern).accept(underTest);

        //--Act
        underTest.visitContains(source, pattern);

        //--Assert
        Assert.assertEquals("%good\\_bad\\_ugly%", underTest.getParameters().get(0));
    }


    @Test
    public void test_that_visitStartsWith_writes_the_correct_like_expression_for_good_contains_bad_with_bad_properly_escaped() {
        //--Arrange
        final Expression source = mock(Expression.class);
        doAccept("good").when(source).accept(underTest);

        final Expression pattern = mock(Expression.class);
        doAccept("bad").when(pattern).accept(underTest);

        //--Act
        underTest.visitStartsWith(source, pattern);

        //--Assert
        Assert.assertEquals(
                "good LIKE REPLACE(REPLACE(REPLACE(bad, '\\', '\\\\'), '%', '\\%'), '_', '\\_') || '%'",
                underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitStartsWith_writes_the_correct_like_expression_for_hello_contains_world_with_world_properly_escaped() {
        //--Arrange
        final Expression source = mock(Expression.class);
        doAccept("hello").when(source).accept(underTest);

        final Expression pattern = mock(Expression.class);
        doAccept("world").when(pattern).accept(underTest);

        //--Act
        underTest.visitStartsWith(source, pattern);

        //--Assert
        Assert.assertEquals(
                "hello LIKE REPLACE(REPLACE(REPLACE(world, '\\', '\\\\'), '%', '\\%'), '_', '\\_') || '%'",
                underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitStartsWith_writes_the_correct_like_expression_for_hello_contains_parameter_with_the_parameter_not_escaped() {
        //--Arrange
        final Expression source = mock(Expression.class);
        doAccept("hello").when(source).accept(underTest);

        final Expression pattern = mock(Expression.class);
        doParameter("world").when(pattern).accept(underTest);

        //--Act
        underTest.visitStartsWith(source, pattern);

        //--Assert
        Assert.assertEquals("hello LIKE ?", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitStartsWith_writes_the_correct_like_expression_for_world_contains_parameter_with_the_parameter_not_escaped() {
        //--Arrange
        final Expression source = mock(Expression.class);
        doAccept("world").when(source).accept(underTest);

        final Expression pattern = mock(Expression.class);
        doParameter("world").when(pattern).accept(underTest);

        //--Act
        underTest.visitStartsWith(source, pattern);

        //--Assert
        Assert.assertEquals("world LIKE ?", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitStartsWith_preserves_any_expressions_prior_to_the_like_clause() {
        //--Arrange
        final Expression source = mock(Expression.class);
        doAccept("hello").when(source).accept(underTest);

        final Expression pattern = mock(Expression.class);
        doParameter("world").when(pattern).accept(underTest);

        underTest.getQuery().append("infinity ");

        //--Act
        underTest.visitStartsWith(source, pattern);

        //--Assert
        Assert.assertEquals("infinity hello LIKE ?", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitStartsWith_writes_the_correct_like_expression_for_hello_contains_parameter_when_parameter_has_a_backslash_in_it() {
        //--Arrange
        final Expression source = mock(Expression.class);
        doAccept("hello").when(source).accept(underTest);

        final Expression pattern = mock(Expression.class);
        doParameter("something\\cool").when(pattern).accept(underTest);

        //--Act
        underTest.visitStartsWith(source, pattern);

        //--Assert
        Assert.assertEquals("something\\\\cool%", underTest.getParameters().get(0));
    }


    @Test
    public void test_that_visitStartsWith_writes_the_correct_like_expression_for_hello_contains_parameter_when_parameter_has_a_percent_in_it() {
        //--Arrange
        final Expression source = mock(Expression.class);
        doAccept("hello").when(source).accept(underTest);

        final Expression pattern = mock(Expression.class);
        doParameter("97%").when(pattern).accept(underTest);

        //--Act
        underTest.visitStartsWith(source, pattern);

        //--Assert
        Assert.assertEquals("97\\%%", underTest.getParameters().get(0));
    }


    @Test
    public void test_that_visitStartsWith_writes_the_correct_like_expression_for_hello_contains_parameter_when_parameter_has_an_underscore_in_it() {
        //--Arrange
        final Expression source = mock(Expression.class);
        doAccept("hello").when(source).accept(underTest);

        final Expression pattern = mock(Expression.class);
        doParameter("good_bad_ugly").when(pattern).accept(underTest);

        //--Act
        underTest.visitStartsWith(source, pattern);

        //--Assert
        Assert.assertEquals("good\\_bad\\_ugly%", underTest.getParameters().get(0));
    }


    @Test
    public void test_that_visitEndsWith_writes_the_correct_like_expression_for_good_contains_bad_with_bad_properly_escaped() {
        //--Arrange
        final Expression source = mock(Expression.class);
        doAccept("good").when(source).accept(underTest);

        final Expression pattern = mock(Expression.class);
        doAccept("bad").when(pattern).accept(underTest);

        //--Act
        underTest.visitEndsWith(source, pattern);

        //--Assert
        Assert.assertEquals(
                "good LIKE '%' || REPLACE(REPLACE(REPLACE(bad, '\\', '\\\\'), '%', '\\%'), '_', '\\_')",
                underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitEndsWith_writes_the_correct_like_expression_for_hello_contains_world_with_world_properly_escaped() {
        //--Arrange
        final Expression source = mock(Expression.class);
        doAccept("hello").when(source).accept(underTest);

        final Expression pattern = mock(Expression.class);
        doAccept("world").when(pattern).accept(underTest);

        //--Act
        underTest.visitEndsWith(source, pattern);

        //--Assert
        Assert.assertEquals(
                "hello LIKE '%' || REPLACE(REPLACE(REPLACE(world, '\\', '\\\\'), '%', '\\%'), '_', '\\_')",
                underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitEndsWith_writes_the_correct_like_expression_for_hello_contains_parameter_with_the_parameter_not_escaped() {
        //--Arrange
        final Expression source = mock(Expression.class);
        doAccept("hello").when(source).accept(underTest);

        final Expression pattern = mock(Expression.class);
        doParameter("world").when(pattern).accept(underTest);

        //--Act
        underTest.visitEndsWith(source, pattern);

        //--Assert
        Assert.assertEquals("hello LIKE ?", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitEndsWith_writes_the_correct_like_expression_for_world_contains_parameter_with_the_parameter_not_escaped() {
        //--Arrange
        final Expression source = mock(Expression.class);
        doAccept("world").when(source).accept(underTest);

        final Expression pattern = mock(Expression.class);
        doParameter("world").when(pattern).accept(underTest);

        //--Act
        underTest.visitEndsWith(source, pattern);

        //--Assert
        Assert.assertEquals("world LIKE ?", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitEndsWith_preserves_any_expressions_prior_to_the_like_clause() {
        //--Arrange
        final Expression source = mock(Expression.class);
        doAccept("hello").when(source).accept(underTest);

        final Expression pattern = mock(Expression.class);
        doParameter("world").when(pattern).accept(underTest);

        underTest.getQuery().append("infinity ");

        //--Act
        underTest.visitEndsWith(source, pattern);

        //--Assert
        Assert.assertEquals("infinity hello LIKE ?", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitEndsWith_writes_the_correct_like_expression_for_hello_contains_parameter_when_parameter_has_a_backslash_in_it() {
        //--Arrange
        final Expression source = mock(Expression.class);
        doAccept("hello").when(source).accept(underTest);

        final Expression pattern = mock(Expression.class);
        doParameter("something\\cool").when(pattern).accept(underTest);

        //--Act
        underTest.visitEndsWith(source, pattern);

        //--Assert
        Assert.assertEquals("%something\\\\cool", underTest.getParameters().get(0));
    }


    @Test
    public void test_that_visitEndsWith_writes_the_correct_like_expression_for_hello_contains_parameter_when_parameter_has_a_percent_in_it() {
        //--Arrange
        final Expression source = mock(Expression.class);
        doAccept("hello").when(source).accept(underTest);

        final Expression pattern = mock(Expression.class);
        doParameter("97%").when(pattern).accept(underTest);

        //--Act
        underTest.visitEndsWith(source, pattern);

        //--Assert
        Assert.assertEquals("%97\\%", underTest.getParameters().get(0));
    }


    @Test
    public void test_that_visitEndsWith_writes_the_correct_like_expression_for_hello_contains_parameter_when_parameter_has_an_underscore_in_it() {
        //--Arrange
        final Expression source = mock(Expression.class);
        doAccept("hello").when(source).accept(underTest);

        final Expression pattern = mock(Expression.class);
        doParameter("good_bad_ugly").when(pattern).accept(underTest);

        //--Act
        underTest.visitEndsWith(source, pattern);

        //--Assert
        Assert.assertEquals("%good\\_bad\\_ugly", underTest.getParameters().get(0));
    }


    @Test
    public void test_that_visitEntity_writes_hello_as_hello_0_when_desired() {
        //--Arrange
        final EntityExpression<?> expr = mock(EntityExpression.class);
        when(expr.getTableName()).thenReturn("hello");
        when(fieldAliasCache.getFieldAlias(same(expr))).thenReturn("hello_0");
        when(metaData.getEscapedObjectName("hello")).thenReturn("\"hello\"");

        //--Act
        underTest.visitEntity(expr);

        //--Assert
        Assert.assertEquals("\"hello\" AS hello_0", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitEntity_writes_world_as_world_3_when_desired() {
        //--Arrange
        final EntityExpression<?> expr = mock(EntityExpression.class);
        when(expr.getTableName()).thenReturn("world");
        when(fieldAliasCache.getFieldAlias(same(expr))).thenReturn("world_3");
        when(metaData.getEscapedObjectName("world")).thenReturn("\"world\"");

        //--Act
        underTest.visitEntity(expr);

        //--Assert
        Assert.assertEquals("\"world\" AS world_3", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitEntity_writes_hello_dot_world_as_hello_world_3_when_desired() {
        //--Arrange
        final EntityExpression<?> expr = mock(EntityExpression.class);
        when(expr.getSchemaName()).thenReturn("hello");
        when(expr.getTableName()).thenReturn("world");
        when(fieldAliasCache.getFieldAlias(same(expr))).thenReturn("hello_world_3");

        when(metaData.getEscapedObjectName("hello")).thenReturn("\"hello\"");
        when(metaData.getEscapedObjectName("world")).thenReturn("\"world\"");

        //--Act
        underTest.visitEntity(expr);

        //--Assert
        Assert.assertEquals("\"hello\".\"world\" AS hello_world_3", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitSelectList_writes_hello_as_world_when_the_only_expression_on_the_list_resolves_to_hello() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        doAccept("hello").when(expr).accept(same(underTest));
        when(fieldAliasCache.getFieldAlias(same(expr))).thenReturn("world");

        //--Act
        underTest.visitSelectList(Arrays.asList(expr));

        //--Assert
        Assert.assertEquals("hello AS world", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitSelectList_writes_good_as_great_comma_java_as_beans_when_the_only_expressions_on_the_list_resolve_to_good_and_java() {
        //--Arrange
        final Expression goodExpr = mock(Expression.class);
        doAccept("good").when(goodExpr).accept(same(underTest));
        when(fieldAliasCache.getFieldAlias(same(goodExpr))).thenReturn("great");

        final Expression javaExpr = mock(Expression.class);
        doAccept("java").when(javaExpr).accept(same(underTest));
        when(fieldAliasCache.getFieldAlias(same(javaExpr))).thenReturn("beans");

        //--Act
        underTest.visitSelectList(Arrays.asList(goodExpr, javaExpr));

        //--Assert
        Assert.assertEquals("good AS great, java AS beans", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitSelect_writes_SELECT_a_FROM_b_WHERE_c_when_selectListExpression_fromExpression_and_whereExpression_evaluate_to_a_b_and_c_respectively() {
        //--Arrange
        final SelectListExpression selectListExpression = mock(SelectListExpression.class);
        doAccept("a").when(selectListExpression).accept(same(underTest));

        final EntityExpression<?> fromExpression = mock(EntityExpression.class);
        doAccept("b").when(fromExpression).accept(same(underTest));

        final BooleanExpression whereExpression = mock(BooleanExpression.class);
        doAccept("c").when(whereExpression).accept(same(underTest));

        //--Act
        underTest.visitSelect(selectListExpression, fromExpression, whereExpression);

        //--Assert
        Assert.assertEquals("SELECT a FROM b WHERE c", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitSelect_writes_SELECT_d_FROM_e_WHERE_f_when_selectListExpression_fromExpression_and_whereExpression_evaluate_to_d_e_and_f_respectively() {
        //--Arrange
        final SelectListExpression selectListExpression = mock(SelectListExpression.class);
        doAccept("d").when(selectListExpression).accept(same(underTest));

        final EntityExpression<?> fromExpression = mock(EntityExpression.class);
        doAccept("e").when(fromExpression).accept(same(underTest));

        final BooleanExpression whereExpression = mock(BooleanExpression.class);
        doAccept("f").when(whereExpression).accept(same(underTest));

        //--Act
        underTest.visitSelect(selectListExpression, fromExpression, whereExpression);

        //--Assert
        Assert.assertEquals("SELECT d FROM e WHERE f", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitSelect_writes_SELECT_a_FROM_b_when_selectListExpression_fromExpression_evaluate_to_a_b_respectively_and_whereExpression_is_null() {
        //--Arrange
        final SelectListExpression selectListExpression = mock(SelectListExpression.class);
        doAccept("a").when(selectListExpression).accept(same(underTest));

        final EntityExpression<?> fromExpression = mock(EntityExpression.class);
        doAccept("b").when(fromExpression).accept(same(underTest));

        //--Act
        underTest.visitSelect(selectListExpression, fromExpression, null);

        //--Assert
        Assert.assertEquals("SELECT a FROM b", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitCount_writes_COUNT_FOO_when_the_expression_evaulates_to_FOO() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        doAccept("FOO").when(expr).accept(same(underTest));

        //--Act
        underTest.visitCount(expr);

        //--Assert
        Assert.assertEquals("COUNT(FOO)", underTest.getQuery().toString());
    }


    @Test
    public void test_that_visitCount_writes_COUNT_BAR_when_the_expression_evaulates_to_BAR() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        doAccept("BAR").when(expr).accept(same(underTest));

        //--Act
        underTest.visitCount(expr);

        //--Assert
        Assert.assertEquals("COUNT(BAR)", underTest.getQuery().toString());
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_visitSelect_outputs_a_group_for_column_a_when_the_user_wants_column_a_and_aggregated_column_b() {
        //--Arrange
        final GenericExpression<String> groupedColumn = mock(GenericExpression.class);
        doAccept("a").when(groupedColumn).accept(any(ExpressionVisitorImpl.class));
        when(fieldAliasCache.getFieldAlias(same(groupedColumn))).thenReturn("a");

        final FakeAggregateExpression aggregatedColumn = mock(FakeAggregateExpression.class);
        doAccept("foo(b)").when(aggregatedColumn).accept(same(underTest));
        when(fieldAliasCache.getFieldAlias(same(aggregatedColumn))).thenReturn("b");

        final SelectListExpression selectListExpression = new SelectListExpression(
                Arrays.asList(
                        groupedColumn,
                        aggregatedColumn));

        final EntityExpression<?> fromExpression = mock(EntityExpression.class);
        doAccept("SomeTableName").when(fromExpression).accept(underTest);

        final BooleanExpression whereExpression = mock(BooleanExpression.class);
        doAccept("1 = 1").when(whereExpression).accept(same(underTest));

        //--Act
        underTest.visitSelect(selectListExpression, fromExpression, whereExpression);

        //--Assert
        Assert.assertEquals(
                "SELECT a AS a, foo(b) AS b FROM SomeTableName WHERE 1 = 1 GROUP BY a", underTest.getQuery()
                        .toString());
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_visitSelect_outputs_a_group_for_columns_a_and_c_when_the_user_wants_column_a_and_aggregated_column_b() {
        //--Arrange
        final GenericExpression<String> groupedColumn = mock(GenericExpression.class);
        doAccept("a").when(groupedColumn).accept(any(ExpressionVisitorImpl.class));
        when(fieldAliasCache.getFieldAlias(same(groupedColumn))).thenReturn("a");

        final GenericExpression<String> otherGroupedColumn = mock(GenericExpression.class);
        doAccept("c").when(otherGroupedColumn).accept(any(ExpressionVisitorImpl.class));
        when(fieldAliasCache.getFieldAlias(same(otherGroupedColumn))).thenReturn("c");

        final FakeAggregateExpression aggregatedColumn = mock(FakeAggregateExpression.class);
        doAccept("foo(b)").when(aggregatedColumn).accept(same(underTest));
        when(fieldAliasCache.getFieldAlias(same(aggregatedColumn))).thenReturn("b");

        final SelectListExpression selectListExpression = new SelectListExpression(
                Arrays.asList(
                        groupedColumn,
                        otherGroupedColumn,
                        aggregatedColumn));

        final EntityExpression<?> fromExpression = mock(EntityExpression.class);
        doAccept("SomeTableName").when(fromExpression).accept(underTest);

        final BooleanExpression whereExpression = mock(BooleanExpression.class);
        doAccept("1 = 1").when(whereExpression).accept(same(underTest));

        //--Act
        underTest.visitSelect(selectListExpression, fromExpression, whereExpression);

        //--Assert
        Assert.assertEquals(
                "SELECT a AS a, c AS c, foo(b) AS b FROM SomeTableName WHERE 1 = 1 GROUP BY a, c", underTest
                        .getQuery()
                        .toString());
    }


    @Test
    public void test_that_visitSelect_outputs_no_group_by_when_the_user_wants_only_aggregated_column_b() {
        //--Arrange
        final FakeAggregateExpression aggregatedColumn = mock(FakeAggregateExpression.class);
        doAccept("foo(b)").when(aggregatedColumn).accept(same(underTest));
        when(fieldAliasCache.getFieldAlias(same(aggregatedColumn))).thenReturn("b");

        final SelectListExpression selectListExpression = new SelectListExpression(Arrays.asList(aggregatedColumn));

        final EntityExpression<?> fromExpression = mock(EntityExpression.class);
        doAccept("SomeTableName").when(fromExpression).accept(underTest);

        final BooleanExpression whereExpression = mock(BooleanExpression.class);
        doAccept("1 = 1").when(whereExpression).accept(same(underTest));

        //--Act
        underTest.visitSelect(selectListExpression, fromExpression, whereExpression);

        //--Assert
        Assert.assertEquals(
                "SELECT foo(b) AS b FROM SomeTableName WHERE 1 = 1", underTest
                        .getQuery()
                        .toString());
    }


    private interface FakeAggregateExpression extends GenericExpression<Integer>, AggregateExpression {
    }

    @Test
    public void
    test_that_visitInsert_the_values_1_2_and_hello_into_the_table_Foo_for_the_fields_First_Second_and_Third_respectively() {
        //--Arrange
        EntityExpression<?> entityExpression = mock(EntityExpression.class);
        when(entityExpression.getTableName()).thenReturn("Bar");
        when(metaData.getEscapedObjectName("Bar")).thenReturn("Foo");

        FieldExpression firstField = mock(FieldExpression.class);
        when(firstField.getFieldName()).thenReturn("1st");
        when(metaData.getEscapedObjectName("1st")).thenReturn("First");
        FieldExpression secondField = mock(FieldExpression.class);
        when(secondField.getFieldName()).thenReturn("2nd");
        when(metaData.getEscapedObjectName("2nd")).thenReturn("Second");
        FieldExpression thirdField = mock(FieldExpression.class);
        when(thirdField.getFieldName()).thenReturn("3rd");
        when(metaData.getEscapedObjectName("3rd")).thenReturn("Third");

        Expression firstValue = mock(Expression.class);
        doAccept("1").when(firstValue).accept(underTest);
        Expression secondValue = mock(Expression.class);
        doAccept("2").when(secondValue).accept(underTest);
        Expression thirdValue = mock(Expression.class);
        doAccept("'hello'").when(thirdValue).accept(underTest);

        InsertModel insertModel = new InsertModel();
        insertModel.setIntoEntityExpression(entityExpression);
        insertModel.setFieldsToFill(Arrays.asList(firstField, secondField, thirdField));
        insertModel.setValuesToInsert(Arrays.asList(firstValue, secondValue, thirdValue));

        //--Act
        underTest.visitInsert(insertModel);

        //--Assert
        assertThat(underTest.getQuery().toString(), is("INSERT INTO Foo (First, Second, Third) VALUES (1, 2, 'hello')"));
    }

    @Test
    public void
    test_that_visitInsert_the_values_world_john_into_the_table_Bar_for_the_fields_Description_and_Name_respectively() {
        //--Arrange
        EntityExpression<?> entityExpression = mock(EntityExpression.class);
        when(entityExpression.getTableName()).thenReturn("Foo");
        when(metaData.getEscapedObjectName("Foo")).thenReturn("Bar");

        FieldExpression descriptionField = mock(FieldExpression.class);
        when(descriptionField.getFieldName()).thenReturn("A");
        when(metaData.getEscapedObjectName("A")).thenReturn("Description");
        FieldExpression nameField = mock(FieldExpression.class);
        when(nameField.getFieldName()).thenReturn("B");
        when(metaData.getEscapedObjectName("B")).thenReturn("Name");

        Expression descriptionValue = mock(Expression.class);
        doAccept("'world'").when(descriptionValue).accept(underTest);
        Expression nameValue = mock(Expression.class);
        doAccept("'john'").when(nameValue).accept(underTest);

        InsertModel insertModel = new InsertModel();
        insertModel.setIntoEntityExpression(entityExpression);
        insertModel.setFieldsToFill(Arrays.asList(descriptionField, nameField));
        insertModel.setValuesToInsert(Arrays.asList(descriptionValue, nameValue));

        //--Act
        underTest.visitInsert(insertModel);

        //--Assert
        assertThat(underTest.getQuery().toString(), is("INSERT INTO Bar (Description, Name) VALUES ('world', 'john')"));
    }

    @Test
    public void
    test_that_visitUpdate_updates_the_record_for_table_Foo_with_primary_keys_are_First_equals_6_and_Second_equals_hello_and_sets_its_fields_Name_and_Description_to_john_and_awesome() {
        //--Arrange
        EntityExpression<?> entityExpression = mock(EntityExpression.class);
        doAccept("Foo").when(entityExpression).accept(underTest);

        BooleanExpression whereExpression = mock(BooleanExpression.class);
        doAccept("First = 6 AND Second = 'hello'").when(whereExpression).accept(underTest);

        FieldExpression nameField = mock(FieldExpression.class);
        doAccept("Name").when(nameField).accept(underTest);
        Expression nameValue = mock(Expression.class);
        doAccept("'john'").when(nameValue).accept(underTest);

        FieldExpression descriptionField = mock(FieldExpression.class);
        doAccept("Description").when(descriptionField).accept(underTest);
        Expression descriptionValue = mock(Expression.class);
        doAccept("'awesome'").when(descriptionValue).accept(underTest);

        UpdateModel updateModel = mock(UpdateModel.class);
        when((Object) updateModel.getEntityExpressionToUpdate()).thenReturn(entityExpression);
        when(updateModel.getWhereExpression()).thenReturn(whereExpression);
        when(updateModel.getFieldsToUpdate()).thenReturn(
                Arrays.asList(
                        Pair.of(nameField, nameValue),
                        Pair.of(descriptionField, descriptionValue)));

        //--Act
        underTest.visitUpdate(updateModel);

        //--Assert
        assertThat(
                underTest.getQuery().toString(), is(
                        "UPDATE Foo SET Name = 'john', Description = 'awesome' WHERE First = 6 AND Second = 'hello'"));
    }

    @Test
    public void
    test_that_visitUpdate_updates_the_record_for_table_Bar_with_primary_key_ID_equals_2_and_sets_its_fields_Active_ItemNumber_and_Price_to_1_123456789_and_47_point_25_respectively
            () {
        //--Arrange
        EntityExpression<?> entityExpression = mock(EntityExpression.class);
        doAccept("Bar").when(entityExpression).accept(underTest);

        BooleanExpression whereExpression = mock(BooleanExpression.class);
        doAccept("ID = 2").when(whereExpression).accept(underTest);

        FieldExpression activeField = mock(FieldExpression.class);
        doAccept("Active").when(activeField).accept(underTest);
        Expression activeValue = mock(Expression.class);
        doAccept("1").when(activeValue).accept(underTest);

        FieldExpression itemNumberField = mock(FieldExpression.class);
        doAccept("ItemNumber").when(itemNumberField).accept(underTest);
        Expression itemNumberValue = mock(Expression.class);
        doAccept("123456789").when(itemNumberValue).accept(underTest);

        FieldExpression priceField = mock(FieldExpression.class);
        doAccept("Price").when(priceField).accept(underTest);
        Expression priceValue = mock(Expression.class);
        doAccept("47.25").when(priceValue).accept(underTest);

        UpdateModel updateModel = mock(UpdateModel.class);
        when((Object) updateModel.getEntityExpressionToUpdate()).thenReturn(entityExpression);
        when(updateModel.getWhereExpression()).thenReturn(whereExpression);
        when(updateModel.getFieldsToUpdate()).thenReturn(
                Arrays.asList(
                        Pair.of(activeField, activeValue),
                        Pair.of(itemNumberField, itemNumberValue),
                        Pair.of(priceField, priceValue)));

        //--Act
        underTest.visitUpdate(updateModel);

        //--Assert
        assertThat(
                underTest.getQuery().toString(), is(
                        "UPDATE Bar SET Active = 1, ItemNumber = 123456789, " +
                        "Price = 47.25 WHERE ID = 2"));
    }
}
