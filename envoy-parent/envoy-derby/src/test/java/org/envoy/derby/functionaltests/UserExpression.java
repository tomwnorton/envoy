package org.envoy.derby.functionaltests;

import org.envoy.expression.*;

public class UserExpression extends AbstractAddressableEntityExpression<User> {
    private final IntegerFieldExpression id = new IntegerFieldExpression(this, "ID").primaryKey();
    private final StringFieldExpression userName = new StringFieldExpression(this, "UserName");
    private final BooleanFieldExpression active = new BooleanFieldExpression(this, "Active");


    public UserExpression() {
        super(User.class);
        setTableName("User");
        getContactInformation().getName().getFirstName().setFieldName("FirstName");
        getContactInformation().getName().getLastName().setFieldName("LastName");
        getContactInformation().getAddressLine1().setFieldName("Address1");
        getContactInformation().getAddressLine2().setFieldName("Address2");
        getContactInformation().getCity().setFieldName("City");
        getContactInformation().getState().setFieldName("State");
        getContactInformation().getPostalCode().setFieldName("Zip");
        getContactInformation().getCountryCode().setFieldName("Country");
    }


    public IntegerFieldExpression getId() {
        return id;
    }


    public StringFieldExpression getUserName() {
        return userName;
    }


    public BooleanFieldExpression isActive() {
        return active;
    }
}
