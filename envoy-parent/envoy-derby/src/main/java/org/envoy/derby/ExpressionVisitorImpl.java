package org.envoy.derby;

import org.apache.commons.lang3.*;
import org.apache.commons.lang3.tuple.*;
import org.envoy.*;
import org.envoy.expression.*;
import org.envoy.query.*;

import java.util.*;

/**
 * Created by thomas on 7/21/14.
 */
public class ExpressionVisitorImpl extends AbstractSqlExpressionVisitor {
    private final SqlDatabaseMetaData metaData;
    private boolean isInContains;
    private boolean isInStartsWith;
    private boolean isInEndsWith;

    private static final String LIKE_ESCAPE_PREFIX = "REPLACE(REPLACE(REPLACE(";
    private static final String LIKE_ESCAPE_SUFFIX = ", '\\', '\\\\'), '%', '\\%'), '_', '\\_')";
    private static final String LIST_ELEMENT_SEPARATOR = ", ";


    public ExpressionVisitorImpl(final SqlDatabaseMetaData metaData) {
        this.metaData = metaData;
    }


    @Override
    public void visitEquals(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        getQuery().append(" = ");
        getQuery().append("(");
        rhs.accept(this);
        getQuery().append(")");
    }


    @Override
    public void visitParameter(final Object value) {
        Object tempValue = value;
        if ((isInContains || isInStartsWith || isInEndsWith) && tempValue instanceof String) {
            StringBuilder sb = new StringBuilder();
            if (isInContains || isInEndsWith) {
                sb.append("%");
            }

            sb.append(
                    tempValue
                            .toString()
                            .replace("\\", "\\\\")
                            .replace("%", "\\%")
                            .replace("_", "\\_"));

            if (isInContains || isInStartsWith) {
                sb.append("%");
            }

            tempValue = sb.toString();
            isInContains = false;
            isInStartsWith = false;
            isInEndsWith = false;
        }
        getQuery().append("?");
        getParameters().add(tempValue);
    }


    @Override
    public void visitNotEquals(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        getQuery().append(" <> ");
        getQuery().append("(");
        rhs.accept(this);
        getQuery().append(")");
    }


    @Override
    public void visitAnd(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        getQuery().append(" AND ");
        getQuery().append("(");
        rhs.accept(this);
        getQuery().append(")");
    }


    @Override
    public void visitOr(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        getQuery().append(" OR ");
        getQuery().append("(");
        rhs.accept(this);
        getQuery().append(")");
    }


    @Override
    public void visitNot(final Expression expr) {
        getQuery().append("NOT ");
        getQuery().append("(");
        expr.accept(this);
        getQuery().append(")");
    }


    @Override
    public <T> void visitIn(final Expression expr, final Collection<T> collection) {
        final ArrayList<String> parts = new ArrayList<>();
        for (final T element : collection) {
            parts.add("?");
            getParameters().add(element);
        }
        expr.accept(this);
        getQuery().append(" IN (").append(StringUtils.join(parts.toArray(), LIST_ELEMENT_SEPARATOR)).append(")");
    }


    @Override
    public void visitField(final FieldExpression fieldExpr) {
        final String tableAlias = metaData.getEscapedObjectName(
                getFieldAliasCache().getFieldAlias(
                        fieldExpr.getEntityExpression()));
        getQuery()
                .append(tableAlias)
                .append(".")
                .append(metaData.getEscapedObjectName(fieldExpr.getFieldName()));
    }


    @Override
    public <T> void visitNull(final Expression expr) {
        expr.accept(this);
        getQuery().append(" IS NULL");
    }


    @Override
    public <T> void visitNotNull(final Expression expr) {
        expr.accept(this);
        getQuery().append(" IS NOT NULL");
    }


    @Override
    public void visitTrue() {
        getQuery().append("1");
    }


    @Override
    public void visitFalse() {
        getQuery().append("0");
    }


    @Override
    public void visitLessThan(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        getQuery().append(" < ");
        getQuery().append("(");
        rhs.accept(this);
        getQuery().append(")");
    }


    @Override
    public void visitLessThanEqual(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        getQuery().append(" <= ");
        getQuery().append("(");
        rhs.accept(this);
        getQuery().append(")");
    }


    @Override
    public void visitGreaterThan(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        getQuery().append(" > ");
        getQuery().append("(");
        rhs.accept(this);
        getQuery().append(")");
    }


    @Override
    public void visitGreaterThanEqual(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        getQuery().append(" >= ");
        getQuery().append("(");
        rhs.accept(this);
        getQuery().append(")");
    }


    @Override
    public void visitConvertToString(final Expression expression) {
        getQuery().append("STR(");
        expression.accept(this);
        getQuery().append(")");
    }


    @Override
    public void visitPlus(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        getQuery().append(" + ");
        getQuery().append("(");
        rhs.accept(this);
        getQuery().append(")");
    }


    @Override
    public void visitMinus(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        getQuery().append(" - (");
        rhs.accept(this);
        getQuery().append(")");
    }


    @Override
    public void visitTimes(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        getQuery().append(" * (");
        rhs.accept(this);
        getQuery().append(")");
    }


    @Override
    public void visitDividedBy(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        getQuery().append(" / (");
        rhs.accept(this);
        getQuery().append(")");
    }


    @Override
    public void visitModulus(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        getQuery().append(" MOD (");
        rhs.accept(this);
        getQuery().append(")");
    }


    @Override
    public void visitCoalesce(final List<? extends Expression> exprs) {
        final StringBuilder temp = getQuery();
        final ArrayList<String> parts = new ArrayList<>();
        for (final Expression expr : exprs) {
            setQuery(new StringBuilder());
            expr.accept(this);
            parts.add(getQuery().toString());
        }
        temp.append("COALESCE(").append(StringUtils.join(parts.toArray(), LIST_ELEMENT_SEPARATOR)).append(")");
        setQuery(temp);
    }


    @Override
    public void visitNegative(final Expression expr) {
        getQuery().append("-");
        expr.accept(this);
    }


    @Override
    public void visitBetween(final Expression lhs, final Expression lowerBound, final Expression upperBound) {
        lhs.accept(this);
        getQuery().append(" BETWEEN ");
        lowerBound.accept(this);
        getQuery().append(" AND ");
        upperBound.accept(this);
    }


    @Override
    public void visitTrim(final Expression expr) {
        getQuery().append("LTRIM(RTRIM(");
        expr.accept(this);
        getQuery().append("))");
    }


    @Override
    public void visitLength(final Expression expr) {
        getQuery().append("LEN(");
        expr.accept(this);
        getQuery().append(")");
    }


    @Override
    public void visitLowerCase(final Expression expr) {
        getQuery().append("LOWER(");
        expr.accept(this);
        getQuery().append(")");
    }


    @Override
    public void visitUpperCase(final Expression expr) {
        getQuery().append("UPPER(");
        expr.accept(this);
        getQuery().append(")");
    }


    @Override
    public void visitConvertStringToInteger(final Expression expr) {
        getQuery().append("CAST(");
        expr.accept(this);
        getQuery().append(" AS BIGINT)");
    }


    @Override
    public void visitConcat(final Expression lhs, final Expression rhs) {
        lhs.accept(this);
        getQuery().append(" || ");
        rhs.accept(this);
    }


    @Override
    public void visitContains(final Expression source, final Expression pattern) {
        source.accept(this);
        getQuery().append(" LIKE ");

        //--This logic will either escape a SQL expression with the SQL REPLACE function
        //  or do a simple like on a parameter that is escaped before being added to the parameters list.
        //--See unit tests for further details.
        final StringBuilder temp = getQuery();
        setQuery(new StringBuilder(temp));
        isInContains = true;
        getQuery().append("'%' || ").append(LIKE_ESCAPE_PREFIX);
        pattern.accept(this);
        if (!isInContains) {
            setQuery(temp);
            getQuery().append("?");
        } else {
            getQuery().append(LIKE_ESCAPE_SUFFIX).append(" || '%'");
        }
    }


    @Override
    public void visitStartsWith(final Expression source, final Expression pattern) {
        source.accept(this);
        getQuery().append(" LIKE ");

        //--This logic will either escape a SQL expression with the SQL REPLACE function
        //  or do a simple like on a parameter that is escaped before being added to the parameters list.
        //--See unit tests for further details.
        final StringBuilder temp = getQuery();
        setQuery(new StringBuilder(temp));
        isInStartsWith = true;
        getQuery().append(LIKE_ESCAPE_PREFIX);
        pattern.accept(this);
        if (!isInStartsWith) {
            setQuery(temp);
            getQuery().append("?");
        } else {
            getQuery().append(LIKE_ESCAPE_SUFFIX).append(" || '%'");
        }
    }


    @Override
    public void visitEndsWith(final Expression source, final Expression pattern) {
        source.accept(this);
        getQuery().append(" LIKE ");

        //--This logic will either escape a SQL expression with the SQL REPLACE function
        //  or do a simple like on a parameter that is escaped before being added to the parameters list.
        //--See unit tests for further details.
        final StringBuilder temp = getQuery();
        setQuery(new StringBuilder(temp));
        isInEndsWith = true;
        getQuery().append("'%' || ").append(LIKE_ESCAPE_PREFIX);
        pattern.accept(this);
        if (!isInEndsWith) {
            setQuery(temp);
            getQuery().append("?");
        } else {
            getQuery().append(LIKE_ESCAPE_SUFFIX);
        }
    }


    @Override
    public void visitSelectList(final List<? extends Expression> expressions) {
        final StringBuilder temp = getQuery();
        final ArrayList<String> aliasList = new ArrayList<>();
        boolean hasAggregatedExpressions = false;
        boolean isFirstGroupedColumn = true;
        final ExpressionVisitorImpl groupByVisitor = new ExpressionVisitorImpl(metaData);
        groupByVisitor.setParameters(getParameters());
        groupByVisitor.setQuery(new StringBuilder());
        groupByVisitor.setFieldAliasCache(getFieldAliasCache());

        for (final Expression expr : expressions) {
            setQuery(new StringBuilder());
            expr.accept(this);
            getQuery().append(" AS ").append(getFieldAliasCache().getFieldAlias(expr));
            aliasList.add(getQuery().toString());

            if (expr instanceof AggregateExpression) {
                hasAggregatedExpressions = true;
            } else {
                if (!isFirstGroupedColumn) {
                    groupByVisitor.getQuery().append(", ");
                }
                expr.accept(groupByVisitor);
                isFirstGroupedColumn = false;
            }
        }

        if (hasAggregatedExpressions && groupByVisitor.getQuery().length() > 0) {
            setGroupBy(groupByVisitor.getQuery());
        }

        temp.append(StringUtils.join(aliasList.toArray(), LIST_ELEMENT_SEPARATOR));
        setQuery(temp);
    }


    @Override
    public void visitEntity(final EntityExpression<?> expr) {
        if (StringUtils.isNotBlank(expr.getSchemaName())) {
            getQuery().append(metaData.getEscapedObjectName(expr.getSchemaName())).append(".");
        }
        getQuery().append(metaData.getEscapedObjectName(expr.getTableName())).append(" AS ").append(
                getFieldAliasCache().getFieldAlias(
                        expr));
    }


    @Override
    public void visitSelect(
            final SelectListExpression selectListExpr,
            final EntityExpression<?> fromExpr,
            final BooleanExpression whereExpr) {
        getQuery().append("SELECT ");
        selectListExpr.accept(this);
        getQuery().append(" FROM ");
        fromExpr.accept(this);
        if (whereExpr != null) {
            getQuery().append(" WHERE ");
            whereExpr.accept(this);
        }
        if (getGroupBy() != null) {
            getQuery().append(" GROUP BY ").append(getGroupBy());
        }
    }


    @Override
    public void visitCount(final Expression expr) {
        getQuery().append("COUNT(");
        expr.accept(this);
        getQuery().append(")");
    }

    @Override
    public void visitInsert(InsertModel insertModel) {
        getQuery().append("INSERT INTO ").append(metaData.getEscapedObjectName(insertModel.getIntoEntityExpression().getTableName()));
        getQuery().append(" (");
        visitFieldExpressionListForInsert(insertModel.getFieldsToFill());
        getQuery().append(") VALUES (");
        visitExpressionList(insertModel.getValuesToInsert());
        getQuery().append(")");
    }

    private void visitFieldExpressionListForInsert(Collection<? extends FieldExpression> fieldExpressions) {
        boolean isFirst = true;
        for (FieldExpression fieldExpression : fieldExpressions) {
            if (!isFirst) {
                getQuery().append(", ");
            }
            isFirst = false;
            getQuery().append(metaData.getEscapedObjectName(fieldExpression.getFieldName()));
        }
    }

    private void visitExpressionList(Collection<? extends Expression> expressions) {
        boolean isFirst = true;
        for (Expression expression : expressions) {
            if (!isFirst) {
                getQuery().append(", ");
            }
            isFirst = false;
            expression.accept(this);
        }
    }

    @Override
    public void visitUpdate(UpdateModel updateModel) {
        getQuery().append("UPDATE ");
        updateModel.getEntityExpressionToUpdate().accept(this);
        getQuery().append(" SET ");
        visitSetPairs(updateModel.getFieldsToUpdate());
        getQuery().append(" WHERE ");
        updateModel.getWhereExpression().accept(this);
    }

    private void visitSetPairs(Collection<Pair<FieldExpression, Expression>> pairs) {
        boolean isFirst = true;
        for (Pair<FieldExpression, Expression> pair : pairs) {
            if (!isFirst) {
                getQuery().append(", ");
            }
            isFirst = false;

            pair.getLeft().accept(this);
            getQuery().append(" = ");
            pair.getRight().accept(this);
        }
    }
}
