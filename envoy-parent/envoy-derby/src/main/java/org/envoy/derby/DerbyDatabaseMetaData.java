package org.envoy.derby;

import org.envoy.*;
import org.envoy.expression.*;
import org.envoy.query.*;

import java.sql.*;

/**
 * Created by thomas on 8/24/14.
 */
public class DerbyDatabaseMetaData extends AbstractSqlDatabaseMetaData {
    public DerbyDatabaseMetaData() {
        super(new ExceptionTransformerImpl());
    }

    @Override
    public <EntityType> QueryResolver<EntityType, Connection> getQueryResolver() {
        return new BasicSqlQueryResolver<>();
    }

    @Override
    public SqlExpressionVisitor createExpressionVisitor() {
        return new ExpressionVisitorImpl(this);
    }
}
