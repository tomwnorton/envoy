package org.envoy.derby;

import org.envoy.exceptions.*;

/**
 * Created by thomas on 8/24/14.
 */
public class ExceptionTransformerImpl implements ExceptionTransformer {
    @Override
    public EnvoyException transform(final Exception e) {
        return new EnvoyException("Could not execute query", e);
    }
}
