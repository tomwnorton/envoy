package org.envoy;

import org.junit.*;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

/**
 * Created by thomas on 8/26/14.
 */
public class PropertyTests {
    Property<Object> underTest;

    @Before
    public void setUp() {
        underTest = new Property<>();
    }

    @Test
    public void test_that_the_value_property_can_be_get_and_set() {
        //--Arrange
        Object value = new Object();

        //--Act
        underTest.setValue(value);

        //--Assert
        assertThat(underTest.getValue(), is(sameInstance(value)));
    }

    @Test
    public void test_that_the_value_property_can_be_set_using_initializeValue() {
        //--Arrange
        Object value = new Object();

        //--Act
        underTest.initializeValue(value);

        //--Assert
        assertThat(underTest.getValue(), is(sameInstance(value)));
    }

    @Test
    public void test_that_changed_is_true_after_setValue_is_called() {
        //--Act
        underTest.setValue(new Object());

        //--Assert
        assertThat(underTest.isChanged(), is(true));
    }

    @Test
    public void test_that_changed_is_false_before_setValue_is_called() {
        assertThat(underTest.isChanged(), is(false));
    }

    @Test
    public void test_that_changed_is_false_after_flagAsUnchanged_is_called() {
        //--Arrange
        underTest.setValue(new Object());

        //--Act
        underTest.flagAsUnchanged();

        //--Assert
        assertThat(underTest.isChanged(), is(false));
    }
}
