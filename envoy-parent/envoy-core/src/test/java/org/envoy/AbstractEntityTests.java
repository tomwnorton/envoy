package org.envoy;

import org.apache.commons.lang3.tuple.*;
import org.envoy.expression.*;
import org.envoy.query.*;
import org.hamcrest.*;
import org.junit.*;
import org.mockito.Matchers;
import org.mockito.*;

import java.util.*;

import static org.envoy.expression.ExpressionMethods.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by thomas on 8/29/14.
 */
public class AbstractEntityTests {
    private static DatabaseMetaData<AutoCloseable> metaData;

    @Mock
    private InsertResolver insertResolver;

    @Mock
    private UpdateResolver updateResolver;

    @Before
    public void setUp() {
        metaData = mock(DatabaseMetaData.class);
        MockitoAnnotations.initMocks(this);
        when(metaData.getInsertResolver()).thenReturn(insertResolver);
        when(metaData.getUpdateResolver()).thenReturn(updateResolver);
        when(metaData.getExpressionFactory()).thenReturn(new ExpressionFactoryImpl());
    }

    @Test
    public void test_that_save_returns_true_when_one_record_was_updated() {
        //--Arrange
        TestEntity entity = new TestEntity();
        entity.setId(1);
        entity.setUserName("bob");

        TestEntityExpression entityExpression = new TestEntityExpression();
        when(
                updateResolver.update(
                        argThat(
                                matchesUpdateModelThat()
                                        .expectsFieldsToUpdateWith(entityExpression.getUserName(), "bob")
                                        .expectsWhereExpressionWith(entityExpression.getId().equalTo(1)))))
                .thenReturn(1);

        //--Assert
        assertThat(entity.save(), is(true));
    }

    @Test
    public void test_that_save_returns_false_when_no_records_were_updated() {
        //--Arrange
        TestEntity entity = new TestEntity();
        entity.setId(1);
        entity.setUserName("bob");

        TestEntityExpression entityExpression = new TestEntityExpression();
        when(
                updateResolver.update(
                        argThat(
                                matchesUpdateModelThat()
                                        .expectsFieldsToUpdateWith(entityExpression.getUserName(), "bob")
                                        .expectsWhereExpressionWith(entityExpression.getId().equalTo(0)))))
                .thenReturn(0);

        //--Assert
        assertThat(entity.save(), is(false));
    }

    @Test
    public void test_that_save_updates_the_fields_on_the_component_when_the_component_has_changed() {
        //--Arrange
        TestEntity entity = new TestEntity();
        entity.setId(1);
        entity.setNested(new TestComponent(24, "John", "Doe"));

        TestEntityExpression entityExpression = new TestEntityExpression();
        when(
                updateResolver.update(
                        argThat(
                                matchesUpdateModelThat()
                                        .expectsFieldsToUpdateWith(entityExpression.getNested().getAge(), 24)
                                        .expectsFieldsToUpdateWith(
                                                entityExpression.getNested().getFirstName(),
                                                "John")
                                        .expectsFieldsToUpdateWith(entityExpression.getNested().getLastName(), "Doe")
                                        .expectsWhereExpressionWith(entityExpression.getId().equalTo(1)))))
                .thenReturn(1);

        //--Assert
        assertThat(entity.save(), is(true));
    }

    @Test
    public void test_that_save_updates_the_entity_when_multiple_primary_key_fields_are_present() {
        //--Arrange
        EntityWithPrimaryKeyComponent entity = new EntityWithPrimaryKeyComponent();
        entity.setValue(24);
        entity.setPrimaryKey(new PrimaryKeyComponent(12, "hello", true));

        EntityWithPrimaryKeyComponentExpression entityExpression = new EntityWithPrimaryKeyComponentExpression();
        BooleanExpression expectedWhere = entityExpression.getPrimaryKey().isThirdKeyPart().equalTo(true)
                .and(entityExpression.getPrimaryKey().getFirstKeyPart().equalTo(12))
                .and(entityExpression.getPrimaryKey().getSecondKeyPart().equalTo("hello"));
        System.out.println(expectedWhere.toString());
        when(
                updateResolver.update(
                        argThat(
                                matchesUpdateModelThat()
                                        .expectsEntityExpression(EntityWithPrimaryKeyComponentExpression.class)
                                        .expectsFieldsToUpdateWith(entityExpression.getValue(), 24)
                                        .expectsWhereExpressionWith(expectedWhere))))
                .thenReturn(1);

        //--Assert
        assertThat(entity.save(), is(true));
    }

    @Test
    public void test_that_save_returns_false_and_does_not_update_when_there_are_no_changes() {
        //--Arrange
        TestEntity entity = new TestEntity();
        when(updateResolver.update(Matchers.any(UpdateModel.class))).thenReturn(1);

        //--Assert
        assertThat(entity.save(), is(false));
    }

    @Test
    public void test_that_save_returns_false_and_does_not_update_when_the_changes_are_marked_as_unchanged() {
        //--Arrange
        TestEntity entity = new TestEntity();
        entity.setUserName("bob");
        entity.getNested().setAge(58);
        when(updateResolver.update(Matchers.any(UpdateModel.class))).thenReturn(1);

        //--Act
        entity.markAsClean();

        //--Assert
        assertThat(entity.save(), is(false));
    }

    @Test
    public void test_that_save_updates_primary_keys_correctly() {
        //--Arrange
        TestEntity entity = new TestEntity();
        entity.setId(1);
        entity.markAsClean();
        entity.setId(2);


        TestEntityExpression entityExpression = new TestEntityExpression();
        when(
                updateResolver.update(
                        argThat(
                                matchesUpdateModelThat()
                                        .expectsFieldsToUpdateWith(entityExpression.getId(), 2)
                                        .expectsWhereExpressionWith(entityExpression.getId().equalTo(1)))))
                .thenReturn(1);

        //--Assert
        assertThat(entity.save(), is(true));
    }

    @Test
    public void test_that_save_updates_changed_fields_on_an_original_component() {
        //--Arrange
        TestEntity entity = new TestEntity();
        entity.setId(1);
        entity.getNested().setAge(25);

        TestEntityExpression entityExpression = new TestEntityExpression();
        when(
                updateResolver.update(
                        argThat(
                                matchesUpdateModelThat()
                                        .expectsFieldsToUpdateWith(entityExpression.getNested().getAge(), 25)
                                        .expectsWhereExpressionWith(entityExpression.getId().equalTo(1)))))
                .thenReturn(1);

        //--Assert
        assertThat(entity.save(), is(true));
    }

    @Test
    public void test_that_save_updates_primary_keys_on_a_component_correctly() {
        //--Arrange
        EntityWithPrimaryKeyComponent entity = new EntityWithPrimaryKeyComponent();
        entity.getPrimaryKey().setFirstKeyPart(9);
        entity.getPrimaryKey().setSecondKeyPart("hello");
        entity.getPrimaryKey().setThirdKeyPart(true);
        entity.markAsClean();
        entity.getPrimaryKey().setFirstKeyPart(12);


        EntityWithPrimaryKeyComponentExpression entityExpression = new EntityWithPrimaryKeyComponentExpression();
        BooleanExpression whereExpression = entityExpression.getPrimaryKey().isThirdKeyPart().equalTo(true)
                .and(entityExpression.getPrimaryKey().getFirstKeyPart().equalTo(9))
                .and(entityExpression.getPrimaryKey().getSecondKeyPart().equalTo("hello"));
        when(
                updateResolver.update(
                        argThat(
                                matchesUpdateModelThat()
                                        .expectsEntityExpression(EntityWithPrimaryKeyComponentExpression.class)
                                        .expectsFieldsToUpdateWith(entityExpression.getPrimaryKey().getFirstKeyPart(), 12)
                                        .expectsWhereExpressionWith(whereExpression))))
                .thenReturn(1);

        //--Assert
        assertThat(entity.save(), is(true));
    }

    @Test
    public void test_that_save_returns_true_when_the_update_does_not_affect_any_records_but_the_insert_is_successful() {
        //--Arrange
        TestEntity entity = new TestEntity();
        entity.setId(9);
        entity.setUserName("bob");
        entity.getNested().setAge(34);
        entity.getNested().setFirstName("Bob");
        entity.getNested().setLastName("Jones");

        TestEntityExpression expression = new TestEntityExpression();
        List<Object> expectedValues = new ArrayList<>();
        for (FieldExpression fieldExpression : expression.getFieldExpressions()) {
            expectedValues.add(new ParameterExpression<>(fieldExpression.getFromRootEntity(entity)));
        }
        when(
                insertResolver.insert(
                        argThat(
                                matchesInsertModelThat()
                                        .insertsInto(expression)
                                        .forFields(expression.getFieldExpressions())
                                        .withValues(expectedValues))))
                .thenReturn(1);

        //--Assert
        assertThat(entity.save(), is(true));
    }

    @Test
    public void test_that_an_entity_with_an_auto_incremented_key_is_inserted_correctly() {
        //--Arrange
        AutoIncrementEntity entity = new AutoIncrementEntity();
        entity.setValue("bob");

        AutoIncrementEntityExpression expression = new AutoIncrementEntityExpression();
        when(
                insertResolver.insert(
                        argThat(
                                matchesInsertModelThat().insertsInto(expression).forFields(
                                        Arrays.<FieldExpression>asList(expression.getValue()))
                                        .withValues(Arrays.<Object>asList(asExpression("bob")))
                                        .withAutoIncrementedExpression(expression.getId())
                        ))).thenReturn(1);

        //--Assert
        assertThat(entity.save(), is(true));
    }

    @Test
    public void test_that_save_marks_the_entity_as_clean_after_an_update_occurs() {
        //--Arrange
        TestEntity entity = new TestEntity();
        entity.setId(1);
        entity.setUserName("bob");

        TestEntityExpression entityExpression = new TestEntityExpression();
        when(
                updateResolver.update(
                        argThat(
                                matchesUpdateModelThat()
                                        .expectsFieldsToUpdateWith(entityExpression.getUserName(), "bob")
                                        .expectsWhereExpressionWith(entityExpression.getId().equalTo(1)))))
                .thenReturn(1);

        //--Act
        entity.save();

        //--Assert
        //--Now the entity should be "clean" and should not save
        assertThat(entity.save(), is(false));
    }


    private UpdateModelMatcher matchesUpdateModelThat() {
        return new UpdateModelMatcher();
    }

    private class UpdateModelMatcher extends BaseMatcher<UpdateModel> {
        Class<? extends EntityExpression<?>> entityExpressionClass = TestEntityExpression.class;
        private Collection<Pair<FieldExpression, Expression>> fieldsToUpdate = new ArrayList<>();
        private BooleanExpression whereExpression;

        public UpdateModelMatcher expectsFieldsToUpdateWith(FieldExpression fieldToUpdate, Object value) {
            fieldsToUpdate.add(Pair.of(fieldToUpdate, (Expression) asExpression(value)));
            return this;
        }

        public UpdateModelMatcher expectsWhereExpressionWith(BooleanExpression whereExpression) {
            this.whereExpression = whereExpression;
            return this;
        }

        public UpdateModelMatcher expectsEntityExpression(Class<? extends EntityExpression<?>> entityExpressionClass) {
            this.entityExpressionClass = entityExpressionClass;
            return this;
        }

        @Override
        public boolean matches(Object o) {
            if (!UpdateModel.class.isAssignableFrom(o.getClass())) {
                return false;
            }
            UpdateModel updateModel = (UpdateModel) o;
            return (updateModel.getEntityExpressionToUpdate() != null
                    && entityExpressionClass.isAssignableFrom(updateModel.getEntityExpressionToUpdate().getClass()))
                   && (fieldsToUpdate == null ||
                       updateModel.getFieldsToUpdate() != null && updateModel.getFieldsToUpdate().containsAll(fieldsToUpdate)
                       && updateModel.getFieldsToUpdate().size() == fieldsToUpdate.size())
                   && (whereExpression == null || whereExpression.equals(updateModel.getWhereExpression()));
        }

        @Override
        public void describeTo(Description description) {
        }
    }

    private InsertModelMatcher matchesInsertModelThat() {
        return new InsertModelMatcher();
    }

    private class InsertModelMatcher extends BaseMatcher<InsertModel> {
        private EntityExpression<?> entityExpression;
        private List<FieldExpression> fieldExpressions;
        private List<Object> values;
        private FieldExpression autoIncrementedExpression;

        @Override
        public boolean matches(Object o) {
            if (!(o instanceof InsertModel)) {
                return false;
            }

            InsertModel insertModel = (InsertModel) o;
            return (entityExpression == null || entityExpression.equals(insertModel.getIntoEntityExpression()))
                   && (fieldExpressions == null || fieldExpressions.equals(insertModel.getFieldsToFill()))
                   && (values == null || values.equals(insertModel.getValuesToInsert()))
                   && (autoIncrementedExpression == null || autoIncrementedExpression.equals(
                    insertModel
                            .getAutoIncrementedExpression()));
        }

        @Override
        public void describeTo(Description description) {
        }

        public InsertModelMatcher insertsInto(EntityExpression<?> entityExpression) {
            this.entityExpression = entityExpression;
            return this;
        }

        public InsertModelMatcher forFields(List<FieldExpression> fieldExpressions) {
            this.fieldExpressions = fieldExpressions;
            return this;
        }

        public InsertModelMatcher withValues(List<Object> values) {
            this.values = values;
            return this;
        }

        public InsertModelMatcher withAutoIncrementedExpression(FieldExpression autoIncrementedExpression) {
            this.autoIncrementedExpression = autoIncrementedExpression;
            return this;
        }
    }

    public static class TestComponent extends AbstractBean {
        private Integer age;
        private String firstName;
        private String lastName;

        public TestComponent() {
        }

        public TestComponent(Integer age, String firstName, String lastName) {
            this.age = age;
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            recordChange(Integer.class);
            this.age = age;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            recordChange(String.class);
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            recordChange(String.class);
            this.lastName = lastName;
        }
    }

    public static class TestComponentExpression extends AbstractComponentExpression<TestComponent> {
        private IntegerFieldExpression age = new IntegerFieldExpression(this);
        private StringFieldExpression firstName = new StringFieldExpression(this);
        private StringFieldExpression lastName = new StringFieldExpression(this);

        public TestComponentExpression(ModelExpression parent) {
            super(parent, TestComponent.class);
        }

        public IntegerFieldExpression getAge() {
            return age;
        }

        public StringFieldExpression getFirstName() {
            return firstName;
        }

        public StringFieldExpression getLastName() {
            return lastName;
        }
    }


    public static class TestEntity extends AbstractEntity<TestEntity> {
        private Integer id;
        private Integer status;
        private String userName;
        private Boolean active;
        private TestComponent nested = new TestComponent();

        public TestEntity() {
            super(TestEntityExpression.class, metaData);
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            recordChange(Integer.class);
            this.id = id;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            recordChange(Integer.class);
            this.status = status;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            recordChange(String.class);
            this.userName = userName;
        }

        public Boolean isActive() {
            return active;
        }

        public void setActive(Boolean active) {
            recordChange(Boolean.class);
            this.active = active;
        }

        public TestComponent getNested() {
            return nested;
        }

        public void setNested(TestComponent nested) {
            recordChange(TestComponent.class);
            this.nested = nested;
        }
    }

    public static class TestEntityExpression extends AbstractEntityExpression<TestEntity> {
        private IntegerFieldExpression id = new IntegerFieldExpression(this, "ID").primaryKey();
        private IntegerFieldExpression status = new IntegerFieldExpression(this, "Status");
        private StringFieldExpression userName = new StringFieldExpression(this, "UserName");
        private BooleanFieldExpression active = new BooleanFieldExpression(this, "Active");
        private TestComponentExpression nested = new TestComponentExpression(this);

        public TestEntityExpression() {
            super(TestEntity.class);
            nested.getAge().setFieldName("Age");
            nested.getFirstName().setFieldName("FirstName");
            nested.getLastName().setFieldName("LastName");
        }

        public IntegerFieldExpression getId() {
            return id;
        }

        public IntegerFieldExpression getStatus() {
            return status;
        }

        public StringFieldExpression getUserName() {
            return userName;
        }

        public BooleanFieldExpression isActive() {
            return active;
        }

        public TestComponentExpression getNested() {
            return nested;
        }
    }

    public static class PrimaryKeyComponent extends AbstractBean {
        private Integer firstKeyPart;
        private String secondKeyPart;
        private Boolean thirdKeyPart;

        public PrimaryKeyComponent() {
        }

        public PrimaryKeyComponent(Integer firstKeyPart, String secondKeyPart, Boolean thirdKeyPart) {
            this.firstKeyPart = firstKeyPart;
            this.secondKeyPart = secondKeyPart;
            this.thirdKeyPart = thirdKeyPart;
        }

        public Integer getFirstKeyPart() {
            return firstKeyPart;
        }

        public void setFirstKeyPart(Integer firstKeyPart) {
            recordChange(Integer.class);
            this.firstKeyPart = firstKeyPart;
        }

        public String getSecondKeyPart() {
            return secondKeyPart;
        }

        public void setSecondKeyPart(String secondKeyPart) {
            recordChange(String.class);
            this.secondKeyPart = secondKeyPart;
        }

        public Boolean isThirdKeyPart() {
            return thirdKeyPart;
        }

        public void setThirdKeyPart(Boolean thirdKeyPart) {
            recordChange(Boolean.class);
            this.thirdKeyPart = thirdKeyPart;
        }
    }

    public static class PrimaryKeyComponentExpression extends AbstractComponentExpression<PrimaryKeyComponent> {
        private IntegerFieldExpression firstKeyPart = new IntegerFieldExpression(this).primaryKey();
        private StringFieldExpression secondKeyPart = new StringFieldExpression(this).primaryKey();
        private BooleanFieldExpression thirdKeyPart = new BooleanFieldExpression(this).primaryKey();

        public PrimaryKeyComponentExpression(ModelExpression parent) {
            super(parent, PrimaryKeyComponent.class);
        }

        public IntegerFieldExpression getFirstKeyPart() {
            return firstKeyPart;
        }

        public StringFieldExpression getSecondKeyPart() {
            return secondKeyPart;
        }

        public BooleanFieldExpression isThirdKeyPart() {
            return thirdKeyPart;
        }
    }

    public static class EntityWithPrimaryKeyComponent extends AbstractEntity<EntityWithPrimaryKeyComponent> {
        private PrimaryKeyComponent primaryKey = new PrimaryKeyComponent();
        private Integer value;

        public EntityWithPrimaryKeyComponent() {
            super(EntityWithPrimaryKeyComponentExpression.class, metaData);
        }

        public PrimaryKeyComponent getPrimaryKey() {
            return primaryKey;
        }

        public void setPrimaryKey(PrimaryKeyComponent primaryKey) {
            recordChange(PrimaryKeyComponent.class);
            this.primaryKey = primaryKey;
        }

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            recordChange(Integer.class);
            this.value = value;
        }
    }

    public static class EntityWithPrimaryKeyComponentExpression extends AbstractEntityExpression<EntityWithPrimaryKeyComponent> {
        private PrimaryKeyComponentExpression primaryKey = new PrimaryKeyComponentExpression(this);
        private IntegerFieldExpression value = new IntegerFieldExpression(this, "ID");

        public EntityWithPrimaryKeyComponentExpression() {
            super(EntityWithPrimaryKeyComponent.class);
            setTableName("TableWithPrimaryKeyComponent");
            primaryKey.getFirstKeyPart().setFieldName("FirstKeyPart");
            primaryKey.getSecondKeyPart().setFieldName("SecondKeyPart");
            primaryKey.isThirdKeyPart().setFieldName("ThirdKeyPart");
        }

        public PrimaryKeyComponentExpression getPrimaryKey() {
            return primaryKey;
        }

        public IntegerFieldExpression getValue() {
            return value;
        }
    }

    public static class AutoIncrementEntity extends AbstractEntity<AutoIncrementEntity> {
        private Integer id;
        private String value;

        public AutoIncrementEntity() {
            super(AutoIncrementEntityExpression.class, metaData);
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public static class AutoIncrementEntityExpression extends AbstractEntityExpression<AutoIncrementEntity> {
        private IntegerFieldExpression id = new IntegerFieldExpression(this, "ID").primaryKey().autoIncrement();
        private StringFieldExpression value = new StringFieldExpression(this, "Value");

        public AutoIncrementEntityExpression() {
            super(AutoIncrementEntity.class);
            setTableName("AutoIncrementTable");
        }

        public IntegerFieldExpression getId() {
            return id;
        }

        public StringFieldExpression getValue() {
            return value;
        }
    }
}
