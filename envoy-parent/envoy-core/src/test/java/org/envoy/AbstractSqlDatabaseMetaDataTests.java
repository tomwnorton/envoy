package org.envoy;

import org.envoy.expression.*;
import org.envoy.query.*;
import org.junit.*;

import java.sql.*;

public class AbstractSqlDatabaseMetaDataTests {
    private AbstractSqlDatabaseMetaData underTest;


    @Before
    public void setUp() {
        underTest = new AbstractSqlDatabaseMetaData(null) {

            @Override
            public <EntityType> QueryResolver<EntityType, Connection> getQueryResolver() {
                return null;
            }


            @Override
            public SqlExpressionVisitor createExpressionVisitor() {
                return null;
            }
        };
    }


    @Test
    public void test_that_getEscapedObjectName_escapes_Foreign_properly() {
        Assert.assertEquals("\"Foreign\"", underTest.getEscapedObjectName("Foreign"));
    }


    @Test
    public void test_that_getEscapedObjectName_escapes_AUTHORIZATION_properly() {
        Assert.assertEquals("\"AUTHORIZATION\"", underTest.getEscapedObjectName("AUTHORIZATION"));
    }


    @Test
    public void test_that_getEscapedObjectName_does_not_escape_HelloWorld() {
        Assert.assertEquals("HelloWorld", underTest.getEscapedObjectName("HelloWorld"));
    }


    @Test
    public void test_that_getEscapedObjectName_escapes_Hello_World_properly() {
        Assert.assertEquals("\"Hello World\"", underTest.getEscapedObjectName("Hello World"));
    }


    @Test
    public void test_that_getEscapedObjectName_escapes_Hello_tab_World_properly() {
        Assert.assertEquals("\"Hello\tWorld\"", underTest.getEscapedObjectName("Hello\tWorld"));
    }


    @Test
    public void test_that_getEscapedObjectName_escapes_Hello_carriage_return_World_properly() {
        Assert.assertEquals("\"Hello\rWorld\"", underTest.getEscapedObjectName("Hello\rWorld"));
    }


    @Test
    public void test_that_getEscapedObjectName_escapes_Hello_line_feed_World_properly() {
        Assert.assertEquals("\"Hello\nWorld\"", underTest.getEscapedObjectName("Hello\nWorld"));
    }


    @Test
    public void test_that_getEscapedObjectName_escapes_Hello_less_than_World_properly() {
        Assert.assertEquals("\"Hello<World\"", underTest.getEscapedObjectName("Hello<World"));
    }


    @Test
    public void test_that_getEscapedObjectName_escapes_Hello_greater_than_World_properly() {
        Assert.assertEquals("\"Hello>World\"", underTest.getEscapedObjectName("Hello>World"));
    }


    @Test
    public void test_that_getEscapedObjectName_escapes_Hello_plus_World_properly() {
        Assert.assertEquals("\"Hello+World\"", underTest.getEscapedObjectName("Hello+World"));
    }


    @Test
    public void test_that_getEscapedObjectName_escapes_Hello_minus_World_properly() {
        Assert.assertEquals("\"Hello-World\"", underTest.getEscapedObjectName("Hello-World"));
    }


    @Test
    public void test_that_getEscapedObjectName_escapes_Hello_times_World_properly() {
        Assert.assertEquals("\"Hello*World\"", underTest.getEscapedObjectName("Hello*World"));
    }


    @Test
    public void test_that_getEscapedObjectName_escapes_Hello_divided_by_World_properly() {
        Assert.assertEquals("\"Hello/World\"", underTest.getEscapedObjectName("Hello/World"));
    }


    @Test
    public void test_that_getEscapedObjectName_escapes_Hello_comma_World_properly() {
        Assert.assertEquals("\"Hello,World\"", underTest.getEscapedObjectName("Hello,World"));
    }


    @Test
    public void test_that_getEscapedObjectName_escapes_Hello_opening_parenthesis_World_properly() {
        Assert.assertEquals("\"Hello(World\"", underTest.getEscapedObjectName("Hello(World"));
    }


    @Test
    public void test_that_getEscapedObjectName_escapes_Hello_closing_parenthesis_World_properly() {
        Assert.assertEquals("\"Hello)World\"", underTest.getEscapedObjectName("Hello)World"));
    }
}
