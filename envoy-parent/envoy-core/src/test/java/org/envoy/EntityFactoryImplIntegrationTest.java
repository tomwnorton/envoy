package org.envoy;

import org.envoy.expression.*;
import org.junit.*;

import java.io.*;

public class EntityFactoryImplIntegrationTest {
    private final EntityFactoryImpl underTest = new EntityFactoryImpl();


    @Test
    public void test_that_createEntity_creates_an_entity_using_its_default_constructor() {
        //--Act
        final EntityToBeConstructed actual = underTest.createEntity(new EntityToBeConstructedExpression());

        //--Assert
        Assert.assertEquals(EntityToBeConstructed.class, actual.getClass());
    }


    @SuppressWarnings("serial")
    static class EntityToBeConstructed implements Serializable {
    }

    public class EntityToBeConstructedExpression extends AbstractEntityExpression<EntityToBeConstructed> {
        public EntityToBeConstructedExpression() {
            super(EntityToBeConstructed.class);
        }
    }
}
