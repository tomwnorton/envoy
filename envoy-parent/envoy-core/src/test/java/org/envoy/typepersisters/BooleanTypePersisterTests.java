package org.envoy.typepersisters;

import org.junit.*;

import java.sql.*;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class BooleanTypePersisterTests {
    private BooleanTypePersister underTest;


    @Before
    public void setUp() {
        underTest = new BooleanTypePersister();
    }


    @Test
    public void test_that_javaType_is_Boolean() {
        Assert.assertEquals(Boolean.class, underTest.getJavaType());
    }


    @Test
    public void test_that_get_calls_rs_getBoolean_with_COLUMN_when_that_is_the_fieldName() throws SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);

        //--Act
        underTest.get(rs, "COLUMN");

        //--Assert
        verify(rs).getBoolean("COLUMN");
    }


    @Test
    public void test_that_get_calls_rs_getBoolean_with_FIELD_when_that_is_the_fieldName() throws SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);

        //--Act
        underTest.get(rs, "FIELD");

        //--Assert
        verify(rs).getBoolean("FIELD");
    }


    @Test
    public void test_that_get_returns_true_when_rs_getBoolean_returns_true() throws SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);
        when(rs.getBoolean(anyString())).thenReturn(true);

        //--Act
        final boolean result = underTest.get(rs, null);

        //--Assert
        Assert.assertTrue(result);
    }


    @Test
    public void test_that_get_returns_false_when_rs_getBoolean_returns_false() throws SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);
        when(rs.getBoolean(anyString())).thenReturn(false);

        //--Act
        final boolean result = underTest.get(rs, null);

        //--Assert
        Assert.assertFalse(result);
    }


    @Test
    public void test_that_get_returns_null_when_rs_wasNull_is_true() throws SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);
        when(rs.wasNull()).thenReturn(true);

        //--Assert
        Assert.assertNull(underTest.get(rs, null));
    }


    @Test
    public void test_that_setImpl_calls_ps_setObject_with_1_as_the_index_when_fieldIndex_is_1() throws SQLException {
        //--Arrange
        final PreparedStatement ps = mock(PreparedStatement.class);

        //--Act
        underTest.setImpl(ps, 1, null);

        //--Assert
        verify(ps).setObject(eq(1), any());
    }


    @Test
    public void test_that_setImpl_calls_ps_setObject_with_3_as_the_index_when_fieldIndex_is_3() throws SQLException {
        //--Arrange
        final PreparedStatement ps = mock(PreparedStatement.class);

        //--Act
        underTest.setImpl(ps, 3, null);

        //--Assert
        verify(ps).setObject(eq(3), any());
    }


    @Test
    public void test_that_setImpl_calls_ps_setObject_with_true_as_the_value_when_value_is_true() throws SQLException {
        //--Arrange
        final PreparedStatement ps = mock(PreparedStatement.class);

        //--Act
        underTest.setImpl(ps, 0, true);

        //--Assert
        verify(ps).setObject(anyInt(), eq(true));
    }


    @Test
    public void test_that_setImpl_calls_ps_setObject_with_false_as_the_value_when_value_is_false() throws SQLException {
        //--Arrange
        final PreparedStatement ps = mock(PreparedStatement.class);

        //--Act
        underTest.setImpl(ps, 0, false);

        //--Assert
        verify(ps).setObject(anyInt(), eq(false));
    }
}
