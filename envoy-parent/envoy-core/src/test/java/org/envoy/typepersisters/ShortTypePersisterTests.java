package org.envoy.typepersisters;

import org.junit.*;

public class ShortTypePersisterTests {
    private ShortTypePersister underTest;


    @Before
    public void setUp() {
        underTest = new ShortTypePersister();
    }


    @Test
    public void test_that_the_type_is_Short() {
        Assert.assertEquals(Short.class, underTest.getJavaType());
    }


    @Test
    public void test_that_toPrimitiveValue_returns_3_when_3_is_given() {
        //--Act
        final short actual = underTest.toPrimitiveValue(3);

        //--Assert
        Assert.assertEquals(3, actual);
    }


    @Test
    public void test_that_toPrimitiveValue_returns_10_when_10_is_given() {
        //--Act
        final short actual = underTest.toPrimitiveValue(10);

        //--Assert
        Assert.assertEquals(10, actual);
    }
}
