package org.envoy.typepersisters;

import org.junit.*;

public class ByteTypePersisterTests {
    private ByteTypePersister underTest;


    @Before
    public void setUp() {
        underTest = new ByteTypePersister();
    }


    @Test
    public void test_that_the_type_is_Byte() {
        Assert.assertEquals(Byte.class, underTest.getJavaType());
    }


    @Test
    public void test_that_toPrimitiveValue_returns_3_when_3_is_given() {
        //--Act
        final byte actual = underTest.toPrimitiveValue(3);

        //--Assert
        Assert.assertEquals(3, actual);
    }


    @Test
    public void test_that_toPrimitiveValue_returns_10_when_10_is_given() {
        //--Act
        final byte actual = underTest.toPrimitiveValue(10);

        //--Assert
        Assert.assertEquals(10, actual);
    }
}
