package org.envoy.typepersisters;

import org.junit.*;

public class LongTypePersisterTests {
    private LongTypePersister underTest;


    @Before
    public void setUp() {
        underTest = new LongTypePersister();
    }


    @Test
    public void test_that_the_type_is_Byte() {
        Assert.assertEquals(Long.class, underTest.getJavaType());
    }


    @Test
    public void test_that_toPrimitiveValue_returns_3_when_3_is_given() {
        //--Act
        final long actual = underTest.toPrimitiveValue(3);

        //--Assert
        Assert.assertEquals(3, actual);
    }


    @Test
    public void test_that_toPrimitiveValue_returns_10_when_10_is_given() {
        //--Act
        final long actual = underTest.toPrimitiveValue(10);

        //--Assert
        Assert.assertEquals(10, actual);
    }
}
