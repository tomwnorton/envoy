package org.envoy.typepersisters;

import org.junit.*;

import java.sql.*;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class AbstractNullableNumericTypePersisterTests {
    @Test
    public void test_that_set_calls_ps_setObject_with_the_correct_index_when_that_index_is_7() throws SQLException {
        //--Arrange
        final PreparedStatement ps = mock(PreparedStatement.class);

        final AbstractNullableNumericTypePersister<Integer> underTest = new AbstractNullableNumericTypePersister<Integer>(
                Integer.class) {

            @Override
            protected Integer toPrimitiveValue(final Number value) {
                return null;
            }
        };

        //--Act
        underTest.setImpl(ps, 7, null);

        //--Assert
        verify(ps).setObject(eq(7), any());
    }


    @Test
    public void test_that_set_calls_ps_setObject_with_the_correct_index_when_that_index_is_22() throws SQLException {
        //--Arrange
        final PreparedStatement ps = mock(PreparedStatement.class);

        final AbstractNullableNumericTypePersister<Integer> underTest = new AbstractNullableNumericTypePersister<Integer>(
                Integer.class) {

            @Override
            protected Integer toPrimitiveValue(final Number value) {
                return null;
            }
        };

        //--Act
        underTest.setImpl(ps, 22, null);

        //--Assert
        verify(ps).setObject(eq(22), any());
    }


    @Test
    public void test_that_set_calls_ps_setObject_with_the_correct_value_when_that_value_is_37() throws SQLException {
        //--Arrange
        final PreparedStatement ps = mock(PreparedStatement.class);

        final AbstractNullableNumericTypePersister<Integer> underTest = new AbstractNullableNumericTypePersister<Integer>(
                Integer.class) {

            @Override
            protected Integer toPrimitiveValue(final Number value) {
                return null;
            }
        };

        //--Act
        underTest.setImpl(ps, 0, 37);

        //--Assert
        verify(ps).setObject(anyInt(), eq(37));
    }


    @Test
    public void test_that_set_calls_ps_setObject_with_the_correct_value_when_that_value_is_null() throws SQLException {
        //--Arrange
        final PreparedStatement ps = mock(PreparedStatement.class);

        final AbstractNullableNumericTypePersister<Integer> underTest = new AbstractNullableNumericTypePersister<Integer>(
                Integer.class) {
            @Override
            protected Integer toPrimitiveValue(final Number value) {
                return null;
            }
        };

        //--Act
        underTest.setImpl(ps, 0, null);

        //--Assert
        verify(ps).setObject(anyInt(), eq(null));
    }


    @Test
    public void test_that_get_calls_rs_getObject_with_the_correct_fieldName_when_that_fieldName_is_COLUMN() throws
            SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);

        final AbstractNullableNumericTypePersister<Integer> underTest = new AbstractNullableNumericTypePersister<Integer>(
                Integer.class) {
            @Override
            protected Integer toPrimitiveValue(final Number value) {
                return null;
            }
        };

        //--Act
        underTest.get(rs, "COLUMN");

        //--Assert
        verify(rs).getObject("COLUMN");
    }


    @Test
    public void test_that_get_calls_rs_getObject_with_the_correct_fieldName_when_that_fieldName_is_FIELD() throws SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);

        final AbstractNullableNumericTypePersister<Integer> underTest = new AbstractNullableNumericTypePersister<Integer>(
                Integer.class) {
            @Override
            protected Integer toPrimitiveValue(final Number value) {
                return null;
            }
        };

        //--Act
        underTest.get(rs, "FIELD");

        //--Assert
        verify(rs).getObject("FIELD");
    }

    private class Wrapper {
        private boolean _wasCalled = false;
    }


    @Test
    public void test_that_get_calls_toPrimitiveValue_when_rs_getObject_returns_a_non_null_value() throws SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);
        when(rs.getObject(anyString())).thenReturn(7);

        final Wrapper wrapper = new Wrapper();

        final AbstractNullableNumericTypePersister<Integer> underTest = new AbstractNullableNumericTypePersister<Integer>(
                Integer.class) {
            @Override
            protected Integer toPrimitiveValue(final Number value) {
                wrapper._wasCalled = true;
                return null;
            }
        };

        //--Act
        underTest.get(rs, "FIELD");

        //--Assert
        Assert.assertTrue(wrapper._wasCalled);
    }


    @Test
    public void test_that_get_calls_toPrimitiveValue_passing_7_as_the_parameter_when_7_is_the_result_of_rs_getObject()
            throws SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);
        when(rs.getObject(anyString())).thenReturn(7);

        final AbstractNullableNumericTypePersister<Integer> underTest = new AbstractNullableNumericTypePersister<Integer>(
                Integer.class) {
            @Override
            protected Integer toPrimitiveValue(final Number value) {
                Assert.assertEquals(7, value);
                return null;
            }
        };

        //--Act
        underTest.get(rs, "FIELD");
    }


    @Test
    public void test_that_get_calls_toPrimitiveValue_passing_11_as_the_parameter_when_11_is_the_result_of_rs_getObject()
            throws SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);
        when(rs.getObject(anyString())).thenReturn(11);

        final AbstractNullableNumericTypePersister<Integer> underTest = new AbstractNullableNumericTypePersister<Integer>(
                Integer.class) {
            @Override
            protected Integer toPrimitiveValue(final Number value) {
                Assert.assertEquals(11, value);
                return null;
            }
        };

        //--Act
        underTest.get(rs, "FIELD");
    }


    @Test
    public void test_that_get_does_not_call_toPrimitiveValue_when_rs_getObject_returns_null() throws SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);
        when(rs.getObject(anyString())).thenReturn(null);

        final Wrapper wrapper = new Wrapper();

        final AbstractNullableNumericTypePersister<Integer> underTest = new AbstractNullableNumericTypePersister<Integer>(
                Integer.class) {
            @Override
            protected Integer toPrimitiveValue(final Number value) {
                wrapper._wasCalled = true;
                return null;
            }
        };

        //--Act
        underTest.get(rs, "FIELD");

        //--Assert
        Assert.assertFalse(wrapper._wasCalled);
    }


    @Test
    public void test_that_get_returns_11_when_toPrimitveType_returns_11() throws SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);
        when(rs.getObject(anyString())).thenReturn(11);

        final int returnValue = 11;

        final AbstractNullableNumericTypePersister<Integer> underTest = new AbstractNullableNumericTypePersister<Integer>(
                Integer.class) {
            @Override
            protected Integer toPrimitiveValue(final Number value) {
                return returnValue;
            }
        };

        //--Act
        Assert.assertSame(returnValue, underTest.get(rs, "FIELD"));
    }
}
