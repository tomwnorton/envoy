package org.envoy.typepersisters;

import org.junit.*;

import java.sql.*;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class StringTypePersisterTests {
    private StringTypePersister underTest;


    @Before
    public void setUp() {
        underTest = new StringTypePersister();
    }


    @Test
    public void test_that_the_javaType_is_string() {
        //--Assert
        Assert.assertEquals(String.class, underTest.getJavaType());
    }


    @Test
    public void test_that_get_calls_rs_getString_when_the_fieldName_is_COLUMN() throws SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);

        //--Act
        underTest.get(rs, "COLUMN");

        //--Assert
        verify(rs).getString("COLUMN");
    }


    @Test
    public void test_that_get_calls_rs_getString_when_the_fieldName_is_FIELD() throws SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);

        //--Act
        underTest.get(rs, "FIELD");

        //--Assert
        verify(rs).getString("FIELD");
    }


    @Test
    public void test_that_get_returns_hello_when_rs_getString_returns_hello() throws SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);
        when(rs.getString(anyString())).thenReturn("hello");

        //--Act
        final String result = underTest.get(rs, null);

        //--Assert
        Assert.assertEquals("hello", result);
    }


    @Test
    public void test_that_get_returns_world_when_rs_getString_returns_world() throws SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);
        when(rs.getString(anyString())).thenReturn("world");

        //--Act
        final String result = underTest.get(rs, null);

        //--Assert
        Assert.assertEquals("world", result);
    }


    @Test
    public void test_that_setImpl_calls_ps_setString_when_fieldIndex_is_1() throws SQLException {
        //--Arrange
        final PreparedStatement ps = mock(PreparedStatement.class);

        //--Act
        underTest.setImpl(ps, 1, null);

        //--Assert
        verify(ps).setString(eq(1), anyString());
    }


    @Test
    public void test_that_setImpl_calls_ps_setString_when_fieldIndex_is_8() throws SQLException {
        //--Arrange
        final PreparedStatement ps = mock(PreparedStatement.class);

        //--Act
        underTest.setImpl(ps, 8, null);

        //--Assert
        verify(ps).setString(eq(8), anyString());
    }


    @Test
    public void test_that_setImpl_calls_ps_setString_when_value_is_hello() throws SQLException {
        //--Arrange
        final PreparedStatement ps = mock(PreparedStatement.class);

        //--Act
        underTest.setImpl(ps, 0, "hello");

        //--Assert
        verify(ps).setString(anyInt(), eq("hello"));
    }


    @Test
    public void test_that_setImpl_calls_ps_setString_when_value_is_world() throws SQLException {
        //--Arrange
        final PreparedStatement ps = mock(PreparedStatement.class);

        //--Act
        underTest.setImpl(ps, 0, "world");

        //--Assert
        verify(ps).setString(anyInt(), eq("world"));
    }
}
