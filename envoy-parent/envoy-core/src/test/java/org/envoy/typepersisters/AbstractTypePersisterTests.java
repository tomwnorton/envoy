package org.envoy.typepersisters;

import org.junit.*;

import java.sql.*;
import java.util.*;

import static org.mockito.Mockito.*;

public class AbstractTypePersisterTests {
    private class Wrapper {
        public boolean isCalled = false;
    }


    @Test
    public void test_that_set_calls_setImpl() throws SQLException {
        // --Arrange
        final Wrapper wrapper = new Wrapper();

        final AbstractTypePersister<Calendar> underTest = new AbstractTypePersister<Calendar>(Calendar.class) {
            @Override
            public Calendar get(final ResultSet rs, final String fieldName) {
                return null;
            }


            @Override
            protected void setImpl(final PreparedStatement ps, final int fieldIndex, final Calendar value) {
                wrapper.isCalled = true;
            }
        };

        // --Act
        underTest.set(null, 0, null);

        // --Assert
        Assert.assertTrue(wrapper.isCalled);
    }


    @Test
    public void test_that_set_calls_setImpl_with_the_correct_ResultSet_instance() throws SQLException {
        // --Arrange
        final PreparedStatement expectedPs = mock(PreparedStatement.class);

        final AbstractTypePersister<Calendar> underTest = new AbstractTypePersister<Calendar>(Calendar.class) {
            @Override
            public Calendar get(final ResultSet rs, final String fieldName) {
                return null;
            }


            @Override
            protected void setImpl(final PreparedStatement ps, final int fieldIndex, final Calendar value) {
                Assert.assertSame(expectedPs, ps);
            }
        };

        // --Act
        underTest.set(expectedPs, 0, null);
    }


    @Test
    public void test_that_set_calls_setImpl_with_the_correct_value_instance() throws SQLException {
        // --Arrange
        final Calendar expectedCalendar = new GregorianCalendar();

        final AbstractTypePersister<Calendar> underTest = new AbstractTypePersister<Calendar>(Calendar.class) {
            @Override
            public Calendar get(final ResultSet rs, final String fieldName) {
                return null;
            }


            @Override
            protected void setImpl(final PreparedStatement ps, final int fieldIndex, final Calendar value) {
                Assert.assertSame(expectedCalendar, value);
            }
        };

        // --Act
        underTest.set(null, 0, expectedCalendar);
    }


    @Test
    public void test_that_set_calls_setImpl_with_the_fieldIndex_1_when_that_is_the_fieldName_given() throws SQLException {
        // --Arrange
        final AbstractTypePersister<Calendar> underTest = new AbstractTypePersister<Calendar>(Calendar.class) {
            @Override
            public Calendar get(final ResultSet rs, final String fieldName) {
                return null;
            }


            @Override
            protected void setImpl(final PreparedStatement ps, final int fieldIndex, final Calendar value) {
                Assert.assertEquals(1, fieldIndex);
            }
        };

        // --Act
        underTest.set(null, 1, null);
    }


    @Test
    public void test_that_set_calls_setImpl_with_the_fieldName_2_when_that_is_the_fieldName_given() throws SQLException {
        // --Arrange
        final AbstractTypePersister<Calendar> underTest = new AbstractTypePersister<Calendar>(Calendar.class) {
            @Override
            public Calendar get(final ResultSet rs, final String fieldName) {
                return null;
            }


            @Override
            protected void setImpl(final PreparedStatement ps, final int fieldIndex, final Calendar value) {
                Assert.assertEquals(2, fieldIndex);
            }
        };

        // --Act
        underTest.set(null, 2, null);
    }
}
