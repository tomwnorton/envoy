package org.envoy.typepersisters;

import org.junit.*;

public class DoubleTypePersisterTests {
    private DoubleTypePersister underTest;


    @Before
    public void setUp() {
        underTest = new DoubleTypePersister();
    }


    @Test
    public void test_that_the_type_is_Byte() {
        Assert.assertEquals(Double.class, underTest.getJavaType());
    }


    @Test
    public void test_that_toPrimitiveValue_returns_3_when_3_is_given() {
        //--Act
        final double actual = underTest.toPrimitiveValue(3);

        //--Assert
        Assert.assertEquals(3.0, actual, 0.01);
    }


    @Test
    public void test_that_toPrimitiveValue_returns_10_when_10_is_given() {
        //--Act
        final double actual = underTest.toPrimitiveValue(10);

        //--Assert
        Assert.assertEquals(10.0, actual, 0.01);
    }
}
