package org.envoy.typepersisters;

import org.junit.*;

import java.sql.*;
import java.util.*;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class CalendarTypePersisterTests {
    private CalendarTypePersister underTest;


    @Before
    public void setUp() {
        underTest = new CalendarTypePersister();
    }


    @Test
    public void test_that_javaType_is_Calendar() {
        Assert.assertEquals(Calendar.class, underTest.getJavaType());
    }


    @Test
    public void test_that_get_calls_rs_getTimestamp_with_COLUMN_when_that_is_the_fieldName() throws SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);
        when(rs.getTimestamp(anyString())).thenReturn(new Timestamp(System.currentTimeMillis()));

        //--Act
        underTest.get(rs, "COLUMN");

        //--Assert
        verify(rs).getTimestamp("COLUMN");
    }


    @Test
    public void test_that_get_calls_rs_getTimestamp_with_FIELD_when_that_is_the_fieldName() throws SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);
        when(rs.getTimestamp(anyString())).thenReturn(new Timestamp(System.currentTimeMillis()));

        //--Act
        underTest.get(rs, "FIELD");

        //--Assert
        verify(rs).getTimestamp("FIELD");
    }


    @Test
    public void test_that_get_returns_a_calendar_that_is_based_on_the_timestamp_with_1_millisecond_returned_by_rs_getTimestamp()
            throws SQLException {
        //--Arrange
        final Timestamp ts = new Timestamp(1L);
        final ResultSet rs = mock(ResultSet.class);
        when(rs.getTimestamp(anyString())).thenReturn(ts);

        //--Act
        final Calendar result = underTest.get(rs, null);

        //--Assert
        Assert.assertSame(ts.getTime(), result.getTimeInMillis());
    }


    @Test
    public void test_that_get_returns_a_calendar_that_is_based_on_the_timestamp_with_23_milliseconds_returned_by_rs_getTimestamp()
            throws SQLException {
        //--Arrange
        final Timestamp ts = new Timestamp(23L);
        final ResultSet rs = mock(ResultSet.class);
        when(rs.getTimestamp(anyString())).thenReturn(ts);

        //--Act
        final Calendar result = underTest.get(rs, null);

        //--Assert
        Assert.assertSame(ts.getTime(), result.getTimeInMillis());
    }


    @Test
    public void test_that_setImpl_calls_ps_setTimestamp_with_1_when_that_is_the_fieldIndex() throws SQLException {
        //--Arrange
        final PreparedStatement ps = mock(PreparedStatement.class);

        //--Act
        underTest.setImpl(ps, 1, new GregorianCalendar());

        //--Assert
        verify(ps).setTimestamp(eq(1), any(Timestamp.class));
    }


    @Test
    public void test_that_setImpl_calls_ps_setTimestamp_with_2_when_that_is_the_fieldIndex() throws SQLException {
        //--Arrange
        final PreparedStatement ps = mock(PreparedStatement.class);

        //--Act
        underTest.setImpl(ps, 2, new GregorianCalendar());

        //--Assert
        verify(ps).setTimestamp(eq(2), any(Timestamp.class));
    }


    @Test
    public void test_that_stImpl_calls_ps_setTimestamp_with_timestamp_of_1_millisecond_when_the_calendar_passed_in_has_time_of_1_millisecond()
            throws SQLException {
        //--Arrange
        final PreparedStatement ps = mock(PreparedStatement.class);
        final GregorianCalendar gc = new GregorianCalendar();
        gc.setTimeInMillis(1L);

        //--Act
        underTest.setImpl(ps, 0, gc);

        //--Assert
        verify(ps).setTimestamp(anyInt(), eq(new Timestamp(1L)));
    }


    @Test
    public void test_that_stImpl_calls_ps_setTimestamp_with_timestamp_of_99_millisecond_when_the_calendar_passed_in_has_time_of_1_millisecond()
            throws SQLException {
        //--Arrange
        final PreparedStatement ps = mock(PreparedStatement.class);
        final GregorianCalendar gc = new GregorianCalendar();
        gc.setTimeInMillis(99L);

        //--Act
        underTest.setImpl(ps, 0, gc);

        //--Assert
        verify(ps).setTimestamp(anyInt(), eq(new Timestamp(99L)));
    }
}
