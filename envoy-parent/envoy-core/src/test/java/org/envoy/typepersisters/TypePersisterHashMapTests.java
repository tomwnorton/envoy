package org.envoy.typepersisters;

import org.junit.*;

import java.util.*;

public class TypePersisterHashMapTests {
    private TypePersisterHashMap underTest;


    @Before
    public void setUp() {
        underTest = new TypePersisterHashMap();
    }


    @Test
    public void test_that_put_adds_the_IntegerTypePersister_correctly() {
        //--Arrange
        final IntegerTypePersister persister = new IntegerTypePersister();

        //--Act
        underTest.put(persister);

        //--Assert
        Assert.assertSame(persister, underTest.get(Integer.class));
    }


    @Test
    public void test_that_put_adds_the_StringTypePersister_correctly() {
        //--Arrange
        final StringTypePersister persister = new StringTypePersister();

        //--Act
        underTest.put(persister);

        //--Assert
        Assert.assertSame(persister, underTest.get(String.class));
    }


    @Test
    public void test_that_2arg_put_adds_IntegerTypePersister_to_int() {
        //--Arrange
        final IntegerTypePersister persister = new IntegerTypePersister();

        //--Act
        underTest.put(Integer.class, persister);

        //--Assert
        Assert.assertSame(persister, underTest.get(int.class));
    }


    @Test
    public void test_that_2arg_put_doesnt_adds_StringTypePersister_to_int() {
        //--Arrange
        final StringTypePersister persister = new StringTypePersister();

        //--Act
        underTest.put(String.class, persister);

        //--Assert
        Assert.assertNull(underTest.get(int.class));
    }


    @Test
    public void test_that_2arg_put_doesnt_adds_CalendarTypePersister_to_int() {
        //--Arrange
        final CalendarTypePersister persister = new CalendarTypePersister();

        //--Act
        underTest.put(Calendar.class, persister);

        //--Assert
        Assert.assertNull(underTest.get(int.class));
    }


    @Test
    public void test_that_2arg_put_adds_LongTypePersister_to_long() {
        //--Arrange
        final LongTypePersister persister = new LongTypePersister();

        //--Act
        underTest.put(Long.class, persister);

        //--Assert
        Assert.assertSame(persister, underTest.get(long.class));
    }


    @Test
    public void test_that_2arg_put_doesnt_adds_StringTypePersister_to_long() {
        //--Arrange
        final StringTypePersister persister = new StringTypePersister();

        //--Act
        underTest.put(String.class, persister);

        //--Assert
        Assert.assertNull(underTest.get(long.class));
    }


    @Test
    public void test_that_2arg_put_doesnt_adds_CalendarTypePersister_to_long() {
        //--Arrange
        final CalendarTypePersister persister = new CalendarTypePersister();

        //--Act
        underTest.put(Calendar.class, persister);

        //--Assert
        Assert.assertNull(underTest.get(long.class));
    }


    @Test
    public void test_that_2arg_put_adds_ShortTypePersister_to_short() {
        //--Arrange
        final ShortTypePersister persister = new ShortTypePersister();

        //--Act
        underTest.put(Short.class, persister);

        //--Assert
        Assert.assertSame(persister, underTest.get(short.class));
    }


    @Test
    public void test_that_2arg_put_adds_ByteTypePersister_to_byte() {
        //--Arrange
        final ByteTypePersister persister = new ByteTypePersister();

        //--Act
        underTest.put(Byte.class, persister);

        //--Assert
        Assert.assertSame(persister, underTest.get(byte.class));
    }


    @Test
    public void test_that_2arg_put_adds_DoubleTypePersister_to_double() {
        //--Arrange
        final DoubleTypePersister persister = new DoubleTypePersister();

        //--Act
        underTest.put(Double.class, persister);

        //--Assert
        Assert.assertSame(persister, underTest.get(double.class));
    }


    @Test
    public void test_that_2arg_put_adds_CharacterTypePersister_to_char() {
        //--Arrange
        final CharacterTypePersister persister = new CharacterTypePersister();

        //--Act
        underTest.put(Character.class, persister);

        //--Assert
        Assert.assertSame(persister, underTest.get(char.class));
    }
}
