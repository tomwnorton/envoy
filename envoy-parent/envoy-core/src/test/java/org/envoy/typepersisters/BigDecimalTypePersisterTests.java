package org.envoy.typepersisters;

import org.junit.*;

import java.math.*;
import java.sql.*;

import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class BigDecimalTypePersisterTests {
    private BigDecimalTypePersister underTest;


    @Before
    public void setUp() {
        underTest = new BigDecimalTypePersister();
    }


    @Test
    public void test_that_get_returns_the_correct_BigDecimal_for_the_field_named_price() throws SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);
        final BigDecimal expected = new BigDecimal(12);
        when(rs.getBigDecimal("price")).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.get(rs, "price"));
    }


    @Test
    public void test_that_get_returns_the_correct_BigDecimal_for_the_field_named_cost() throws SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);
        final BigDecimal expected = new BigDecimal(12);
        when(rs.getBigDecimal("cost")).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.get(rs, "cost"));
    }


    @Test
    public void test_that_setImpl_sets_the_correct_BigDecimal_on_the_seventh_index() throws SQLException {
        //--Arrange
        final PreparedStatement ps = mock(PreparedStatement.class);
        final BigDecimal expected = new BigDecimal(12);

        //--Act
        underTest.setImpl(ps, 7, expected);

        //--Assert
        verify(ps).setBigDecimal(eq(7), same(expected));
    }


    @Test
    public void test_that_setImpl_sets_the_correct_BigDecimal_on_the_15th_index() throws SQLException {
        //--Arrange
        final PreparedStatement ps = mock(PreparedStatement.class);
        final BigDecimal expected = new BigDecimal(12);

        //--Act
        underTest.setImpl(ps, 15, expected);

        //--Assert
        verify(ps).setBigDecimal(eq(15), same(expected));
    }
}
