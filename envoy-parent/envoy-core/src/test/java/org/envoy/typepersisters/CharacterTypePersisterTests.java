package org.envoy.typepersisters;

import org.junit.*;

import java.sql.*;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.*;

public class CharacterTypePersisterTests {
    private CharacterTypePersister underTest;


    @Before
    public void setUp() {
        underTest = new CharacterTypePersister();
    }


    @Test
    public void test_that_the_javaType_is_string() {
        //--Assert
        Assert.assertEquals(Character.class, underTest.getJavaType());
    }


    @Test
    public void test_that_get_returns_null_when_the_string_is_blank() throws SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);
        when(rs.getString(anyString())).thenReturn("");

        //--Act
        final Character result = underTest.get(rs, "COLUMN");

        //--Assert
        Assert.assertNull(result);
    }


    @Test
    public void test_that_get_returns_null_when_the_string_is_null() throws SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);
        when(rs.getString(anyString())).thenReturn(null);

        //--Act
        final Character result = underTest.get(rs, "COLUMN");

        //--Assert
        Assert.assertNull(result);
    }


    @Test
    public void test_that_get_calls_rs_getString_when_the_fieldName_is_COLUMN() throws SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);

        //--Act
        underTest.get(rs, "COLUMN");

        //--Assert
        verify(rs).getString("COLUMN");
    }


    @Test
    public void test_that_get_calls_rs_getString_when_the_fieldName_is_FIELD() throws SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);

        //--Act
        underTest.get(rs, "FIELD");

        //--Assert
        verify(rs).getString("FIELD");
    }


    @Test
    public void test_that_get_returns_a_when_rs_getString_returns_a() throws SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);
        when(rs.getString(anyString())).thenReturn("a");

        //--Act
        final char result = underTest.get(rs, null);

        //--Assert
        Assert.assertEquals('a', result);
    }


    @Test
    public void test_that_get_returns_b_when_rs_getString_returns_b() throws SQLException {
        //--Arrange
        final ResultSet rs = mock(ResultSet.class);
        when(rs.getString(anyString())).thenReturn("b");

        //--Act
        final char result = underTest.get(rs, null);

        //--Assert
        Assert.assertEquals('b', result);
    }


    @Test
    public void test_that_setImpl_calls_ps_setString_with_null_when_value_is_null() throws SQLException {
        //--Arrange
        final PreparedStatement ps = mock(PreparedStatement.class);

        //--Act
        underTest.setImpl(ps, 1, null);

        //--Assert
        verify(ps).setString(anyInt(), isNull(String.class));
    }


    @Test
    public void test_that_setImpl_calls_ps_setString_when_fieldIndex_is_1() throws SQLException {
        //--Arrange
        final PreparedStatement ps = mock(PreparedStatement.class);

        //--Act
        underTest.setImpl(ps, 1, null);

        //--Assert
        verify(ps).setString(eq(1), anyString());
    }


    @Test
    public void test_that_setImpl_calls_ps_setString_when_fieldIndex_is_8() throws SQLException {
        //--Arrange
        final PreparedStatement ps = mock(PreparedStatement.class);

        //--Act
        underTest.setImpl(ps, 8, null);

        //--Assert
        verify(ps).setString(eq(8), anyString());
    }


    @Test
    public void test_that_setImpl_calls_ps_setString_when_value_is_a() throws SQLException {
        //--Arrange
        final PreparedStatement ps = mock(PreparedStatement.class);

        //--Act
        underTest.setImpl(ps, 0, 'a');

        //--Assert
        verify(ps).setString(anyInt(), eq("a"));
    }


    @Test
    public void test_that_setImpl_calls_ps_setString_when_value_is_b() throws SQLException {
        //--Arrange
        final PreparedStatement ps = mock(PreparedStatement.class);

        //--Act
        underTest.setImpl(ps, 0, 'b');

        //--Assert
        verify(ps).setString(anyInt(), eq("b"));
    }
}
