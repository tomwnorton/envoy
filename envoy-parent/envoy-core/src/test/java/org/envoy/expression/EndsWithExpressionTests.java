package org.envoy.expression;

import org.junit.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class EndsWithExpressionTests extends BinaryExpressionTestBase {
    @Override
    protected AbstractExpression<?> createTestObject(
            final Expression lhs,
            final Expression rhs,
            final ExpressionFactory expressionFactory) {
        return new EndsWithExpression(lhs, rhs, expressionFactory);
    }


    @Test
    public void test_that_accept_calls_visitEndsWith() {
        //--Act
        underTest.accept(expressionVisitor);

        //--Assert
        verify(expressionVisitor).visitEndsWith(same(lhs), same(rhs));
    }
}
