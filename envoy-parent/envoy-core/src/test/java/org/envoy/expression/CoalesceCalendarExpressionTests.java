package org.envoy.expression;

import org.junit.*;

import java.util.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class CoalesceCalendarExpressionTests extends ExpressionTestBase<AbstractExpression<?>> {
    private List<Expression> exprs;


    @Override
    protected AbstractExpression<?> createTestObjectForExpressionFactoryTest(final ExpressionFactory expressionFactory) {
        exprs = new ArrayList<Expression>();
        return new CoalesceCalendarExpression(exprs, expressionFactory);
    }


    @Test
    public void test_that_accept_calls_vistCoalesce() {
        //--Act
        underTest.accept(expressionVisitor);

        //--Assert
        verify(expressionVisitor).visitCoalesce(same(exprs));
    }
}
