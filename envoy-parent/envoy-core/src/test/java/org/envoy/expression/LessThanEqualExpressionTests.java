package org.envoy.expression;

import org.junit.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class LessThanEqualExpressionTests {
    @Test
    public void test_that_the_expressionFactory_is_handed_to_the_super_class() {
        //--Arrange
        final ExpressionFactory expressionFactory = mock(ExpressionFactory.class);
        final LessThanEqualExpression underTest = new LessThanEqualExpression(null, null, expressionFactory);

        //--Assert
        Assert.assertSame(expressionFactory, underTest.getExpressionFactory());
    }


    @Test
    public void test_that_accept_calls_visitLessThanEqualTo_with_the_correct_arguments() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);
        final ExpressionVisitor visitor = mock(ExpressionVisitor.class);
        final LessThanEqualExpression underTest = new LessThanEqualExpression(lhs, rhs, null);

        //--Act
        underTest.accept(visitor);

        //--Assert
        verify(visitor).visitLessThanEqual(same(lhs), same(rhs));
    }
}
