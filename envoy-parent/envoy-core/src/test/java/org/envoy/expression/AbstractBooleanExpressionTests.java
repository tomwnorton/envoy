package org.envoy.expression;

import org.junit.*;

import java.util.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class AbstractBooleanExpressionTests {
    private class TestableBooleanExpression extends AbstractBooleanExpression {
        public TestableBooleanExpression(final ExpressionFactory expressionFactory) {
            super(expressionFactory);
        }


        @Override
        public void accept(final ExpressionVisitor visitor) {
        }
    }

    private ExpressionFactory expressionFactory;
    private TestableBooleanExpression underTest;


    @Before
    public void setUp() {
        expressionFactory = mock(ExpressionFactory.class);
        underTest = new TestableBooleanExpression(this.expressionFactory);
    }


    @Test
    public void test_that_and_returns_the_correct_AndExpression() {
        //--Arrange
        final AndExpression andExpression = mock(AndExpression.class);
        final BooleanExpression rhs = mock(BooleanExpression.class);
        when(expressionFactory.createAndExpression(same(underTest), same(rhs))).thenReturn(andExpression);

        //--Assert
        Assert.assertSame(andExpression, underTest.and(rhs));
    }


    @Test
    public void test_that_or_returns_the_correct_OrExpression() {
        //--Arrange
        final OrExpression orExpression = mock(OrExpression.class);
        final BooleanExpression rhs = mock(BooleanExpression.class);
        when(this.expressionFactory.createOrExpression(same(this.underTest), same(rhs))).thenReturn(orExpression);

        //--Assert
        Assert.assertSame(orExpression, this.underTest.or(rhs));
    }


    @Test
    public void test_that_not_returns_the_correct_NotExpression() {
        //--Arrange
        final NotExpression notExpression = mock(NotExpression.class);
        when(expressionFactory.createNotExpression(same(underTest))).thenReturn(notExpression);

        //--Assert
        Assert.assertSame(notExpression, underTest.not());
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_coalesceInternal_returns_the_correct_CoalesceBooleanExpression_instance() {
        //--Arrange
        final GenericExpression<Boolean> firstExpr = mock(GenericExpression.class);
        final GenericExpression<Boolean> secondExpr = mock(GenericExpression.class);
        final List<GenericExpression<Boolean>> exprs = Arrays.<GenericExpression<Boolean>>asList(
                underTest,
                firstExpr,
                secondExpr);
        final CoalesceBooleanExpression expected = mock(CoalesceBooleanExpression.class);
        when(this.expressionFactory.createCoalesceBooleanExpression(exprs)).thenReturn(expected);

        //--Assert
        Assert.assertSame(
                expected,
                underTest.coalesceInternal(Arrays.<GenericExpression<Boolean>>asList(firstExpr, secondExpr)));
    }
}
