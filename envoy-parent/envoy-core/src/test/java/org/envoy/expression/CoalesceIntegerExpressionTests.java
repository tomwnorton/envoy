package org.envoy.expression;

import org.junit.*;

import java.util.*;

import static org.mockito.Mockito.*;

public class CoalesceIntegerExpressionTests extends ExpressionTestBase<CoalesceIntegerExpression<Number>> {
    private final List<Expression> expectedList = new ArrayList<Expression>();


    @Override
    protected CoalesceIntegerExpression<Number> createTestObjectForExpressionFactoryTest(final ExpressionFactory expressionFactory) {
        return new CoalesceIntegerExpression<Number>(expectedList, expressionFactory);
    }


    @Test
    public void test_that_accept_calls_visitCoalesce_with_the_correct_arguments() {
        //--Act
        underTest.accept(expressionVisitor);

        //--Assert
        verify(expressionVisitor).visitCoalesce(expectedList);
    }
}
