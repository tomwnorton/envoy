package org.envoy.expression;

import org.junit.*;
import org.junit.experimental.theories.*;
import org.junit.runner.*;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

@RunWith(Theories.class)
public class CommonFieldExpressionLogicImplTests {
    @DataPoints
    public static int[] ages = new int[]{7, 9};

    @DataPoints
    public static String[] names = new String[]{"John", "Tim"};

    @DataPoints
    public static boolean[] activeStates = new boolean[]{true, false};


    @Theory
    public void test_that_age_can_be_set_when_testComponent_is_null(int age) {
        //--Arrange
        TestEntityExpression expr = new TestEntityExpression();
        TestEntity entity = new TestEntity();

        //--Act
        expr.getTestComponent().getAge().setFromRootEntity(entity, age);

        //--Assert
        assertThat(entity.getTestComponent().getAge(), is(age));
    }


    @Theory
    public void test_that_age_can_be_read(int age) {
        //--Arrange
        TestEntityExpression expr = new TestEntityExpression();
        TestEntity entity = new TestEntity();
        entity.setTestComponent(new TestComponent());
        entity.getTestComponent().setAge(age);

        //--Assert
        assertThat(expr.getTestComponent().getAge().getFromRootEntity(entity), is((Object) age));
    }


    @Theory
    public void test_that_name_can_be_set_when_testComponent_is_null(String name) {
        //--Arrange
        TestEntityExpression expr = new TestEntityExpression();
        TestEntity entity = new TestEntity();

        //--Act
        expr.getTestComponent().getName().setFromRootEntity(entity, name);

        //--Assert
        assertThat(entity.getTestComponent().getName(), is(name));
    }


    @Theory
    public void test_that_name_can_be_read(String name) {
        //--Arrange
        TestEntityExpression expr = new TestEntityExpression();
        TestEntity entity = new TestEntity();
        entity.setTestComponent(new TestComponent());
        entity.getTestComponent().setName(name);

        //--Assert
        assertThat(expr.getTestComponent().getName().getFromRootEntity(entity), is((Object) name));
    }


    @Theory
    public void test_that_active_can_be_set_when_testComponent_is_null(boolean active) {
        //--Arrange
        TestEntityExpression expr = new TestEntityExpression();
        TestEntity entity = new TestEntity();

        //--Act
        expr.getTestComponent().isActive().setFromRootEntity(entity, active);

        //--Assert
        assertThat(entity.getTestComponent().isActive(), is(active));
    }


    @Theory
    public void test_that_active_can_be_read(boolean active) {
        //--Arrange
        TestEntityExpression expr = new TestEntityExpression();
        TestEntity entity = new TestEntity();
        entity.setTestComponent(new TestComponent());
        entity.getTestComponent().setActive(active);

        //--Assert
        assertThat(expr.getTestComponent().isActive().getFromRootEntity(entity), is((Object) active));
    }


    @Test
    public void test_that_setFromRootEntity_does_not_replace_testComponent_when_it_is_already_not_null() {
        //--Arrange
        TestEntityExpression expr = new TestEntityExpression();
        TestEntity entity = new TestEntity();
        TestComponent testComponent = new TestComponent();
        entity.setTestComponent(testComponent);

        //--Act
        expr.getTestComponent().getAge().setFromRootEntity(entity, 1);

        //--Assert
        assertThat(testComponent.getAge(), is(1));
        assertThat(entity.getTestComponent(), is(sameInstance(testComponent)));
    }


    @Test
    public void test_that_setRootEntity_does_not_instantiate_testComponent_when_testComponent_is_null_and_so_is_the_new_value_of_the_property_on_it() {
        //--Arrange
        TestEntityExpression expr = new TestEntityExpression();
        TestEntity entity = new TestEntity();

        //--Act
        expr.getTestComponent().getName().setFromRootEntity(entity, null);

        //--Assert
        assertNull(entity.getTestComponent());
    }


    @Test
    public void test_that_getFromRootEntity_returns_null_when_the_component_is_null() {
        //--Arrange
        TestEntity entity = new TestEntity();
        TestEntityExpression expr = new TestEntityExpression();

        //--Assert
        assertThat(expr.getTestComponent().getAge().getFromRootEntity(entity), is((Object) null));
    }


    static class TestEntity {
        private TestComponent _testComponent;


        public TestComponent getTestComponent() {
            return _testComponent;
        }


        public void setTestComponent(TestComponent testComponent) {
            _testComponent = testComponent;
        }
    }


    static class TestEntityExpression extends AbstractEntityExpression<TestEntity> {
        private TestComponentExpression _testComponent = new TestComponentExpression(this);


        public TestEntityExpression() {
            super(TestEntity.class);
        }


        public TestComponentExpression getTestComponent() {
            return this._testComponent;
        }
    }


    static class TestComponent {
        private Integer _age;
        private String _name;
        private Boolean _active;


        public Integer getAge() {
            return _age;
        }


        public void setAge(Integer age) {
            _age = age;
        }


        public String getName() {
            return _name;
        }


        public void setName(String name) {
            _name = name;
        }


        public Boolean isActive() {
            return _active;
        }


        public void setActive(Boolean active) {
            _active = active;
        }
    }


    static class TestComponentExpression extends AbstractComponentExpression<TestComponent> {
        private IntegerFieldExpression _age = new IntegerFieldExpression(this, "AgeInYears");
        private StringFieldExpression _name = new StringFieldExpression(this, "FullName");
        private BooleanFieldExpression _active = new BooleanFieldExpression(this, "Active");


        public TestComponentExpression(ModelExpression parent) {
            super(parent, TestComponent.class);
        }


        public IntegerFieldExpression getAge() {
            return this._age;
        }


        public StringFieldExpression getName() {
            return this._name;
        }


        public BooleanFieldExpression isActive() {
            return this._active;
        }
    }
}
