package org.envoy.expression;

import org.junit.*;
import org.mockito.*;

import java.io.*;
import java.util.*;

import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class AbstractObjectInstanceExpressionTests {

    @Mock
    private ExpressionFactory expressionFactory;

    private class UnderTest extends AbstractObjectInstanceExpression<Serializable, Serializable, UnderTest> {
        private final UnderTest retValue;


        UnderTest(final ExpressionFactory expressionFactory, final UnderTest retValue) {
            super(expressionFactory);
            this.retValue = retValue;
        }


        @Override
        protected UnderTest coalesceInternal(final List<GenericExpression<Serializable>> exprs) {
            return retValue;
        }


        @Override
        public void accept(final ExpressionVisitor visitor) {
        }


        @Override
        public Class<?> getType() {
            return null;
        }
    }

    private UnderTest underTest;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        underTest = spy(new UnderTest(expressionFactory, new UnderTest(expressionFactory, null)));
    }


    @Test
    public void test_that_equalTo_for_a_right_hand_expression_returns_the_correct_BooleanExpression_instance() {
        //--Arrange
        final UnderTest rhs = mock(UnderTest.class);
        final EqualToExpressionImpl equalsExpr = mock(EqualToExpressionImpl.class);
        when(expressionFactory.createEqualsExpression(same(underTest), same(rhs))).thenReturn(equalsExpr);

        //--Assert
        Assert.assertSame(equalsExpr, underTest.equalTo(rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_equalTo_for_a_right_hand_value_returns_the_correct_BooleanExpression_instance() {
        //--Arrange
        final EqualToExpressionImpl equalsExpr = mock(EqualToExpressionImpl.class);
        final Serializable value = mock(Serializable.class);
        final ParameterExpression<Serializable> parameter = mock(ParameterExpression.class);

        when(expressionFactory.createEqualsExpression(same(underTest), same(parameter))).thenReturn(equalsExpr);
        when(expressionFactory.createParameterExpression(same(value))).thenReturn(parameter);

        //--Assert
        Assert.assertSame(equalsExpr, underTest.equalTo(value));
    }


    @Test
    public void test_that_notEqualTo_for_a_right_hand_expression_returns_the_correct_BooleanExpression_instance() {
        //--Arrange
        final UnderTest rhs = mock(UnderTest.class);
        final NotEqualToExpression equalsExpr = mock(NotEqualToExpression.class);
        when(expressionFactory.createNotEqualExpression(same(underTest), same(rhs))).thenReturn(equalsExpr);

        //--Assert
        Assert.assertSame(equalsExpr, underTest.notEqualTo(rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_notEqualTo_for_a_right_hand_value_returns_the_correct_BooleanExpression_instance() {
        //--Arrange
        final NotEqualToExpression equalsExpr = mock(NotEqualToExpression.class);
        final Serializable value = mock(Serializable.class);
        final ParameterExpression<Serializable> parameter = mock(ParameterExpression.class);

        when(expressionFactory.createNotEqualExpression(same(underTest), same(parameter))).thenReturn(equalsExpr);
        when(expressionFactory.createParameterExpression(same(value))).thenReturn(parameter);

        //--Assert
        Assert.assertSame(equalsExpr, underTest.notEqualTo(value));
    }


    @Test
    public void test_that_coalesce_accepting_only_one_value_calls_the_coalesceInternal_with_the_correct_argument() {
        //--Arrange
        final Serializable value = mock(Serializable.class);
        final ParameterExpression<Serializable> parameter = new ParameterExpression<Serializable>(value);

        when(this.expressionFactory.createParameterExpression(same(value))).thenReturn(parameter);

        //--Assert
        Assert.assertSame(underTest.retValue, underTest.coalesce(value));
        verify(underTest).coalesceInternal(Arrays.<GenericExpression<Serializable>>asList(parameter));
    }


    @Test
    public void test_that_coalesce_accepting_a_list_of_expressions_calls_coalesceInternal_with_the_correct_argument() {
        //--Arrange
        final List<GenericExpression<Serializable>> arg = new ArrayList<GenericExpression<Serializable>>();

        //--Assert
        Assert.assertSame(underTest.retValue, underTest.coalesce(arg));
        verify(underTest).coalesceInternal(arg);
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_coalesce_accepting_a_variable_number_of_expressions_calls_coalesceInternal_with_the_correct_argument() {
        //--Arrange
        final UnderTest firstExpr = mock(UnderTest.class);
        final UnderTest secondExpr = mock(UnderTest.class);
        final List<UnderTest> expected = Arrays.asList(firstExpr, secondExpr);

        //--Assert
        Assert.assertSame(underTest.retValue, underTest.coalesce(firstExpr, secondExpr));
        verify(underTest).coalesceInternal((List<GenericExpression<Serializable>>) (Object) expected);
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_in_accepting_a_bunch_of_parameters_returns_the_correct_expression() {
        //--Arrange
        final InExpression<Serializable> expected = mock(InExpression.class);
        final Serializable firstArg = mock(Serializable.class);
        final Serializable secondArg = mock(Serializable.class);

        when(expressionFactory.createInExpression(same(underTest), eq(Arrays.asList(firstArg, secondArg))))
                .thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.in(firstArg, secondArg));
    }
}
