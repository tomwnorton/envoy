package org.envoy.expression;

public class AbstractFloatingPointFieldExpressionTests extends GenericFieldExpressionTestBase<Double> {
    @Override
    protected GenericFieldExpression<Double> createUnderTest(
            final ExpressionFactory expressionFactory,
            final CommonFieldExpressionLogic commonFieldExpressionLogic) {
        return new TestFloatingPointFieldExpression(expressionFactory, commonFieldExpressionLogic);
    }

    private class TestFloatingPointFieldExpression extends AbstractFloatingPointFieldExpression<Double, TestFloatingPointFieldExpression> {
        TestFloatingPointFieldExpression(
                ExpressionFactory expressionFactory,
                CommonFieldExpressionLogic commonFieldExpressionLogic) {
            super(expressionFactory, commonFieldExpressionLogic);
        }

        @Override
        public Class<Double> getType() {
            return Double.class;
        }
    }


    @Override
    protected Double createExpectedValue() {
        return new Double(0.0);
    }
}
