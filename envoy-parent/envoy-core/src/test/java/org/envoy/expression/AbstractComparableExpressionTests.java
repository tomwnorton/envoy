package org.envoy.expression;

import org.junit.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class AbstractComparableExpressionTests {
    private class UnderTest extends AbstractComparableExpression<Number, Number, UnderTest> {
        public UnderTest() {
            super(expressionFactory);
        }


        @Override
        public void accept(final ExpressionVisitor visitor) {
        }


        @Override
        protected UnderTest coalesceInternal(final List<GenericExpression<Number>> exprs) {
            return null;
        }


        @Override
        public Class<?> getType() {
            return null;
        }
    }

    @SuppressWarnings("serial")
    private class MockNumber extends Number implements Comparable<MockNumber> {
        @Override
        public int compareTo(final MockNumber o) {
            return 0;
        }


        @Override
        public int intValue() {
            return 0;
        }


        @Override
        public long longValue() {
            return 0;
        }


        @Override
        public float floatValue() {
            return 0;
        }


        @Override
        public double doubleValue() {
            return 0;
        }
    }

    @Mock
    private ExpressionFactory expressionFactory;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void test_that_AbstractComparableExpression_extends_AbstractSerializableInstanceExpression() {
        AbstractObjectInstanceExpression.class.isAssignableFrom(AbstractComparableExpression.class);
    }


    @Test
    public void test_that_lessThan_expecting_an_expression_returns_the_correct_LessThanExpression() {
        //--Arrange
        final UnderTest underTest = new UnderTest();
        final UnderTest rhs = mock(UnderTest.class);
        final LessThanExpression expected = mock(LessThanExpression.class);
        when(expressionFactory.createLessThanExpression(same(underTest), same(rhs))).thenReturn(expected);

        //--Act
        final BooleanExpression actual = underTest.lessThan(rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_lessThan_expecting_a_value_returns_the_correct_LessThanExpression_by_placing_the_value_into_a_ParameterExpression() {
        //--Arrange
        final UnderTest underTest = new UnderTest();
        final LessThanExpression expected = mock(LessThanExpression.class);
        final MockNumber rhs = new MockNumber();
        final ParameterExpression<MockNumber> parameterExpression = mock(ParameterExpression.class);
        when(expressionFactory.createParameterExpression(same(rhs))).thenReturn(parameterExpression);
        when(expressionFactory.createLessThanExpression(same(underTest), same(parameterExpression))).thenReturn(expected);

        //--Act
        final BooleanExpression actual = underTest.lessThan(rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_lessThanEqual_expecting_an_expression_returns_the_correct_LessThanEqualExpression() {
        //--Arrange
        final UnderTest underTest = new UnderTest();
        final UnderTest rhs = mock(UnderTest.class);
        final LessThanEqualExpression expected = mock(LessThanEqualExpression.class);
        when(expressionFactory.createLessThanEqualExpression(same(underTest), same(rhs))).thenReturn(expected);

        //--Act
        final BooleanExpression actual = underTest.lessThanEqual(rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_lessThanEqual_expecting_a_value_returns_the_correct_LessThanEqualExpression_by_placing_the_value_into_a_ParameterExpression() {
        //--Arrange
        final UnderTest underTest = new UnderTest();
        final LessThanEqualExpression expected = mock(LessThanEqualExpression.class);
        final MockNumber rhs = new MockNumber();
        final ParameterExpression<MockNumber> parameterExpression = mock(ParameterExpression.class);
        when(expressionFactory.createParameterExpression(same(rhs))).thenReturn(parameterExpression);
        when(expressionFactory.createLessThanEqualExpression(same(underTest), same(parameterExpression)))
                .thenReturn(expected);

        //--Act
        final BooleanExpression actual = underTest.lessThanEqual(rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_greaterThan_expecting_an_expression_returns_the_correct_GreaterThanExpression() {
        //--Arrange
        final UnderTest underTest = new UnderTest();
        final UnderTest rhs = mock(UnderTest.class);
        final GreaterThanExpression expected = mock(GreaterThanExpression.class);
        when(expressionFactory.createGreaterThanExpression(same(underTest), same(rhs))).thenReturn(expected);

        //--Act
        final BooleanExpression actual = underTest.greaterThan(rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_greaterThan_expecting_a_value_returns_the_correct_GreaterThanExpression_by_placing_the_value_into_a_ParameterExpression() {
        //--Arrange
        final UnderTest underTest = new UnderTest();
        final GreaterThanExpression expected = mock(GreaterThanExpression.class);
        final MockNumber rhs = new MockNumber();
        final ParameterExpression<MockNumber> parameterExpression = mock(ParameterExpression.class);
        when(expressionFactory.createParameterExpression(same(rhs))).thenReturn(parameterExpression);
        when(expressionFactory.createGreaterThanExpression(same(underTest), same(parameterExpression))).thenReturn(expected);

        //--Act
        final BooleanExpression actual = underTest.greaterThan(rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_greaterThanEqual_expecting_an_expression_returns_the_correct_GreaterThanEqualExpression() {
        //--Arrange
        final UnderTest underTest = new UnderTest();
        final UnderTest rhs = mock(UnderTest.class);
        final GreaterThanEqualExpression expected = mock(GreaterThanEqualExpression.class);
        when(expressionFactory.createGreaterThanEqualExpression(same(underTest), same(rhs))).thenReturn(expected);

        //--Act
        final BooleanExpression actual = underTest.greaterThanEqual(rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_greaterThanEqual_expecting_a_value_returns_the_correct_GreaterThanEqualExpression_by_placing_the_value_into_a_ParameterExpression() {
        //--Arrange
        final UnderTest underTest = new UnderTest();
        final GreaterThanEqualExpression expected = mock(GreaterThanEqualExpression.class);
        final MockNumber rhs = new MockNumber();
        final ParameterExpression<MockNumber> parameterExpression = mock(ParameterExpression.class);
        when(expressionFactory.createParameterExpression(same(rhs))).thenReturn(parameterExpression);
        when(expressionFactory.createGreaterThanEqualExpression(same(underTest), same(parameterExpression))).thenReturn(
                expected);

        //--Act
        final BooleanExpression actual = underTest.greaterThanEqual(rhs);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_between_expecting_expressions_returns_the_correct_BetweenExpression() {
        //--Arrange
        final UnderTest underTest = new UnderTest();
        final UnderTest lowerBound = new UnderTest();
        final UnderTest upperBound = new UnderTest();
        final BetweenExpression expected = new BetweenExpression(null, null, null, null);

        when(expressionFactory.createBetweenExpression(same(underTest), same(lowerBound), same(upperBound))).thenReturn(
                expected);

        //--Act
        final BooleanExpression actual = underTest.between(lowerBound, upperBound);

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_between_expecting_values_returns_the_correct_BetweenExpression() {
        //--Arrange
        final UnderTest underTest = new UnderTest();
        final MockNumber lowerBound = new MockNumber();
        final ParameterExpression<MockNumber> lowerBoundParam = new ParameterExpression<AbstractComparableExpressionTests.MockNumber>(
                null);
        final MockNumber upperBound = new MockNumber();
        final ParameterExpression<MockNumber> upperBoundParam = new ParameterExpression<AbstractComparableExpressionTests.MockNumber>(
                null);
        final BetweenExpression expected = new BetweenExpression(null, null, null, null);

        when(expressionFactory.createParameterExpression(same(lowerBound))).thenReturn(lowerBoundParam);
        when(expressionFactory.createParameterExpression(same(upperBound))).thenReturn(upperBoundParam);
        when(expressionFactory.createBetweenExpression(same(underTest), same(lowerBoundParam), same(upperBoundParam)))
                .thenReturn(expected);

        //--Act
        final BooleanExpression actual = underTest.between(lowerBound, upperBound);

        //--Assert
        Assert.assertSame(expected, actual);
    }
}
