package org.envoy.expression;

import org.junit.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class AbstractNumericExpressionTests {
    private class UnderTest extends AbstractNumericExpression<Number> {
        public UnderTest() {
            super(AbstractNumericExpressionTests.this.expressionFactory);
        }


        @Override
        public void accept(final ExpressionVisitor visitor) {
        }


        @Override
        protected NumericExpression<Number> coalesceInternal(final List<GenericExpression<Number>> exprs) {
            return null;
        }


        @Override
        public Class<?> getType() {
            return null;
        }
    }

    @Mock
    private ExpressionFactory expressionFactory;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void test_that_NumericExpressionImpl_extends_AbstractComparableExpression() {
        AbstractComparableExpression.class.isAssignableFrom(AbstractNumericExpression.class);
    }


    @Test
    public void test_that_convertToString_returns_the_correct_StringExpression() {
        //--Arrange
        final UnderTest underTest = new UnderTest();
        final ConvertToStringExpression expected = new ConvertToStringExpression(null, null);
        when(expressionFactory.createConvertToStringExpression(same(underTest))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.convertToString());
    }
}
