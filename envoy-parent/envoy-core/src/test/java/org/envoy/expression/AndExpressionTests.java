package org.envoy.expression;

import org.junit.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class AndExpressionTests {
    @Test
    public void test_that_accept_calls_visitAnd_with_the_arguments_lhs_and_rhs() {
        //--Arrange
        final ExpressionVisitor visitor = mock(ExpressionVisitor.class);
        final BooleanExpression lhs = mock(BooleanExpression.class);
        final BooleanExpression rhs = mock(BooleanExpression.class);

        final AndExpression underTest = new AndExpression(lhs, rhs, null);

        //--Act
        underTest.accept(visitor);

        //--Assert
        verify(visitor).visitAnd(same(lhs), same(rhs));
    }


    @Test
    public void test_that_accept_calls_visitAnd_with_the_arguments_lhs_equals_true_and_rhs_when_lhs_is_a_BooleanFieldExpression_and_rhs_is_not() {
        //--Arrange
        final ExpressionVisitor visitor = mock(ExpressionVisitor.class);
        final BooleanExpression lhs = mock(BooleanFieldExpression.class);
        final BooleanExpression rhs = mock(BooleanExpression.class);
        final EqualToExpressionImpl equalsExpr = mock(EqualToExpressionImpl.class);
        final BooleanConstantExpression trueExpr = mock(BooleanConstantExpression.class);

        final ExpressionFactory expressionFactory = mock(ExpressionFactory.class);
        when(expressionFactory.createBooleanConstantExpression(true)).thenReturn(trueExpr);
        when(expressionFactory.createEqualsExpression(lhs, trueExpr)).thenReturn(equalsExpr);

        final AndExpression underTest = new AndExpression(lhs, rhs, expressionFactory);

        //--Act
        underTest.accept(visitor);

        //--Assert
        verify(visitor).visitAnd(same(equalsExpr), same(rhs));
    }


    @Test
    public void test_that_accept_calls_visitAnd_with_the_arguments_lhs_and_rhs_equals_true_when_rhs_is_a_BooleanFieldExpression_and_lhs_is_not() {
        //--Arrange
        final ExpressionVisitor visitor = mock(ExpressionVisitor.class);
        final BooleanExpression lhs = mock(BooleanExpression.class);
        final BooleanExpression rhs = mock(BooleanFieldExpression.class);
        final EqualToExpressionImpl equalsExpr = mock(EqualToExpressionImpl.class);
        final BooleanConstantExpression trueExpr = mock(BooleanConstantExpression.class);

        final ExpressionFactory expressionFactory = mock(ExpressionFactory.class);
        when(expressionFactory.createBooleanConstantExpression(true)).thenReturn(trueExpr);
        when(expressionFactory.createEqualsExpression(rhs, trueExpr)).thenReturn(equalsExpr);

        final AndExpression underTest = new AndExpression(lhs, rhs, expressionFactory);

        //--Act
        underTest.accept(visitor);

        //--Assert
        verify(visitor).visitAnd(same(lhs), same(equalsExpr));
    }


    @Test
    public void test_that_getExpressionFactory_returns_the_correct_expressionFactory() {
        //--Arrange
        final ExpressionFactory expressionFactory = mock(ExpressionFactory.class);
        final AndExpression underTest = new AndExpression(null, null, expressionFactory);

        //--Assert
        Assert.assertSame(expressionFactory, underTest.getExpressionFactory());
    }
}
