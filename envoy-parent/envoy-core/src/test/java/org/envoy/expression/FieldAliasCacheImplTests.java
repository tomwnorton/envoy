package org.envoy.expression;

import org.junit.*;

import static org.mockito.Mockito.*;

public class FieldAliasCacheImplTests {
    private FieldAliasCacheImpl underTest;


    @Before
    public void setUp() {
        underTest = new FieldAliasCacheImpl();
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_getFieldAlias_returns_foo_0_bar_when_the_only_field_aliased_is_a_field_named_bar_on_a_table_aliased_with_foo_0() {
        //--Arrange
        final FieldExpression expr = mock(FieldExpression.class);
        final EntityExpression<Object> entityExpr = mock(EntityExpression.class);
        when((EntityExpression<Object>) expr.getEntityExpression()).thenReturn(entityExpr);
        when(entityExpr.getTableName()).thenReturn("foo");
        when(expr.getFieldName()).thenReturn("bar");

        //--Assert
        Assert.assertEquals("foo_0_bar", underTest.getFieldAlias(expr));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_getFieldAlias_returns_alpha_1_beta_when_the_only_field_aliased_is_a_field_named_beta_on_a_table_aliased_with_alpha_1() {
        //--Arrange
        final FieldExpression expr = mock(FieldExpression.class);
        final EntityExpression<Object> entityExpr = mock(EntityExpression.class);
        when((EntityExpression<Object>) expr.getEntityExpression()).thenReturn(entityExpr);
        when(entityExpr.getTableName()).thenReturn("alpha");
        when(expr.getFieldName()).thenReturn("beta");

        //--This is the first EntityExpression for the alpha table, aliased alpha0
        final EntityExpression<?> otherEntity = mock(EntityExpression.class);
        when(otherEntity.getTableName()).thenReturn("alpha");
        underTest.getFieldAlias(otherEntity);

        //--Assert
        Assert.assertEquals("alpha_1_beta", underTest.getFieldAlias(expr));
    }


    @Test
    public void test_that_getFieldAlias_returns_calculated_0_when_the_only_field_is_a_calculated_field() {
        //--Arrange
        final Expression expr = mock(Expression.class);

        //--Assert
        Assert.assertEquals("calculated_0", underTest.getFieldAlias(expr));
    }


    @Test
    public void test_that_getFieldAlias_returns_calculated_1_when_the_given_field_is_the_second_calculated_field() {
        //--Arrange
        final Expression firstExpr = mock(Expression.class);
        final Expression secondExpr = mock(Expression.class);

        underTest.getFieldAlias(firstExpr);

        //--Assert
        Assert.assertEquals("calculated_1", underTest.getFieldAlias(secondExpr));
    }


    @Test
    public void test_that_getFieldAlias_returns_calculated_0_when_the_given_field_is_the_only_caluclated_field_but_it_is_called_twice() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        underTest.getFieldAlias(expr);

        //--Assert
        Assert.assertEquals("calculated_0", underTest.getFieldAlias(expr));
    }


    @Test
    public void test_that_getFieldAlias_returns_foo_0_when_the_given_expression_is_an_EntityExpression_and_it_is_the_first_EntityExpression_to_be_given() {
        //--Arrange
        final EntityExpression<?> expr = mock(EntityExpression.class);
        when(expr.getTableName()).thenReturn("foo");

        //--Assert
        Assert.assertEquals("foo_0", underTest.getFieldAlias(expr));
    }


    @Test
    public void test_that_getFieldAlias_returns_bar_0_when_the_given_expression_is_an_EntityExpression_and_it_is_the_first_EntityExpression_to_be_given() {
        //--Arrange
        final EntityExpression<?> expr = mock(EntityExpression.class);
        when(expr.getTableName()).thenReturn("bar");

        //--Assert
        Assert.assertEquals("bar_0", underTest.getFieldAlias(expr));
    }


    @Test
    public void test_that_getFieldAlias_returns_foo_1_when_the_given_expression_is_an_EntityExpression_and_it_is_the_second_EntityExpression_to_be_given() {
        //--Arrange
        final EntityExpression<?> firstExpr = mock(EntityExpression.class);
        when(firstExpr.getTableName()).thenReturn("foo");
        underTest.getFieldAlias(firstExpr);

        final EntityExpression<?> secondExpr = mock(EntityExpression.class);
        when(secondExpr.getTableName()).thenReturn("foo");

        //--Assert
        Assert.assertEquals("foo_1", underTest.getFieldAlias(secondExpr));
    }


    @Test
    public void test_that_getFieldAlias_returns_foo_0_when_the_given_expression_is_an_EntityExpression_was_retrieved_twice() {
        //--Arrange
        final EntityExpression<?> expr = mock(EntityExpression.class);
        when(expr.getTableName()).thenReturn("foo");
        underTest.getFieldAlias(expr);

        //--Assert
        Assert.assertEquals("foo_0", underTest.getFieldAlias(expr));
    }


    @Test
    public void test_that_getFieldAlias_returns_foo_0_when_the_given_expression_is_an_EntityExpression_and_it_is_the_first_EntityExpression_named_foo_to_be_given() {
        //--Arrange
        final EntityExpression<?> firstExpr = mock(EntityExpression.class);
        when(firstExpr.getTableName()).thenReturn("bar");
        underTest.getFieldAlias(firstExpr);

        final EntityExpression<?> secondExpr = mock(EntityExpression.class);
        when(secondExpr.getTableName()).thenReturn("foo");

        //--Assert
        Assert.assertEquals("foo_0", underTest.getFieldAlias(secondExpr));
    }


    @Test
    public void test_that_getFieldAlias_returns_foo_2_when_the_given_expression_is_an_EntityExpression_and_it_is_the_third_EntityExpression_named_foo_to_be_given() {
        //--Arrange
        final EntityExpression<?> firstExpr = mock(EntityExpression.class);
        when(firstExpr.getTableName()).thenReturn("foo");
        underTest.getFieldAlias(firstExpr);

        final EntityExpression<?> secondExpr = mock(EntityExpression.class);
        when(secondExpr.getTableName()).thenReturn("foo");
        underTest.getFieldAlias(secondExpr);

        final EntityExpression<?> thirdExpr = mock(EntityExpression.class);
        when(thirdExpr.getTableName()).thenReturn("foo");

        //--Assert
        Assert.assertEquals("foo_2", underTest.getFieldAlias(thirdExpr));
    }


    @Test
    public void test_that_getFieldAlias_returns_foo_bar_0_when_the_given_expression_is_an_EntityExpression_it_has_schema_name_foo_and_table_name_bar() {
        //--Arrange
        final EntityExpression<?> expr = mock(EntityExpression.class);
        when(expr.getTableName()).thenReturn("bar");
        when(expr.getSchemaName()).thenReturn("foo");

        //--Assert
        Assert.assertEquals("foo_bar_0", underTest.getFieldAlias(expr));
    }


    @Test
    public void test_that_getFieldAlias_returns_bar_foo_0_when_the_given_expression_is_an_EntityExpression_it_has_schema_name_bar_and_table_name_foo() {
        //--Arrange
        final EntityExpression<?> expr = mock(EntityExpression.class);
        when(expr.getTableName()).thenReturn("foo");
        when(expr.getSchemaName()).thenReturn("bar");

        //--Assert
        Assert.assertEquals("bar_foo_0", underTest.getFieldAlias(expr));
    }
}
