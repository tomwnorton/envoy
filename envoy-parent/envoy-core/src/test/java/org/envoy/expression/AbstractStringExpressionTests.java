package org.envoy.expression;

import org.junit.*;

import java.io.*;
import java.util.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class AbstractStringExpressionTests extends ExpressionTestBase<AbstractStringExpression> {
    private class UnderTest extends AbstractStringExpression {
        UnderTest(final ExpressionFactory expressionFactory) {
            super(expressionFactory);
        }


        @Override
        public void accept(final ExpressionVisitor visitor) {
        }
    }


    @Override
    protected AbstractStringExpression createTestObjectForExpressionFactoryTest(final ExpressionFactory expressionFactory) {
        return new UnderTest(expressionFactory);
    }


    @Test
    public void test_that_coalesceInternal_return_the_correct_CoalesceStringExpression() {
        //--Arrange
        final List<GenericExpression<String>> exprs = new ArrayList<GenericExpression<String>>();
        final CoalesceStringExpression expected = new CoalesceStringExpression(null, null);
        when(expressionFactory.createCoalesceStringExpression(same(exprs))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.coalesceInternal(exprs));
    }


    @Test
    public void test_that_trim_returns_the_correct_TrimExpression() {
        //--Arrange
        final TrimExpression expected = new TrimExpression(null, null);
        when(expressionFactory.createTrimExpression(same(underTest))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.trim());
    }


    @Test
    public void test_that_length_returns_the_correct_LengthExpression() {
        //--Arrange
        final LengthExpression expected = new LengthExpression(null, null);
        when(expressionFactory.createLengthExpression(same(underTest))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.length());
    }


    @Test
    public void test_that_toLowerCase_returns_the_correct_LowerCaseExpression() {
        //--Arrange
        final LowerCaseExpression expected = new LowerCaseExpression(null, null);
        when(expressionFactory.createLowerCaseExpression(same(underTest))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.toLowerCase());
    }


    @Test
    public void test_that_toUpperCase_returns_the_correct_UpperCaseExpression() {
        //--Arrange
        final UpperCaseExpression expected = new UpperCaseExpression(null, null);
        when(expressionFactory.createUpperCaseExpression(same(underTest))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.toUpperCase());
    }


    @Test
    public void test_that_convertToInteger_returns_the_correct_ConvertStringToIntegerExpression() {
        //--Arrange
        final ConvertStringToIntegerExpression expected = new ConvertStringToIntegerExpression(null, null);
        when(expressionFactory.createConvertStringToIntegerExpression(same(underTest))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.toInteger());
    }


    @Test
    public void test_that_concat_expecting_an_expression_returns_the_correct_ConcatExpression() {
        //--Arrange
        final StringExpression rhs = mock(StringExpression.class);
        final ConcatExpression expected = new ConcatExpression(null, null, null);
        when(expressionFactory.createConcatExpression(same(underTest), same(rhs))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.concat(rhs));
    }


    @Test
    public void test_that_concat_expecting_a_string_returns_the_correct_ConcatExpression() {
        //--Arrange
        final Serializable rhs = mock(Serializable.class);
        final ParameterExpression<Object> parameter = new ParameterExpression<Object>(null);
        final ConcatExpression expected = new ConcatExpression(null, null, null);
        when(expressionFactory.createParameterExpression((Object) rhs)).thenReturn(parameter);
        when(expressionFactory.createConcatExpression(same(underTest), same(parameter))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.concat(rhs));
    }


    @Test
    public void test_that_contains_expecting_an_expression_returns_the_correct_ContainsExpression() {
        //--Arrange
        final StringExpression rhs = mock(StringExpression.class);
        final ContainsExpression expected = new ContainsExpression(null, null, null);
        when(expressionFactory.createContainsExpression(same(underTest), same(rhs))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.contains(rhs));
    }


    @Test
    public void test_that_contains_expecting_a_string_returns_the_correct_ContainsExpression_for_the_pattern_hello() {
        //--Arrange
        final ParameterExpression<String> parameter = new ParameterExpression<String>(null);
        final ContainsExpression expected = new ContainsExpression(null, null, null);
        when(expressionFactory.createParameterExpression("hello")).thenReturn(parameter);
        when(expressionFactory.createContainsExpression(same(underTest), same(parameter))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.contains("hello"));
    }


    @Test
    public void test_that_contains_expecting_a_string_returns_the_correct_ContainsExpression_for_the_pattern_world() {
        //--Arrange
        final ParameterExpression<String> parameter = new ParameterExpression<String>(null);
        final ContainsExpression expected = new ContainsExpression(null, null, null);
        when(expressionFactory.createParameterExpression("world")).thenReturn(parameter);
        when(expressionFactory.createContainsExpression(same(underTest), same(parameter))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.contains("world"));
    }


    @Test
    public void test_that_startsWith_expecting_an_expression_returns_the_correct_StartsWithExpression() {
        //--Arrange
        final StringExpression rhs = mock(StringExpression.class);
        final StartsWithExpression expected = new StartsWithExpression(null, null, null);
        when(expressionFactory.createStartsWithExpression(same(underTest), same(rhs))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.startsWith(rhs));
    }


    @Test
    public void test_that_startsWith_expecting_a_string_returns_the_correct_StartsWithExpression_for_the_pattern_hello() {
        //--Arrange
        final ParameterExpression<String> parameter = new ParameterExpression<String>(null);
        final StartsWithExpression expected = new StartsWithExpression(null, null, null);
        when(expressionFactory.createParameterExpression("hello")).thenReturn(parameter);
        when(expressionFactory.createStartsWithExpression(same(underTest), same(parameter))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.startsWith("hello"));
    }


    @Test
    public void test_that_startsWith_expecting_a_string_returns_the_correct_StartsWithExpression_for_the_pattern_world() {
        //--Arrange
        final ParameterExpression<String> parameter = new ParameterExpression<String>(null);
        final StartsWithExpression expected = new StartsWithExpression(null, null, null);
        when(expressionFactory.createParameterExpression("world")).thenReturn(parameter);
        when(expressionFactory.createStartsWithExpression(same(underTest), same(parameter))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.startsWith("world"));
    }


    @Test
    public void test_that_endsWith_expecting_an_expression_returns_the_correct_EndsWithExpression() {
        //--Arrange
        final StringExpression rhs = mock(StringExpression.class);
        final EndsWithExpression expected = new EndsWithExpression(null, null, null);
        when(expressionFactory.createEndsWithExpression(same(underTest), same(rhs))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.endsWith(rhs));
    }


    @Test
    public void test_that_endsWith_expecting_a_string_returns_the_correct_EndsWithExpression_for_the_pattern_hello() {
        //--Arrange
        final ParameterExpression<String> parameter = new ParameterExpression<String>(null);
        final EndsWithExpression expected = new EndsWithExpression(null, null, null);
        when(expressionFactory.createParameterExpression("hello")).thenReturn(parameter);
        when(expressionFactory.createEndsWithExpression(same(underTest), same(parameter))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.endsWith("hello"));
    }


    @Test
    public void test_that_endsWith_expecting_a_string_returns_the_correct_EndsWithExpression_for_the_pattern_world() {
        //--Arrange
        final ParameterExpression<String> parameter = new ParameterExpression<String>(null);
        final EndsWithExpression expected = new EndsWithExpression(null, null, null);
        when(expressionFactory.createParameterExpression("world")).thenReturn(parameter);
        when(expressionFactory.createEndsWithExpression(same(underTest), same(parameter))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.endsWith("world"));
    }
}
