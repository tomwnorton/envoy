package org.envoy.expression;

import org.junit.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class NotEqualToExpressionTests {
    @Test
    public void test_that_accept_calls_the_visitNotEquals_with_the_correct_arguments() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);
        final ExpressionVisitor visitor = mock(ExpressionVisitor.class);
        final NotEqualToExpression underTest = new NotEqualToExpression(lhs, rhs, null);

        //--Act
        underTest.accept(visitor);

        //--Assert
        verify(visitor).visitNotEquals(same(lhs), same(rhs));
    }


    @Test
    public void test_that_accept_calls_the_visitNotNull_with_the_correct_arguments_when_lhs_is_an_expression_but_rhs_is_null() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final ExpressionVisitor visitor = mock(ExpressionVisitor.class);
        final NotEqualToExpression underTest = new NotEqualToExpression(lhs, null, null);

        //--Act
        underTest.accept(visitor);

        //--Assert
        verify(visitor).visitNotNull(same(lhs));
    }


    @Test
    public void test_that_expressionFactory_is_the_correct_ExpressionFactory_instance() {
        //--Arrange
        final ExpressionFactory expressionFactory = mock(ExpressionFactory.class);
        final NotEqualToExpression underTest = new NotEqualToExpression(null, null, expressionFactory);

        //--Assert
        Assert.assertSame(expressionFactory, underTest.getExpressionFactory());
    }
}
