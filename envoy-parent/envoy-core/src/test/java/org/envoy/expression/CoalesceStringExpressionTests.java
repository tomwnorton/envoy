package org.envoy.expression;

import org.junit.*;

import java.util.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class CoalesceStringExpressionTests extends ExpressionTestBase<CoalesceStringExpression> {
    private List<Expression> exprs;


    @Override
    protected CoalesceStringExpression createTestObjectForExpressionFactoryTest(final ExpressionFactory expressionFactory) {
        exprs = new ArrayList<Expression>();
        return new CoalesceStringExpression(exprs, expressionFactory);
    }


    @Test
    public void test_that_accept_calls_visitCoalesce_with_the_correct_arguments() {
        //--Act
        underTest.accept(expressionVisitor);

        //--Assert
        verify(expressionVisitor).visitCoalesce(same(exprs));
    }
}
