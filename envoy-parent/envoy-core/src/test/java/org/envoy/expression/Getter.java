package org.envoy.expression;

import java.io.*;

public interface Getter<T extends Serializable, Parent extends Serializable> {
    public T get(Parent parent);
}
