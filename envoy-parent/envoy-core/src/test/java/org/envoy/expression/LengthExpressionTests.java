package org.envoy.expression;

import org.junit.*;
import org.mockito.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class LengthExpressionTests extends ExpressionTestBase<LengthExpression> {
    @Mock
    private Expression operand;


    @Override
    protected LengthExpression createTestObjectForExpressionFactoryTest(final ExpressionFactory expressionFactory) {
        return new LengthExpression(operand, expressionFactory);
    }


    @Test
    public void test_that_accept_calls_visitLength() {
        //--Act
        underTest.accept(expressionVisitor);

        //--Assert
        verify(expressionVisitor).visitLength(same(operand));
    }
}
