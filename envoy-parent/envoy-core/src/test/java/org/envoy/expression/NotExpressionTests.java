package org.envoy.expression;

import org.junit.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class NotExpressionTests {
    @Test
    public void test_that_expressionFactory_is_the_correct_ExpressionFactory_instance() {
        //--Arrange
        final ExpressionFactory expressionFactory = mock(ExpressionFactory.class);
        final NotExpression underTest = new NotExpression(null, expressionFactory);

        //--Assert
        Assert.assertSame(expressionFactory, underTest.getExpressionFactory());
    }


    @Test
    public void test_that_accept_calls_visitNot_with_the_correct_parameters_when_expr_is_a_normal_boolean_expression() {
        //--Arrange
        final BooleanExpression expr = mock(BooleanExpression.class);
        final ExpressionVisitor visitor = mock(ExpressionVisitor.class);
        final NotExpression underTest = new NotExpression(expr, null);

        //--Act
        underTest.accept(visitor);

        //--Assert
        verify(visitor).visitNot(same(expr));
    }


    @Test
    public void test_that_accept_calls_visitNot_with_the_correct_parameters_when_expr_is_a_boolean_field_expression() {
        //--Arrange
        final BooleanFieldExpression expr = mock(BooleanFieldExpression.class);
        final BooleanConstantExpression falseExpr = mock(BooleanConstantExpression.class);
        final ExpressionVisitor visitor = mock(ExpressionVisitor.class);
        final ExpressionFactory expressionFactory = mock(ExpressionFactory.class);
        final NotExpression underTest = new NotExpression(expr, expressionFactory);

        when(expressionFactory.createBooleanConstantExpression(false)).thenReturn(falseExpr);

        //--Act
        underTest.accept(visitor);

        //--Assert
        verify(visitor).visitEquals(same(expr), same(falseExpr));
    }
}
