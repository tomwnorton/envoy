package org.envoy.expression;

import org.junit.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class NegativeFloatingPointExpressionTests extends UnaryExpressionTestBase {
    @Override
    protected AbstractExpression<?> createTestObject(final Expression operand, final ExpressionFactory expressionFactory) {
        return new NegativeFloatingPointExpression<Number>(operand, expressionFactory);
    }


    @Test
    public void test_that_accept_calls_visitNegative() {
        //--Act
        underTest.accept(expressionVisitor);

        //--Assert
        verify(expressionVisitor).visitNegative(same(operand));
    }
}
