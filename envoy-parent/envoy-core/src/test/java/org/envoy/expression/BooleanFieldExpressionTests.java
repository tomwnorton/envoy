package org.envoy.expression;

public class BooleanFieldExpressionTests extends GenericFieldExpressionTestBase<Boolean> {
    @Override
    protected GenericFieldExpression<Boolean> createUnderTest(
            final ExpressionFactory expressionFactory,
            final CommonFieldExpressionLogic commonFieldExpressionLogic) {
        return new BooleanFieldExpression(expressionFactory, commonFieldExpressionLogic);
    }


    @Override
    protected Boolean createExpectedValue() {
        return new Boolean(true);
    }
}
