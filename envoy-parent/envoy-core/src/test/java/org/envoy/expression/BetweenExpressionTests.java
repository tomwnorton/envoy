package org.envoy.expression;

import org.junit.*;
import org.mockito.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class BetweenExpressionTests extends ExpressionTestBase<BetweenExpression> {
    @Mock
    private Expression lhs;

    @Mock
    private Expression lowerBound;

    @Mock
    private Expression upperBound;


    @Override
    protected BetweenExpression createTestObjectForExpressionFactoryTest(final ExpressionFactory expressionFactory) {
        return new BetweenExpression(lhs, lowerBound, upperBound, expressionFactory);
    }


    @Test
    public void test_that_accept_calls_visitBetween_with_the_correct_arguments() {
        //--Act
        underTest.accept(expressionVisitor);

        //--Assert
        verify(expressionVisitor).visitBetween(same(lhs), same(lowerBound), same(upperBound));
    }
}
