package org.envoy.expression;

import org.junit.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class SelectExpressionTests {
    @SuppressWarnings("unchecked")
    @Test
    public void test_that_accept_calls_visitSelect_with_the_correct_arguments() {
        //--Arrange
        final ExpressionVisitor visitor = mock(ExpressionVisitor.class);
        final SelectListExpression selectListExpr = new SelectListExpression(null);
        final EntityExpression<Object> fromExpr = mock(EntityExpression.class);
        final BooleanExpression whereExpr = mock(BooleanExpression.class);
        final SelectExpression<Object> underTest = new SelectExpression<>(selectListExpr, fromExpr, whereExpr);

        //--Act
        underTest.accept(visitor);

        //-Assert
        verify(visitor).visitSelect(same(selectListExpr), same(fromExpr), same(whereExpr));
    }
}
