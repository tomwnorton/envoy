package org.envoy.expression;

import org.junit.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class EqualToExpressionImplTests {
    @Test
    public void test_that_accept_calls_visitEquals_with_the_correct_arguments_when_lhs_and_rhs_are_expressions() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);
        final ExpressionVisitor visitor = mock(ExpressionVisitor.class);

        final EqualToExpressionImpl underTest = new EqualToExpressionImpl(lhs, rhs, null);

        //--Act
        underTest.accept(visitor);

        //--Assert
        verify(visitor).visitEquals(same(lhs), same(rhs));
    }


    @Test
    public void test_that_accept_calls_visitNull_with_the_correct_arguments_when_lhs_is_an_expressions_and_rhs_is_null() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final ExpressionVisitor visitor = mock(ExpressionVisitor.class);

        final EqualToExpressionImpl underTest = new EqualToExpressionImpl(lhs, null, null);

        //--Act
        underTest.accept(visitor);

        //--Assert
        verify(visitor).visitNull(same(lhs));
    }


    @Test
    public void test_that_expressionFactory_returns_the_correct_ExpressionFactory_instance() {
        //--Arrange
        final ExpressionFactory expressionFactory = mock(ExpressionFactory.class);
        final EqualToExpressionImpl underTest = new EqualToExpressionImpl(null, null, expressionFactory);

        //--Assert
        Assert.assertSame(expressionFactory, underTest.getExpressionFactory());
    }
}
