package org.envoy.expression;

import org.junit.*;
import org.mockito.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class CountExpressionTests {
    @Mock
    private ExpressionFactory factory;

    @Mock
    private ExpressionVisitor visitor;

    @Mock
    private Expression operand;

    private CountExpression underTest;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        underTest = new CountExpression(operand, factory);
    }


    @Test
    public void test_that_getExpressionFactory_returns_the_correct_ExpressionFactor() {
        Assert.assertSame(factory, underTest.getExpressionFactory());
    }


    @Test
    public void test_that_accept_calls_visitCount_with_the_correct_arguments() {
        //--Act
        underTest.accept(visitor);

        //--Assert
        verify(visitor).visitCount(same(operand));
    }
}
