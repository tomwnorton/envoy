package org.envoy.expression;

import org.junit.*;

import java.util.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class InExpressionTests {
    @SuppressWarnings("unchecked")
    @Test
    public void test_that_accept_calls_visitIn_with_the_correct_parameters() {
        //--Arrange
        final ExpressionVisitor visitor = mock(ExpressionVisitor.class);
        final Collection<Integer> collection = mock(Collection.class);
        final Expression expr = mock(Expression.class);
        final InExpression<Integer> underTest = new InExpression<Integer>(expr, collection, null);

        //--Act
        underTest.accept(visitor);

        //--Assert
        verify(visitor).visitIn(same(expr), same(collection));
    }
}
