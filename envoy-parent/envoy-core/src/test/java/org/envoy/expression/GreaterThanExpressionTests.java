package org.envoy.expression;

import org.junit.*;
import org.mockito.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class GreaterThanExpressionTests {
    @Mock
    private ExpressionFactory expressionFactory;

    @Mock
    private ExpressionVisitor expressionVisitor;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void test_that_the_ExpressionFactory_is_set() {
        //--Arrange
        final GreaterThanExpression underTest = new GreaterThanExpression(null, null, expressionFactory);

        //--Assert
        Assert.assertSame(expressionFactory, underTest.getExpressionFactory());
    }


    @Test
    public void test_that_visitGreaterThan_is_called_with_the_correct_arguments() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);
        final GreaterThanExpression underTest = new GreaterThanExpression(lhs, rhs, null);

        //--Act
        underTest.accept(expressionVisitor);

        //--Assert
        verify(expressionVisitor).visitGreaterThan(same(lhs), same(rhs));
    }
}
