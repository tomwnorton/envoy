package org.envoy.expression;

import org.junit.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class NegativeIntegerExpressionTests extends ExpressionTestBase<AbstractExpression<?>> {
    @Override
    protected AbstractExpression<?> createTestObjectForExpressionFactoryTest(final ExpressionFactory expressionFactory) {
        return new NegativeIntegerExpression<Number>(null, expressionFactory);
    }


    @Test
    public void test_that_visitNegative_is_called_with_the_correct_argument() {
        //--Arrange
        final Expression expr = mock(Expression.class);
        final NegativeIntegerExpression<Number> underTest = new NegativeIntegerExpression<Number>(expr, null);

        //--Act
        underTest.accept(expressionVisitor);

        //--Assert
        verify(expressionVisitor).visitNegative(same(expr));
    }
}
