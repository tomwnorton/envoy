package org.envoy.expression;

import org.junit.*;
import org.mockito.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class GreaterThanEqualExpressionTests {
    @Mock
    private ExpressionFactory expressionFactory;

    @Mock
    private ExpressionVisitor expressionVisitor;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void test_that_the_ExpressionFactory_is_passed_to_AbstractBooleanExpression() {
        //--Arrange
        final GreaterThanEqualExpression underTest = new GreaterThanEqualExpression(null, null, expressionFactory);

        //--Assert
        Assert.assertSame(expressionFactory, underTest.getExpressionFactory());
    }


    @Test
    public void test_that_accept_calls_visitGreaterThanEqual_with_the_correct_arguments() {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);
        final GreaterThanEqualExpression underTest = new GreaterThanEqualExpression(lhs, rhs, null);

        //--Act
        underTest.accept(expressionVisitor);

        //--Assert
        verify(expressionVisitor).visitGreaterThanEqual(same(lhs), same(rhs));
    }
}
