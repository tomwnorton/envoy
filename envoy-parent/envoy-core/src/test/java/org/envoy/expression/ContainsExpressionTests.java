package org.envoy.expression;

import org.junit.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class ContainsExpressionTests extends BinaryExpressionTestBase {
    @Override
    protected AbstractExpression<?> createTestObject(
            final Expression lhs,
            final Expression rhs,
            final ExpressionFactory expressionFactory) {
        return new ContainsExpression(lhs, rhs, expressionFactory);
    }


    @Test
    public void test_that_accept_calls_visitContains() {
        //--Act
        underTest.accept(expressionVisitor);

        //--Assert
        verify(expressionVisitor).visitContains(same(lhs), same(rhs));
    }
}
