package org.envoy.expression;

import org.junit.*;

import static org.mockito.Mockito.*;

public class LowerCaseExpressionTests extends UnaryExpressionTestBase {
    @Override
    protected AbstractExpression<?> createTestObject(final Expression operand, final ExpressionFactory expressionFactory) {
        return new LowerCaseExpression(operand, expressionFactory);
    }


    @Test
    public void test_that_accept_calls_visitLowerCase() {
        //--Arrange
        underTest.accept(expressionVisitor);

        //--Assert
        verify(expressionVisitor).visitLowerCase(operand);
    }
}
