package org.envoy.expression;

import org.junit.*;

import static org.mockito.Mockito.*;

public class ParameterExpressionTests {
    @Test
    public void test_that_accept_calls_visitParameter_with_the_7_as_its_parameter_when_7_is_used_to_construct_the_ParameterExpression() {
        //--Arrange
        final ParameterExpression<Integer> underTest = new ParameterExpression<Integer>(7);
        final ExpressionVisitor visitor = mock(ExpressionVisitor.class);

        //--Act
        underTest.accept(visitor);

        //--Assert
        verify(visitor).visitParameter(7);
    }
}
