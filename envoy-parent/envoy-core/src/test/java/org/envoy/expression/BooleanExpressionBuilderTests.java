package org.envoy.expression;

import org.junit.*;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class BooleanExpressionBuilderTests {
    private BooleanExpressionBuilder underTest;


    @Before
    public void setUp() {
        underTest = new BooleanExpressionBuilder();
    }


    @Test
    public void test_that_and_expr_get_returns_the_expr() {
        //--Arrange
        final BooleanExpression expected = mock(BooleanExpression.class);

        //--Act
        final BooleanExpression actual = underTest.and(expected).get();

        //--Assert
        assertThat(actual, is(sameInstance(expected)));
    }


    @Test
    public void test_that_and_expr1_and_expr2_get_returns_the_correct_expression() {
        //--Arrange
        final BooleanExpression expected = mock(BooleanExpression.class);
        final BooleanExpression expr1 = mock(BooleanExpression.class);
        final BooleanExpression expr2 = mock(BooleanExpression.class);
        when(expr1.and(expr2)).thenReturn(expected);

        //--Act
        final BooleanExpression actual = underTest.and(expr1).and(expr2).get();

        //--Assert
        assertThat(actual, is(sameInstance(expected)));
    }


    @Test
    public void test_that_or_expr_get_returns_the_expr() {
        //--Arrange
        final BooleanExpression expected = mock(BooleanExpression.class);

        //--Act
        final BooleanExpression actual = underTest.or(expected).get();

        //--Assert
        assertThat(actual, is(sameInstance(expected)));
    }


    @Test
    public void test_that_or_expr1_or_expr2_get_returns_the_correct_expression() {
        //--Arrange
        final BooleanExpression expected = mock(BooleanExpression.class);
        final BooleanExpression expr1 = mock(BooleanExpression.class);
        final BooleanExpression expr2 = mock(BooleanExpression.class);
        when(expr1.or(expr2)).thenReturn(expected);

        //--Act
        final BooleanExpression actual = underTest.or(expr1).or(expr2).get();

        //--Assert
        assertThat(actual, is(sameInstance(expected)));
    }
}
