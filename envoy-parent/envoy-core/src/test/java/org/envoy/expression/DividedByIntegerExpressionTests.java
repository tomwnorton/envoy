package org.envoy.expression;

import org.junit.*;

import static org.mockito.Mockito.*;

public class DividedByIntegerExpressionTests extends BinaryExpressionTestBase {
    @Override
    protected AbstractExpression<?> createTestObject(
            final Expression lhs,
            final Expression rhs,
            final ExpressionFactory expressionFactory) {
        return new DividedByIntegerExpression(lhs, rhs, expressionFactory);
    }


    @Test
    public void test_that_accept_calls_visitDivide_with_the_correct_arguments() {
        //--Act
        underTest.accept(expressionVisitor);

        //--Assert
        verify(expressionVisitor).visitDividedBy(lhs, rhs);
    }
}
