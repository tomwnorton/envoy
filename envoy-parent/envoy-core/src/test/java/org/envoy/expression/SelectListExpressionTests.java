package org.envoy.expression;

import org.junit.*;

import java.util.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class SelectListExpressionTests {
    @Test
    public void test_that_accept_calls_visitSelectList_with_the_correct_list() {
        //--Arrange
        final ExpressionVisitor visitor = mock(ExpressionVisitor.class);
        final List<Expression> expected = new ArrayList<>();
        final SelectListExpression underTest = new SelectListExpression(expected);

        //--Act
        underTest.accept(visitor);

        //--Assert
        verify(visitor).visitSelectList(same(expected));
    }
}
