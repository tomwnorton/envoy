package org.envoy.expression;

import org.junit.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class ExpressionCollectionTests {
    @Test
    public void test_that_a_boolean_field_expression_is_added_as_an_equals_expression() {
        //--Arrange
        final BooleanFieldExpression fieldExpr = mock(BooleanFieldExpression.class);
        final BooleanConstantExpression trueExpr = mock(BooleanConstantExpression.class);
        final EqualToExpressionImpl equalExpr = mock(EqualToExpressionImpl.class);

        final ExpressionFactory expressionFactory = mock(ExpressionFactory.class);
        when(expressionFactory.createBooleanConstantExpression(true)).thenReturn(trueExpr);
        when(expressionFactory.createEqualsExpression(same(fieldExpr), same(trueExpr))).thenReturn(equalExpr);

        final ExpressionCollection underTest = new ExpressionCollection(expressionFactory);

        //--Act
        underTest.add(fieldExpr);

        //--Assert
        Assert.assertSame(equalExpr, underTest.iterator().next());
    }


    @Test
    public void test_that_a_different_type_of_boolean_expression_is_added_normally() {
        //--Arrange
        final BooleanExpression expr = mock(BooleanExpression.class);
        final ExpressionCollection underTest = new ExpressionCollection(null);

        //--Act
        underTest.add(expr);

        //--Assert
        Assert.assertSame(expr, underTest.iterator().next());
    }
}
