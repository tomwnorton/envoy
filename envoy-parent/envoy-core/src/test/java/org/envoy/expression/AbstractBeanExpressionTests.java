package org.envoy.expression;

import org.junit.*;

import java.beans.*;
import java.util.*;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

/**
 * Created by thomas on 7/20/14.
 */
public class AbstractBeanExpressionTests {
    @Test
    public void test_that_the_property_fieldExpressions_contains_the_correct_FieldExpression_instances() {
        //--Arrange
        OuterBeanExpression underTest = new OuterBeanExpression();
        List<FieldExpression> expected = Arrays.<FieldExpression>asList(
                underTest.getAge(),
                underTest.getName(),
                underTest.getInnerBean().getPrice(),
                underTest.isActive());

        //--Act
        List<FieldExpression> actual = underTest.getFieldExpressions();

        //--Assert
        assertThat(actual.containsAll(expected), is(true));
    }


    public static class OuterBeanExpression extends AbstractOuterBeanExpression {
        private IntegerFieldExpression age = new IntegerFieldExpression(this);
        private StringFieldExpression name = new StringFieldExpression(this);
        private InnerBeanExpression innerBean = new InnerBeanExpression();


        public IntegerFieldExpression getAge() {
            return age;
        }


        public StringFieldExpression getName() {
            return name;
        }


        public InnerBeanExpression getInnerBean() {
            return innerBean;
        }

        @Override
        public PropertyDescriptor getPropertyDescriptor() {
            return null;
        }
    }

    public static abstract class AbstractOuterBeanExpression extends AbstractTestBeanExpression {
        private BooleanFieldExpression active = new BooleanFieldExpression(this);


        public BooleanFieldExpression isActive() {
            return active;
        }
    }

    public static class InnerBeanExpression extends AbstractTestBeanExpression {
        private BigDecimalFieldExpression price = new BigDecimalFieldExpression(this);


        public BigDecimalFieldExpression getPrice() {
            return price;
        }

        @Override
        public PropertyDescriptor getPropertyDescriptor() {
            return null;
        }
    }

    public static abstract class AbstractTestBeanExpression extends AbstractBeanExpression<Object> {
        public AbstractTestBeanExpression() {
            super(Object.class);
        }


        @Override
        public Object getFromRootEntity(Object root) {
            return null;
        }


        @Override
        public void setFromRootEntity(Object root, Object value) {

        }


        @Override
        public String getTableAlias() {
            return null;
        }


        @Override
        public EntityExpression<?> getEntityExpression() {
            return null;
        }


        @Override
        public void setEntityExpression(EntityExpression<?> entityExpression) {

        }


        @Override
        public FieldAliasCache getFieldAliasCache() {
            return null;
        }


        @Override
        public void accept(ExpressionVisitor visitor) {

        }
    }
}
