package org.envoy.expression;

import org.junit.*;
import org.junit.runner.*;
import org.powermock.core.classloader.annotations.*;
import org.powermock.modules.junit4.*;

import java.io.*;
import java.util.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ExpressionFactoryImpl.class)
public class ExpressionFactoryImplTests {
    private ExpressionFactoryImpl underTest;


    @Before
    public void setUp() {
        underTest = new ExpressionFactoryImpl();
    }


    @Test
    public void test_that_createEqualsExpression_returns_the_correct_EqualToExpression_instance() throws Exception {
        //--Arrange
        final EqualToExpressionImpl equalExpression = mock(EqualToExpressionImpl.class);
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);

        whenNew(EqualToExpressionImpl.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(equalExpression);

        //--Assert
        Assert.assertSame(equalExpression, underTest.createEqualsExpression(lhs, rhs));
    }


    @Test
    public void test_that_createNotEqualExpression_returns_the_correct_NotEqualExpression_instance() throws Exception {
        //--Arrange
        final NotEqualToExpression notEqualExpression = mock(NotEqualToExpression.class);
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);

        whenNew(NotEqualToExpression.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(
                notEqualExpression);

        //--Assert
        Assert.assertSame(notEqualExpression, underTest.createNotEqualExpression(lhs, rhs));
    }


    @Test
    public void test_that_createAndExpression_returns_the_correct_AndExpression_instance() throws Exception {
        //--Arrange
        final AndExpression andExpression = mock(AndExpression.class);
        final BooleanExpression lhs = mock(BooleanExpression.class);
        final BooleanExpression rhs = mock(BooleanExpression.class);

        whenNew(AndExpression.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(andExpression);

        //--Assert
        Assert.assertSame(andExpression, underTest.createAndExpression(lhs, rhs));
    }


    @Test
    public void test_that_createOrExpression_returns_the_correct_OrExpression_instance() throws Exception {
        //--Arrange
        final OrExpression orExpression = mock(OrExpression.class);
        final BooleanExpression lhs = mock(BooleanExpression.class);
        final BooleanExpression rhs = mock(BooleanExpression.class);

        whenNew(OrExpression.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(orExpression);

        //--Assert
        Assert.assertSame(orExpression, underTest.createOrExpression(lhs, rhs));
    }


    @Test
    public void test_that_createNotExpression_returns_the_correct_NotExpression_instance() throws Exception {
        //--Arrange
        final NotExpression notExpression = mock(NotExpression.class);
        final BooleanExpression expr = mock(BooleanExpression.class);

        whenNew(NotExpression.class).withArguments(same(expr), same(underTest)).thenReturn(notExpression);

        //--Assert
        Assert.assertSame(notExpression, underTest.createNotExpression(expr));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_createInExpression_returns_the_correct_InExpression_instance() throws Exception {
        //--Arrange
        final InExpression<Integer> inExpression = mock(InExpression.class);
        final Expression expr = mock(Expression.class);
        final Collection<Integer> collection = mock(Collection.class);
        whenNew(InExpression.class).withArguments(same(expr), same(collection), same(underTest)).thenReturn(inExpression);

        //--Assert
        Assert.assertSame(inExpression, underTest.createInExpression(expr, collection));
    }


    @Test
    public void test_that_createBooleanConstantExpression_returns_the_constant_for_true_when_true_is_passed_in() throws
            Exception {
        //--Arrange
        final BooleanConstantExpression expr = mock(BooleanConstantExpression.class);
        whenNew(BooleanConstantExpression.class).withArguments(true).thenReturn(expr);

        //--Assert
        Assert.assertSame(expr, underTest.createBooleanConstantExpression(true));
    }


    @Test
    public void test_that_createBooleanConstantExpression_returns_the_constant_for_false_when_false_is_passed_in() throws
            Exception {
        //--Arrange
        final BooleanConstantExpression expr = mock(BooleanConstantExpression.class);
        whenNew(BooleanConstantExpression.class).withArguments(false).thenReturn(expr);

        //--Assert
        Assert.assertSame(expr, underTest.createBooleanConstantExpression(false));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_createParameterExpression_returns_the_correct_ParameterExpression() throws Exception {
        //--Arrange
        final Serializable value = mock(Serializable.class);
        final ParameterExpression<Serializable> expected = mock(ParameterExpression.class);
        whenNew(ParameterExpression.class).withArguments(same(value)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createParameterExpression(value));
    }

    private class BasicSerializableExpression<T extends Serializable> extends
            AbstractObjectInstanceExpression<T, T, BasicSerializableExpression<T>> {
        BasicSerializableExpression() {
            super(null);
        }


        @Override
        public void accept(final ExpressionVisitor visitor) {
            //To change body of implemented methods use File | Settings | File Templates.
        }


        @Override
        protected BasicSerializableExpression<T> coalesceInternal(final List<GenericExpression<T>> exprs) {
            return null;
        }


        @Override
        public Class<?> getType() {
            return null;
        }
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_createLessThanExpression_returns_the_correct_LessThanExpression() throws Exception {
        //--Arrange
        final AbstractNumericExpression<Number> lhs = mock(AbstractNumericExpression.class);
        final AbstractNumericExpression<Number> rhs = mock(AbstractNumericExpression.class);
        final LessThanExpression expected = mock(LessThanExpression.class);
        whenNew(LessThanExpression.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createLessThanExpression(lhs, rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_createLessThanEqualExpression_returns_the_correct_LessThanEqualToExpression() throws Exception {
        //--Arrange
        final AbstractNumericExpression<Number> lhs = mock(AbstractNumericExpression.class);
        final AbstractNumericExpression<Number> rhs = mock(AbstractNumericExpression.class);
        final LessThanEqualExpression expected = mock(LessThanEqualExpression.class);
        whenNew(LessThanEqualExpression.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createLessThanEqualExpression(lhs, rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_createGreaterThanExpression_returns_the_correct_GreaterThanToExpression() throws Exception {
        //--Arrange
        final AbstractNumericExpression<Number> lhs = mock(AbstractNumericExpression.class);
        final AbstractNumericExpression<Number> rhs = mock(AbstractNumericExpression.class);
        final GreaterThanExpression expected = mock(GreaterThanExpression.class);
        whenNew(GreaterThanExpression.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createGreaterThanExpression(lhs, rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_createGreaterThanEqualExpression_returns_the_correct_GreaterThanEqualToExpression() throws Exception {
        //--Arrange
        final AbstractNumericExpression<Number> lhs = mock(AbstractNumericExpression.class);
        final AbstractNumericExpression<Number> rhs = mock(AbstractNumericExpression.class);
        final GreaterThanEqualExpression expected = mock(GreaterThanEqualExpression.class);
        whenNew(GreaterThanEqualExpression.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createGreaterThanEqualExpression(lhs, rhs));
    }


    @Test
    public void test_that_createConvertToStringExpression_returns_the_correct_ConvertToStringExpression() throws Exception {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final ConvertToStringExpression expected = mock(ConvertToStringExpression.class);
        whenNew(ConvertToStringExpression.class).withArguments(same(lhs), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createConvertToStringExpression(lhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_createPlusIntegerExpression_returns_the_correct_PlusIntegerExpression() throws Exception {
        //--Arrange
        final AbstractNumericExpression<Number> lhs = mock(AbstractNumericExpression.class);
        final AbstractNumericExpression<Number> rhs = mock(AbstractNumericExpression.class);
        final PlusIntegerExpression expected = mock(PlusIntegerExpression.class);
        whenNew(PlusIntegerExpression.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createPlusIntegerExpression(lhs, rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_createPlusFloatingPointExpression_returns_the_correct_PlusFloatingPointExpression() throws Exception {
        //--Arrange
        final AbstractNumericExpression<Number> lhs = mock(AbstractNumericExpression.class);
        final AbstractNumericExpression<Number> rhs = mock(AbstractNumericExpression.class);
        final PlusFloatingPointExpression expected = mock(PlusFloatingPointExpression.class);
        whenNew(PlusFloatingPointExpression.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createPlusFloatingPointExpression(lhs, rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_createMinusIntegerExpression_returns_the_correct_MinusIntegerExpression() throws Exception {
        //--Arrange
        final AbstractNumericExpression<Number> lhs = mock(AbstractNumericExpression.class);
        final AbstractNumericExpression<Number> rhs = mock(AbstractNumericExpression.class);
        final MinusIntegerExpression expected = mock(MinusIntegerExpression.class);
        whenNew(MinusIntegerExpression.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createMinusIntegerExpression(lhs, rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_createMinusFloatingPointExpression_returns_the_correct_MinusFloatingPointExpression() throws
            Exception {
        //--Arrange
        final AbstractNumericExpression<Number> lhs = mock(AbstractNumericExpression.class);
        final AbstractNumericExpression<Number> rhs = mock(AbstractNumericExpression.class);
        final MinusFloatingPointExpression expected = mock(MinusFloatingPointExpression.class);
        whenNew(MinusFloatingPointExpression.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createMinusFloatingPointExpression(lhs, rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_createTimesIntegerExpression_returns_the_correct_TimesIntegerExpression() throws Exception {
        //--Arrange
        final AbstractNumericExpression<Number> lhs = mock(AbstractNumericExpression.class);
        final AbstractNumericExpression<Number> rhs = mock(AbstractNumericExpression.class);
        final TimesIntegerExpression expected = mock(TimesIntegerExpression.class);
        whenNew(TimesIntegerExpression.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createTimesIntegerExpression(lhs, rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_createTimesFloatingPointExpression_returns_the_correct_TimesFloatingPointExpression() throws
            Exception {
        //--Arrange
        final AbstractNumericExpression<Number> lhs = mock(AbstractNumericExpression.class);
        final AbstractNumericExpression<Number> rhs = mock(AbstractNumericExpression.class);
        final TimesFloatingPointExpression expected = mock(TimesFloatingPointExpression.class);
        whenNew(TimesFloatingPointExpression.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createTimesFloatingPointExpression(lhs, rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_createDividedByIntegerExpression_returns_the_correct_DividedByIntegerExpression() throws Exception {
        //--Arrange
        final AbstractNumericExpression<Number> lhs = mock(AbstractNumericExpression.class);
        final AbstractNumericExpression<Number> rhs = mock(AbstractNumericExpression.class);
        final DividedByIntegerExpression expected = mock(DividedByIntegerExpression.class);
        whenNew(DividedByIntegerExpression.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createDividedByIntegerExpression(lhs, rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_createDividedByFloatingPointExpression_returns_the_correct_DividedByFloatingPointExpression()
            throws Exception {
        //--Arrange
        final AbstractNumericExpression<Number> lhs = mock(AbstractNumericExpression.class);
        final AbstractNumericExpression<Number> rhs = mock(AbstractNumericExpression.class);
        final DividedByFloatingPointExpression expected = mock(DividedByFloatingPointExpression.class);
        whenNew(DividedByFloatingPointExpression.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(
                expected);

        //--Assert
        Assert.assertSame(expected, underTest.createDividedByFloatingPointExpression(lhs, rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_createModulusIntegerExpression_returns_the_correct_ModulusIntegerExpression() throws Exception {
        //--Arrange
        final AbstractNumericExpression<Number> lhs = mock(AbstractNumericExpression.class);
        final AbstractNumericExpression<Number> rhs = mock(AbstractNumericExpression.class);
        final ModulusIntegerExpression expected = mock(ModulusIntegerExpression.class);
        whenNew(ModulusIntegerExpression.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createModulusIntegerExpression(lhs, rhs));
    }


    @Test
    public void test_that_createCoalesceBooleanExpression_returns_the_correct_CoalesceBooleanExpression() throws Exception {
        //--Arrange
        final List<GenericExpression<Boolean>> exprs = new ArrayList<GenericExpression<Boolean>>();
        final CoalesceBooleanExpression expected = mock(CoalesceBooleanExpression.class);
        whenNew(CoalesceBooleanExpression.class).withArguments(same(exprs), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createCoalesceBooleanExpression(exprs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_createCoalesceIntegerExpression_returns_the_correct_CoalesceIntegerExpression() throws Exception {
        //--Arrange
        final List<GenericExpression<Number>> exprs = new ArrayList<GenericExpression<Number>>();
        final CoalesceIntegerExpression<Number> expected = mock(CoalesceIntegerExpression.class);
        whenNew(CoalesceIntegerExpression.class).withArguments(same(exprs), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createCoalesceIntegerExpression(exprs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_createCoalesceFloatingPointExpression_returns_the_correct_CoalesceFloatingPointExpression()
            throws Exception {
        //--Arrange
        final List<GenericExpression<Number>> exprs = new ArrayList<GenericExpression<Number>>();
        final CoalesceFloatingPointExpression<Number> expected = mock(CoalesceFloatingPointExpression.class);
        whenNew(CoalesceFloatingPointExpression.class).withArguments(same(exprs), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createCoalesceFloatingPointExpression(exprs));
    }


    @Test
    public void test_that_createNegativeIntegerExpression_returns_the_correct_NegativeIntegerExpression() throws Exception {
        //--Arrange
        final Expression operand = mock(Expression.class);
        final NegativeIntegerExpression<Number> expected = new NegativeIntegerExpression<Number>(null, null);
        whenNew(NegativeIntegerExpression.class).withArguments(same(operand), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createNegativeIntegerExpression(operand));
    }


    @Test
    public void test_that_createNegativeFloatingPointExpression_returns_the_correct_NegativeFloatingPointExpression()
            throws Exception {
        //--Arrange
        final Expression operand = mock(Expression.class);
        final NegativeFloatingPointExpression<Number> expected = new NegativeFloatingPointExpression<Number>(null, null);
        whenNew(NegativeFloatingPointExpression.class).withArguments(same(operand), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createNegativeFloatingPointExpression(operand));
    }


    @Test
    public void test_that_createBetweenExpression_returns_the_correct_BetweenExpression() throws Exception {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression lowerBound = mock(Expression.class);
        final Expression upperBound = mock(Expression.class);
        final BetweenExpression expected = new BetweenExpression(null, null, null, null);
        whenNew(BetweenExpression.class)
                .withArguments(same(lhs), same(lowerBound), same(upperBound), same(underTest))
                .thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createBetweenExpression(lhs, lowerBound, upperBound));
    }


    @Test
    public void test_that_createCoalesceStringExpression_returns_the_correct_CoalesceStringExpression() throws Exception {
        //--Arrange
        final List<GenericExpression<String>> exprs = new ArrayList<GenericExpression<String>>();
        final CoalesceStringExpression expected = new CoalesceStringExpression(null, null);
        whenNew(CoalesceStringExpression.class).withArguments(same(exprs), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createCoalesceStringExpression(exprs));
    }


    @Test
    public void test_that_createTrimExpression_returns_the_correct_TrimExpression() throws Exception {
        //--Arrange
        final Expression expr = mock(Expression.class);
        final TrimExpression expected = new TrimExpression(null, null);
        whenNew(TrimExpression.class).withArguments(same(expr), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createTrimExpression(expr));
    }


    @Test
    public void test_that_createLengthExpression_returns_the_correct_LengthExpression() throws Exception {
        //--Arrange
        final Expression expr = mock(Expression.class);
        final LengthExpression expected = new LengthExpression(null, null);
        whenNew(LengthExpression.class).withArguments(same(expr), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createLengthExpression(expr));
    }


    @Test
    public void test_that_createLowerCaseExpression_returns_the_correct_LowerCaseExpression() throws Exception {
        //--Arrange
        final Expression expr = mock(Expression.class);
        final LowerCaseExpression expected = new LowerCaseExpression(null, null);
        whenNew(LowerCaseExpression.class).withArguments(same(expr), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createLowerCaseExpression(expr));
    }


    @Test
    public void test_that_createUpperCaseExpression_returns_the_correct_UpperCaseExpression() throws Exception {
        //--Arrange
        final Expression expr = mock(Expression.class);
        final UpperCaseExpression expected = new UpperCaseExpression(null, null);
        whenNew(UpperCaseExpression.class).withArguments(same(expr), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createUpperCaseExpression(expr));
    }


    @Test
    public void test_that_createConvertStringToIntegerExpression_returns_the_correct_ConvertStringToIntegerExpression()
            throws Exception {
        //--Arrange
        final Expression expr = mock(Expression.class);
        final ConvertStringToIntegerExpression expected = new ConvertStringToIntegerExpression(null, null);
        whenNew(ConvertStringToIntegerExpression.class).withArguments(same(expr), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createConvertStringToIntegerExpression(expr));
    }


    @Test
    public void test_that_createConcatExpression_returns_the_correct_ConcatExpression() throws Exception {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);
        final ConcatExpression expected = new ConcatExpression(null, null, null);
        whenNew(ConcatExpression.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createConcatExpression(lhs, rhs));
    }


    @Test
    public void test_that_createContainsExpression_returns_the_correct_ContainsExpression() throws Exception {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);
        final ContainsExpression expected = new ContainsExpression(null, null, null);
        whenNew(ContainsExpression.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createContainsExpression(lhs, rhs));
    }


    @Test
    public void test_that_createStartsWithExpression_returns_the_correct_StartsWithExpression() throws Exception {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);
        final StartsWithExpression expected = new StartsWithExpression(null, null, null);
        whenNew(StartsWithExpression.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createStartsWithExpression(lhs, rhs));
    }


    @Test
    public void test_that_createEndsWithExpression_returns_the_correct_EndsWithExpression() throws Exception {
        //--Arrange
        final Expression lhs = mock(Expression.class);
        final Expression rhs = mock(Expression.class);
        final EndsWithExpression expected = new EndsWithExpression(null, null, null);
        whenNew(EndsWithExpression.class).withArguments(same(lhs), same(rhs), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createEndsWithExpression(lhs, rhs));
    }


    @Test
    public void test_that_createCoalesceCalendarExpression_returns_the_correct_CoalesceCalendarExpression() throws Exception {
        //--Arrange
        final List<GenericExpression<Calendar>> exprs = new ArrayList<GenericExpression<Calendar>>();
        final CoalesceCalendarExpression expected = new CoalesceCalendarExpression(null, null);
        whenNew(CoalesceCalendarExpression.class).withArguments(same(exprs), same(underTest)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createCoalesceCalendarExpression(exprs));
    }


    @Test
    public void test_that_createSelectListExpression_returns_the_correct_SelectListExpression() throws Exception {
        //--Arrange
        final List<Expression> exprs = new ArrayList<Expression>();
        final SelectListExpression expected = new SelectListExpression(null);
        whenNew(SelectListExpression.class).withArguments(same(exprs)).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.createSelectListExpression(exprs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_createSelectExpression_returns_the_correct_SelectExpression() throws Exception {
        //--Arrange
        final SelectListExpression selectListExpression = new SelectListExpression(null);
        final EntityExpression<Object> fromExpression = mock(EntityExpression.class);
        final BooleanExpression whereExpression = mock(BooleanExpression.class);
        final SelectExpression<Object> expected = new SelectExpression<>(null, null, null);
        whenNew(SelectExpression.class).withArguments(same(selectListExpression), same(fromExpression), same(whereExpression))
                .thenReturn(expected);

        //--Assert
        Assert.assertSame(
                expected, underTest.createSelectExpression(
                        selectListExpression,
                        fromExpression,
                        whereExpression));
    }
}
