package org.envoy.expression;

import org.junit.*;

import java.util.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class AbstractCalendarExpressionTests extends ExpressionTestBase<AbstractCalendarExpression> {
    private class UnderTest extends AbstractCalendarExpression {
        UnderTest(final ExpressionFactory expressionFactory) {
            super(expressionFactory);
        }


        @Override
        public void accept(final ExpressionVisitor visitor) {
        }
    }


    @Override
    protected AbstractCalendarExpression createTestObjectForExpressionFactoryTest(final ExpressionFactory expressionFactory) {
        return new UnderTest(expressionFactory);
    }


    @Test
    public void test_that_coalesceInternal_returns_the_correct_CoalesceCalendarExpression_instance() {
        //--Arrange
        final List<GenericExpression<Calendar>> exprs = new ArrayList<GenericExpression<Calendar>>();
        final CoalesceCalendarExpression expected = new CoalesceCalendarExpression(null, null);
        when(expressionFactory.createCoalesceCalendarExpression(same(exprs))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.coalesceInternal(exprs));
    }
}
