package org.envoy.expression;

public class StringFieldExpressionTests extends GenericFieldExpressionTestBase<String> {
    @Override
    protected GenericFieldExpression<String> createUnderTest(
            final ExpressionFactory expressionFactory,
            final CommonFieldExpressionLogic commonFieldExpressionLogic) {
        return new StringFieldExpression(expressionFactory, commonFieldExpressionLogic);
    }


    @Override
    protected String createExpectedValue() {
        return "hello";
    }
}
