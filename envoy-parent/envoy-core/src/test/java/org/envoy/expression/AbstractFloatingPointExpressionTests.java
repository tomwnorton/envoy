package org.envoy.expression;

import org.junit.*;

import java.util.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class AbstractFloatingPointExpressionTests extends ExpressionTestBase<AbstractFloatingPointExpression<Number>> {
    private class UnderTest extends AbstractFloatingPointExpression<Number> {

        public UnderTest(final ExpressionFactory expressionFactory) {
            super(expressionFactory);
        }


        @Override
        public void accept(final ExpressionVisitor visitor) {
        }


        @Override
        public Class<?> getType() {
            return null;
        }
    }


    @Override
    protected AbstractFloatingPointExpression<Number> createTestObjectForExpressionFactoryTest(
            final ExpressionFactory expressionFactory) {
        return new UnderTest(expressionFactory);
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_plus_FloatingPointExpression_returns_the_correct_FloatingPointExpression() {
        //--Arrange
        final FloatingPointExpression<Number> rhs = mock(FloatingPointExpression.class);
        final PlusFloatingPointExpression expected = new PlusFloatingPointExpression(null, null, null);
        when(expressionFactory.createPlusFloatingPointExpression(same(underTest), same(rhs))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.plus(rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_minus_FloatingPointExpression_returns_the_correct_FloatingPointExpression() {
        //--Arrange
        final FloatingPointExpression<Number> rhs = mock(FloatingPointExpression.class);
        final MinusFloatingPointExpression expected = new MinusFloatingPointExpression(null, null, null);
        when(expressionFactory.createMinusFloatingPointExpression(same(this.underTest), same(rhs))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.minus(rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_times_FloatingPointExpression_returns_the_correct_FloatingPointExpression() {
        //--Arrange
        final FloatingPointExpression<Number> rhs = mock(FloatingPointExpression.class);
        final TimesFloatingPointExpression expected = new TimesFloatingPointExpression(null, null, null);
        when(expressionFactory.createTimesFloatingPointExpression(same(underTest), same(rhs))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.times(rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_dividedBy_FloatingPointExpression_returns_the_correct_FloatingPointExpression() {
        //--Arrange
        final FloatingPointExpression<Number> rhs = mock(FloatingPointExpression.class);
        final DividedByFloatingPointExpression expected = new DividedByFloatingPointExpression(null, null, null);
        when(expressionFactory.createDividedByFloatingPointExpression(same(this.underTest), same(rhs))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.dividedBy(rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_coalesceInternal_returns_the_correct_CoalesceFloatingPointExpression_instance() {
        //--Arrange
        final GenericExpression<Number> firstExpr = mock(GenericExpression.class);
        final GenericExpression<Number> secondExpr = mock(GenericExpression.class);
        final List<GenericExpression<Number>> exprs = Arrays.<GenericExpression<Number>>asList(
                underTest,
                firstExpr,
                secondExpr);
        final CoalesceFloatingPointExpression<Number> expected = mock(CoalesceFloatingPointExpression.class);
        when(this.expressionFactory.createCoalesceFloatingPointExpression(exprs)).thenReturn(expected);

        //--Assert
        Assert.assertSame(
                expected,
                underTest.coalesceInternal(Arrays.<GenericExpression<Number>>asList(firstExpr, secondExpr)));
    }


    @Test
    public void test_that_negative_returns_the_correct_NegativeFloatingPointExpression_instance() {
        //--Arrange
        final NegativeFloatingPointExpression<Number> expected = new NegativeFloatingPointExpression<Number>(null, null);
        when(expressionFactory.createNegativeFloatingPointExpression(same(underTest))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.negative());
    }
}
