package org.envoy.expression;

import org.junit.*;

import java.util.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class CoalesceBooleanExpressionTests extends ExpressionTestBase<CoalesceBooleanExpression> {
    private final List<Expression> expectedList = new ArrayList<Expression>();


    @Override
    protected CoalesceBooleanExpression createTestObjectForExpressionFactoryTest(final ExpressionFactory expressionFactory) {
        return new CoalesceBooleanExpression(expectedList, expressionFactory);
    }


    @Test
    public void test_that_visitCoalesce_is_called_with_the_correct_arguments() {
        //--Act
        underTest.accept(expressionVisitor);

        //--Assert
        verify(expressionVisitor).visitCoalesce(same(expectedList));
    }
}
