package org.envoy.expression;

import org.junit.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class MinusIntegerExpressionTests extends BinaryExpressionTestBase {
    @Override
    protected AbstractExpression<?> createTestObject(
            final Expression lhs,
            final Expression rhs,
            final ExpressionFactory expressionFactory) {
        return new MinusIntegerExpression(lhs, rhs, expressionFactory);
    }


    @Test
    public void test_that_accept_calls_visitMinus_with_the_correct_arguments() {
        //--Act
        underTest.accept(expressionVisitor);

        //--Assert
        verify(expressionVisitor).visitMinus(same(lhs), same(rhs));
    }
}
