package org.envoy.expression;

import org.junit.*;
import org.mockito.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

/**
 * Created with IntelliJ IDEA.
 * User: thomas
 * Date: 2/3/13
 * Time: 12:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class LessThanExpressionTests {
    @Mock
    private ExpressionFactory expressionFactory;

    @Mock
    private ExpressionVisitor visitor;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void test_that_the_ExpressionFactory_is_passed_to_the_super_class() {
        //--Act
        final LessThanExpression underTest = new LessThanExpression(null, null, expressionFactory);

        //--Assert
        Assert.assertSame(expressionFactory, underTest.getExpressionFactory());
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_accept_calls_visitLessThan_with_the_correct_arguments() {
        //--Arrange
        final AbstractNumericExpression<Number> lhs = mock(AbstractNumericExpression.class);
        final AbstractNumericExpression<Number> rhs = mock(AbstractNumericExpression.class);
        final LessThanExpression underTest = new LessThanExpression(lhs, rhs, null);

        //--Act
        underTest.accept(visitor);

        //--Assert
        verify(visitor).visitLessThan(same(lhs), same(rhs));
    }
}
