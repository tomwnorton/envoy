package org.envoy.expression;

public class AbstractIntegerFieldExpressionTests extends GenericFieldExpressionTestBase<Integer> {
    @Override
    protected GenericFieldExpression<Integer> createUnderTest(
            final ExpressionFactory expressionFactory,
            final CommonFieldExpressionLogic commonFieldExpressionLogic) {
        return new TestIntegerFieldExpression(expressionFactory, commonFieldExpressionLogic);
    }

    private class TestIntegerFieldExpression extends AbstractIntegerFieldExpression<Integer, TestIntegerFieldExpression> {
        TestIntegerFieldExpression(
                ExpressionFactory expressionFactory,
                CommonFieldExpressionLogic commonFieldExpressionLogic) {
            super(expressionFactory, commonFieldExpressionLogic);
        }

        @Override
        public Class<Integer> getType() {
            return Integer.class;
        }
    }


    @Override
    protected Integer createExpectedValue() {
        return new Integer(27);
    }
}
