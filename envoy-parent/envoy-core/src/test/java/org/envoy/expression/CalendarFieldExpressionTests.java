package org.envoy.expression;

import java.util.*;

public class CalendarFieldExpressionTests extends GenericFieldExpressionTestBase<Calendar> {
    @Override
    protected GenericFieldExpression<Calendar> createUnderTest(
            final ExpressionFactory expressionFactory,
            final CommonFieldExpressionLogic commonFieldExpressionLogic) {
        return new CalendarFieldExpression(expressionFactory, commonFieldExpressionLogic);
    }


    @Override
    protected Calendar createExpectedValue() {
        return new GregorianCalendar();
    }
}
