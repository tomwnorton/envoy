package org.envoy.expression;

import org.junit.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class ConvertToStringExpressionTests extends UnaryExpressionTestBase {
    @Test
    public void test_that_accept_calls_visitConvertToString_with_the_correct_argument() {
        //--Act
        underTest.accept(expressionVisitor);

        //--Assert
        verify(expressionVisitor).visitConvertToString(same(operand));
    }


    @Override
    protected AbstractExpression<?> createTestObject(final Expression expression, final ExpressionFactory expressionFactory) {
        return new ConvertToStringExpression(expression, expressionFactory);
    }
}
