package org.envoy.expression;

import org.junit.*;

import java.io.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class AbstractEntityExpressionTests {
    @SuppressWarnings("serial")
    private class TestEntity implements Serializable {
    }

    private class TestEntityExpression extends AbstractEntityExpression<TestEntity> {
        public TestEntityExpression() {
            super(TestEntity.class);
        }
    }


    @Test
    public void test_that_getFromRootEntity_returns_the_given_entity() {
        //--Arrange
        final TestEntity entity = new TestEntity();
        final TestEntityExpression underTest = new TestEntityExpression();

        //--Assert
        Assert.assertSame(entity, underTest.getFromRootEntity(entity));
    }


    @Test
    public void test_that_getTableAlias_returns_foo_from_fieldAliasCache() {
        //--Arrange
        final FieldAliasCache cache = mock(FieldAliasCache.class);

        final TestEntityExpression underTest = new TestEntityExpression();
        underTest.setFieldAliasCache(cache);

        when(cache.getFieldAlias(same(underTest))).thenReturn("foo");

        //--Assert
        Assert.assertEquals("foo", underTest.getTableAlias());
    }


    @Test
    public void test_that_getTableAlias_returns_bar_from_fieldAliasCache() {
        //--Arrange
        final FieldAliasCache cache = mock(FieldAliasCache.class);

        final TestEntityExpression underTest = new TestEntityExpression();
        underTest.setFieldAliasCache(cache);

        when(cache.getFieldAlias(same(underTest))).thenReturn("bar");

        //--Assert
        Assert.assertEquals("bar", underTest.getTableAlias());
    }


    @Test
    public void test_that_accept_calls_visitEntity_with_the_entityExpression() {
        //--Arrange
        final ExpressionVisitor visitor = mock(ExpressionVisitor.class);
        final TestEntityExpression underTest = new TestEntityExpression();

        //--Act
        underTest.accept(visitor);

        //--Assert
        verify(visitor).visitEntity(same(underTest));
    }
}
