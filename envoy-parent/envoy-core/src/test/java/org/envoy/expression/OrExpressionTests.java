package org.envoy.expression;

import org.junit.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class OrExpressionTests {
    @Test
    public void test_that_expressionFactory_is_the_correct_ExpressionFactory() {
        //--Arrange
        final ExpressionFactory expressionFactory = mock(ExpressionFactory.class);
        final OrExpression underTest = new OrExpression(null, null, expressionFactory);

        //--Assert
        Assert.assertSame(expressionFactory, underTest.getExpressionFactory());
    }


    @Test
    public void test_that_accept_calls_visitOr_with_the_correct_arguments_when_lhs_and_rhs_are_normal_boolean_expressions() {
        //--Arrange
        final BooleanExpression lhs = mock(BooleanExpression.class);
        final BooleanExpression rhs = mock(BooleanExpression.class);
        final ExpressionVisitor visitor = mock(ExpressionVisitor.class);
        final OrExpression underTest = new OrExpression(lhs, rhs, null);

        //--Act
        underTest.accept(visitor);

        //--Assert
        verify(visitor).visitOr(same(lhs), same(rhs));
    }


    @Test
    public void test_that_accept_calls_visitOr_with_lhs_being_an_equal_to_expression_when_lhs_is_a_boolean_field_expression() {
        //--Arrange
        final BooleanFieldExpression lhs = mock(BooleanFieldExpression.class);
        final BooleanExpression rhs = mock(BooleanExpression.class);
        final BooleanConstantExpression trueExpr = mock(BooleanConstantExpression.class);
        final EqualToExpressionImpl equalExpr = mock(EqualToExpressionImpl.class);
        final ExpressionVisitor visitor = mock(ExpressionVisitor.class);
        final ExpressionFactory expressionFactory = mock(ExpressionFactory.class);
        final OrExpression underTest = new OrExpression(lhs, rhs, expressionFactory);

        when(expressionFactory.createBooleanConstantExpression(true)).thenReturn(trueExpr);
        when(expressionFactory.createEqualsExpression(lhs, trueExpr)).thenReturn(equalExpr);

        //--Act
        underTest.accept(visitor);

        //--Assert
        verify(visitor).visitOr(same(equalExpr), same(rhs));
    }


    @Test
    public void test_that_accept_calls_visitOr_with_rhs_being_an_equal_to_expression_when_rhs_is_a_boolean_field_expression() {
        //--Arrange
        final BooleanExpression lhs = mock(BooleanExpression.class);
        final BooleanFieldExpression rhs = mock(BooleanFieldExpression.class);
        final BooleanConstantExpression trueExpr = mock(BooleanConstantExpression.class);
        final EqualToExpressionImpl equalExpr = mock(EqualToExpressionImpl.class);
        final ExpressionVisitor visitor = mock(ExpressionVisitor.class);
        final ExpressionFactory expressionFactory = mock(ExpressionFactory.class);
        final OrExpression underTest = new OrExpression(lhs, rhs, expressionFactory);

        when(expressionFactory.createBooleanConstantExpression(true)).thenReturn(trueExpr);
        when(expressionFactory.createEqualsExpression(rhs, trueExpr)).thenReturn(equalExpr);

        //--Act
        underTest.accept(visitor);

        //--Assert
        verify(visitor).visitOr(same(lhs), same(equalExpr));
    }
}
