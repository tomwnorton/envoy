package org.envoy.expression;

import org.junit.*;
import org.mockito.*;

import java.io.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public abstract class GenericFieldExpressionTestBase<T> {
    @Mock
    private ExpressionFactory expressionFactory;

    @Mock
    private EntityExpression<Object> entityExpression;

    @Mock
    protected CommonFieldExpressionLogic commonFieldExpressionLogic;

    protected GenericFieldExpression<T> underTest;


    protected abstract GenericFieldExpression<T> createUnderTest(
            ExpressionFactory expressionFactory,
            CommonFieldExpressionLogic commonFieldExpressionLogic);


    protected abstract T createExpectedValue();


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        underTest = createUnderTest(expressionFactory, commonFieldExpressionLogic);

        when((Object) commonFieldExpressionLogic.getEntityExpression()).thenReturn(entityExpression);
    }


    @Test
    public void test_that_getFromRootEntity_returns_the_object_returned_by_the_internal_getFromRootEntity_logic() {
        //--Arrange
        final Object expected = createExpectedValue();
        final Object root = mock(Serializable.class);
        when(commonFieldExpressionLogic.getFromRootEntity(same(root))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.getFromRootEntity(root));
    }


    @Test
    public void test_that_setFromRootEntity_calls_the_internal_setFromRootEntity_logic() {
        //--Arrange
        final T value = createExpectedValue();
        final Object root = mock(Serializable.class);

        //--Act
        underTest.setFromRootEntity(root, value);

        //--Assert
        verify(commonFieldExpressionLogic).setFromRootEntity(same(root), same(value));
    }


    @Test
    public void test_that_getFieldName_returns_foo_when_foo_is_returned_from_the_internal_getFieldName_logic() {
        //--Arrange
        when(commonFieldExpressionLogic.getFieldName()).thenReturn("foo");

        //--Assert
        Assert.assertEquals("foo", underTest.getFieldName());
    }


    @Test
    public void test_that_getFieldName_returns_bar_when_bar_is_returned_from_the_internal_getFieldName_logic() {
        //--Arrange
        when(commonFieldExpressionLogic.getFieldName()).thenReturn("bar");

        //--Assert
        Assert.assertEquals("bar", underTest.getFieldName());
    }


    @Test
    public void test_that_setFieldName_sets_the_fieldName_to_foo_when_foo_is_given() {
        //--Act
        underTest.setFieldName("foo");

        //--Assert
        verify(commonFieldExpressionLogic).setFieldName("foo");
    }


    @Test
    public void test_that_setFieldName_sets_the_fieldName_to_bar_when_bar_is_given() {
        //--Act
        underTest.setFieldName("bar");

        //--Assert
        verify(commonFieldExpressionLogic).setFieldName("bar");
    }


    @Test
    public void test_that_getTableAlias_returns_the_value_of_the_internal_getTableAlias_when_that_value_is_foo() {
        //--Arrange
        when(commonFieldExpressionLogic.getTableAlias()).thenReturn("foo");

        //--Assert
        Assert.assertEquals("foo", underTest.getTableAlias());
    }


    @Test
    public void test_that_getTableAlias_returns_the_value_of_the_internal_getTableAlias_when_that_value_is_bar() {
        //--Arrange
        when(commonFieldExpressionLogic.getTableAlias()).thenReturn("bar");

        //--Assert
        Assert.assertEquals("bar", underTest.getTableAlias());
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_getEntityExpression_returns_the_internal_entityExpression() {
        //--Arrange
        final EntityExpression<Object> expected = mock(EntityExpression.class);
        when((Object) commonFieldExpressionLogic.getEntityExpression()).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.getEntityExpression());
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_setEntityExpression_calls_the_internal_setEntityExpression_with_the_correct_argument() {
        //--Arrange
        final EntityExpression<Object> expected = mock(EntityExpression.class);

        //--Act
        underTest.setEntityExpression(expected);

        //--Assert
        verify(commonFieldExpressionLogic).setEntityExpression(same(expected));
    }


    @Test
    public void test_that_accept_calls_visitField_with_the_correct_argument() {
        //--Arrange
        final ExpressionVisitor visitor = mock(ExpressionVisitor.class);

        //--Act
        underTest.accept(visitor);

        //--Assert
        verify(visitor).visitField(underTest);
    }
}
