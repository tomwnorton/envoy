package org.envoy.expression;

import org.junit.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class ConvertStringToIntegerExpressionTests extends UnaryExpressionTestBase {
    @Override
    protected AbstractExpression<?> createTestObject(final Expression operand, final ExpressionFactory expressionFactory) {
        return new ConvertStringToIntegerExpression(operand, expressionFactory);
    }


    @Test
    public void test_that_accept_calls_visitConvertStringToInteger() {
        //--Act
        underTest.accept(expressionVisitor);

        //--Assert
        verify(expressionVisitor).visitConvertStringToInteger(same(operand));
    }
}
