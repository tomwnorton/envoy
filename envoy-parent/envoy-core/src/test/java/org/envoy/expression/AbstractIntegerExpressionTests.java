package org.envoy.expression;

import org.junit.*;

import java.util.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class AbstractIntegerExpressionTests extends ExpressionTestBase<AbstractIntegerExpression<Number>> {
    private class UnderTest extends AbstractIntegerExpression<Number> {

        public UnderTest(final ExpressionFactory expressionFactory) {
            super(expressionFactory);
        }


        @Override
        public void accept(final ExpressionVisitor visitor) {
        }


        @Override
        public Class<?> getType() {
            return null;
        }
    }


    @Override
    protected AbstractIntegerExpression<Number> createTestObjectForExpressionFactoryTest(final ExpressionFactory expressionFactory) {
        return new UnderTest(expressionFactory);
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_plus_IntegerExpression_returns_the_correct_IntegerExpression() {
        //--Arrange
        final IntegerExpression<Number> rhs = mock(IntegerExpression.class);
        final PlusIntegerExpression expected = new PlusIntegerExpression(null, null, null);
        when(expressionFactory.createPlusIntegerExpression(same(underTest), same(rhs))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.plus(rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_plus_FloatingPointExpression_returns_the_correct_FloatingPointExpression() {
        //--Arrange
        final FloatingPointExpression<Number> rhs = mock(FloatingPointExpression.class);
        final PlusFloatingPointExpression expected = new PlusFloatingPointExpression(null, null, null);
        when(expressionFactory.createPlusFloatingPointExpression(same(underTest), same(rhs))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.plus(rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_minus_IntegerExpression_returns_the_correct_IntegerExpression() {
        //--Arrange
        final IntegerExpression<Number> rhs = mock(IntegerExpression.class);
        final MinusIntegerExpression expected = new MinusIntegerExpression(null, null, null);
        when(expressionFactory.createMinusIntegerExpression(same(underTest), same(rhs))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.minus(rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_minus_FloatingPointExpression_returns_the_correct_FloatingPointExpression() {
        //--Arrange
        final FloatingPointExpression<Number> rhs = mock(FloatingPointExpression.class);
        final MinusFloatingPointExpression expected = new MinusFloatingPointExpression(null, null, null);
        when(expressionFactory.createMinusFloatingPointExpression(same(underTest), same(rhs))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.minus(rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_times_IntegerExpression_returns_the_correct_IntegerExpression() {
        //--Arrange
        final IntegerExpression<Number> rhs = mock(IntegerExpression.class);
        final TimesIntegerExpression expected = new TimesIntegerExpression(null, null, null);
        when(expressionFactory.createTimesIntegerExpression(same(underTest), same(rhs))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.times(rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_times_FloatingPointExpression_returns_the_correct_FloatingPointExpression() {
        //--Arrange
        final FloatingPointExpression<Number> rhs = mock(FloatingPointExpression.class);
        final TimesFloatingPointExpression expected = new TimesFloatingPointExpression(null, null, null);
        when(expressionFactory.createTimesFloatingPointExpression(same(underTest), same(rhs))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.times(rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_dividedBy_IntegerExpression_returns_the_correct_IntegerExpression() {
        //--Arrange
        final IntegerExpression<Number> rhs = mock(IntegerExpression.class);
        final DividedByIntegerExpression expected = new DividedByIntegerExpression(null, null, null);
        when(expressionFactory.createDividedByIntegerExpression(same(underTest), same(rhs))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.dividedBy(rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_dividedBy_FloatingPointExpression_returns_the_correct_FloatingPointExpression() {
        //--Arrange
        final FloatingPointExpression<Number> rhs = mock(FloatingPointExpression.class);
        final DividedByFloatingPointExpression expected = new DividedByFloatingPointExpression(null, null, null);
        when(expressionFactory.createDividedByFloatingPointExpression(same(underTest), same(rhs))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.dividedBy(rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_modulus_returns_the_correct_IntegerExpression() {
        //--Arrange
        final IntegerExpression<Number> rhs = mock(IntegerExpression.class);
        final ModulusIntegerExpression expected = new ModulusIntegerExpression(null, null, null);
        when(expressionFactory.createModulusIntegerExpression(same(underTest), same(rhs))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.modulus(rhs));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_coalesceInternal_returns_the_correct_CoalesceIntegerExpression_instance() {
        //--Arrange
        final GenericExpression<Number> firstExpr = mock(GenericExpression.class);
        final GenericExpression<Number> secondExpr = mock(GenericExpression.class);
        final List<GenericExpression<Number>> exprs = Arrays.<GenericExpression<Number>>asList(
                underTest,
                firstExpr,
                secondExpr);
        final CoalesceIntegerExpression<Number> expected = mock(CoalesceIntegerExpression.class);
        when(expressionFactory.createCoalesceIntegerExpression(exprs)).thenReturn(expected);

        //--Assert
        Assert.assertSame(
                expected,
                underTest.coalesceInternal(Arrays.<GenericExpression<Number>>asList(firstExpr, secondExpr)));
    }


    @Test
    public void test_that_negative_returns_the_correct_NegativeIntegerExpression_instance() {
        //--Arrange
        final NegativeIntegerExpression<Number> expected = new NegativeIntegerExpression<Number>(null, null);
        when(expressionFactory.createNegativeIntegerExpression(same(underTest))).thenReturn(expected);

        //--Assert
        Assert.assertSame(expected, underTest.negative());
    }
}
