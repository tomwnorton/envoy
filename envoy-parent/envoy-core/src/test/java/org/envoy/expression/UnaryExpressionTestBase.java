package org.envoy.expression;

import org.mockito.*;

public abstract class UnaryExpressionTestBase extends ExpressionTestBase<AbstractExpression<?>> {
    @Mock
    protected Expression operand;


    protected abstract AbstractExpression<?> createTestObject(Expression operand, ExpressionFactory expressionFactory);


    @Override
    protected AbstractExpression<?> createTestObjectForExpressionFactoryTest(final ExpressionFactory expressionFactory) {
        return createTestObject(operand, expressionFactory);
    }
}
