package org.envoy.expression;

import org.junit.*;
import org.mockito.*;

public abstract class ExpressionTestBase<T extends AbstractExpression<?>> {
    @Mock
    protected ExpressionFactory expressionFactory;

    @Mock
    protected ExpressionVisitor expressionVisitor;

    protected T underTest;


    protected abstract T createTestObjectForExpressionFactoryTest(ExpressionFactory expressionFactory);


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        underTest = createTestObjectForExpressionFactoryTest(this.expressionFactory);
    }


    @Test
    public void test_that_ExpressionFactory_is_set() {
        Assert.assertSame(expressionFactory, underTest.getExpressionFactory());
    }
}
