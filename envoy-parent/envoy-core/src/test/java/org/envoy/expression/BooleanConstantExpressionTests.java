package org.envoy.expression;

import org.junit.*;

import static org.mockito.Mockito.*;

public class BooleanConstantExpressionTests {
    @Test
    public void test_that_accept_calls_visitTrue_when_the_value_is_true() {
        //--Arrange
        final BooleanConstantExpression underTest = new BooleanConstantExpression(true);
        final ExpressionVisitor visitor = mock(ExpressionVisitor.class);

        //--Act
        underTest.accept(visitor);

        //--Assert
        verify(visitor).visitTrue();
    }


    @Test
    public void test_that_accept_calls_visitFalse_when_the_value_is_false() {
        //--Arrange
        final BooleanConstantExpression underTest = new BooleanConstantExpression(false);
        final ExpressionVisitor visitor = mock(ExpressionVisitor.class);

        //--Act
        underTest.accept(visitor);

        //--Assert
        verify(visitor).visitFalse();
    }
}
