package org.envoy.expression;

import org.mockito.*;

public abstract class BinaryExpressionTestBase extends ExpressionTestBase<AbstractExpression<?>> {
    @Mock
    protected Expression lhs;

    @Mock
    protected Expression rhs;


    protected abstract AbstractExpression<?> createTestObject(
            Expression lhs,
            Expression rhs,
            ExpressionFactory expressionFactory);


    @Override
    protected AbstractExpression<?> createTestObjectForExpressionFactoryTest(final ExpressionFactory expressionFactory) {
        return this.createTestObject(lhs, rhs, expressionFactory);
    }
}
