package org.envoy.query.grouping;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
public @interface ColumnAggregator {
}
