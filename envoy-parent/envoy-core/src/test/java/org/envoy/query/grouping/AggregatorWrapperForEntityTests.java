package org.envoy.query.grouping;

import org.envoy.*;
import org.envoy.expression.*;
import org.envoy.query.*;
import org.junit.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;

public class AggregatorWrapperForEntityTests {
    @Mock
    private EntityFactory entityFactory;

    @Mock
    private static SqlDatabaseMetaData metaData;

    @Mock
    private UpdateResolver updateResolver;

    @Mock
    private InsertResolver insertResolver;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(metaData.getUpdateResolver()).thenReturn(updateResolver);
        when(metaData.getInsertResolver()).thenReturn(insertResolver);
        when(updateResolver.update(any(UpdateModel.class))).thenReturn(1);
        when(insertResolver.insert(any(InsertModel.class))).thenReturn(1);
    }


    @Test
    public void test_that_the_correct_entity_is_returned() {
        //--Arrange
        final TestEntityExpression entityExpr = new TestEntityExpression();
        final TestEntity entity = new TestEntity();
        when(entityFactory.createEntity(same(entityExpr))).thenReturn(entity);

        final AggregatorWrapperForEntity<TestEntity> underTest = new AggregatorWrapperForEntity<>(
                entityFactory,
                entityExpr,
                new ArrayList<FieldExpression>());

        //--Act
        final TestEntity actual = underTest.aggregate(new ArrayList<>());

        //--Assert
        Assert.assertSame(entity, actual);
    }


    @Test
    public void test_that_the_entity_is_set_with_the_correct_properties() {
        //--Arrange
        final Object object1 = new Object();
        final FieldExpression expr1 = mock(FieldExpression.class);

        final Object object2 = new Object();
        final FieldExpression expr2 = mock(FieldExpression.class);

        final Object object3 = new Object();
        final FieldExpression expr3 = mock(FieldExpression.class);

        final TestEntity entity = new TestEntity();
        final TestEntityExpression entityExpr = new TestEntityExpression();
        when(entityFactory.createEntity(same(entityExpr))).thenReturn(entity);

        final AggregatorWrapperForEntity<TestEntity> underTest = new AggregatorWrapperForEntity<>(
                entityFactory,
                entityExpr,
                Arrays.asList(expr1, expr2, expr3));

        //--Act
        underTest.aggregate(Arrays.asList(object1, object2, object3));

        //--Assert
        verify(expr1).setFromRootEntity(same(entity), same(object1));
        verify(expr2).setFromRootEntity(same(entity), same(object2));
        verify(expr3).setFromRootEntity(same(entity), same(object3));
    }


    @Test
    public void test_that_the_entity_is_clean_after_being_populated() {
        //--Arrange
        final Object object1 = new Object();
        final FieldExpression expr1 = mock(FieldExpression.class);

        final Object object2 = new Object();
        final FieldExpression expr2 = mock(FieldExpression.class);

        final Object object3 = new Object();
        final FieldExpression expr3 = mock(FieldExpression.class);

        final TestEntity entity = mock(TestEntity.class);
        final TestEntityExpression entityExpr = new TestEntityExpression();
        when(entityFactory.createEntity(same(entityExpr))).thenReturn(entity);

        final AggregatorWrapperForEntity<TestEntity> underTest = new AggregatorWrapperForEntity<>(
                entityFactory,
                entityExpr,
                Arrays.asList(expr1, expr2, expr3));

        //--Act
        underTest.aggregate(Arrays.asList(object1, object2, object3));

        //--Assert
        InOrder order = inOrder(expr1, expr2, expr3, entity);
        order.verify(expr1).setFromRootEntity(same(entity), same(object1));
        order.verify(expr2).setFromRootEntity(same(entity), same(object2));
        order.verify(expr3).setFromRootEntity(same(entity), same(object3));
        order.verify(entity).markAsClean();
    }


    public static class TestEntity extends AbstractEntity<TestEntity> {
        public TestEntity() {
            super(TestEntityExpression.class, metaData);
        }
    }

    public static class TestEntityExpression extends AbstractEntityExpression<TestEntity> {
        public TestEntityExpression() {
            super(TestEntity.class);
        }
    }
}
