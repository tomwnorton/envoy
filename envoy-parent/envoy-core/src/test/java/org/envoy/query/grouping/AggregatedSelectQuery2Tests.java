package org.envoy.query.grouping;

import org.envoy.query.*;
import org.mockito.*;

public class AggregatedSelectQuery2Tests extends AggregatedSelectQueryTestBase<AggregatedSelectQuery2<Object, Object, Object>> {
    @ColumnAggregator
    @Mock
    private ColumnAggregator2<Object, Object, Object> _columnAggregator;


    @Override
    protected AggregatedSelectQuery2<Object, Object, Object> createObjectUnderTest(UnderTestFactoryModel factoryModel) {
        return new AggregatedSelectQuery2<>(
                factoryModel.getMetaData(),
                factoryModel.getModel(),
                factoryModel.getExpressions());
    }


    @Override
    protected ListableSelectQuery<Object, Object> createActualObject(AggregatedSelectQuery2<Object, Object, Object> underTest) {
        return underTest.as(this._columnAggregator);
    }
}
