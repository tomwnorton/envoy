package org.envoy.query.grouping;

import org.envoy.query.*;
import org.mockito.*;

public class AggregatedSelectQuery7Tests extends
        AggregatedSelectQueryTestBase<AggregatedSelectQuery7<Object, Object, Object, Object, Object, Object, Object, Object>> {
    @ColumnAggregator
    @Mock
    private ColumnAggregator7<Object, Object, Object, Object, Object, Object, Object, Object> _columnAggregator;


    @Override
    protected AggregatedSelectQuery7<Object, Object, Object, Object, Object, Object, Object, Object> createObjectUnderTest(
            UnderTestFactoryModel factoryModel) {
        return new AggregatedSelectQuery7<>(
                factoryModel.getMetaData(),
                factoryModel.getModel(),
                factoryModel.getExpressions());
    }


    @Override
    protected ListableSelectQuery<Object, Object> createActualObject(
            AggregatedSelectQuery7<Object, Object, Object, Object, Object, Object, Object, Object> underTest) {
        return underTest.as(this._columnAggregator);
    }
}
