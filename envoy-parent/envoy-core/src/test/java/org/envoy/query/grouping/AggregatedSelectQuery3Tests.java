package org.envoy.query.grouping;

import org.envoy.query.*;
import org.mockito.*;

public class AggregatedSelectQuery3Tests extends
        AggregatedSelectQueryTestBase<AggregatedSelectQuery3<Object, Object, Object, Object>> {
    @ColumnAggregator
    @Mock
    private ColumnAggregator3<Object, Object, Object, Object> _columnAggregator;


    @Override
    protected AggregatedSelectQuery3<Object, Object, Object, Object> createObjectUnderTest(UnderTestFactoryModel factoryModel) {
        return new AggregatedSelectQuery3<>(
                factoryModel.getMetaData(),
                factoryModel.getModel(),
                factoryModel.getExpressions());
    }


    @Override
    protected ListableSelectQuery<Object, Object> createActualObject(
            AggregatedSelectQuery3<Object, Object, Object, Object> underTest) {
        return underTest.as(this._columnAggregator);
    }
}
