package org.envoy.query.grouping;

import org.envoy.query.*;
import org.mockito.*;

public class AggregatedSelectQuery4Tests extends
        AggregatedSelectQueryTestBase<AggregatedSelectQuery4<Object, Object, Object, Object, Object>> {
    @ColumnAggregator
    @Mock
    private ColumnAggregator4<Object, Object, Object, Object, Object> _columnAggregator;


    @Override
    protected AggregatedSelectQuery4<Object, Object, Object, Object, Object> createObjectUnderTest(UnderTestFactoryModel factoryModel) {
        return new AggregatedSelectQuery4<>(
                factoryModel.getMetaData(),
                factoryModel.getModel(),
                factoryModel.getExpressions());
    }


    @Override
    protected ListableSelectQuery<Object, Object> createActualObject(
            AggregatedSelectQuery4<Object, Object, Object, Object, Object> underTest) {
        return underTest.as(this._columnAggregator);
    }
}
