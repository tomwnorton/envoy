package org.envoy.query.grouping;

import org.envoy.query.*;
import org.mockito.*;

public class AggregatedSelectQuery14Tests extends
        AggregatedSelectQueryTestBase<AggregatedSelectQuery14<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object>> {
    @ColumnAggregator
    @Mock
    private ColumnAggregator14<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object> _columnAggregator;


    @Override
    protected AggregatedSelectQuery14<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object> createObjectUnderTest(
            UnderTestFactoryModel factoryModel) {
        return new AggregatedSelectQuery14<>(
                factoryModel.getMetaData(),
                factoryModel.getModel(),
                factoryModel.getExpressions());
    }


    @Override
    protected ListableSelectQuery<Object, Object> createActualObject(
            AggregatedSelectQuery14<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object> underTest) {
        return underTest.as(this._columnAggregator);
    }
}
