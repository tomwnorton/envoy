package org.envoy.query.grouping;

import org.envoy.DatabaseMetaData;
import org.envoy.expression.*;
import org.envoy.query.*;
import org.junit.*;
import org.mockito.*;

import java.lang.reflect.*;
import java.sql.*;
import java.util.*;

import static org.mockito.Mockito.*;

public abstract class AggregatedSelectQueryTestBase<UnderTest extends AggregatedSelectQuery<Object>> {
    private List<Expression> _expressions;
    private SelectQueryModel<Object, Object> _model;
    private UnderTest _underTest;
    private Field _columnAggregatorField;

    protected ListableSelectQuery<Object, Object> _actual;

    @Mock
    private DatabaseMetaData _metaData;

    @Mock
    private Connection _connection;

    protected class UnderTestFactoryModel {
        private DatabaseMetaData _metaData;
        private Connection _connection;
        private SelectQueryModel<Object, Object> _model;
        private List<Expression> _expressions;


        public DatabaseMetaData getMetaData() {
            return this._metaData;
        }


        public UnderTestFactoryModel setMetaData(DatabaseMetaData metaData) {
            this._metaData = metaData;
            return this;
        }


        public Connection getConnection() {
            return this._connection;
        }


        public UnderTestFactoryModel setConnection(Connection connection) {
            this._connection = connection;
            return this;
        }


        public SelectQueryModel<Object, Object> getModel() {
            return this._model;
        }


        public UnderTestFactoryModel setModel(SelectQueryModel<Object, Object> model) {
            this._model = model;
            return this;
        }


        public List<Expression> getExpressions() {
            return this._expressions;
        }


        public UnderTestFactoryModel setExpressions(List<Expression> expressions) {
            this._expressions = expressions;
            return this;
        }
    }


    protected abstract UnderTest createObjectUnderTest(UnderTestFactoryModel factoryModel);


    protected abstract ListableSelectQuery<Object, Object> createActualObject(UnderTest underTest);


    @SuppressWarnings("unchecked")
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this._expressions = new ArrayList<>();
        this._model = new SelectQueryModel<>();
        this._model.setFrom(mock(EntityExpression.class));
        this._model.setWhereFilter(mock(BooleanExpression.class));
        this._underTest = this.createObjectUnderTest(
                new UnderTestFactoryModel()
                        .setMetaData(this._metaData)
                        .setConnection(this._connection)
                        .setModel(this._model)
                        .setExpressions(this._expressions));
        this._actual = this.createActualObject(this._underTest);

        this._columnAggregatorField = this.getColumnAggregatorField();
        this._columnAggregatorField.setAccessible(true);
    }


    private Field getColumnAggregatorField() {
        for (Field field : this.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(ColumnAggregator.class)) {
                return field;
            }
        }
        throw new RuntimeException("No ColumnAggregator present.");
    }


    @Test
    public void test_that_as_returns_a_ListableSelectQuery_with_the_correct_metaData() {
        Assert.assertSame(this._metaData, this._actual.getMetaData());
    }


    @Test
    public void test_that_as_returns_a_ListableSelectQuery_with_the_correct_selectList() {
        Assert.assertSame(this._expressions, this._actual.getModel().getSelectList());
    }


    @Test
    public void test_that_as_returns_a_ListableSelectQuery_with_the_correct_from() {
        Assert.assertSame(this._model.getFrom(), this._actual.getModel().getFrom());
    }


    @Test
    public void test_that_as_returns_a_ListableSelectQuery_with_the_correct_where() {
        Assert.assertSame(this._model.getWhereFilter(), this._actual.getModel().getWhereFilter());
    }


    @Test
    public void test_that_as_returns_a_ListableSelectQuery_with_the_correct_aggregateWrapper()
            throws IllegalArgumentException,
            IllegalAccessException {
        Assert.assertSame(
                this._columnAggregatorField.get(this),
                this._actual.getModel().getAggregatorWrapper().getAggregator());
    }
}
