package org.envoy.query.grouping;

import org.envoy.query.*;
import org.mockito.*;

public class AggregatedSelectQuery18Tests extends
        AggregatedSelectQueryTestBase<AggregatedSelectQuery18<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object>> {
    @ColumnAggregator
    @Mock
    private ColumnAggregator18<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object> _columnAggregator;


    @Override
    protected AggregatedSelectQuery18<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object> createObjectUnderTest(
            UnderTestFactoryModel factoryModel) {
        return new AggregatedSelectQuery18<>(
                factoryModel.getMetaData(),
                factoryModel.getModel(),
                factoryModel.getExpressions());
    }


    @Override
    protected ListableSelectQuery<Object, Object> createActualObject(
            AggregatedSelectQuery18<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object> underTest) {
        return underTest.as(this._columnAggregator);
    }
}
