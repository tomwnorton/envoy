package org.envoy.query;

import org.envoy.*;
import org.envoy.expression.*;
import org.envoy.query.grouping.*;
import org.envoy.typepersisters.*;
import org.junit.*;
import org.junit.rules.*;
import org.mockito.*;
import org.mockito.invocation.*;
import org.mockito.stubbing.*;

import java.io.*;
import java.math.*;
import java.sql.*;
import java.util.*;

import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class BasicSqlQueryResolverTests {
    @Mock
    private SqlDatabaseMetaData metaData;

    @Mock
    private TypePersisterMap typePersisterMap;

    @Mock
    private Connection connection;

    @Mock
    private PreparedStatement ps;

    @Mock
    private ResultSet rs;

    @Mock
    private SqlExpressionVisitor expressionVisitor;

    @Mock
    private ExpressionFactory expressionFactory;

    @Mock
    private FieldAliasCache fieldAliasCache;

    @Mock
    private AggregatorWrapper<Serializable> aggregatorWrapper;

    @InjectMocks
    private BasicSqlQueryResolver<Serializable> underTest;

    private SelectQueryModel<Serializable, Serializable> sqlQuery;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();


    @Before
    public void setUp() throws SQLException {
        underTest = new BasicSqlQueryResolver<Serializable>();
        MockitoAnnotations.initMocks(this);

        when(connection.prepareStatement(anyString())).thenReturn(ps);
        when(ps.executeQuery()).thenReturn(rs);

        when(metaData.getTypePersisterMap()).thenReturn(typePersisterMap);
        when(metaData.createExpressionVisitor()).thenReturn(expressionVisitor);
        when(metaData.getExpressionFactory()).thenReturn(expressionFactory);

        sqlQuery = new SelectQueryModel<Serializable, Serializable>();
        sqlQuery.setFrom(new TestExpression());
        sqlQuery.getFrom().setFieldAliasCache(fieldAliasCache);
        sqlQuery.setAggregatorWrapper(aggregatorWrapper);
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_rs_creates_one_entity_and_sets_its_three_fields_when_one_record_is_returned()
            throws SQLException {
        //--Arrange
        final TypePersister<Object> firstPersister = mock(TypePersister.class);
        final TypePersister<Object> secondPersister = mock(TypePersister.class);
        final TypePersister<Object> thirdPersister = mock(TypePersister.class);

        final Object firstValue = new Object();
        final Object secondValue = new Object();
        final Object thirdValue = new Object();

        final GenericFieldExpression<Object> firstExpression = mock(GenericFieldExpression.class);
        final GenericFieldExpression<Object> secondExpression = mock(GenericFieldExpression.class);
        final GenericFieldExpression<Object> thirdExpression = mock(GenericFieldExpression.class);

        final Serializable entity = mock(Serializable.class);

        when(rs.next()).thenReturn(true, false);
        when((TypePersister<Object>) typePersisterMap.get(any())).thenReturn(
                firstPersister,
                secondPersister,
                thirdPersister);

        when(firstPersister.get(same(rs), eq("firstFieldName"))).thenReturn(firstValue);
        when(secondPersister.get(same(rs), eq("secondFieldName"))).thenReturn(secondValue);
        when(thirdPersister.get(same(rs), eq("thirdFieldName"))).thenReturn(thirdValue);

        when(fieldAliasCache.getFieldAlias(same(firstExpression))).thenReturn("firstFieldName");
        when(fieldAliasCache.getFieldAlias(same(secondExpression))).thenReturn("secondFieldName");
        when(fieldAliasCache.getFieldAlias(same(thirdExpression))).thenReturn("thirdFieldName");

        sqlQuery.setSelectList(Arrays.asList(firstExpression, secondExpression, thirdExpression));
        when(aggregatorWrapper.aggregate(Arrays.asList(firstValue, secondValue, thirdValue))).thenReturn(entity);

        //--Act
        final List<Serializable> actual = underTest.resolve(
                metaData,
                sqlQuery,
                connection);

        //--Assert
        Assert.assertSame(entity, actual.get(0));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_ps_is_closed_after_rs_is_finished_when_no_SQLException_is_thrown() throws SQLException {
        //--Arrange
        final TypePersister<Object> firstPersister = mock(TypePersister.class);

        final Object firstValue = new Object();

        final GenericFieldExpression<Object> firstExpression = mock(GenericFieldExpression.class);

        final Serializable entity = mock(Serializable.class);

        when(rs.next()).thenReturn(true, false);
        when((TypePersister<Object>) typePersisterMap.get(any())).thenReturn(firstPersister);

        when(firstPersister.get(same(rs), eq("firstFieldName"))).thenReturn(firstValue);

        when(firstExpression.getFieldName()).thenReturn("firstFieldName");

        sqlQuery.setSelectList(Arrays.asList(firstExpression));
        when(aggregatorWrapper.aggregate(Arrays.asList(firstValue))).thenReturn(entity);

        //--Act
        underTest.resolve(metaData, sqlQuery, connection);

        //--Assert
        final InOrder order = inOrder(firstPersister, ps);
        order.verify(firstPersister).get(same(rs), anyString());
        order.verify(ps, times(1)).close();
    }


    @SuppressWarnings("unchecked")
    @Test(expected = SQLException.class)
    public void test_that_ps_is_closed_when_a_SQLException_is_thrown_by_rs_next() throws SQLException {
        //--Arrange
        when(rs.next()).thenThrow(SQLException.class);

        //--Act
        underTest.resolve(metaData, sqlQuery, connection);

        //--Assert
        verify(ps).close();
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_rs_is_closed_after_rs_is_finished_when_no_SQLException_is_thrown() throws SQLException {
        //--Arrange
        final TypePersister<Object> firstPersister = mock(TypePersister.class);

        final Object firstValue = new Object();

        final GenericFieldExpression<Object> firstExpression = mock(GenericFieldExpression.class);

        final Serializable entity = mock(Serializable.class);

        when(rs.next()).thenReturn(true, false);
        when((TypePersister<Object>) typePersisterMap.get(any())).thenReturn(firstPersister);

        when(firstPersister.get(same(rs), eq("firstFieldName"))).thenReturn(firstValue);

        when(firstExpression.getFieldName()).thenReturn("firstFieldName");

        sqlQuery.setSelectList(Arrays.asList(firstExpression));
        when(aggregatorWrapper.aggregate(Arrays.asList(firstValue))).thenReturn(entity);

        //--Act
        underTest.resolve(metaData, sqlQuery, connection);

        //--Assert
        final InOrder order = inOrder(firstPersister, rs, ps);
        order.verify(firstPersister).get(same(rs), anyString());
        order.verify(rs).close();
        order.verify(ps).close();
    }


    @SuppressWarnings("unchecked")
    @Test(expected = SQLException.class)
    public void test_that_there_is_no_NullPointerException_when_rs_is_null() throws SQLException {
        //--Arrange
        when(ps.executeQuery()).thenThrow(SQLException.class);

        //--Act
        underTest.resolve(metaData, sqlQuery, connection);
    }


    @SuppressWarnings("unchecked")
    @Test(expected = SQLException.class)
    public void test_that_there_is_no_NullPointerException_when_ps_is_null() throws SQLException {
        //--Arrange
        when(connection.prepareStatement(anyString())).thenThrow(SQLException.class);

        //--Act
        underTest.resolve(metaData, sqlQuery, connection);
    }


    @Test
    public void test_that_an_exception_thrown_by_ps_close_is_squished_by_an_exception_thrown_from_the_try()
            throws SQLException {
        //--Arrange
        final SQLException expected = new SQLException();
        final SQLException squished = new SQLException();
        when(rs.next()).thenThrow(expected);
        doThrow(squished).when(ps).close();

        //--Assert
        expectedException.expect(sameInstance(expected));

        //--Act
        underTest.resolve(metaData, sqlQuery, connection);
    }


    @Test
    public void test_that_an_exception_thrown_by_ps_close_is_propagated_when_no_exception_is_thrown_from_the_try()
            throws SQLException {
        //--Arrange
        final SQLException expected = new SQLException();
        doThrow(expected).when(ps).close();

        //--Assert
        expectedException.expect(sameInstance(expected));

        //--Act
        underTest.resolve(metaData, sqlQuery, connection);
    }


    @Test
    public void test_that_an_exception_thrown_by_rs_close_is_squished_by_an_exception_thrown_from_the_try()
            throws SQLException {
        //--Arrange
        final SQLException expected = new SQLException();
        final SQLException squished = new SQLException();
        when(rs.next()).thenThrow(expected);
        doThrow(squished).when(rs).close();

        //--Assert
        expectedException.expect(sameInstance(expected));

        //--Act
        underTest.resolve(metaData, sqlQuery, connection);
    }


    @Test(expected = SQLException.class)
    public void test_that_ps_close_is_still_called_when_rs_close_throws_an_exception() throws SQLException {
        //--Arrange
        doThrow(SQLException.class).when(rs).close();

        //--Act
        underTest.resolve(metaData, sqlQuery, connection);

        //--Assert
        verify(ps).close();
    }


    @Test
    public void test_that_an_exception_thrown_by_rs_close_is_propagated_when_no_exception_is_thrown_from_the_try()
            throws SQLException {
        //--Arrange
        final SQLException expected = new SQLException();
        doThrow(expected).when(rs).close();

        //--Assert
        expectedException.expect(sameInstance(expected));

        //--Act
        underTest.resolve(metaData, sqlQuery, connection);
    }


    @Test
    public void test_that_the_query_given_to_the_visitor_is_used_in_the_PreparedStatement_when_the_query_is_populated_with_hello()
            throws SQLException {
        //--Arrange
        doAnswer(
                new Answer<Void>() {

                    @Override
                    public Void answer(final InvocationOnMock invocation) throws Throwable {
                        ((StringBuilder) invocation.getArguments()[0]).append("hello");
                        return null;
                    }
                }).when(expressionVisitor).setQuery(any(StringBuilder.class));

        //--Act
        underTest.resolve(metaData, sqlQuery, connection);

        //--Assert
        verify(connection).prepareStatement("hello");
    }


    @Test
    public void test_that_the_query_given_to_the_visitor_is_used_in_the_PreparedStatement_when_the_query_is_populated_with_world()
            throws SQLException {
        //--Arrange
        doAnswer(
                new Answer<Void>() {

                    @Override
                    public Void answer(final InvocationOnMock invocation) throws Throwable {
                        ((StringBuilder) invocation.getArguments()[0]).append("world");
                        return null;
                    }
                }).when(expressionVisitor).setQuery(any(StringBuilder.class));

        //--Act
        underTest.resolve(metaData, sqlQuery, connection);

        //--Assert
        verify(connection).prepareStatement("world");
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_the_parameters_list_given_to_the_visitor_is_the_same_list_used_by_the_PreparedStatement()
            throws SQLException {
        //--Arrange
        final BigDecimal first = new BigDecimal(0);
        final BigDecimal second = new BigDecimal(0);
        doAnswer(
                new Answer<Void>() {

                    @Override
                    public Void answer(final InvocationOnMock invocation) throws Throwable {
                        final List<Object> parameters = (List<Object>) invocation.getArguments()[0];
                        parameters.add(first);
                        parameters.add(second);
                        return null;
                    }
                }).when(expressionVisitor).setParameters(anyListOf(Object.class));

        final TypePersister<BigDecimal> typePersister = mock(TypePersister.class);
        when((TypePersister<BigDecimal>) typePersisterMap.get(BigDecimal.class)).thenReturn(typePersister);

        //--Act
        underTest.resolve(metaData, sqlQuery, connection);

        //--Assert
        verify(typePersister).set(same(ps), eq(1), same(first));
        verify(typePersister).set(same(ps), eq(2), same(second));
    }


    @Test
    public void test_that_visitSelect_is_called_correctly() throws SQLException {
        //--Arrange
        final TestExpression fromExpression = new TestExpression();
        final List<Expression> selectList = new ArrayList<>();
        final BooleanExpression whereExpression = mock(BooleanExpression.class);
        final SelectListExpression selectListExpression = new SelectListExpression(null);
        when(expressionFactory.createSelectListExpression(same(selectList))).thenReturn(selectListExpression);

        sqlQuery.setFrom(fromExpression);
        sqlQuery.setSelectList(selectList);
        sqlQuery.setWhereFilter(whereExpression);

        //--Act
        underTest.resolve(metaData, sqlQuery, connection);

        //--Assert
        verify(expressionVisitor).visitSelect(
                same(selectListExpression), same(fromExpression), same(
                        whereExpression));
    }


    @Test
    public void test_that_the_visitor_has_its_fieldAliasCache_set_correctly() throws SQLException {
        //--Act
        underTest.resolve(metaData, sqlQuery, connection);

        //--Assert
        verify(expressionVisitor).setFieldAliasCache(same(fieldAliasCache));
    }


    private class TestExpression extends AbstractEntityExpression<Serializable> {
        public TestExpression() {
            super(null);
        }
    }
}
