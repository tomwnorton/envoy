package org.envoy.query.grouping;

import org.junit.*;
import org.mockito.*;

import java.io.*;
import java.util.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class AggregatorWrapper10Tests {
    @Mock
    private ColumnAggregator10<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object> _aggregator;

    private AggregatorWrapper10<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object> _underTest;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this._underTest = new AggregatorWrapper10<>(this._aggregator);
    }


    @Test
    public void test_that_aggregate_calls_the_columnAggregator_correctly() {
        final Object column1 = mock(Serializable.class);
        final Object column2 = mock(Serializable.class);
        final Object column3 = mock(Serializable.class);
        final Object column4 = mock(Serializable.class);
        final Object column5 = mock(Serializable.class);
        final Object column6 = mock(Serializable.class);
        final Object column7 = mock(Serializable.class);
        final Object column8 = mock(Serializable.class);
        final Object column9 = mock(Serializable.class);
        final Object column10 = mock(Serializable.class);
        final Object expected = mock(Serializable.class);

        when(
                this._aggregator.aggregate(
                        same(column1),
                        same(column2),
                        same(column3),
                        same(column4),
                        same(column5),
                        same(column6),
                        same(column7),
                        same(column8),
                        same(column9),
                        same(column10))).thenReturn(expected);

        //--Act
        final Object actual = this._underTest.aggregate(
                Arrays.asList(
                        column1, column2, column3, column4, column5, column6, column7, column8, column9, column10));

        //--Assert
        Assert.assertSame(expected, actual);
    }
}