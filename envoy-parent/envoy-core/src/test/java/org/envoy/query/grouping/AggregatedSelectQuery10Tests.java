package org.envoy.query.grouping;

import org.envoy.query.*;
import org.mockito.*;

public class AggregatedSelectQuery10Tests extends
        AggregatedSelectQueryTestBase<AggregatedSelectQuery10<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object>> {
    @ColumnAggregator
    @Mock
    private ColumnAggregator10<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object> _columnAggregator;


    @Override
    protected AggregatedSelectQuery10<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object> createObjectUnderTest(
            UnderTestFactoryModel factoryModel) {
        return new AggregatedSelectQuery10<>(
                factoryModel.getMetaData(),
                factoryModel.getModel(),
                factoryModel.getExpressions());
    }


    @Override
    protected ListableSelectQuery<Object, Object> createActualObject(
            AggregatedSelectQuery10<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object> underTest) {
        return underTest.as(this._columnAggregator);
    }
}
