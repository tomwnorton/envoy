package org.envoy.query;

import org.envoy.DatabaseMetaData;
import org.envoy.exceptions.*;
import org.envoy.expression.*;
import org.junit.*;
import org.junit.rules.*;
import org.mockito.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class ListableSelectQueryTests {
    @Mock
    private DatabaseMetaData metaData;

    @Mock
    private QueryResolver<Serializable, AutoCloseable> queryResolver;

    @Mock
    private ExceptionTransformer exceptionTransformer;

    @Mock
    private AutoCloseable connection;

    @Mock
    private EntityExpression<Serializable> fromExpression;

    private ListableSelectQuery<Serializable, Serializable> underTest;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(metaData.<Serializable>getQueryResolver()).thenReturn(queryResolver);
        when(metaData.getExceptionTransformer()).thenReturn(exceptionTransformer);
        when(metaData.createConnection()).thenReturn(connection);

        underTest = new ListableSelectQuery<>(metaData, fromExpression);
    }


    @Test
    public void test_that_resolve_is_passed_an_open_connection() throws Exception {
        //--Arrange
        List<Serializable> expected = new ArrayList<>();
        when(queryResolver.resolve(any(DatabaseMetaData.class), any(SelectQueryModel.class), same(connection))).thenReturn(
                expected);

        //--Act
        List<Serializable> actual = underTest.list();

        //--Assert
        assertThat(actual, is(sameInstance(expected)));
        InOrder expectedOrder = inOrder(queryResolver, connection);
        expectedOrder.verify(queryResolver).resolve(
                any(DatabaseMetaData.class),
                any(SelectQueryModel.class),
                any(AutoCloseable.class));
        expectedOrder.verify(connection).close();
    }


    @Test
    public void test_that_list_calls_the_resolver_correctly() throws Exception {
        //--Arrange
        final List<Serializable> expected = new ArrayList<>();
        when(queryResolver.resolve(same(metaData), same(underTest.model), any(AutoCloseable.class))).thenReturn(
                expected);

        //--Act
        final List<Serializable> actual = underTest.list();

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_list_throws_the_correct_EnvoyException_when_resolve_throws_a_SQLException_and_the_ExceptionTransformer_handles_it()
            throws Exception {
        //--Arrange
        final SQLException sqle = new SQLException();
        when(queryResolver.resolve(any(DatabaseMetaData.class), any(SelectQueryModel.class), any(AutoCloseable.class)))
                .thenThrow(sqle);

        final EnvoyException expected = new EnvoyException();
        when(exceptionTransformer.transform(same(sqle))).thenReturn(expected);

        //--Assert
        expectedException.expect(sameInstance(expected));

        //--Act
        underTest.list();
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_list_throws_a_default_EnvoyException_when_resolve_throws_a_SQLException_but_the_ExceptionTransformer_doesnt_handle_it()
            throws Exception {
        //--Arrange
        final SQLException sqle = new SQLException();
        when(queryResolver.resolve(any(DatabaseMetaData.class), any(SelectQueryModel.class), any(AutoCloseable.class)))
                .thenThrow(sqle);

        //--Assert
        expectedException.expect(EnvoyException.class);
        expectedException.expectMessage("Envoy could not execute the query.");
        expectedException.expectCause(sameInstance(sqle));

        //--Act
        underTest.list();
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_list_throws_a_default_EnvoyException_when_resolve_throws_a_SQLException_but_there_is_no_ExceptionTransformer()
            throws Exception {
        //--Arrange
        final SQLException sqle = new SQLException();
        when(queryResolver.resolve(any(DatabaseMetaData.class), any(SelectQueryModel.class), any(AutoCloseable.class)))
                .thenThrow(sqle);
        when(metaData.getExceptionTransformer()).thenReturn(null);

        //--Assert
        expectedException.expect(EnvoyException.class);
        expectedException.expectMessage("Envoy could not execute the query.");
        expectedException.expectCause(sameInstance(sqle));

        //--Act
        underTest.list();
    }


    @Test
    public void test_that_single_calls_the_resolver_correctly() throws Exception {
        //--Arrange
        final Serializable expected = mock(Serializable.class);
        final List<Serializable> list = Arrays.asList(expected);
        when(queryResolver.resolve(same(metaData), same(underTest.model), any(AutoCloseable.class))).thenReturn(
                list);

        //--Act
        final Serializable actual = underTest.single();

        //--Assert
        Assert.assertSame(expected, actual);
    }


    @Test
    public void test_that_single_returns_null_when_resolve_returns_an_empty_list() throws Exception {
        //--Arrange
        final List<Serializable> list = new ArrayList<>();
        when(queryResolver.resolve(same(metaData), same(underTest.model), any(AutoCloseable.class))).thenReturn(
                list);

        //--Act
        final Serializable actual = underTest.single();

        //--Assert
        Assert.assertNull(actual);
    }


    @Test
    public void test_that_single_returns_null_when_resolve_returns_null() throws Exception {
        //--Arrange
        when(queryResolver.resolve(same(metaData), same(underTest.model), any(AutoCloseable.class))).thenReturn(
                null);

        //--Act
        final Serializable actual = underTest.single();

        //--Assert
        Assert.assertNull(actual);
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_single_throws_the_correct_EnvoyException_when_resolve_throws_a_SQLException_and_the_ExceptionTransformer_handles_it()
            throws Exception {
        //--Arrange
        final SQLException sqle = new SQLException();
        when(queryResolver.resolve(any(DatabaseMetaData.class), any(SelectQueryModel.class), any(AutoCloseable.class)))
                .thenThrow(sqle);

        final EnvoyException expected = new EnvoyException();
        when(exceptionTransformer.transform(same(sqle))).thenReturn(expected);

        //--Assert
        expectedException.expect(sameInstance(expected));

        //--Act
        underTest.single();
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_single_throws_a_default_EnvoyException_when_resolve_throws_a_SQLException_but_the_ExceptionTransformer_doesnt_handle_it()
            throws Exception {
        //--Arrange
        final SQLException sqle = new SQLException();
        when(queryResolver.resolve(any(DatabaseMetaData.class), any(SelectQueryModel.class), any(AutoCloseable.class)))
                .thenThrow(sqle);

        //--Assert
        expectedException.expect(EnvoyException.class);
        expectedException.expectMessage("Envoy could not execute the query.");
        expectedException.expectCause(sameInstance(sqle));

        //--Act
        underTest.single();
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_single_throws_a_default_EnvoyException_when_resolve_throws_a_SQLException_but_there_is_no_ExceptionTransformer()
            throws Exception {
        //--Arrange
        final SQLException sqle = new SQLException();
        when(queryResolver.resolve(any(DatabaseMetaData.class), any(SelectQueryModel.class), any(AutoCloseable.class)))
                .thenThrow(sqle);
        when(metaData.getExceptionTransformer()).thenReturn(null);

        //--Assert
        expectedException.expect(EnvoyException.class);
        expectedException.expectMessage("Envoy could not execute the query.");
        expectedException.expectCause(sameInstance(sqle));

        //--Act
        underTest.single();
    }
}
