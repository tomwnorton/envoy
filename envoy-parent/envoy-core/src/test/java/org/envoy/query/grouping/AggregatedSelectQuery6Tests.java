package org.envoy.query.grouping;

import org.envoy.query.*;
import org.mockito.*;

public class AggregatedSelectQuery6Tests extends
        AggregatedSelectQueryTestBase<AggregatedSelectQuery6<Object, Object, Object, Object, Object, Object, Object>> {
    @ColumnAggregator
    @Mock
    private ColumnAggregator6<Object, Object, Object, Object, Object, Object, Object> _columnAggregator;


    @Override
    protected AggregatedSelectQuery6<Object, Object, Object, Object, Object, Object, Object> createObjectUnderTest(
            UnderTestFactoryModel factoryModel) {
        return new AggregatedSelectQuery6<>(
                factoryModel.getMetaData(),
                factoryModel.getModel(),
                factoryModel.getExpressions());
    }


    @Override
    protected ListableSelectQuery<Object, Object> createActualObject(
            AggregatedSelectQuery6<Object, Object, Object, Object, Object, Object, Object> underTest) {
        return underTest.as(this._columnAggregator);
    }
}
