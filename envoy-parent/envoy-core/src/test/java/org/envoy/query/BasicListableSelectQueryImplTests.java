package org.envoy.query;

import org.envoy.DatabaseMetaData;
import org.envoy.*;
import org.envoy.exceptions.*;
import org.envoy.expression.*;
import org.envoy.query.grouping.*;
import org.junit.*;
import org.junit.runner.*;
import org.mockito.*;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.*;
import org.powermock.modules.junit4.*;

import javax.sql.*;
import java.io.*;
import java.sql.*;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(BasicListableSelectQueryImpl.class)
public class BasicListableSelectQueryImplTests {
    @Mock
    private DatabaseMetaData metaData;

    @Mock
    private DataSource dataSource;

    @Mock
    private QueryResolver<TestEntity, AutoCloseable> queryResolver;

    @Mock
    private ExceptionTransformer exceptionTransformer;

    @Mock
    private ExpressionFactory expressionFactory;

    @Mock
    private EntityFactory entityFactory;


    @Before
    public void setUp() throws SQLException {
        MockitoAnnotations.initMocks(this);

        when(metaData.<TestEntity>getQueryResolver()).thenReturn(queryResolver);
        when(metaData.getExceptionTransformer()).thenReturn(exceptionTransformer);
        when(metaData.getExpressionFactory()).thenReturn(expressionFactory);
        when(metaData.getEntityFactory()).thenReturn(entityFactory);
    }


    @SuppressWarnings("serial")
    private class TestEntity implements Serializable {
    }

    private class TestEntityExpression extends AbstractEntityExpression<TestEntity> {
        public TestEntityExpression() {
            super(TestEntity.class);
        }
    }


    @Test
    public void test_that_the_correct_aggregator_is_used() throws Exception {
        //--Arrange
        final ArrayList<FieldExpression> fieldExpessions = new ArrayList<>();

        final TestEntityExpression entityExpr = mock(TestEntityExpression.class);
        when(entityExpr.getFieldExpressions()).thenReturn(fieldExpessions);

        final AggregatorWrapperForEntity<TestEntity> expectedAggregator = new AggregatorWrapperForEntity<>(
                null,
                null,
                null);
        whenNew(AggregatorWrapperForEntity.class).withArguments(
                same(entityFactory),
                same(entityExpr),
                same(fieldExpessions)).thenReturn(expectedAggregator);

        //--Act
        final BasicListableSelectQueryImpl<TestEntity> underTest = new BasicListableSelectQueryImpl<>(
                metaData,
                entityExpr);

        //--Assert
        assertSame(expectedAggregator, underTest.model.getAggregatorWrapper());
    }


    @SuppressWarnings({"unchecked", "rawtypes"})
    @Test
    public void test_that_list_uses_its_whereExpression_for_the_WHERE_clause_of_the_main_query() throws SQLException {
        //--Arrange
        final BooleanExpression whereExpression = mock(BooleanExpression.class);
        final BasicListableSelectQueryImpl<TestEntity> underTest = new BasicListableSelectQueryImpl<TestEntity>(
                metaData,
                new TestEntityExpression());

        //--Act
        underTest.where(whereExpression).list();

        //--Assert
        final ArgumentCaptor<SelectQueryModel> sqlQueryArg = ArgumentCaptor.forClass(SelectQueryModel.class);
        verify(queryResolver).resolve(any(DatabaseMetaData.class), sqlQueryArg.capture(), any(Connection.class));
        Assert.assertSame(whereExpression, sqlQueryArg.getValue().getWhereFilter());
    }


    @SuppressWarnings({"unchecked", "rawtypes"})
    @Test
    public void test_that_list_uses_an_equals_true_expression_for_the_WHERE_clause_of_the_main_query_when_the_given_whereClause_is_a_BooleanFieldExpression()
            throws SQLException {
        //--Arrange
        final BooleanFieldExpression whereExpression = mock(BooleanFieldExpression.class);
        final EqualToExpressionImpl expected = mock(EqualToExpressionImpl.class);
        final BooleanConstantExpression trueExpr = mock(BooleanConstantExpression.class);
        final BasicListableSelectQueryImpl<TestEntity> underTest = new BasicListableSelectQueryImpl<TestEntity>(
                metaData,
                new TestEntityExpression());

        when(expressionFactory.createBooleanConstantExpression(true)).thenReturn(trueExpr);
        when(
                expressionFactory.createEqualsExpression(
                        same(whereExpression),
                        same(trueExpr))).thenReturn(expected);

        //--Act
        underTest.where(whereExpression).list();

        //--Assert
        final ArgumentCaptor<SelectQueryModel> sqlQueryArg = ArgumentCaptor.forClass(SelectQueryModel.class);
        verify(queryResolver).resolve(any(DatabaseMetaData.class), sqlQueryArg.capture(), any(Connection.class));
        Assert.assertSame(expected, sqlQueryArg.getValue().getWhereFilter());
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_SQLExceptions_are_transformed_into_EnvoyExceptions() throws SQLException {
        //--Arrange
        final EnvoyException expectedEnvoyException = new EnvoyException();
        final SQLException sqlException = new SQLException();

        when(
                queryResolver.resolve(
                        any(DatabaseMetaData.class),
                        any(SelectQueryModel.class),
                        any(Connection.class)))
                .thenThrow(
                        sqlException);
        when(exceptionTransformer.transform(same(sqlException))).thenReturn(expectedEnvoyException);

        final BasicListableSelectQueryImpl<TestEntity> underTest = new BasicListableSelectQueryImpl<TestEntity>(
                metaData,
                new TestEntityExpression());

        //--Assert
        boolean exceptionThrown = false;
        try {
            underTest.list();
        } catch (final EnvoyException e) {
            exceptionThrown = true;
            Assert.assertSame(expectedEnvoyException, e);
        }
        Assert.assertTrue(exceptionThrown);
    }


    @SuppressWarnings("unchecked")
    @Test(expected = EnvoyException.class)
    public void test_that_a_basic_EnvoyException_is_thrown_when_ExceptionTransformer_returns_null() throws SQLException {
        //--Arrange
        final SQLException sqlException = new SQLException();

        when(
                queryResolver.resolve(
                        any(DatabaseMetaData.class),
                        any(SelectQueryModel.class),
                        any(Connection.class)))
                .thenThrow(
                        sqlException);
        when(exceptionTransformer.transform(same(sqlException))).thenReturn(null);

        final BasicListableSelectQueryImpl<TestEntity> underTest = new BasicListableSelectQueryImpl<TestEntity>(
                metaData,
                new TestEntityExpression());

        //--Act
        underTest.list();
    }


    @SuppressWarnings("unchecked")
    @Test(expected = EnvoyException.class)
    public void test_that_a_basic_EnvoyException_is_thrown_when_there_is_no_ExceptionTransformer() throws SQLException {
        //--Arrange
        final SQLException sqlException = new SQLException();

        when(
                queryResolver.resolve(
                        any(DatabaseMetaData.class),
                        any(SelectQueryModel.class),
                        any(Connection.class)))
                .thenThrow(
                        sqlException);
        when(metaData.getExceptionTransformer()).thenReturn(null);

        final BasicListableSelectQueryImpl<TestEntity> underTest = new BasicListableSelectQueryImpl<TestEntity>(
                metaData,
                new TestEntityExpression());

        //--Act
        underTest.list();
    }


    @SuppressWarnings("unchecked")
    @Test
    public void test_that_all_fields_are_set_on_the_select_list_by_default() throws SQLException {
        //--Arrange
        final List<FieldExpression> expected = new ArrayList<>();
        final TestEntityExpression fromExpr = mock(TestEntityExpression.class);
        when(fromExpr.getFieldExpressions()).thenReturn(expected);

        final BasicListableSelectQueryImpl<TestEntity> underTest = new BasicListableSelectQueryImpl<BasicListableSelectQueryImplTests.TestEntity>(
                metaData,
                fromExpr);

        //--Act
        underTest.list();

        //--Assert
        final ArgumentCaptor<SelectQueryModel<TestEntity, TestEntity>> captor = (ArgumentCaptor<SelectQueryModel<TestEntity, TestEntity>>) (Object) ArgumentCaptor
                .forClass(SelectQueryModel.class);
        verify(queryResolver).resolve(any(DatabaseMetaData.class), captor.capture(), any(Connection.class));
        Assert.assertSame(expected, captor.getValue().getSelectList());
    }
}
