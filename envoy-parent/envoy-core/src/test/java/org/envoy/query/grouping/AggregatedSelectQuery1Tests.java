package org.envoy.query.grouping;

import org.envoy.query.*;
import org.mockito.*;

public class AggregatedSelectQuery1Tests extends AggregatedSelectQueryTestBase<AggregatedSelectQuery1<Object, Object>> {
    @ColumnAggregator
    @Mock
    private ColumnAggregator1<Object, Object> _columnAggregator;


    @Override
    protected AggregatedSelectQuery1<Object, Object> createObjectUnderTest(UnderTestFactoryModel factoryModel) {
        return new AggregatedSelectQuery1<>(
                factoryModel.getMetaData(),
                factoryModel.getModel(),
                factoryModel.getExpressions());
    }


    @Override
    protected ListableSelectQuery<Object, Object> createActualObject(AggregatedSelectQuery1<Object, Object> underTest) {
        return underTest.as(this._columnAggregator);
    }
}
