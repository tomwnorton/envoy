package org.envoy.query;

import org.envoy.*;
import org.envoy.exceptions.*;
import org.envoy.expression.*;
import org.envoy.typepersisters.*;
import org.junit.*;
import org.junit.rules.*;
import org.mockito.*;

import javax.sql.*;
import java.sql.*;
import java.util.*;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.anyListOf;
import static org.mockito.Mockito.*;

/**
 * Created by thomas on 9/21/14.
 */
public class SqlInsertResolverTests {
    @Mock
    private SqlDatabaseMetaData metaData;

    @Mock
    private SqlExpressionVisitor expressionVisitor;

    @Mock
    private DataSource dataSource;

    @Mock
    private Connection connection;

    @Mock
    private ExceptionTransformer exceptionTransformer;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private SqlInsertResolver underTest;
    private InsertModel insertModel;

    @Before
    public void setUp() throws SQLException {
        MockitoAnnotations.initMocks(this);
        underTest = new SqlInsertResolver(metaData);
        when(metaData.createExpressionVisitor()).thenReturn(expressionVisitor);
        when(metaData.getExceptionTransformer()).thenReturn(exceptionTransformer);
        when(metaData.getDataSource()).thenReturn(dataSource);
        when(dataSource.getConnection()).thenReturn(connection);

        when(expressionVisitor.getQuery()).thenReturn(new StringBuilder());

        TypePersisterMap map = mock(TypePersisterMap.class);
        when(map.get(any())).thenReturn(mock(TypePersister.class));
        when(metaData.getTypePersisterMap()).thenReturn(map);
        when(connection.prepareStatement(anyString())).thenReturn(mock(PreparedStatement.class));

        insertModel = new InsertModel();
        insertModel.setIntoEntityExpression(mock(EntityExpression.class));
    }

    @Test
    public void test_that_insert_returns_1_when_running_insert_sql_hello() throws SQLException {
        //--Arrange
        when(expressionVisitor.getQuery()).thenReturn(new StringBuilder("hello"));

        PreparedStatement ps = mock(PreparedStatement.class);
        when(connection.prepareStatement("hello")).thenReturn(ps);
        when(ps.executeUpdate()).thenReturn(1);

        //--Assert
        assertThat(underTest.insert(insertModel), is(1));
    }

    @Test
    public void test_that_the_query_and_parameters_list_are_initialized_before_anything_else_happens() throws SQLException {
        //--Act
        underTest.insert(insertModel);

        //--Assert
        InOrder order = inOrder(expressionVisitor);
        order.verify(expressionVisitor).setQuery(any(StringBuilder.class));
        order.verify(expressionVisitor).setParameters(anyListOf(Object.class));
        order.verify(expressionVisitor).visitInsert(any(InsertModel.class));
    }

    @Test
    public void test_that_insert_returns_0_when_running_insert_sql_world() throws SQLException {
        //--Arrange
        when(expressionVisitor.getQuery()).thenReturn(new StringBuilder("world"));

        PreparedStatement ps = mock(PreparedStatement.class);
        when(connection.prepareStatement("world")).thenReturn(ps);
        when(ps.executeUpdate()).thenReturn(0);

        //--Assert
        assertThat(underTest.insert(insertModel), is(0));
    }

    @Test
    public void test_that_insert_returns_0_when_running_insert_sql_hello() throws SQLException {
        //--Arrange
        when(expressionVisitor.getQuery()).thenReturn(new StringBuilder("hello"));

        PreparedStatement ps = mock(PreparedStatement.class);
        when(connection.prepareStatement("hello")).thenReturn(ps);
        when(ps.executeUpdate()).thenReturn(0);

        //--Assert
        assertThat(underTest.insert(insertModel), is(0));
    }

    @Test
    public void test_that_insert_sets_the_parameters_for_world_12_and_true_before_the_query_executes() throws SQLException {
        //--Arrange
        PreparedStatement ps = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(ps);
        when(ps.executeUpdate()).thenReturn(1);

        ArrayList<Object> parameters = new ArrayList<>();
        parameters.add("world");
        parameters.add(12);
        parameters.add(true);
        when(expressionVisitor.getParameters()).thenReturn(parameters);

        TypePersister<String> stringTypePersister = mock(TypePersister.class);
        TypePersister<Integer> integerTypePersister = mock(TypePersister.class);
        TypePersister<Boolean> booleanTypePersister = mock(TypePersister.class);

        TypePersisterMap map = mock(TypePersisterMap.class);
        when((Object) map.get(String.class)).thenReturn(stringTypePersister);
        when((Object) map.get(Integer.class)).thenReturn(integerTypePersister);
        when((Object) map.get(Boolean.class)).thenReturn(booleanTypePersister);

        when(metaData.getTypePersisterMap()).thenReturn(map);

        //--Act
        underTest.insert(insertModel);

        //--Assert
        InOrder order = inOrder(stringTypePersister, integerTypePersister, booleanTypePersister, ps);
        order.verify(stringTypePersister).set(same(ps), eq(1), eq("world"));
        order.verify(integerTypePersister).set(same(ps), eq(2), eq(12));
        order.verify(booleanTypePersister).set(same(ps), eq(3), eq(true));
        order.verify(ps).executeUpdate();
    }

    @Test
    public void test_that_insert_sets_the_parameters_for_17_hello_before_the_query_executes() throws SQLException {
        //--Arrange
        PreparedStatement ps = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(ps);
        when(ps.executeUpdate()).thenReturn(1);

        ArrayList<Object> parameters = new ArrayList<>();
        parameters.add(17);
        parameters.add("hello");
        when(expressionVisitor.getParameters()).thenReturn(parameters);

        TypePersister<Integer> integerTypePersister = mock(TypePersister.class);
        TypePersister<String> stringTypePersister = mock(TypePersister.class);

        TypePersisterMap map = mock(TypePersisterMap.class);
        when((Object) map.get(String.class)).thenReturn(stringTypePersister);
        when((Object) map.get(Integer.class)).thenReturn(integerTypePersister);

        when(metaData.getTypePersisterMap()).thenReturn(map);

        //--Act
        underTest.insert(insertModel);

        //--Assert
        InOrder order = inOrder(integerTypePersister, stringTypePersister, ps);
        order.verify(integerTypePersister).set(same(ps), eq(1), eq(17));
        order.verify(stringTypePersister).set(same(ps), eq(2), eq("hello"));
        order.verify(ps).executeUpdate();
    }

    @Test
    public void test_that_insert_closes_the_PreparedStatement_and_the_Connection_when_no_exceptions_are_thrown() throws
            SQLException {
        //--Arrange
        PreparedStatement ps = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(ps);

        //--Act
        underTest.insert(insertModel);

        //--Assert
        InOrder order = inOrder(ps, connection);
        order.verify(ps).executeUpdate();
        order.verify(ps).close();
        order.verify(connection).close();
    }

    @Test
    public void test_that_insert_wraps_any_exception_thrown_by_executeUpdate() throws SQLException {
        //--Arrange
        PreparedStatement ps = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(ps);

        SQLException expectedSQLException = new SQLException();
        EnvoyException e = new EnvoyException();
        when(exceptionTransformer.transform(same(expectedSQLException))).thenReturn(e);
        doThrow(expectedSQLException).when(ps).executeUpdate();
        expectedException.expect(sameInstance(e));

        //--Act
        underTest.insert(insertModel);
    }

    @Test
    public void test_that_insert_throws_the_correct_EnvoyException_when_ps_close_throws_a_SQLException() throws SQLException {
        //--Arrange
        PreparedStatement ps = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(ps);

        SQLException expectedSQLException = new SQLException();
        EnvoyException e = new EnvoyException();
        doThrow(expectedSQLException).when(ps).close();
        when(exceptionTransformer.transform(same(expectedSQLException))).thenReturn(e);
        expectedException.expect(sameInstance(e));

        //--Act
        underTest.insert(insertModel);
    }

    @Test
    public void test_that_the_insertModel_is_visited_before_prepareStatement_is_called() throws SQLException {
        //--Arrange
        PreparedStatement ps = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(ps);

        //--Act
        underTest.insert(insertModel);

        //--Assert
        InOrder order = inOrder(expressionVisitor, connection);
        order.verify(expressionVisitor).visitInsert(same(insertModel));
        order.verify(connection).prepareStatement(anyString());
    }

    @Test
    public void test_that_insert_sets_the_parameters_for_null_2_before_the_query_executes() throws SQLException {
        //--Arrange
        PreparedStatement ps = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(ps);
        when(ps.executeUpdate()).thenReturn(1);

        ArrayList<Object> parameters = new ArrayList<>();
        parameters.add(null);
        parameters.add(2);
        when(expressionVisitor.getParameters()).thenReturn(parameters);

        TypePersister<Integer> integerTypePersister = mock(TypePersister.class);
        TypePersister<String> stringTypePersister = mock(TypePersister.class);

        TypePersisterMap map = mock(TypePersisterMap.class);
        when((Object) map.get(String.class)).thenReturn(stringTypePersister);
        when((Object) map.get(Integer.class)).thenReturn(integerTypePersister);

        when(metaData.getTypePersisterMap()).thenReturn(map);

        //--Act
        underTest.insert(insertModel);

        //--Assert
        InOrder order = inOrder(ps, integerTypePersister);
        order.verify(ps).setObject(1, null);
        order.verify(integerTypePersister).set(same(ps), eq(2), eq(2));
        order.verify(ps).executeUpdate();
    }
}
