package org.envoy.query.grouping;

import org.junit.*;
import org.mockito.*;

import java.io.*;
import java.util.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class AggregatorWrapper1Tests {
    @Mock
    private ColumnAggregator1<Object, Object> _columnAggregator;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void test_that_aggregate_calls_the_columnAggregator_correctly() {
        //--Arrange
        final Object column1 = mock(Serializable.class);
        final Object expected = mock(Serializable.class);
        final AggregatorWrapper1<Object, Object> underTest = new AggregatorWrapper1<>(this._columnAggregator);

        when(this._columnAggregator.aggregate(same(column1))).thenReturn(expected);

        //--Act
        final Object actual = underTest.aggregate(Arrays.asList(column1));

        //--Assert
        Assert.assertSame(expected, actual);
    }
}
