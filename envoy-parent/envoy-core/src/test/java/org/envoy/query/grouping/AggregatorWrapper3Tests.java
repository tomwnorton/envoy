package org.envoy.query.grouping;

import org.junit.*;
import org.mockito.*;

import java.io.*;
import java.util.*;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

public class AggregatorWrapper3Tests {
    @Mock
    private ColumnAggregator3<Object, Object, Object, Object> _aggregator;

    private AggregatorWrapper3<Object, Object, Object, Object> _underTest;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this._underTest = new AggregatorWrapper3<>(this._aggregator);
    }


    @Test
    public void test_that_aggregate_calls_the_columnAggregator_correctly() {
        //--Arrange
        final Object column1 = mock(Serializable.class);
        final Object column2 = mock(Serializable.class);
        final Object column3 = mock(Serializable.class);
        final Object expected = mock(Serializable.class);

        when(this._aggregator.aggregate(same(column1), same(column2), same(column3))).thenReturn(expected);

        //--Act
        final Object actual = this._underTest.aggregate(Arrays.asList(column1, column2, column3));

        //--Assert
        Assert.assertSame(expected, actual);
    }
}
