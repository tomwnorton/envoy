package org.envoy.exceptions;

import org.envoy.expression.*;

public class EnvoyEntityExpressionInstantiationException extends EnvoyException {
    private static final long serialVersionUID = -2595105498682640197L;
    private static final String START_OF_MESSAGE = "Could not set initialize an entity expression: ";


    public EnvoyEntityExpressionInstantiationException(final Class<?> clazz, final Throwable cause) {
        super(START_OF_MESSAGE + getEntityExpressionNameOrNull(clazz), cause);
    }


    public EnvoyEntityExpressionInstantiationException(final EntityExpression<?> expr, final Throwable cause) {
        this(getEntityExpressionClassOrNull(expr), cause);
    }


    private static String getEntityExpressionNameOrNull(final Class<?> clazz) {
        return clazz == null
               ? "null"
               : clazz.getCanonicalName();
    }


    private static Class<?> getEntityExpressionClassOrNull(final EntityExpression<?> expr) {
        return expr == null
               ? null
               : expr.getClass();
    }
}
