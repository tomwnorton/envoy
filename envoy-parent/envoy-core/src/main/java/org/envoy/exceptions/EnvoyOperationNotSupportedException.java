package org.envoy.exceptions;

/**
 * This exception should be thrown from any ExpressionVisitor method implementation
 * when the corresponding database does not support the given Envoy operation.
 *
 * @author thomas
 */
public class EnvoyOperationNotSupportedException extends EnvoyException {
    private static final long serialVersionUID = 3067110782355771702L;


    public EnvoyOperationNotSupportedException() {
    }


    public EnvoyOperationNotSupportedException(final String message) {
        super(message);
    }


    public EnvoyOperationNotSupportedException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
