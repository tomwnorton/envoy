package org.envoy.exceptions;

public class EnvoyModelExpressionNotMappedToEntityPropertyException extends EnvoyException {
    private static final long serialVersionUID = 1928958436320251148L;


    public EnvoyModelExpressionNotMappedToEntityPropertyException(final String propertyName) {
        super(propertyName);
    }
}
