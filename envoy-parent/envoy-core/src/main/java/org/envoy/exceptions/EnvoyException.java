package org.envoy.exceptions;

public class EnvoyException extends RuntimeException {
    private static final long serialVersionUID = 8941905539176362889L;


    public EnvoyException() {
    }


    public EnvoyException(Throwable t) {
        super(t);
    }


    public EnvoyException(final String message) {
        super(message);
    }


    public EnvoyException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
