package org.envoy.exceptions;

public interface ExceptionTransformer {
    EnvoyException transform(Exception e);
}
