package org.envoy.exceptions;

public class EnvoyReflectionException extends EnvoyException {
    private static final long serialVersionUID = -9208217736201177820L;


    public EnvoyReflectionException() {
    }


    public EnvoyReflectionException(Throwable t) {
        super(t);
    }


    public EnvoyReflectionException(final String message) {
        super(message);
    }


    public EnvoyReflectionException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
