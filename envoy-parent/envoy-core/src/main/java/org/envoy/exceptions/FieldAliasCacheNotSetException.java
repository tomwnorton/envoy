package org.envoy.exceptions;

public class FieldAliasCacheNotSetException extends RuntimeException {
    private static final long serialVersionUID = 5539528104999461424L;


    public FieldAliasCacheNotSetException() {
    }


    public FieldAliasCacheNotSetException(final String message) {
        super(message);
    }


    public FieldAliasCacheNotSetException(final Throwable cause) {
        super(cause);
    }


    public FieldAliasCacheNotSetException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
