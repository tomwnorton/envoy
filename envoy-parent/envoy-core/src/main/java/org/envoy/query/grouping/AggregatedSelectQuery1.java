package org.envoy.query.grouping;

import org.envoy.*;
import org.envoy.expression.*;
import org.envoy.query.*;

import java.util.*;

public class AggregatedSelectQuery1<Column1, FromType> extends AggregatedSelectQuery<FromType> {
    public AggregatedSelectQuery1(
            final DatabaseMetaData metaData,
            final SelectQueryModel<FromType, FromType> model,
            final List<? extends Expression> expressions) {
        super(metaData, model, expressions);
    }


    public <EntityType> ListableSelectQuery<FromType, EntityType> as(final ColumnAggregator1<EntityType, Column1> columnAggregator) {
        return this.createListableSelectQuery(new AggregatorWrapper1<>(columnAggregator));
    }
}
