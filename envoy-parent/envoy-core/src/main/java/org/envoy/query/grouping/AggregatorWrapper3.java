package org.envoy.query.grouping;

import java.util.*;

public class AggregatorWrapper3<To, Column1, Column2, Column3> implements AggregatorWrapper<To> {
    private final ColumnAggregator3<To, Column1, Column2, Column3> _columnAggregator;


    public AggregatorWrapper3(final ColumnAggregator3<To, Column1, Column2, Column3> columnAggregator) {
        this._columnAggregator = columnAggregator;
    }


    @SuppressWarnings("unchecked")
    @Override
    public To aggregate(List<Object> values) {
        return this._columnAggregator.aggregate((Column1) values.get(0), (Column2) values.get(1), (Column3) values.get(2));
    }


    @Override
    public Object getAggregator() {
        return this._columnAggregator;
    }
}
