package org.envoy.query;

/**
 * Created by thomas on 8/29/14.
 */
public interface UpdateResolver {
    public int update(UpdateModel updateModel);
}
