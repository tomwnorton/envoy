package org.envoy.query;

import org.apache.commons.lang3.tuple.*;
import org.envoy.*;
import org.envoy.expression.*;

import java.util.*;

import static org.envoy.expression.ExpressionMethods.*;

/**
 * Created by thomas on 8/30/14.
 */
public class UpdateModel {
    private EntityExpression<?> entityExpressionToUpdate;
    private Collection<Pair<FieldExpression, Expression>> fieldsToUpdate = new ArrayList<>();
    private BooleanExpression whereExpression;
    private final DatabaseMetaData<? extends AutoCloseable> metaData;

    public static UpdateModel createForEntity(
            DatabaseMetaData<? extends AutoCloseable> metaData,
            EntityExpression<?> entityExpression,
            Object entity,
            List<Pair<FieldExpression, Object>> expressionsForPrimaryKeys,
            List<FieldExpression> expressionsForChangedFields) {
        UpdateModel updateModel = new UpdateModel(metaData);
        updateModel.setEntityExpressionToUpdate(entityExpression);
        updateModel.setWhereClauseFromPrimaryKeyFields(expressionsForPrimaryKeys, entity);
        updateModel.addExpressionsForChangedProperties(expressionsForChangedFields, entity);
        return updateModel;
    }

    private void setWhereClauseFromPrimaryKeyFields(List<Pair<FieldExpression, Object>> primaryKeyFields, Object entity) {
        for (Pair<FieldExpression, Object> primaryKeyPair : primaryKeyFields) {
            addPrimaryKeyToWhereClause(primaryKeyPair.getLeft(), primaryKeyPair.getRight());
        }
    }

    private void addPrimaryKeyToWhereClause(FieldExpression fieldExpression, Object value) {
        BooleanExpression equalExpression = metaData.getExpressionFactory().createEqualsExpression(
                fieldExpression,
                asExpression(value));
        if (whereExpression == null) {
            whereExpression = equalExpression;
        } else {
            whereExpression = whereExpression.and(equalExpression);
        }
    }

    private void addExpressionsForChangedProperties(List<FieldExpression> expressionsForChangedProperties, Object entity) {
        for (FieldExpression fieldExpression : expressionsForChangedProperties) {
            addFieldToUpdateFromEntity(fieldExpression, entity);
        }
    }

    private UpdateModel addFieldToUpdateFromEntity(FieldExpression fieldExpression, Object entity) {
        return addFieldToUpdate(fieldExpression, fieldExpression.getFromRootEntity(entity));
    }

    private UpdateModel addFieldToUpdate(FieldExpression fieldExpression, Object value) {
        return addFieldToUpdate(fieldExpression, asExpression(value));
    }

    private UpdateModel addFieldToUpdate(FieldExpression fieldExpression, Expression valueExpression) {
        fieldsToUpdate.add(Pair.of(fieldExpression, valueExpression));
        return this;
    }

    public UpdateModel(DatabaseMetaData<? extends AutoCloseable> metaData) {
        this.metaData = metaData;
    }

    public EntityExpression<?> getEntityExpressionToUpdate() {
        return entityExpressionToUpdate;
    }

    public UpdateModel setEntityExpressionToUpdate(EntityExpression<?> entityExpressionToUpdate) {
        this.entityExpressionToUpdate = entityExpressionToUpdate;
        return this;
    }

    public Collection<Pair<FieldExpression, Expression>> getFieldsToUpdate() {
        return fieldsToUpdate;
    }

    public UpdateModel setFieldsToUpdate(Collection<Pair<FieldExpression, Expression>> fieldsToUpdate) {
        this.fieldsToUpdate = fieldsToUpdate;
        return this;
    }

    public BooleanExpression getWhereExpression() {
        return whereExpression;
    }

    public UpdateModel setWhereExpression(BooleanExpression whereExpression) {
        this.whereExpression = whereExpression;
        return this;
    }
}
