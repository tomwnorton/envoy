package org.envoy.query.grouping;

import java.util.*;

public class AggregatorWrapper6<To, Column1, Column2, Column3, Column4, Column5, Column6> implements AggregatorWrapper<To> {
    private final ColumnAggregator6<To, Column1, Column2, Column3, Column4, Column5, Column6> _columnAggregator;


    public AggregatorWrapper6(ColumnAggregator6<To, Column1, Column2, Column3, Column4, Column5, Column6> columnAggregator) {
        _columnAggregator = columnAggregator;
    }


    @SuppressWarnings("unchecked")
    @Override
    public To aggregate(List<Object> values) {
        return this._columnAggregator.aggregate(
                (Column1) values.get(1 - 1), (Column2) values.get(2 - 1), (Column3) values.get(
                        3 - 1), (Column4) values.get(4 - 1), (Column5) values.get(5 - 1), (Column6) values.get(6 - 1));
    }


    @Override
    public Object getAggregator() {
        return this._columnAggregator;
    }
}