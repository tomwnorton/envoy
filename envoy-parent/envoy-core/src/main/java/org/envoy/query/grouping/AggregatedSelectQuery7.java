package org.envoy.query.grouping;

import org.envoy.*;
import org.envoy.expression.*;
import org.envoy.query.*;

import java.util.*;

public class AggregatedSelectQuery7<Column1, Column2, Column3, Column4, Column5, Column6, Column7, FromType> extends AggregatedSelectQuery<FromType> {
    public AggregatedSelectQuery7(
            DatabaseMetaData metaData,
            SelectQueryModel<FromType, FromType> model,
            List<? extends Expression> expressions) {
        super(metaData, model, expressions);
    }


    public <EntityType> ListableSelectQuery<FromType, EntityType> as(
            ColumnAggregator7<EntityType, Column1, Column2, Column3, Column4, Column5, Column6, Column7> columnAggregator) {
        return this.createListableSelectQuery(new AggregatorWrapper7<>(columnAggregator));
    }
}