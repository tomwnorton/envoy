package org.envoy.query.grouping;

import org.envoy.*;
import org.envoy.expression.*;
import org.envoy.query.*;

import java.util.*;

public class AggregatedSelectQuery14<Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11, Column12, Column13, Column14, FromType> extends AggregatedSelectQuery<FromType> {
    public AggregatedSelectQuery14(
            DatabaseMetaData metaData,
            SelectQueryModel<FromType, FromType> model,
            List<? extends Expression> expressions) {
        super(metaData, model, expressions);
    }


    public <EntityType> ListableSelectQuery<FromType, EntityType> as(
            ColumnAggregator14<EntityType, Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11, Column12, Column13, Column14> columnAggregator) {
        return this.createListableSelectQuery(new AggregatorWrapper14<>(columnAggregator));
    }
}