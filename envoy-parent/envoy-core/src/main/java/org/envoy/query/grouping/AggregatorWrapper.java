package org.envoy.query.grouping;

import java.util.*;

public interface AggregatorWrapper<To> {
    public To aggregate(List<Object> values);


    public Object getAggregator();
}
