package org.envoy.query;

import org.envoy.DatabaseMetaData;

import java.sql.*;
import java.util.*;

public interface QueryResolver<ResultType, ConnectionType extends AutoCloseable> {
    List<ResultType> resolve(DatabaseMetaData metaData, SelectQueryModel<?, ResultType> sqlQuery, ConnectionType connection)
            throws SQLException;
}
