package org.envoy.query;

import org.envoy.expression.*;
import org.envoy.query.grouping.*;

import java.util.*;

public class SelectQueryModel<FromType, ResultType> {
    private List<? extends Expression> selectList;
    private EntityExpression<FromType> from;
    private BooleanExpression whereFilter;
    private AggregatorWrapper<ResultType> aggregatorWrapper;


    public List<? extends Expression> getSelectList() {
        return selectList;
    }


    public void setSelectList(final List<? extends Expression> selectList) {
        this.selectList = selectList;
    }


    public EntityExpression<FromType> getFrom() {
        return from;
    }


    public void setFrom(final EntityExpression<FromType> from) {
        this.from = from;
    }


    public BooleanExpression getWhereFilter() {
        return whereFilter;
    }


    public void setWhereFilter(final BooleanExpression whereFilter) {
        this.whereFilter = whereFilter;
    }


    public AggregatorWrapper<ResultType> getAggregatorWrapper() {
        return aggregatorWrapper;
    }


    public void setAggregatorWrapper(final AggregatorWrapper<ResultType> aggregatorWrapper) {
        this.aggregatorWrapper = aggregatorWrapper;
    }
}
