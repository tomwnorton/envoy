package org.envoy.query.grouping;

import java.util.*;

public class AggregatorWrapper8<To, Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8> implements AggregatorWrapper<To> {
    private final ColumnAggregator8<To, Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8> _columnAggregator;


    public AggregatorWrapper8(ColumnAggregator8<To, Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8> columnAggregator) {
        _columnAggregator = columnAggregator;
    }


    @SuppressWarnings("unchecked")
    @Override
    public To aggregate(List<Object> values) {
        return this._columnAggregator.aggregate(
                (Column1) values.get(1 - 1),
                (Column2) values.get(2 - 1),
                (Column3) values.get(
                        3 - 1),
                (Column4) values.get(4 - 1),
                (Column5) values.get(5 - 1),
                (Column6) values.get(6 - 1),
                (Column7) values.get(7 - 1),
                (Column8) values.get(8 - 1));
    }


    @Override
    public Object getAggregator() {
        return this._columnAggregator;
    }
}