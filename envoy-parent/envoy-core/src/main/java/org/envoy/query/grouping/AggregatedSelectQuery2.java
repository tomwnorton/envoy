package org.envoy.query.grouping;

import org.envoy.*;
import org.envoy.expression.*;
import org.envoy.query.*;

import java.util.*;

public class AggregatedSelectQuery2<Column1, Column2, FromType> extends AggregatedSelectQuery<FromType> {

    public AggregatedSelectQuery2(
            DatabaseMetaData metaData,
            SelectQueryModel<FromType, FromType> model,
            List<? extends Expression> expressions) {
        super(metaData, model, expressions);
    }


    public <EntityType> ListableSelectQuery<FromType, EntityType> as(ColumnAggregator2<EntityType, Column1, Column2> aggregator) {
        return this.createListableSelectQuery(new AggregatorWrapper2<>(aggregator));
    }
}
