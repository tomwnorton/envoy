package org.envoy.query.grouping;

import java.util.*;

public class AggregatorWrapper16<To, Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11, Column12, Column13, Column14, Column15, Column16> implements AggregatorWrapper<To> {
    private final ColumnAggregator16<To, Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11, Column12, Column13, Column14, Column15, Column16> _columnAggregator;


    public AggregatorWrapper16(ColumnAggregator16<To, Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11, Column12, Column13, Column14, Column15, Column16> columnAggregator) {
        _columnAggregator = columnAggregator;
    }


    @SuppressWarnings("unchecked")
    @Override
    public To aggregate(List<Object> values) {
        return this._columnAggregator.aggregate(
                (Column1) values.get(1 - 1),
                (Column2) values.get(2 - 1),
                (Column3) values.get(
                        3 - 1),
                (Column4) values.get(4 - 1),
                (Column5) values.get(5 - 1),
                (Column6) values.get(6 - 1),
                (Column7) values.get(7 - 1),
                (Column8) values.get(8 - 1),
                (Column9) values.get(9 - 1),
                (Column10) values.get(10 - 1),
                (Column11) values.get(11 - 1),
                (Column12) values.get(12 - 1),
                (Column13) values.get(13 - 1),
                (Column14) values.get(14 - 1),
                (Column15) values.get(15 - 1),
                (Column16) values.get(16 - 1));
    }


    @Override
    public Object getAggregator() {
        return this._columnAggregator;
    }
}