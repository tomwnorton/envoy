package org.envoy.query.grouping;

public interface ColumnAggregator7<To, Column1, Column2, Column3, Column4, Column5, Column6, Column7> {
    public To aggregate(
            Column1 column1,
            Column2 column2,
            Column3 column3,
            Column4 column4,
            Column5 column5,
            Column6 column6,
            Column7 column7);
}