package org.envoy.query.grouping;

public interface ColumnAggregator1<To, Column1> {
    public To aggregate(Column1 column1);
}
