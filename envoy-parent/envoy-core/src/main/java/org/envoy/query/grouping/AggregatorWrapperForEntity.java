package org.envoy.query.grouping;

import org.envoy.*;
import org.envoy.expression.*;

import java.util.*;

public class AggregatorWrapperForEntity<To> implements AggregatorWrapper<To> {
    private final EntityFactory entityFactory;
    private final EntityExpression<To> entityExpression;
    private final List<? extends FieldExpression> expressions;


    public AggregatorWrapperForEntity(
            final EntityFactory entityFactory,
            final EntityExpression<To> entityExpression,
            final List<? extends FieldExpression> expressions) {
        this.entityFactory = entityFactory;
        this.entityExpression = entityExpression;
        this.expressions = expressions;
    }


    @Override
    public To aggregate(final List<Object> values) {
        final To entity = entityFactory.createEntity(entityExpression);

        for (int i = 0; i < expressions.size(); i++) {
            expressions.get(i).setFromRootEntity(entity, values.get(i));
        }
        ((AbstractEntity<?>) entity).markAsClean();

        return entity;
    }


    @Override
    public Object getAggregator() {
        return null;
    }
}
