package org.envoy.query;

import org.envoy.*;
import org.envoy.expression.*;

public class QueryBuilderImpl implements QueryBuilder {
    private final DatabaseMetaData metaData;


    public QueryBuilderImpl(final DatabaseMetaData metaData) {
        this.metaData = metaData;
    }


    @Override
    public <Entity> BasicListableSelectQueryImpl<Entity> from(final AbstractEntityExpression<Entity> expression) {
        return new BasicListableSelectQueryImpl<Entity>(metaData, expression);
    }
}
