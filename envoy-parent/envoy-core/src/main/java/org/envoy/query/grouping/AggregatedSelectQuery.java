package org.envoy.query.grouping;

import org.envoy.*;
import org.envoy.expression.*;
import org.envoy.query.*;

import java.util.*;

public abstract class AggregatedSelectQuery<FromType> extends SelectQuery<FromType, FromType> {
    public AggregatedSelectQuery(
            final DatabaseMetaData metaData,
            final SelectQueryModel<FromType, FromType> model,
            final List<? extends Expression> expressions) {
        this.metaData = metaData;
        this.model = new SelectQueryModel<>();
        this.model.setSelectList(expressions);
        this.model.setWhereFilter(model.getWhereFilter());
        this.model.setFrom(model.getFrom());
    }


    protected <EntityType> ListableSelectQuery<FromType, EntityType> createListableSelectQuery(
            final AggregatorWrapper<EntityType> aggregatorWrapper) {
        final SelectQueryModel<FromType, EntityType> model = new SelectQueryModel<>();
        model.setSelectList(this.model.getSelectList());
        model.setFrom(this.model.getFrom());
        model.setWhereFilter(this.model.getWhereFilter());
        model.setAggregatorWrapper(aggregatorWrapper);
        return new ListableSelectQuery<>(this.metaData, model);
    }
}
