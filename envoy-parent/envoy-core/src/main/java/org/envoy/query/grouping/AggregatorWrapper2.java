package org.envoy.query.grouping;

import java.util.*;

public class AggregatorWrapper2<To, Column1, Column2> implements AggregatorWrapper<To> {
    private final ColumnAggregator2<To, Column1, Column2> _aggregator;


    public AggregatorWrapper2(final ColumnAggregator2<To, Column1, Column2> aggregator) {
        this._aggregator = aggregator;
    }


    @SuppressWarnings("unchecked")
    @Override
    public To aggregate(final List<Object> values) {
        return this._aggregator.aggregate((Column1) values.get(0), (Column2) values.get(1));
    }


    @Override
    public ColumnAggregator2<To, Column1, Column2> getAggregator() {
        return this._aggregator;
    }
}
