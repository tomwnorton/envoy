package org.envoy.query;

import org.envoy.*;
import org.envoy.expression.*;

import java.sql.*;
import java.util.*;

/**
 * Created by thomas on 9/21/14.
 */
public class SqlInsertResolver implements InsertResolver {
    private final SqlDatabaseMetaData metaData;

    public SqlInsertResolver(SqlDatabaseMetaData metaData) {
        this.metaData = metaData;
    }

    @Override
    public int insert(InsertModel insertModel) {
        SqlExpressionVisitor expressionVisitor = (SqlExpressionVisitor) metaData.createExpressionVisitor();
        expressionVisitor.setQuery(new StringBuilder());
        expressionVisitor.setParameters(new ArrayList<>());
        expressionVisitor.setFieldAliasCache(insertModel.getIntoEntityExpression().getFieldAliasCache());
        expressionVisitor.visitInsert(insertModel);
        String sql = expressionVisitor.getQuery().toString();
        System.out.println(sql);
        try (
                Connection connection = metaData.getDataSource().getConnection(); PreparedStatement ps = connection
                .prepareStatement(sql)) {
            int index = 1;
            for (Object parameter : expressionVisitor.getParameters()) {
                if (parameter == null) {
                    ps.setObject(index, null);
                } else {
                    metaData.getTypePersisterMap().get(parameter.getClass()).set(ps, index, parameter);
                }
                index++;
            }
            return ps.executeUpdate();
        } catch (SQLException e) {
            throw metaData.getExceptionTransformer().transform(e);
        }
    }
}
