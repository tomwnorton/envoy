package org.envoy.query;

import org.envoy.DatabaseMetaData;
import org.envoy.expression.*;

import java.sql.*;
import java.util.*;

public class BasicSqlQueryResolver<ResultType> implements QueryResolver<ResultType, Connection> {
    @Override
    public List<ResultType> resolve(
            final DatabaseMetaData metaData,
            final SelectQueryModel<?, ResultType> sqlQuery,
            final Connection connection)
            throws SQLException {
        final SqlExpressionVisitor expressionVisitor = (SqlExpressionVisitor) metaData.createExpressionVisitor();
        final StringBuilder sb = new StringBuilder();
        final ArrayList<Object> parameters = new ArrayList<Object>();
        expressionVisitor.setQuery(sb);
        expressionVisitor.setParameters(parameters);
        expressionVisitor.setFieldAliasCache(sqlQuery.getFrom().getFieldAliasCache());
        expressionVisitor.visitSelect(
                metaData.getExpressionFactory().createSelectListExpression(sqlQuery.getSelectList()),
                sqlQuery.getFrom(),
                sqlQuery.getWhereFilter());

        try (
                PreparedStatement ps = connection.prepareStatement(sb.toString());
                ResultSet rs = this.openResultSet(ps, parameters, metaData)) {
            final List<ResultType> results = new ArrayList<ResultType>();
            final FieldAliasCache fieldAliasCache = sqlQuery.getFrom().getFieldAliasCache();
            while (rs.next()) {
                final List<Object> values = new ArrayList<>();
                for (final Expression selectListItem : sqlQuery.getSelectList()) {
                    values.add(
                            metaData.getTypePersisterMap().get(selectListItem.getType())
                                    .get(rs, fieldAliasCache.getFieldAlias(selectListItem)));
                }
                results.add(sqlQuery.getAggregatorWrapper().aggregate(values));
            }
            return results;
        }
    }


    private ResultSet openResultSet(final PreparedStatement ps, final List<Object> parameters, final DatabaseMetaData metaData)
            throws SQLException {
        for (int i = 0; i < parameters.size(); i++) {
            final Object param = parameters.get(i);
            metaData.getTypePersisterMap().get(param.getClass()).set(ps, i + 1, param);
        }

        return ps.executeQuery();
    }
}
