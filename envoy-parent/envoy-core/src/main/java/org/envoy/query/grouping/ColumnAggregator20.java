package org.envoy.query.grouping;

public interface ColumnAggregator20<To, Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11, Column12, Column13, Column14, Column15, Column16, Column17, Column18, Column19, Column20> {
    public To aggregate(
            Column1 column1,
            Column2 column2,
            Column3 column3,
            Column4 column4,
            Column5 column5,
            Column6 column6,
            Column7 column7,
            Column8 column8,
            Column9 column9,
            Column10 column10,
            Column11 column11,
            Column12 column12,
            Column13 column13,
            Column14 column14,
            Column15 column15,
            Column16 column16,
            Column17 column17,
            Column18 column18,
            Column19 column19,
            Column20 column20);
}