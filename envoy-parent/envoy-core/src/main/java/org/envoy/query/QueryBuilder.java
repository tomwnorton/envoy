package org.envoy.query;

import org.envoy.expression.*;

public interface QueryBuilder {
    <Entity> BasicListableSelectQueryImpl<Entity> from(AbstractEntityExpression<Entity> expression);
}
