package org.envoy.query.grouping;

public interface ColumnAggregator3<To, Column1, Column2, Column3> {
    public To aggregate(Column1 column1, Column2 column2, Column3 column3);
}
