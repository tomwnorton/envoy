package org.envoy.query.grouping;

import java.util.*;

public class AggregatorWrapper5<To, Column1, Column2, Column3, Column4, Column5> implements AggregatorWrapper<To> {
    private final ColumnAggregator5<To, Column1, Column2, Column3, Column4, Column5> _columnAggregator;


    public AggregatorWrapper5(ColumnAggregator5<To, Column1, Column2, Column3, Column4, Column5> columnAggregator) {
        _columnAggregator = columnAggregator;
    }


    @SuppressWarnings("unchecked")
    @Override
    public To aggregate(List<Object> values) {
        return this._columnAggregator.aggregate(
                (Column1) values.get(1 - 1), (Column2) values.get(2 - 1), (Column3) values.get(
                        3 - 1), (Column4) values.get(4 - 1), (Column5) values.get(5 - 1));
    }


    @Override
    public Object getAggregator() {
        return this._columnAggregator;
    }
}