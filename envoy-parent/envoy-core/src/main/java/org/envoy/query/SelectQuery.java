package org.envoy.query;

import org.envoy.*;

public abstract class SelectQuery<FromType, ResultType> {
    protected DatabaseMetaData metaData;
    protected SelectQueryModel<FromType, ResultType> model;


    public DatabaseMetaData getMetaData() {
        return metaData;
    }


    public SelectQueryModel<FromType, ResultType> getModel() {
        return model;
    }
}
