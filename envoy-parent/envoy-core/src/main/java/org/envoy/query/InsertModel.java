package org.envoy.query;

import org.envoy.expression.*;

import java.util.*;

/**
 * Created by thomas on 8/30/14.
 */
public class InsertModel {
    private EntityExpression<?> intoEntityExpression;
    private Collection<FieldExpression> fieldsToFill;
    private Collection<Expression> valuesToInsert;
    private FieldExpression autoIncrementedExpression;

    public EntityExpression<?> getIntoEntityExpression() {
        return intoEntityExpression;
    }

    public void setIntoEntityExpression(EntityExpression<?> intoEntityExpression) {
        this.intoEntityExpression = intoEntityExpression;
    }

    public Collection<FieldExpression> getFieldsToFill() {
        return fieldsToFill;
    }

    public void setFieldsToFill(Collection<FieldExpression> fieldsToFill) {
        this.fieldsToFill = fieldsToFill;
    }

    public Collection<Expression> getValuesToInsert() {
        return valuesToInsert;
    }

    public void setValuesToInsert(Collection<Expression> valuesToInsert) {
        this.valuesToInsert = valuesToInsert;
    }

    public FieldExpression getAutoIncrementedExpression() {
        return autoIncrementedExpression;
    }

    public void setAutoIncrementedExpression(FieldExpression autoIncrementedExpression) {
        this.autoIncrementedExpression = autoIncrementedExpression;
    }
}
