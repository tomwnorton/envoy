package org.envoy.query;

import org.envoy.*;
import org.envoy.expression.*;

import java.sql.*;

/**
 * Created by thomas on 9/8/14.
 */
public class SqlUpdateResolver implements UpdateResolver {
    private final SqlDatabaseMetaData metaData;

    public SqlUpdateResolver(SqlDatabaseMetaData metaData) {
        this.metaData = metaData;
    }

    @Override
    public int update(UpdateModel updateModel) {
        SqlExpressionVisitor expressionVisitor = (SqlExpressionVisitor) metaData.createExpressionVisitor();
        expressionVisitor.visitUpdate(updateModel);

        try (
                Connection connection = metaData.getDataSource().getConnection();
                PreparedStatement ps = connection.prepareStatement(expressionVisitor.getQuery().toString())) {

            int index = 1;
            for (Object parameter : expressionVisitor.getParameters()) {
                if (parameter == null) {
                    ps.setObject(index, null);
                } else {
                    metaData.getTypePersisterMap().get(parameter.getClass()).set(ps, index, parameter);
                }
                index++;
            }
            return ps.executeUpdate();
        } catch (SQLException e) {
            throw metaData.getExceptionTransformer().transform(e);
        }
    }
}
