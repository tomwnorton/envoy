package org.envoy.query.grouping;

import org.envoy.*;
import org.envoy.expression.*;
import org.envoy.query.*;

import java.util.*;

public class AggregatedSelectQuery3<Column1, Column2, Column3, FromType> extends AggregatedSelectQuery<FromType> {
    public AggregatedSelectQuery3(
            DatabaseMetaData metaData,
            SelectQueryModel<FromType, FromType> model,
            List<? extends Expression> expressions) {
        super(metaData, model, expressions);
    }


    public <EntityType> ListableSelectQuery<FromType, EntityType> as(
            ColumnAggregator3<EntityType, Column1, Column2, Column3> columnAggregator) {
        return this.createListableSelectQuery(new AggregatorWrapper3<>(columnAggregator));
    }
}
