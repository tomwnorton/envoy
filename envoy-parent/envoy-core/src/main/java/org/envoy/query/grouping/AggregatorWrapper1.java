package org.envoy.query.grouping;

import java.util.*;

public class AggregatorWrapper1<To, Column1> implements AggregatorWrapper<To> {
    private final ColumnAggregator1<To, Column1> _aggregator;


    public AggregatorWrapper1(final ColumnAggregator1<To, Column1> aggregator) {
        this._aggregator = aggregator;
    }


    @SuppressWarnings("unchecked")
    @Override
    public To aggregate(final List<Object> values) {
        return this._aggregator.aggregate((Column1) values.get(0));
    }


    @Override
    public ColumnAggregator1<To, Column1> getAggregator() {
        return this._aggregator;
    }
}
