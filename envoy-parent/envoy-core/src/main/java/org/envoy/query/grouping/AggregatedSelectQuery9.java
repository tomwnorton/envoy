package org.envoy.query.grouping;

import org.envoy.*;
import org.envoy.expression.*;
import org.envoy.query.*;

import java.util.*;

public class AggregatedSelectQuery9<Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, FromType> extends AggregatedSelectQuery<FromType> {
    public AggregatedSelectQuery9(
            DatabaseMetaData metaData,
            SelectQueryModel<FromType, FromType> model,
            List<? extends Expression> expressions) {
        super(metaData, model, expressions);
    }


    public <EntityType> ListableSelectQuery<FromType, EntityType> as(
            ColumnAggregator9<EntityType, Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9> columnAggregator) {
        return this.createListableSelectQuery(new AggregatorWrapper9<>(columnAggregator));
    }
}