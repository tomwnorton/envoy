package org.envoy.query.grouping;

import org.envoy.*;
import org.envoy.expression.*;
import org.envoy.query.*;

import java.util.*;

public class AggregatedSelectQuery5<Column1, Column2, Column3, Column4, Column5, FromType> extends AggregatedSelectQuery<FromType> {
    public AggregatedSelectQuery5(
            DatabaseMetaData metaData,
            SelectQueryModel<FromType, FromType> model,
            List<? extends Expression> expressions) {
        super(metaData, model, expressions);
    }


    public <EntityType> ListableSelectQuery<FromType, EntityType> as(
            ColumnAggregator5<EntityType, Column1, Column2, Column3, Column4, Column5> columnAggregator) {
        return this.createListableSelectQuery(new AggregatorWrapper5<>(columnAggregator));
    }
}