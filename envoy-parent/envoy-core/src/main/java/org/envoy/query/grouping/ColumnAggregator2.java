package org.envoy.query.grouping;

public interface ColumnAggregator2<To, Column1, Column2> {
    public To aggregate(Column1 column1, Column2 column2);
}
