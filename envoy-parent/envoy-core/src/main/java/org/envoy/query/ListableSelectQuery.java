package org.envoy.query;

import org.envoy.*;
import org.envoy.exceptions.*;
import org.envoy.expression.*;

import java.util.*;

public class ListableSelectQuery<EntityType, ResultType> extends SelectQuery<EntityType, ResultType> {

    public ListableSelectQuery(
            final DatabaseMetaData metaData,
            final SelectQueryModel<EntityType, ResultType> model) {
        this.metaData = metaData;
        this.model = model;
    }


    public ListableSelectQuery(
            final DatabaseMetaData metaData,
            final EntityExpression<EntityType> fromExpression) {
        this.metaData = metaData;
        model = new SelectQueryModel<>();
        model.setFrom(fromExpression);
    }


    public List<ResultType> list() {
        try (AutoCloseable connection = metaData.createConnection()) {
            return metaData.<ResultType>getQueryResolver().resolve(metaData, model, connection);
        } catch (final Exception e) {
            if (metaData.getExceptionTransformer() != null) {
                final EnvoyException wrapperException = metaData.getExceptionTransformer().transform(e);
                if (wrapperException != null) {
                    throw wrapperException;
                }
            }

            throw new EnvoyException("Envoy could not execute the query.", e);
        }
    }


    public ResultType single() {
        final List<ResultType> results = list();
        if (results != null && !results.isEmpty()) {
            return results.get(0);
        }
        return null;
    }
}
