package org.envoy.query;

import org.envoy.*;
import org.envoy.expression.*;
import org.envoy.query.grouping.*;

import java.util.*;

public class BasicListableSelectQueryImpl<EntityType> extends ListableSelectQuery<EntityType, EntityType> {
    public BasicListableSelectQueryImpl(
            final DatabaseMetaData metaData,
            final AbstractEntityExpression<EntityType> fromExpression) {
        super(metaData, fromExpression);
        List<FieldExpression> fieldExpressions = fromExpression.getFieldExpressions();
        this.model.setSelectList(fieldExpressions);
        this.model.setAggregatorWrapper(
                new AggregatorWrapperForEntity<>(
                        metaData.getEntityFactory(),
                        model.getFrom(),
                        fieldExpressions));
    }


    public BasicListableSelectQueryImpl<EntityType> where(final BooleanExpression expr) {
        if (expr instanceof BooleanFieldExpression) {
            this.where(
                    metaData.getExpressionFactory().createEqualsExpression(
                            expr,
                            metaData.getExpressionFactory().createBooleanConstantExpression(true)));
        } else if (model.getWhereFilter() != null) {
            model.setWhereFilter(ExpressionFactoryImpl.get().createAndExpression(model.getWhereFilter(), expr));
        } else {
            model.setWhereFilter(expr);
        }
        return this;
    }


    public <Column1> AggregatedSelectQuery1<Column1, EntityType> aggregate(final GenericExpression<Column1> column1) {
        return new AggregatedSelectQuery1<>(metaData, model, Arrays.asList(column1));
    }


    public <Column1, Column2> AggregatedSelectQuery2<Column1, Column2, EntityType> aggregate(
            final GenericExpression<Column1> column1,
            final GenericExpression<Column2> column2) {
        return new AggregatedSelectQuery2<>(metaData, model, Arrays.asList(column1, column2));
    }


    public <Column1, Column2, Column3> AggregatedSelectQuery3<Column1, Column2, Column3, EntityType> aggregate(
            GenericExpression<Column1> column1,
            GenericExpression<Column2> column2,
            GenericExpression<Column3> column3) {
        return new AggregatedSelectQuery3<>(metaData, model, Arrays.asList(column1, column2, column3));
    }


    public <Column1, Column2, Column3, Column4> AggregatedSelectQuery4<Column1, Column2, Column3, Column4, EntityType> aggregate(
            GenericExpression<Column1> column1,
            GenericExpression<Column2> column2,
            GenericExpression<Column3> column3,
            GenericExpression<Column4> column4) {
        return new AggregatedSelectQuery4<>(metaData, model, Arrays.asList(column1, column2, column3, column4));
    }


    public <Column1, Column2, Column3, Column4, Column5> AggregatedSelectQuery5<Column1, Column2, Column3, Column4, Column5, EntityType> aggregate(
            GenericExpression<Column1> column1,
            GenericExpression<Column2> column2,
            GenericExpression<Column3> column3,
            GenericExpression<Column4> column4,
            GenericExpression<Column5> column5) {
        return new AggregatedSelectQuery5<>(metaData, model, Arrays.asList(column1, column2, column3, column4, column5));
    }


    public <Column1, Column2, Column3, Column4, Column5, Column6> AggregatedSelectQuery6<Column1, Column2, Column3, Column4, Column5, Column6, EntityType> aggregate(
            GenericExpression<Column1> column1,
            GenericExpression<Column2> column2,
            GenericExpression<Column3> column3,
            GenericExpression<Column4> column4,
            GenericExpression<Column5> column5,
            GenericExpression<Column6> column6) {
        return new AggregatedSelectQuery6<>(
                metaData, model, Arrays.asList(
                column1,
                column2,
                column3,
                column4,
                column5,
                column6));
    }


    public <Column1, Column2, Column3, Column4, Column5, Column6, Column7> AggregatedSelectQuery7<Column1, Column2, Column3, Column4, Column5, Column6, Column7, EntityType> aggregate(
            GenericExpression<Column1> column1,
            GenericExpression<Column2> column2,
            GenericExpression<Column3> column3,
            GenericExpression<Column4> column4,
            GenericExpression<Column5> column5,
            GenericExpression<Column6> column6,
            GenericExpression<Column7> column7) {
        return new AggregatedSelectQuery7<>(
                metaData, model, Arrays.asList(
                column1,
                column2,
                column3,
                column4,
                column5,
                column6,
                column7));
    }


    public <Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8> AggregatedSelectQuery8<Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, EntityType> aggregate(
            GenericExpression<Column1> column1,
            GenericExpression<Column2> column2,
            GenericExpression<Column3> column3,
            GenericExpression<Column4> column4,
            GenericExpression<Column5> column5,
            GenericExpression<Column6> column6,
            GenericExpression<Column7> column7,
            GenericExpression<Column8> column8) {
        return new AggregatedSelectQuery8<>(
                metaData, model, Arrays.asList(
                column1,
                column2,
                column3,
                column4,
                column5,
                column6,
                column7,
                column8));
    }


    public <Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9> AggregatedSelectQuery9<Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, EntityType> aggregate(
            GenericExpression<Column1> column1,
            GenericExpression<Column2> column2,
            GenericExpression<Column3> column3,
            GenericExpression<Column4> column4,
            GenericExpression<Column5> column5,
            GenericExpression<Column6> column6,
            GenericExpression<Column7> column7,
            GenericExpression<Column8> column8,
            GenericExpression<Column9> column9) {
        return new AggregatedSelectQuery9<>(
                metaData, model, Arrays.asList(
                column1,
                column2,
                column3,
                column4,
                column5,
                column6,
                column7,
                column8,
                column9));
    }


    public <Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10> AggregatedSelectQuery10<Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, EntityType> aggregate(
            GenericExpression<Column1> column1,
            GenericExpression<Column2> column2,
            GenericExpression<Column3> column3,
            GenericExpression<Column4> column4,
            GenericExpression<Column5> column5,
            GenericExpression<Column6> column6,
            GenericExpression<Column7> column7,
            GenericExpression<Column8> column8,
            GenericExpression<Column9> column9,
            GenericExpression<Column10> column10) {
        return new AggregatedSelectQuery10<>(
                metaData, model, Arrays.asList(
                column1,
                column2,
                column3,
                column4,
                column5,
                column6,
                column7,
                column8,
                column9,
                column10));
    }


    public <Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11> AggregatedSelectQuery11<Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11, EntityType> aggregate(
            GenericExpression<Column1> column1,
            GenericExpression<Column2> column2,
            GenericExpression<Column3> column3,
            GenericExpression<Column4> column4,
            GenericExpression<Column5> column5,
            GenericExpression<Column6> column6,
            GenericExpression<Column7> column7,
            GenericExpression<Column8> column8,
            GenericExpression<Column9> column9,
            GenericExpression<Column10> column10,
            GenericExpression<Column11> column11) {
        return new AggregatedSelectQuery11<>(
                metaData, model, Arrays.asList(
                column1,
                column2,
                column3,
                column4,
                column5,
                column6,
                column7,
                column8,
                column9,
                column10,
                column11));
    }


    public <Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11, Column12> AggregatedSelectQuery12<Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11, Column12, EntityType> aggregate(
            GenericExpression<Column1> column1,
            GenericExpression<Column2> column2,
            GenericExpression<Column3> column3,
            GenericExpression<Column4> column4,
            GenericExpression<Column5> column5,
            GenericExpression<Column6> column6,
            GenericExpression<Column7> column7,
            GenericExpression<Column8> column8,
            GenericExpression<Column9> column9,
            GenericExpression<Column10> column10,
            GenericExpression<Column11> column11,
            GenericExpression<Column12> column12) {
        return new AggregatedSelectQuery12<>(
                metaData, model, Arrays.asList(
                column1,
                column2,
                column3,
                column4,
                column5,
                column6,
                column7,
                column8,
                column9,
                column10,
                column11,
                column12));
    }


    public <Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11, Column12, Column13> AggregatedSelectQuery13<Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11, Column12, Column13, EntityType> aggregate(
            GenericExpression<Column1> column1,
            GenericExpression<Column2> column2,
            GenericExpression<Column3> column3,
            GenericExpression<Column4> column4,
            GenericExpression<Column5> column5,
            GenericExpression<Column6> column6,
            GenericExpression<Column7> column7,
            GenericExpression<Column8> column8,
            GenericExpression<Column9> column9,
            GenericExpression<Column10> column10,
            GenericExpression<Column11> column11,
            GenericExpression<Column12> column12,
            GenericExpression<Column13> column13) {
        return new AggregatedSelectQuery13<>(
                metaData, model, Arrays.asList(
                column1,
                column2,
                column3,
                column4,
                column5,
                column6,
                column7,
                column8,
                column9,
                column10,
                column11,
                column12,
                column13));
    }


    public <Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11, Column12, Column13, Column14> AggregatedSelectQuery14<Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11, Column12, Column13, Column14, EntityType> aggregate(
            GenericExpression<Column1> column1,
            GenericExpression<Column2> column2,
            GenericExpression<Column3> column3,
            GenericExpression<Column4> column4,
            GenericExpression<Column5> column5,
            GenericExpression<Column6> column6,
            GenericExpression<Column7> column7,
            GenericExpression<Column8> column8,
            GenericExpression<Column9> column9,
            GenericExpression<Column10> column10,
            GenericExpression<Column11> column11,
            GenericExpression<Column12> column12,
            GenericExpression<Column13> column13,
            GenericExpression<Column14> column14) {
        return new AggregatedSelectQuery14<>(
                metaData, model, Arrays.asList(
                column1,
                column2,
                column3,
                column4,
                column5,
                column6,
                column7,
                column8,
                column9,
                column10,
                column11,
                column12,
                column13,
                column14));
    }


    public <Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11, Column12, Column13, Column14, Column15> AggregatedSelectQuery15<Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11, Column12, Column13, Column14, Column15, EntityType> aggregate(
            GenericExpression<Column1> column1,
            GenericExpression<Column2> column2,
            GenericExpression<Column3> column3,
            GenericExpression<Column4> column4,
            GenericExpression<Column5> column5,
            GenericExpression<Column6> column6,
            GenericExpression<Column7> column7,
            GenericExpression<Column8> column8,
            GenericExpression<Column9> column9,
            GenericExpression<Column10> column10,
            GenericExpression<Column11> column11,
            GenericExpression<Column12> column12,
            GenericExpression<Column13> column13,
            GenericExpression<Column14> column14,
            GenericExpression<Column15> column15) {
        return new AggregatedSelectQuery15<>(
                metaData, model, Arrays.asList(
                column1,
                column2,
                column3,
                column4,
                column5,
                column6,
                column7,
                column8,
                column9,
                column10,
                column11,
                column12,
                column13,
                column14,
                column15));
    }


    public <Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11, Column12, Column13, Column14, Column15, Column16> AggregatedSelectQuery16<Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11, Column12, Column13, Column14, Column15, Column16, EntityType> aggregate(
            GenericExpression<Column1> column1,
            GenericExpression<Column2> column2,
            GenericExpression<Column3> column3,
            GenericExpression<Column4> column4,
            GenericExpression<Column5> column5,
            GenericExpression<Column6> column6,
            GenericExpression<Column7> column7,
            GenericExpression<Column8> column8,
            GenericExpression<Column9> column9,
            GenericExpression<Column10> column10,
            GenericExpression<Column11> column11,
            GenericExpression<Column12> column12,
            GenericExpression<Column13> column13,
            GenericExpression<Column14> column14,
            GenericExpression<Column15> column15,
            GenericExpression<Column16> column16) {
        return new AggregatedSelectQuery16<>(
                metaData, model, Arrays.asList(
                column1,
                column2,
                column3,
                column4,
                column5,
                column6,
                column7,
                column8,
                column9,
                column10,
                column11,
                column12,
                column13,
                column14,
                column15,
                column16));
    }


    public <Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11, Column12, Column13, Column14, Column15, Column16, Column17> AggregatedSelectQuery17<Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11, Column12, Column13, Column14, Column15, Column16, Column17, EntityType> aggregate(
            GenericExpression<Column1> column1,
            GenericExpression<Column2> column2,
            GenericExpression<Column3> column3,
            GenericExpression<Column4> column4,
            GenericExpression<Column5> column5,
            GenericExpression<Column6> column6,
            GenericExpression<Column7> column7,
            GenericExpression<Column8> column8,
            GenericExpression<Column9> column9,
            GenericExpression<Column10> column10,
            GenericExpression<Column11> column11,
            GenericExpression<Column12> column12,
            GenericExpression<Column13> column13,
            GenericExpression<Column14> column14,
            GenericExpression<Column15> column15,
            GenericExpression<Column16> column16,
            GenericExpression<Column17> column17) {
        return new AggregatedSelectQuery17<>(
                metaData, model, Arrays.asList(
                column1,
                column2,
                column3,
                column4,
                column5,
                column6,
                column7,
                column8,
                column9,
                column10,
                column11,
                column12,
                column13,
                column14,
                column15,
                column16,
                column17));
    }


    public <Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11, Column12, Column13, Column14, Column15, Column16, Column17, Column18> AggregatedSelectQuery18<Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11, Column12, Column13, Column14, Column15, Column16, Column17, Column18, EntityType> aggregate(
            GenericExpression<Column1> column1,
            GenericExpression<Column2> column2,
            GenericExpression<Column3> column3,
            GenericExpression<Column4> column4,
            GenericExpression<Column5> column5,
            GenericExpression<Column6> column6,
            GenericExpression<Column7> column7,
            GenericExpression<Column8> column8,
            GenericExpression<Column9> column9,
            GenericExpression<Column10> column10,
            GenericExpression<Column11> column11,
            GenericExpression<Column12> column12,
            GenericExpression<Column13> column13,
            GenericExpression<Column14> column14,
            GenericExpression<Column15> column15,
            GenericExpression<Column16> column16,
            GenericExpression<Column17> column17,
            GenericExpression<Column18> column18) {
        return new AggregatedSelectQuery18<>(
                metaData, model, Arrays.asList(
                column1,
                column2,
                column3,
                column4,
                column5,
                column6,
                column7,
                column8,
                column9,
                column10,
                column11,
                column12,
                column13,
                column14,
                column15,
                column16,
                column17,
                column18));
    }


    public <Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11, Column12, Column13, Column14, Column15, Column16, Column17, Column18, Column19> AggregatedSelectQuery19<Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11, Column12, Column13, Column14, Column15, Column16, Column17, Column18, Column19, EntityType> aggregate(
            GenericExpression<Column1> column1,
            GenericExpression<Column2> column2,
            GenericExpression<Column3> column3,
            GenericExpression<Column4> column4,
            GenericExpression<Column5> column5,
            GenericExpression<Column6> column6,
            GenericExpression<Column7> column7,
            GenericExpression<Column8> column8,
            GenericExpression<Column9> column9,
            GenericExpression<Column10> column10,
            GenericExpression<Column11> column11,
            GenericExpression<Column12> column12,
            GenericExpression<Column13> column13,
            GenericExpression<Column14> column14,
            GenericExpression<Column15> column15,
            GenericExpression<Column16> column16,
            GenericExpression<Column17> column17,
            GenericExpression<Column18> column18,
            GenericExpression<Column19> column19) {
        return new AggregatedSelectQuery19<>(
                metaData, model, Arrays.asList(
                column1,
                column2,
                column3,
                column4,
                column5,
                column6,
                column7,
                column8,
                column9,
                column10,
                column11,
                column12,
                column13,
                column14,
                column15,
                column16,
                column17,
                column18,
                column19));
    }


    public <Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11, Column12, Column13, Column14, Column15, Column16, Column17, Column18, Column19, Column20> AggregatedSelectQuery20<Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11, Column12, Column13, Column14, Column15, Column16, Column17, Column18, Column19, Column20, EntityType> aggregate(
            GenericExpression<Column1> column1,
            GenericExpression<Column2> column2,
            GenericExpression<Column3> column3,
            GenericExpression<Column4> column4,
            GenericExpression<Column5> column5,
            GenericExpression<Column6> column6,
            GenericExpression<Column7> column7,
            GenericExpression<Column8> column8,
            GenericExpression<Column9> column9,
            GenericExpression<Column10> column10,
            GenericExpression<Column11> column11,
            GenericExpression<Column12> column12,
            GenericExpression<Column13> column13,
            GenericExpression<Column14> column14,
            GenericExpression<Column15> column15,
            GenericExpression<Column16> column16,
            GenericExpression<Column17> column17,
            GenericExpression<Column18> column18,
            GenericExpression<Column19> column19,
            GenericExpression<Column20> column20) {
        return new AggregatedSelectQuery20<>(
                metaData, model, Arrays.asList(
                column1,
                column2,
                column3,
                column4,
                column5,
                column6,
                column7,
                column8,
                column9,
                column10,
                column11,
                column12,
                column13,
                column14,
                column15,
                column16,
                column17,
                column18,
                column19,
                column20));
    }
    // @placeholder:aggregate() -- DO NOT DELETE!!!
}
