package org.envoy.query.grouping;

public interface ColumnAggregator4<To, Column1, Column2, Column3, Column4> {
    public To aggregate(Column1 column1, Column2 column2, Column3 column3, Column4 column4);
}