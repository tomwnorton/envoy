package org.envoy;

import org.envoy.exceptions.*;
import org.envoy.expression.*;
import org.envoy.query.*;

import javax.sql.*;
import java.sql.*;
import java.util.*;
import java.util.regex.*;

public abstract class AbstractSqlDatabaseMetaData extends AbstractDatabaseMetaData<Connection> implements SqlDatabaseMetaData {
    private DataSource dataSource;
    private List<String> reservedWords;
    private FieldAliasCache fieldAliasCache;
    private final InsertResolver insertResolver = new SqlInsertResolver(this);
    private final UpdateResolver updateResolver = new SqlUpdateResolver(this);


    public AbstractSqlDatabaseMetaData(final ExceptionTransformer exceptionTransformer) {
        super(exceptionTransformer);
        this.fieldAliasCache = new FieldAliasCacheImpl();
    }


    @Override
    public DataSource getDataSource() {
        return dataSource;
    }


    public void setDataSource(final DataSource dataSource) {
        this.dataSource = dataSource;
    }


    @Override
    public Connection createConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            if (getExceptionTransformer() != null) {
                EnvoyException transformedEx = getExceptionTransformer().transform(e);
                if (transformedEx != null) {
                    throw transformedEx;
                }
            }
            throw new EnvoyException("Could not create a connection", e);
        }
    }


    @Override
    public List<String> getReservedWords() {
        //--Used a bundle because it is easier to enumerate large lists of strings that way.
        if (reservedWords == null) {
            reservedWords = Arrays.asList(ResourceBundle.getBundle("org.envoy.sql").getString("reserved.words").split(","));
        }
        return reservedWords;
    }


    @Override
    public String getOperatorsRegexClass() {
        return " \\t\\r\\n<>=+\\-*/,\\(\\)!";
    }


    @Override
    public String getStartForDatabaseObjectEscape() {
        return "\"";
    }


    @Override
    public String getEndForDatabaseObjectEscape() {
        return "\"";
    }


    @Override
    public String getEscapedObjectName(final String objectName) {
        if (getReservedWords().contains(objectName.toUpperCase())
            || Pattern.compile("[" + getOperatorsRegexClass() + "]").matcher(objectName).find()) {
            return getStartForDatabaseObjectEscape() + objectName + getEndForDatabaseObjectEscape();
        }
        return objectName;
    }


    @Override
    public FieldAliasCache getFieldAliasCache() {
        return fieldAliasCache;
    }


    public void setFieldAliasCache(final FieldAliasCache fieldAliasCache) {
        this.fieldAliasCache = fieldAliasCache;
    }

    @Override
    public InsertResolver getInsertResolver() {
        return insertResolver;
    }

    @Override
    public UpdateResolver getUpdateResolver() {
        return updateResolver;
    }
}
