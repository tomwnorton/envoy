package org.envoy;

import org.envoy.exceptions.*;
import org.envoy.expression.*;
import org.envoy.query.*;
import org.envoy.typepersisters.*;

/**
 * Metadata about a database.
 */
public interface DatabaseMetaData<ConnectionType extends AutoCloseable> {
    public EntityFactory getEntityFactory();


    public TypePersisterMap getTypePersisterMap();


    public <EntityType> QueryResolver<EntityType, ConnectionType> getQueryResolver();


    public ExceptionTransformer getExceptionTransformer();


    public ExpressionFactory getExpressionFactory();


    public ExpressionVisitor createExpressionVisitor();

    public AutoCloseable createConnection();

    public QueryBuilder getQueryBuilder();

    public InsertResolver getInsertResolver();

    public UpdateResolver getUpdateResolver();
}
