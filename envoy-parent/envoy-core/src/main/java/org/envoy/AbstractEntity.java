package org.envoy;

import org.apache.commons.lang3.tuple.*;
import org.envoy.exceptions.*;
import org.envoy.expression.*;
import org.envoy.query.*;

import java.lang.reflect.*;
import java.util.*;

/**
 * Created by thomas on 8/28/14.
 */
public abstract class AbstractEntity<EntityType extends AbstractEntity<EntityType>> extends AbstractBean {
    private final Class<? extends EntityExpression<EntityType>> expressionClass;
    private final DatabaseMetaData<? extends AutoCloseable> metaData;

    public AbstractEntity(
            Class<? extends EntityExpression<EntityType>> expressionClass,
            DatabaseMetaData<? extends AutoCloseable> metaData) {
        this.expressionClass = expressionClass;
        this.metaData = metaData;
    }

    public boolean save() {
        boolean result = tryToUpdate() ? true : tryToInsert();
        markAsClean();
        return result;
    }

    private boolean tryToUpdate() {
        EntityExpression<EntityType> entityExpression = createEntityExpression();
        List<FieldExpression> changedExpressions = getFieldExpressionsForChangedProperties(entityExpression);
        if (changedExpressions.isEmpty()) {
            return false;
        }
        List<Pair<FieldExpression, Object>> primaryKeyValues = getPrimaryKeyValues(entityExpression);

        UpdateModel updateModel = UpdateModel.createForEntity(
                metaData,
                entityExpression,
                this,
                primaryKeyValues,
                changedExpressions);

        return metaData.getUpdateResolver().update(updateModel) > 0;
    }

    private boolean tryToInsert() {
        EntityExpression<EntityType> entityExpression = createEntityExpression();
        InsertModel insertModel = new InsertModel();
        insertModel.setIntoEntityExpression(entityExpression);
        insertModel.setFieldsToFill(new ArrayList<FieldExpression>());
        //insertModel.setFieldsToFill(entityExpression.getFieldExpressions());
        List<Expression> values = new ArrayList<>();
        for (FieldExpression fieldExpression : entityExpression.getFieldExpressions()) {
            if (fieldExpression.isAutoIncrement()) {
                insertModel.setAutoIncrementedExpression(fieldExpression);
            } else {
                insertModel.getFieldsToFill().add(fieldExpression);
                values.add(new ParameterExpression<>(fieldExpression.getFromRootEntity(this)));
            }
        }
        insertModel.setValuesToInsert(values);
        return metaData.getInsertResolver().insert(insertModel) > 0;
    }

    private EntityExpression<EntityType> createEntityExpression() {
        try {
            return expressionClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new EnvoyReflectionException(e);
        }
    }

    private List<FieldExpression> getFieldExpressionsForChangedProperties(EntityExpression<EntityType> entityExpression) {
        List<FieldExpression> modelExpressions = new ArrayList<>();
        for (ModelExpression modelExpression : entityExpression.getFlattenedModelExpressions()) {
            if (doesModelNeedToBeSaved(modelExpression)) {
                if (modelExpression instanceof FieldExpression
                    && !isFieldExpressionForUnchangedPrimaryKeyField((FieldExpression) modelExpression)) {
                    modelExpressions.add((FieldExpression) modelExpression);
                } else if (modelExpression instanceof BeanExpression) {
                    BeanExpression beanExpression = (BeanExpression) modelExpression;
                    modelExpressions.addAll(
                            getOnlyFieldExpressionsFromChangedBeanExpressionThatAreNotForUnchangedPrimaryKeys(
                                    beanExpression));
                }
            }
        }
        return modelExpressions;
    }

    private List<FieldExpression> getOnlyFieldExpressionsFromChangedBeanExpressionThatAreNotForUnchangedPrimaryKeys(
            BeanExpression beanExpression) {
        List<FieldExpression> fieldExpressions = new ArrayList<>();
        AbstractBean nestedBean = (AbstractBean) getOldValueForModelExpression(beanExpression);
        for (FieldExpression nestedFieldExpression : beanExpression.getFieldExpressions()) {
            if (!nestedFieldExpression.isPrimaryKey()) {
                fieldExpressions.add(nestedFieldExpression);
            } else if (nestedBean != null) {
                try {
                    if (nestedFieldExpression.getPropertyDescriptor().getReadMethod().invoke(nestedBean) != null) {
                        fieldExpressions.add(nestedFieldExpression);
                    }
                } catch (IllegalAccessException | InvocationTargetException e) {
                    throw new EnvoyReflectionException(e);
                }
            }
        }
        return fieldExpressions;
    }

    private List<Pair<FieldExpression, Object>> getPrimaryKeyValues(EntityExpression<EntityType> entityExpression) {
        List<Pair<FieldExpression, Object>> primaryKeyValues = new ArrayList<>();
        for (FieldExpression primaryKeyExpression : entityExpression.getPrimaryKeys()) {
            Object oldValue = getOldValueForModelExpression(primaryKeyExpression);
            if (oldValue != null) {
                primaryKeyValues.add(Pair.of(primaryKeyExpression, oldValue));
            } else {
                Object currentValue = primaryKeyExpression.getFromRootEntity(this);
                primaryKeyValues.add(Pair.of(primaryKeyExpression, currentValue));
            }
        }
        return primaryKeyValues;
    }

    @Override
    public void markAsClean() {
        super.markAsClean();
    }
}
