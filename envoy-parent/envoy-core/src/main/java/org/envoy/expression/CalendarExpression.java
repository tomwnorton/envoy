package org.envoy.expression;

import java.util.*;

public interface CalendarExpression extends ComparableExpression<Calendar, Calendar, CalendarExpression> {
}
