package org.envoy.expression;

class DividedByFloatingPointExpression extends AbstractFloatingPointBinaryExpression {
    public DividedByFloatingPointExpression(
            final Expression lhs,
            final Expression rhs,
            final ExpressionFactory expressionFactory) {
        super(lhs, rhs, expressionFactory);
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitDividedBy(this.lhs, this.rhs);
    }
}
