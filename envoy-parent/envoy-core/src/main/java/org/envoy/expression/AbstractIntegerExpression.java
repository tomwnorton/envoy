package org.envoy.expression;

import java.math.*;
import java.util.*;

abstract class AbstractIntegerExpression<T extends Number> extends AbstractNumericExpression<T> implements IntegerExpression<T> {
    public AbstractIntegerExpression(final ExpressionFactory expressionFactory) {
        super(expressionFactory);
    }


    @Override
    public IntegerExpression<Long> plus(final IntegerExpression<?> expr) {
        return getExpressionFactory().createPlusIntegerExpression(this, expr);
    }


    @Override
    public FloatingPointExpression<BigDecimal> plus(final FloatingPointExpression<?> expr) {
        return getExpressionFactory().createPlusFloatingPointExpression(this, expr);
    }


    @Override
    public IntegerExpression<Long> minus(final IntegerExpression<?> expr) {
        return getExpressionFactory().createMinusIntegerExpression(this, expr);
    }


    @Override
    public FloatingPointExpression<BigDecimal> minus(final FloatingPointExpression<?> expr) {
        return getExpressionFactory().createMinusFloatingPointExpression(this, expr);
    }


    @Override
    public IntegerExpression<Long> times(final IntegerExpression<?> expr) {
        return getExpressionFactory().createTimesIntegerExpression(this, expr);
    }


    @Override
    public FloatingPointExpression<BigDecimal> times(final FloatingPointExpression<?> expr) {
        return getExpressionFactory().createTimesFloatingPointExpression(this, expr);
    }


    @Override
    public IntegerExpression<Long> dividedBy(final IntegerExpression<?> expr) {
        return getExpressionFactory().createDividedByIntegerExpression(this, expr);
    }


    @Override
    public FloatingPointExpression<BigDecimal> dividedBy(final FloatingPointExpression<?> expr) {
        return getExpressionFactory().createDividedByFloatingPointExpression(this, expr);
    }


    @Override
    public IntegerExpression<Long> modulus(final IntegerExpression<?> expr) {
        return getExpressionFactory().createModulusIntegerExpression(this, expr);
    }


    @Override
    protected IntegerExpression<T> coalesceInternal(final List<GenericExpression<T>> exprs) {
        final ArrayList<GenericExpression<T>> allExprs = new ArrayList<GenericExpression<T>>();
        allExprs.add(this);
        allExprs.addAll(exprs);

        return getExpressionFactory().createCoalesceIntegerExpression(allExprs);
    }


    @Override
    public IntegerExpression<T> negative() {
        return getExpressionFactory().createNegativeIntegerExpression(this);
    }
}
