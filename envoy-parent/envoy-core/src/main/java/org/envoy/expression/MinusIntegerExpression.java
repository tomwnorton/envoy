package org.envoy.expression;

class MinusIntegerExpression extends AbstractIntegerBinaryExpression {
    public MinusIntegerExpression(final Expression lhs, final Expression rhs, final ExpressionFactory expressionFactory) {
        super(lhs, rhs, expressionFactory);
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitMinus(lhs, rhs);
    }
}
