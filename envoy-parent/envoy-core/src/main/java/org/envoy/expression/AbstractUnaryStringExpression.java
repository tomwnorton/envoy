package org.envoy.expression;

abstract class AbstractUnaryStringExpression extends AbstractStringExpression {
    private final Expression operand;


    protected abstract void onAccept(Expression operand, ExpressionVisitor visitor);


    AbstractUnaryStringExpression(final Expression operand, final ExpressionFactory expressionFactory) {
        super(expressionFactory);
        this.operand = operand;
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        this.onAccept(operand, visitor);
    }
}
