package org.envoy.expression;

class GreaterThanExpression extends AbstractBooleanBinaryExpression {
    public GreaterThanExpression(final Expression lhs, final Expression rhs, final ExpressionFactory expressionFactory) {
        super(lhs, rhs, expressionFactory);
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitGreaterThan(lhs, rhs);
    }
}
