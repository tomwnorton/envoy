package org.envoy.expression;

public class DoubleFieldExpression extends AbstractFloatingPointFieldExpression<Double, DoubleFieldExpression> {
    public DoubleFieldExpression(final ModelExpression parent) {
        super(parent);
    }


    public DoubleFieldExpression(final ModelExpression parent, final String fieldName) {
        super(parent, fieldName);
    }


    @Override
    public Class<Double> getType() {
        return Double.class;
    }
}
