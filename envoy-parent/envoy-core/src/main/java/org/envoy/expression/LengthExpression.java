package org.envoy.expression;

class LengthExpression extends AbstractIntegerExpression<Long> {
    private final Expression operand;


    public LengthExpression(final Expression operand, final ExpressionFactory expressionFactory) {
        super(expressionFactory);
        this.operand = operand;
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitLength(operand);
    }


    @Override
    public Class<?> getType() {
        return Integer.class;
    }
}
