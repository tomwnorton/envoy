package org.envoy.expression;

class LowerCaseExpression extends AbstractUnaryStringExpression {
    LowerCaseExpression(final Expression operand, final ExpressionFactory expressionFactory) {
        super(operand, expressionFactory);
    }


    @Override
    protected void onAccept(final Expression operand, final ExpressionVisitor visitor) {
        visitor.visitLowerCase(operand);
    }
}
