package org.envoy.expression;

import java.beans.*;

public abstract class AbstractComponentExpression<T> extends AbstractBeanExpression<T> {
    private final CommonFieldExpressionLogic commonFieldExpressionLogic;


    public AbstractComponentExpression(ModelExpression parent, Class<?> entityClass) {
        super(entityClass);
        commonFieldExpressionLogic = new CommonFieldExpressionLogicImpl(parent, this);
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
    }


    @Override
    public Object getFromRootEntity(final Object root) {
        return commonFieldExpressionLogic.getFromRootEntity(root);
    }


    @Override
    public void setFromRootEntity(final Object root, final Object value) {
        commonFieldExpressionLogic.setFromRootEntity(root, value);
    }


    @Override
    public String getTableAlias() {
        return commonFieldExpressionLogic.getTableAlias();
    }


    @Override
    public EntityExpression<?> getEntityExpression() {
        return commonFieldExpressionLogic.getEntityExpression();
    }


    @Override
    public void setEntityExpression(final EntityExpression<?> entityExpression) {
        commonFieldExpressionLogic.setEntityExpression(entityExpression);
    }


    @Override
    public FieldAliasCache getFieldAliasCache() {
        return commonFieldExpressionLogic.getFieldAliasCache();
    }

    @Override
    public PropertyDescriptor getPropertyDescriptor() {
        return commonFieldExpressionLogic.getPropertyDescriptor();
    }
}
