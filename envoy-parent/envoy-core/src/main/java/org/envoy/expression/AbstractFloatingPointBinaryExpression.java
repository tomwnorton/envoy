package org.envoy.expression;

import java.math.*;
import java.util.*;

abstract class AbstractFloatingPointBinaryExpression extends AbstractFloatingPointExpression<BigDecimal> {
    protected final Expression lhs;
    protected final Expression rhs;


    public AbstractFloatingPointBinaryExpression(
            final Expression lhs,
            final Expression rhs,
            final ExpressionFactory expressionFactory) {
        super(expressionFactory);
        this.lhs = lhs;
        this.rhs = rhs;
    }


    @Override
    public Class<?> getType() {
        if (Arrays.asList(lhs.getType(), rhs.getType()).contains(BigDecimal.class)) {
            return BigDecimal.class;
        }
        if (Arrays.asList(lhs.getType(), rhs.getType()).contains(Double.class)) {
            return Double.class;
        }
        return Float.class;
    }
}
