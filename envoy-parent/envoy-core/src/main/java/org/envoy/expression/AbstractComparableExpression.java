package org.envoy.expression;

abstract class AbstractComparableExpression<CompareType, T extends CompareType, ExpressionType extends ComparableExpression<CompareType, T, ExpressionType>>
        extends
        AbstractObjectInstanceExpression<CompareType, T, ExpressionType> implements ComparableExpression<CompareType, T, ExpressionType> {
    AbstractComparableExpression(final ExpressionFactory expressionFactory) {
        super(expressionFactory);
    }


    @Override
    public BooleanExpression lessThan(final ExpressionType expression) {
        return getExpressionFactory().createLessThanExpression(this, expression);
    }


    @Override
    public BooleanExpression lessThan(final CompareType value) {
        return getExpressionFactory().createLessThanExpression(this, getExpressionFactory().createParameterExpression(value));
    }


    @Override
    public BooleanExpression lessThanEqual(final ExpressionType expression) {
        return getExpressionFactory().createLessThanEqualExpression(this, expression);
    }


    @Override
    public BooleanExpression lessThanEqual(final CompareType value) {
        return getExpressionFactory()
                .createLessThanEqualExpression(this, getExpressionFactory().createParameterExpression(value));
    }


    @Override
    public BooleanExpression greaterThan(final ExpressionType expression) {
        return getExpressionFactory().createGreaterThanExpression(this, expression);
    }


    @Override
    public BooleanExpression greaterThan(final CompareType value) {
        return getExpressionFactory().createGreaterThanExpression(
                this,
                getExpressionFactory().createParameterExpression(value));
    }


    @Override
    public BooleanExpression greaterThanEqual(final ExpressionType expression) {
        return getExpressionFactory().createGreaterThanEqualExpression(this, expression);
    }


    @Override
    public BooleanExpression greaterThanEqual(final CompareType value) {
        return getExpressionFactory().createGreaterThanEqualExpression(
                this,
                getExpressionFactory().createParameterExpression(value));
    }


    @Override
    public BooleanExpression between(final ExpressionType lowerBound, final ExpressionType upperBound) {
        return getExpressionFactory().createBetweenExpression(this, lowerBound, upperBound);
    }


    @Override
    public BooleanExpression between(final CompareType lowerBound, final CompareType upperBound) {
        return getExpressionFactory().createBetweenExpression(
                this,
                getExpressionFactory().createParameterExpression(lowerBound),
                getExpressionFactory().createParameterExpression(upperBound));
    }
}
