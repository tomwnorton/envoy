package org.envoy.expression;

public interface GenericFieldExpression<T> extends GenericModelExpression<T>, FieldExpression {
}
