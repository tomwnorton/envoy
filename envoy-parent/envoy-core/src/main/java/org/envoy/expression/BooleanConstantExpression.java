package org.envoy.expression;

public class BooleanConstantExpression implements GenericExpression<Boolean> {
    private final boolean value;


    public BooleanConstantExpression(final boolean value) {
        this.value = value;
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        if (value) {
            visitor.visitTrue();
        } else {
            visitor.visitFalse();
        }
    }


    @Override
    public Class<?> getType() {
        return Boolean.class;
    }
}
