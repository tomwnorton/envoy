package org.envoy.expression;

abstract class AbstractBinaryStringExpression extends AbstractStringExpression {
    private final Expression lhs;
    private final Expression rhs;


    protected abstract void onAccept(Expression lhs, Expression rhs, ExpressionVisitor visitor);


    public AbstractBinaryStringExpression(
            final Expression lhs,
            final Expression rhs,
            final ExpressionFactory expressionFactory) {
        super(expressionFactory);
        this.lhs = lhs;
        this.rhs = rhs;
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        this.onAccept(lhs, rhs, visitor);
    }
}
