package org.envoy.expression;

import org.apache.commons.lang3.*;

import java.beans.*;

/**
 * Represents a database table bit field.
 *
 * @author thomas
 */
public class BooleanFieldExpression extends AbstractBooleanExpression implements GenericFieldExpression<Boolean> {
    private final CommonFieldExpressionLogic commonFieldExpressionLogic;


    public BooleanFieldExpression(final ModelExpression parent) {
        this(parent, null);
    }


    public BooleanFieldExpression(final ModelExpression parent, final String fieldName) {
        super(ExpressionFactoryImpl.get());
        commonFieldExpressionLogic = new CommonFieldExpressionLogicImpl(parent, this, fieldName);
    }


    BooleanFieldExpression(
            final ExpressionFactory expressionFactory,
            final CommonFieldExpressionLogic commonFieldExpressionLogic) {
        super(expressionFactory);
        this.commonFieldExpressionLogic = commonFieldExpressionLogic;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof BooleanFieldExpression)) {
            return false;
        }

        BooleanFieldExpression other = (BooleanFieldExpression) obj;
        return ObjectUtils.equals(
                commonFieldExpressionLogic.getEntityExpression(),
                other.commonFieldExpressionLogic.getEntityExpression())
               && ObjectUtils.equals(
                commonFieldExpressionLogic.getFieldName(),
                other.commonFieldExpressionLogic.getFieldName());
    }

    @Override
    public String toString() {
        return ObjectUtils.toString(commonFieldExpressionLogic.getEntityExpression()) + "."
               + commonFieldExpressionLogic.getFieldName();
    }

    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitField(this);
    }


    @Override
    public Object getFromRootEntity(final Object root) {
        return commonFieldExpressionLogic.getFromRootEntity(root);
    }


    @Override
    public void setFromRootEntity(final Object root, final Object value) {
        commonFieldExpressionLogic.setFromRootEntity(root, value);
    }


    @Override
    public Class<Boolean> getType() {
        return Boolean.class;
    }


    @Override
    public String getFieldName() {
        return commonFieldExpressionLogic.getFieldName();
    }


    @Override
    public void setFieldName(final String fieldName) {
        commonFieldExpressionLogic.setFieldName(fieldName);
    }


    @Override
    public String getTableAlias() {
        return commonFieldExpressionLogic.getTableAlias();
    }


    @Override
    public EntityExpression<?> getEntityExpression() {
        return commonFieldExpressionLogic.getEntityExpression();
    }


    @Override
    public void setEntityExpression(final EntityExpression<?> entityExpression) {
        commonFieldExpressionLogic.setEntityExpression(entityExpression);
    }


    @Override
    public FieldAliasCache getFieldAliasCache() {
        return commonFieldExpressionLogic.getFieldAliasCache();
    }

    @Override
    public PropertyDescriptor getPropertyDescriptor() {
        return commonFieldExpressionLogic.getPropertyDescriptor();
    }

    @Override
    public boolean isPrimaryKey() {
        return commonFieldExpressionLogic.isPrimaryKey();
    }

    public BooleanFieldExpression primaryKey() {
        commonFieldExpressionLogic.setPrimaryKey(true);
        return this;
    }

    @Override
    public boolean isAutoIncrement() {
        return false;
    }
}
