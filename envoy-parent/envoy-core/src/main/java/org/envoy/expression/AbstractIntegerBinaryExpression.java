package org.envoy.expression;

import java.util.*;

abstract class AbstractIntegerBinaryExpression extends AbstractIntegerExpression<Long> {
    protected final Expression lhs;
    protected final Expression rhs;


    public AbstractIntegerBinaryExpression(
            final Expression lhs,
            final Expression rhs,
            final ExpressionFactory expressionFactory) {
        super(expressionFactory);
        this.lhs = lhs;
        this.rhs = rhs;
    }


    @Override
    public Class<?> getType() {
        if (Arrays.asList(lhs.getType(), rhs.getType()).contains(Long.class)) {
            return Long.class;
        }
        if (Arrays.asList(lhs.getType(), rhs.getType()).contains(Integer.class)) {
            return Integer.class;
        }
        if (Arrays.asList(lhs.getType(), rhs.getType()).contains(Short.class)) {
            return Short.class;
        }
        return Byte.class;
    }
}
