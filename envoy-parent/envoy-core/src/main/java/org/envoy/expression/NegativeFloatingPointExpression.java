package org.envoy.expression;

class NegativeFloatingPointExpression<T extends Number> extends AbstractFloatingPointUnaryExpression<T> {
    public NegativeFloatingPointExpression(final Expression operand, final ExpressionFactory expressionFactory) {
        super(operand, expressionFactory);
    }


    @Override
    protected void accept(final ExpressionVisitor visitor, final Expression operand) {
        visitor.visitNegative(operand);
    }
}
