package org.envoy.expression;

import org.envoy.exceptions.*;

import java.util.*;

public class ExpressionFactoryImpl implements ExpressionFactory {
    private static final ExpressionFactoryImpl instance = new ExpressionFactoryImpl();


    public static ExpressionFactoryImpl get() {
        return ExpressionFactoryImpl.instance;
    }


    @Override
    public EqualToExpressionImpl createEqualsExpression(final Expression lhs, final Expression rhs) {
        return new EqualToExpressionImpl(lhs, rhs, this);
    }


    @Override
    public NotEqualToExpression createNotEqualExpression(final Expression lhs, final Expression rhs) {
        return new NotEqualToExpression(lhs, rhs, this);
    }


    @Override
    public AndExpression createAndExpression(final BooleanExpression lhs, final BooleanExpression rhs) {
        return new AndExpression(lhs, rhs, this);
    }


    @Override
    public OrExpression createOrExpression(final BooleanExpression lhs, final BooleanExpression rhs) {
        return new OrExpression(lhs, rhs, this);
    }


    @Override
    public NotExpression createNotExpression(final BooleanExpression expr) {
        return new NotExpression(expr, this);
    }


    @Override
    public <T> InExpression<T> createInExpression(final Expression expr, final Collection<T> collection) {
        return new InExpression<T>(expr, collection, this);
    }


    @Override
    public BooleanConstantExpression createBooleanConstantExpression(final boolean value) {
        return new BooleanConstantExpression(value);
    }


    @Override
    public <EntityType, EntityExpressionType extends EntityExpression<EntityType>> EntityExpressionType createEntityExpression(
            final Class<? extends EntityExpressionType> clazz) {
        try {
            return clazz.newInstance();
        } catch (final InstantiationException e) {
            throw new EnvoyException("Could not create object of type " + clazz.getName(), e);
        } catch (final IllegalAccessException e) {
            throw new EnvoyException("Could not create object of type " + clazz.getName(), e);
        }
    }


    @Override
    public <EntityType> ParameterExpression<EntityType> createParameterExpression(final EntityType value) {
        return new ParameterExpression<EntityType>(value);
    }


    @Override
    public LessThanExpression createLessThanExpression(final Expression lhs, final Expression rhs) {
        return new LessThanExpression(lhs, rhs, this);
    }


    @Override
    public LessThanEqualExpression createLessThanEqualExpression(final Expression lhs, final Expression rhs) {
        return new LessThanEqualExpression(lhs, rhs, this);
    }


    @Override
    public GreaterThanExpression createGreaterThanExpression(final Expression lhs, final Expression rhs) {
        return new GreaterThanExpression(lhs, rhs, this);
    }


    @Override
    public GreaterThanEqualExpression createGreaterThanEqualExpression(final Expression lhs, final Expression rhs) {
        return new GreaterThanEqualExpression(lhs, rhs, this);
    }


    @Override
    public ConvertToStringExpression createConvertToStringExpression(final Expression expr) {
        return new ConvertToStringExpression(expr, this);
    }


    @Override
    public PlusIntegerExpression createPlusIntegerExpression(final Expression lhs, final Expression rhs) {
        return new PlusIntegerExpression(lhs, rhs, this);
    }


    @Override
    public PlusFloatingPointExpression createPlusFloatingPointExpression(final Expression lhs, final Expression rhs) {
        return new PlusFloatingPointExpression(lhs, rhs, this);
    }


    @Override
    public MinusIntegerExpression createMinusIntegerExpression(final Expression lhs, final Expression rhs) {
        return new MinusIntegerExpression(lhs, rhs, this);
    }


    @Override
    public MinusFloatingPointExpression createMinusFloatingPointExpression(final Expression lhs, final Expression rhs) {
        return new MinusFloatingPointExpression(lhs, rhs, this);
    }


    @Override
    public TimesIntegerExpression createTimesIntegerExpression(final Expression lhs, final Expression rhs) {
        return new TimesIntegerExpression(lhs, rhs, this);
    }


    @Override
    public TimesFloatingPointExpression createTimesFloatingPointExpression(final Expression lhs, final Expression rhs) {
        return new TimesFloatingPointExpression(lhs, rhs, this);
    }


    @Override
    public DividedByIntegerExpression createDividedByIntegerExpression(final Expression lhs, final Expression rhs) {
        return new DividedByIntegerExpression(lhs, rhs, this);
    }


    @Override
    public DividedByFloatingPointExpression createDividedByFloatingPointExpression(final Expression lhs, final Expression rhs) {
        return new DividedByFloatingPointExpression(lhs, rhs, this);
    }


    @Override
    public ModulusIntegerExpression createModulusIntegerExpression(final Expression lhs, final Expression rhs) {
        return new ModulusIntegerExpression(lhs, rhs, this);
    }


    @Override
    public CoalesceBooleanExpression createCoalesceBooleanExpression(final List<GenericExpression<Boolean>> exprs) {
        return new CoalesceBooleanExpression(exprs, this);
    }


    @Override
    public <T extends Number> CoalesceIntegerExpression<T> createCoalesceIntegerExpression(final List<GenericExpression<T>> exprs) {
        return new CoalesceIntegerExpression<T>(exprs, this);
    }


    @Override
    public <T extends Number> CoalesceFloatingPointExpression<T> createCoalesceFloatingPointExpression(
            final List<GenericExpression<T>> exprs) {
        return new CoalesceFloatingPointExpression<T>(exprs, this);
    }


    @Override
    public <T extends Number> NegativeIntegerExpression<T> createNegativeIntegerExpression(final Expression operand) {
        return new NegativeIntegerExpression<T>(operand, this);
    }


    @Override
    public <T extends Number> NegativeFloatingPointExpression<T> createNegativeFloatingPointExpression(final Expression operand) {
        return new NegativeFloatingPointExpression<T>(operand, this);
    }


    @Override
    public BetweenExpression createBetweenExpression(
            final Expression lhs,
            final Expression lowerBound,
            final Expression upperBound) {
        return new BetweenExpression(lhs, lowerBound, upperBound, this);
    }


    @Override
    public CoalesceStringExpression createCoalesceStringExpression(final List<GenericExpression<String>> exprs) {
        return new CoalesceStringExpression(exprs, this);
    }


    @Override
    public TrimExpression createTrimExpression(final Expression expr) {
        return new TrimExpression(expr, this);
    }


    @Override
    public LengthExpression createLengthExpression(final Expression expr) {
        return new LengthExpression(expr, this);
    }


    @Override
    public LowerCaseExpression createLowerCaseExpression(final Expression expr) {
        return new LowerCaseExpression(expr, this);
    }


    @Override
    public UpperCaseExpression createUpperCaseExpression(final Expression expr) {
        return new UpperCaseExpression(expr, this);
    }


    @Override
    public ConvertStringToIntegerExpression createConvertStringToIntegerExpression(final Expression expr) {
        return new ConvertStringToIntegerExpression(expr, this);
    }


    @Override
    public ConcatExpression createConcatExpression(final Expression lhs, final Expression rhs) {
        return new ConcatExpression(lhs, rhs, this);
    }


    @Override
    public ContainsExpression createContainsExpression(final Expression source, final Expression pattern) {
        return new ContainsExpression(source, pattern, this);
    }


    @Override
    public StartsWithExpression createStartsWithExpression(final Expression source, final Expression pattern) {
        return new StartsWithExpression(source, pattern, this);
    }


    @Override
    public EndsWithExpression createEndsWithExpression(final Expression source, final Expression pattern) {
        return new EndsWithExpression(source, pattern, this);
    }


    @Override
    public CoalesceCalendarExpression createCoalesceCalendarExpression(final List<GenericExpression<Calendar>> exprs) {
        return new CoalesceCalendarExpression(exprs, this);
    }


    @Override
    public SelectListExpression createSelectListExpression(final List<? extends Expression> exprs) {
        return new SelectListExpression(exprs);
    }


    @Override
    public <T> SelectExpression<T> createSelectExpression(
            final SelectListExpression selectListExpression,
            final EntityExpression<T> fromExpression,
            final BooleanExpression whereExpression) {
        return new SelectExpression<>(selectListExpression, fromExpression, whereExpression);
    }
}
