package org.envoy.expression;

class OrExpression extends AbstractBooleanBinaryExpression {
    public OrExpression(final BooleanExpression lhs, final BooleanExpression rhs, final ExpressionFactory expressionFactory) {
        super(lhs, rhs, expressionFactory);
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        Expression left = lhs;
        Expression right = rhs;

        if (lhs instanceof BooleanFieldExpression) {
            left =
                    getExpressionFactory().createEqualsExpression(
                            lhs,
                            getExpressionFactory().createBooleanConstantExpression(true));
        }

        if (rhs instanceof BooleanFieldExpression) {
            right =
                    getExpressionFactory().createEqualsExpression(
                            rhs,
                            getExpressionFactory().createBooleanConstantExpression(true));
        }

        visitor.visitOr(left, right);
    }
}
