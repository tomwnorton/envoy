package org.envoy.expression;

import java.util.*;

class CoalesceCalendarExpression extends AbstractCalendarExpression {
    private final List<? extends Expression> exprs;


    public CoalesceCalendarExpression(final List<? extends Expression> exprs, final ExpressionFactory expressionFactory) {
        super(expressionFactory);
        this.exprs = exprs;
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitCoalesce(exprs);
    }
}
