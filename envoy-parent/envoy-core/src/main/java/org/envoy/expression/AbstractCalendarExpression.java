package org.envoy.expression;

import java.util.*;

public abstract class AbstractCalendarExpression extends AbstractComparableExpression<Calendar, Calendar, CalendarExpression>
        implements
        CalendarExpression {
    AbstractCalendarExpression(final ExpressionFactory expressionFactory) {
        super(expressionFactory);
    }


    @Override
    protected final CalendarExpression coalesceInternal(final List<GenericExpression<Calendar>> exprs) {
        return getExpressionFactory().createCoalesceCalendarExpression(exprs);
    }


    @Override
    public Class<?> getType() {
        return Calendar.class;
    }
}
