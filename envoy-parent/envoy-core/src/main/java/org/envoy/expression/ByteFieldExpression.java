package org.envoy.expression;

public class ByteFieldExpression extends AbstractIntegerFieldExpression<Byte, ByteFieldExpression> {
    public ByteFieldExpression(final ModelExpression parent) {
        super(parent);
    }


    public ByteFieldExpression(final ModelExpression parent, final String fieldName) {
        super(parent, fieldName);
    }


    @Override
    public Class<Byte> getType() {
        return Byte.class;
    }
}
