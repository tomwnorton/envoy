package org.envoy.expression;

import org.envoy.exceptions.*;

import java.beans.*;
import java.lang.reflect.*;
import java.util.*;

/**
 * Created by thomas on 7/20/14.
 */
public abstract class AbstractBeanExpression<T> implements GenericBeanExpression<T> {
    private Class<?> entityType;


    public AbstractBeanExpression(Class<?> entityType) {
        this.entityType = entityType;
    }


    @Override
    public Class<?> getType() {
        return entityType;
    }


    @Override
    public List<FieldExpression> getFieldExpressions() {
        List<FieldExpression> fieldExpressions = new ArrayList<>();
        for (ModelExpression modelExpression : getFlattenedModelExpressions()) {
            if (modelExpression instanceof FieldExpression) {
                fieldExpressions.add((FieldExpression) modelExpression);
            }
        }
        return fieldExpressions;
    }


    @Override
    public List<ModelExpression> getFlattenedModelExpressions() {
        try {
            List<ModelExpression> fieldExpressions = new ArrayList<>();
            for (MethodDescriptor methodDescriptor : Introspector.getBeanInfo(this.getClass()).getMethodDescriptors()) {
                if (methodDescriptor.getMethod().getParameterTypes().length == 0) {
                    if (FieldExpression.class.isAssignableFrom(methodDescriptor.getMethod().getReturnType())) {
                        FieldExpression fieldExpression = (FieldExpression) methodDescriptor.getMethod().invoke(this);
                        fieldExpressions.add(fieldExpression);
                    }

                    if (BeanExpression.class.isAssignableFrom(methodDescriptor.getMethod().getReturnType())
                        && !methodDescriptor.getName().equals("getEntityExpression")) {
                        BeanExpression beanExpression = (BeanExpression) methodDescriptor.getMethod().invoke(this);
                        fieldExpressions.add(beanExpression);
                        fieldExpressions.addAll(beanExpression.getFlattenedModelExpressions());
                    }
                }
            }
            return fieldExpressions;
        } catch (IntrospectionException | InvocationTargetException | IllegalAccessException e) {
            throw new EnvoyReflectionException(e);
        }
    }
}
