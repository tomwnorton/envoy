package org.envoy.expression;

class ConvertToStringExpression extends AbstractStringExpression {
    private final Expression expression;


    public ConvertToStringExpression(final Expression expression, final ExpressionFactory expressionFactory) {
        super(expressionFactory);
        this.expression = expression;
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitConvertToString(expression);
    }
}
