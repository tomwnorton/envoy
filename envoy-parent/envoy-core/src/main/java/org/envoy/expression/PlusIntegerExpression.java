package org.envoy.expression;

class PlusIntegerExpression extends AbstractIntegerBinaryExpression {
    public PlusIntegerExpression(final Expression lhs, final Expression rhs, final ExpressionFactory expressionFactory) {
        super(lhs, rhs, expressionFactory);
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitPlus(lhs, rhs);
    }
}
