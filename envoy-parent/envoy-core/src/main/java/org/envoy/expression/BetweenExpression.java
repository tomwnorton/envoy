package org.envoy.expression;

/**
 * Expression to determine if lhs is a value between lowerBound and upperBound.
 *
 * @author thomas
 */
class BetweenExpression extends AbstractBooleanExpression {
    private final Expression lhs;
    private final Expression lowerBound;
    private final Expression upperBound;


    public BetweenExpression(
            final Expression lhs,
            final Expression lowerBound,
            final Expression upperBound,
            final ExpressionFactory expressionFactory) {
        super(expressionFactory);
        this.lhs = lhs;
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitBetween(lhs, lowerBound, upperBound);
    }
}
