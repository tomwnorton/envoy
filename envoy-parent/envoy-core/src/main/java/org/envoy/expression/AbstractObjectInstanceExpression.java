package org.envoy.expression;

import java.util.*;

abstract class AbstractObjectInstanceExpression<CompareType, T extends CompareType, ExpressionType extends ObjectInstanceExpression<CompareType, T, ExpressionType>>
        extends
        AbstractExpression<T> implements ObjectInstanceExpression<CompareType, T, ExpressionType> {
    AbstractObjectInstanceExpression(final ExpressionFactory expressionFactory) {
        super(expressionFactory);
    }


    @Override
    public BooleanExpression equalTo(final ExpressionType rhs) {
        return this.getExpressionFactory().createEqualsExpression(this, rhs);
    }


    @Override
    public BooleanExpression equalTo(final T value) {
        return getExpressionFactory().createEqualsExpression(
                this,
                getExpressionFactory().createParameterExpression(value));
    }


    @Override
    public BooleanExpression notEqualTo(final ExpressionType rhs) {
        return getExpressionFactory().createNotEqualExpression(this, rhs);
    }


    @Override
    public BooleanExpression notEqualTo(final T value) {
        return getExpressionFactory().createNotEqualExpression(
                this,
                getExpressionFactory().createParameterExpression(value));
    }


    @Override
    public ExpressionType coalesce(final T value) {
        return coalesceInternal(
                Arrays.<GenericExpression<T>>asList(
                        getExpressionFactory().createParameterExpression(
                                value)));
    }


    @Override
    public ExpressionType coalesce(final List<GenericExpression<T>> exprs) {
        return coalesceInternal(exprs);
    }


    @SuppressWarnings("unchecked")
    @Override
    public ExpressionType coalesce(final GenericExpression<T>... exprs) {
        return coalesceInternal(Arrays.<GenericExpression<T>>asList(exprs));
    }


    @SuppressWarnings("unchecked")
    @Override
    public BooleanExpression in(final T... exprs) {
        return getExpressionFactory().createInExpression(this, Arrays.asList(exprs));
    }


    protected abstract ExpressionType coalesceInternal(final List<GenericExpression<T>> exprs);
}