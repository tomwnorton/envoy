package org.envoy.expression;

class ConvertStringToIntegerExpression extends AbstractIntegerExpression<Long> {
    private final Expression operand;


    public ConvertStringToIntegerExpression(final Expression operand, final ExpressionFactory expressionFactory) {
        super(expressionFactory);
        this.operand = operand;
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitConvertStringToInteger(operand);
    }


    @Override
    public Class<?> getType() {
        return Long.class;
    }
}
