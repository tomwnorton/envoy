package org.envoy.expression;

import org.envoy.query.*;

import java.util.*;

public interface ExpressionVisitor {
    public void visitEquals(final Expression lhs, final Expression rhs);


    public void visitParameter(final Object value);


    public void visitNotEquals(final Expression lhs, final Expression rhs);


    public void visitAnd(final Expression lhs, final Expression rhs);


    public void visitOr(final Expression lhs, final Expression rhs);


    public void visitNot(final Expression expr);


    public <T> void visitIn(final Expression expr, final Collection<T> collection);


    public void visitField(final FieldExpression fieldExpr);


    public <T> void visitNull(final Expression expr);


    public <T> void visitNotNull(final Expression expr);


    public void visitTrue();


    public void visitFalse();


    public void visitLessThan(final Expression lhs, final Expression rhs);


    public void visitLessThanEqual(final Expression lhs, final Expression rhs);


    public void visitGreaterThan(final Expression lhs, final Expression rhs);


    public void visitGreaterThanEqual(final Expression lhs, final Expression rhs);


    public void visitBetween(Expression lhs, Expression lowerBound, Expression upperBound);


    public void visitConvertToString(final Expression expression);


    public void visitPlus(final Expression lhs, final Expression rhs);


    public void visitMinus(final Expression lhs, final Expression rhs);


    public void visitTimes(final Expression lhs, final Expression rhs);


    public void visitDividedBy(final Expression lhs, final Expression rhs);


    public void visitModulus(final Expression lhs, final Expression rhs);


    public void visitCoalesce(final List<? extends Expression> exprs);


    public void visitNegative(final Expression expr);


    public void visitTrim(final Expression expr);


    public void visitLength(final Expression expr);


    public void visitLowerCase(final Expression expr);


    public void visitUpperCase(final Expression expr);


    public void visitConvertStringToInteger(final Expression expr);


    public void visitConcat(Expression lhs, Expression rhs);


    /**
     * Generates the appropriate query criteria that acts just like the Java
     * code:
     * <p/>
     * <code>source.contains(pattern)</code>
     * <p/>
     * If The SQL database can only implement this feature via the LIKE clause,
     * then don't forget to escape the special characters in non-parameters, as
     * in this example:
     * <p/>
     * {@code source LIKE REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(pattern, '\', '\\'), '%', '\%'), '_', '\_'), '[', '\['), ']', '\]') ESCAPE '\'}
     *
     * @param source  The string expression through which to search
     * @param pattern The string expression to find
     */
    public void visitContains(Expression source, Expression pattern);


    /**
     * Generates the appropriate query criteria that acts just like the Java
     * code:
     * <p/>
     * <code>source.startsWith(pattern)</code>
     * <p/>
     * If The SQL database can only implement this feature via the LIKE clause,
     * then don't forget to escape the special characters in non-parameters, as
     * in this example:
     * <p/>
     * {@code source LIKE REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(pattern, '\', '\\'), '%', '\%'), '_', '\_'), '[', '\['), ']', '\]') ESCAPE '\'}
     *
     * @param source  The string expression through which to search
     * @param pattern The string expression with which {@code source} is expected to
     *                start.
     */
    public void visitStartsWith(Expression source, Expression pattern);


    /**
     * Generates the appropriate query criteria that acts just like the Java
     * code:
     * <p/>
     * <code>source.endsWith(pattern)</code>
     * <p/>
     * If The SQL database can only implement this feature via the LIKE clause,
     * then don't forget to escape the special characters in non-parameters, as
     * in this example:
     * <p/>
     * {@code source LIKE REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(pattern, '\', '\\'), '%', '\%'), '_', '\_'), '[', '\['), ']', '\]') ESCAPE '\'}
     *
     * @param source  The string expression through which to search
     * @param pattern The string expression with which {@code source} is expected to
     *                start.
     */
    public void visitEndsWith(Expression source, Expression pattern);


    public void visitSelectList(List<? extends Expression> expressions);


    public void visitEntity(EntityExpression<?> expr);


    public void visitSelect(SelectListExpression selectListExpr, EntityExpression<?> fromExpr, BooleanExpression whereExpr);


    public void visitCount(Expression expr);

    public void visitInsert(InsertModel insertModel);

    public void visitUpdate(UpdateModel updateModel);
}
