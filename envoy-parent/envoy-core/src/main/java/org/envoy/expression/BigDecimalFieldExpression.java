package org.envoy.expression;

import java.math.*;

public class BigDecimalFieldExpression extends AbstractFloatingPointFieldExpression<BigDecimal, BigDecimalFieldExpression> {
    public BigDecimalFieldExpression(final ModelExpression parent) {
        super(parent);
    }


    public BigDecimalFieldExpression(final ModelExpression parent, final String fieldName) {
        super(parent, fieldName);
    }


    @Override
    public Class<BigDecimal> getType() {
        return BigDecimal.class;
    }
}
