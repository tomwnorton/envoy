package org.envoy.expression;

import java.math.*;

public interface FloatingPointExpression<T extends Number> extends NumericExpression<T> {
    FloatingPointExpression<BigDecimal> plus(NumericExpression<?> expr);


    FloatingPointExpression<BigDecimal> minus(NumericExpression<?> expr);


    FloatingPointExpression<BigDecimal> times(NumericExpression<?> expr);


    FloatingPointExpression<BigDecimal> dividedBy(NumericExpression<?> expr);


    FloatingPointExpression<T> negative();
}
