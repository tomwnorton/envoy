package org.envoy.expression;

import java.util.*;

public abstract class AbstractSqlExpressionVisitor implements SqlExpressionVisitor {
    private StringBuilder query;
    private List<Object> parameters;
    private StringBuilder groupBy;
    private FieldAliasCache fieldAliasCache;


    @Override
    public StringBuilder getQuery() {
        return query;
    }


    @Override
    public void setQuery(final StringBuilder query) {
        this.query = query;
    }


    @Override
    public List<Object> getParameters() {
        return parameters;
    }


    @Override
    public void setParameters(final List<Object> parameters) {
        this.parameters = parameters;
    }

    @Override
    public FieldAliasCache getFieldAliasCache() {
        return fieldAliasCache;
    }

    @Override
    public void setFieldAliasCache(FieldAliasCache fieldAliasCache) {
        this.fieldAliasCache = fieldAliasCache;
    }

    protected StringBuilder getGroupBy() {
        return groupBy;
    }


    protected void setGroupBy(final StringBuilder groupBy) {
        this.groupBy = groupBy;
    }
}
