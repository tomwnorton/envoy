package org.envoy.expression;

class MinusFloatingPointExpression extends AbstractFloatingPointBinaryExpression {
    public MinusFloatingPointExpression(final Expression lhs, final Expression rhs, final ExpressionFactory expressionFactory) {
        super(lhs, rhs, expressionFactory);
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitMinus(lhs, rhs);
    }
}
