package org.envoy.expression;

import org.apache.commons.lang3.*;
import org.envoy.exceptions.*;

import java.beans.*;
import java.util.*;

public abstract class AbstractEntityExpression<Entity> extends AbstractBeanExpression<Entity> implements EntityExpression<Entity> {
    private FieldAliasCache fieldAliasCache = new FieldAliasCacheImpl();
    private String tableName;
    private String schemaName;


    public AbstractEntityExpression(final Class<? extends Entity> entityClass) {
        super(entityClass);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !this.getClass().equals(obj.getClass())) {
            return false;
        }

        AbstractEntityExpression<Entity> rhs = (AbstractEntityExpression<Entity>) obj;
        return getEntityClass().equals(rhs.getEntityClass())
               && ObjectUtils.equals(schemaName, rhs.schemaName)
               && ObjectUtils.equals(tableName, rhs.tableName);
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (StringUtils.isNotBlank(schemaName)) {
            sb.append(schemaName);
        }
        sb.append(tableName);
        return sb.toString();
    }

    @Override
    @SuppressWarnings("unchecked")
    public Class<? extends Entity> getEntityClass() {
        return (Class<? extends Entity>) this.getType();
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitEntity(this);
    }


    @Override
    @SuppressWarnings("unchecked")
    public Object getFromRootEntity(final Object root) {
        return root;
    }


    @Override
    public void setFromRootEntity(final Object root, final Object value) {
        throw new EnvoyException("Cannot overwrite root object.");
    }


    @Override
    public String getTableAlias() {
        return this.fieldAliasCache.getFieldAlias(this);
    }


    @Override
    public String getTableName() {
        return tableName;
    }


    @Override
    public void setTableName(final String tableName) {
        this.tableName = tableName;
    }


    @Override
    public EntityExpression<?> getEntityExpression() {
        return this;
    }


    @Override
    public void setEntityExpression(final EntityExpression<?> entityExpression) {
    }


    @Override
    public FieldAliasCache getFieldAliasCache() {
        return fieldAliasCache;
    }


    @Override
    public void setFieldAliasCache(final FieldAliasCache fieldAliasCache) {
        this.fieldAliasCache = fieldAliasCache;
    }


    @Override
    public String getSchemaName() {
        return schemaName;
    }


    @Override
    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    @Override
    public PropertyDescriptor getPropertyDescriptor() {
        throw new UnsupportedOperationException();
    }

    public List<FieldExpression> getPrimaryKeys() {
        List<FieldExpression> fieldExpressions = new ArrayList<>();
        for (FieldExpression fieldExpression : getFieldExpressions()) {
            if (fieldExpression.isPrimaryKey()) {
                fieldExpressions.add(fieldExpression);
            }
        }
        return fieldExpressions;
    }
}
