package org.envoy.expression;

abstract class AbstractBooleanBinaryExpression extends AbstractBooleanExpression {
    protected final Expression lhs;
    protected final Expression rhs;


    public AbstractBooleanBinaryExpression(
            final Expression lhs,
            final Expression rhs,
            final ExpressionFactory expressionFactory) {
        super(expressionFactory);
        this.lhs = lhs;
        this.rhs = rhs;
    }
}
