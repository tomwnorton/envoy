package org.envoy.expression;

class NotEqualToExpression extends AbstractBooleanBinaryExpression {
    public NotEqualToExpression(final Expression lhs, final Expression rhs, final ExpressionFactory expressionFactory) {
        super(lhs, rhs, expressionFactory);
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        if (rhs == null) {
            visitor.visitNotNull(lhs);
        } else {
            visitor.visitNotEquals(lhs, rhs);
        }
    }
}
