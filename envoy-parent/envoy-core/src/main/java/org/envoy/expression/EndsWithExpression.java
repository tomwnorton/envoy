package org.envoy.expression;

class EndsWithExpression extends AbstractBinaryStringExpression {
    public EndsWithExpression(final Expression source, final Expression pattern, final ExpressionFactory expressionFactory) {
        super(source, pattern, expressionFactory);
    }


    @Override
    protected void onAccept(final Expression source, final Expression pattern, final ExpressionVisitor visitor) {
        visitor.visitEndsWith(source, pattern);
    }
}
