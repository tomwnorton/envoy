package org.envoy.expression;

public interface BooleanExpression extends GenericExpression<Boolean> {
    BooleanExpression and(BooleanExpression expression);


    BooleanExpression or(BooleanExpression expression);


    BooleanExpression not();
}
