package org.envoy.expression;

abstract class AbstractExpression<T> implements GenericExpression<T> {
    private final ExpressionFactory expressionFactory;


    AbstractExpression(final ExpressionFactory expressionFactory) {
        this.expressionFactory = expressionFactory;
    }


    final ExpressionFactory getExpressionFactory() {
        return expressionFactory;
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && getClass().equals(obj.getClass());
    }
}
