package org.envoy.expression;

class TrimExpression extends AbstractUnaryStringExpression {
    TrimExpression(final Expression expr, final ExpressionFactory expressionFactory) {
        super(expr, expressionFactory);
    }


    @Override
    protected void onAccept(final Expression operand, final ExpressionVisitor visitor) {
        visitor.visitTrim(operand);
    }
}
