package org.envoy.expression;

public interface NumericExpression<T extends Number> extends ComparableExpression<Number, T, NumericExpression<T>> {
    public StringExpression convertToString();
}
