package org.envoy.expression;

class NegativeIntegerExpression<T extends Number> extends AbstractIntegerExpression<T> {
    private final Expression expr;


    public NegativeIntegerExpression(final Expression expr, final ExpressionFactory expressionFactory) {
        super(expressionFactory);
        this.expr = expr;
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitNegative(expr);
    }


    @Override
    public Class<?> getType() {
        return expr.getType();
    }
}
