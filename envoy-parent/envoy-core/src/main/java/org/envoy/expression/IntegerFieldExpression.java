package org.envoy.expression;

public class IntegerFieldExpression extends AbstractIntegerFieldExpression<Integer, IntegerFieldExpression> {
    public IntegerFieldExpression(final ModelExpression parent) {
        super(parent);
    }


    public IntegerFieldExpression(final ModelExpression parent, final String fieldName) {
        super(parent, fieldName);
    }


    @Override
    public Class<Integer> getType() {
        return Integer.class;
    }
}
