package org.envoy.expression;

import java.util.*;

public interface SqlExpressionVisitor extends ExpressionVisitor {
    public StringBuilder getQuery();


    public void setQuery(StringBuilder query);


    public List<Object> getParameters();


    public void setParameters(List<Object> parameters);


    public FieldAliasCache getFieldAliasCache();


    public void setFieldAliasCache(FieldAliasCache fieldAliasCache);
}
