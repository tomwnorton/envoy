package org.envoy.expression;

import java.util.*;

/**
 * Factory methods for envoy expressions. This class is used by envoy's api.
 * Please think twice before using it directly.
 * <p/>
 * This class only contains methods that perform constructor calls. No other
 * initialization is done to the expressions by this class.
 *
 * @author thomas
 */
public interface ExpressionFactory {
    public EqualToExpressionImpl createEqualsExpression(final Expression lhs, final Expression rhs);


    public NotEqualToExpression createNotEqualExpression(final Expression lhs, final Expression rhs);


    public AndExpression createAndExpression(final BooleanExpression lhs, final BooleanExpression rhs);


    public OrExpression createOrExpression(final BooleanExpression lhs, final BooleanExpression rhs);


    public NotExpression createNotExpression(final BooleanExpression expr);


    public <T> InExpression<T> createInExpression(final Expression expr, final Collection<T> collection);


    public BooleanConstantExpression createBooleanConstantExpression(boolean value);


    public <EntityType, EntityExpressionType extends EntityExpression<EntityType>> EntityExpressionType createEntityExpression(
            final Class<? extends EntityExpressionType> clazz);


    public <EntityType> ParameterExpression<EntityType> createParameterExpression(final EntityType value);


    public LessThanExpression createLessThanExpression(final Expression lhs, final Expression rhs);


    public LessThanEqualExpression createLessThanEqualExpression(final Expression lhs, final Expression rhs);


    public GreaterThanExpression createGreaterThanExpression(final Expression lhs, final Expression rhs);


    public GreaterThanEqualExpression createGreaterThanEqualExpression(final Expression lhs, final Expression rhs);


    public ConvertToStringExpression createConvertToStringExpression(final Expression expr);


    public PlusIntegerExpression createPlusIntegerExpression(final Expression lhs, final Expression rhs);


    public PlusFloatingPointExpression createPlusFloatingPointExpression(final Expression lhs, final Expression rhs);


    public MinusIntegerExpression createMinusIntegerExpression(final Expression lhs, final Expression rhs);


    public MinusFloatingPointExpression createMinusFloatingPointExpression(final Expression lhs, final Expression rhs);


    public TimesIntegerExpression createTimesIntegerExpression(final Expression lhs, final Expression rhs);


    public TimesFloatingPointExpression createTimesFloatingPointExpression(final Expression lhs, final Expression rhs);


    public DividedByIntegerExpression createDividedByIntegerExpression(final Expression lhs, final Expression rhs);


    public DividedByFloatingPointExpression createDividedByFloatingPointExpression(final Expression lhs, final Expression rhs);


    public ModulusIntegerExpression createModulusIntegerExpression(final Expression lhs, final Expression rhs);


    public CoalesceBooleanExpression createCoalesceBooleanExpression(final List<GenericExpression<Boolean>> exprs);


    public <T extends Number> CoalesceIntegerExpression<T> createCoalesceIntegerExpression(final List<GenericExpression<T>> exprs);


    public <T extends Number> CoalesceFloatingPointExpression<T> createCoalesceFloatingPointExpression(
            final List<GenericExpression<T>> exprs);


    public <T extends Number> NegativeIntegerExpression<T> createNegativeIntegerExpression(final Expression operand);


    public <T extends Number> NegativeFloatingPointExpression<T> createNegativeFloatingPointExpression(final Expression operand);


    public BetweenExpression createBetweenExpression(Expression lhs, Expression lowerBound, Expression upperBound);


    public CoalesceStringExpression createCoalesceStringExpression(List<GenericExpression<String>> exprs);


    public TrimExpression createTrimExpression(Expression expr);


    public LengthExpression createLengthExpression(Expression expr);


    public LowerCaseExpression createLowerCaseExpression(Expression expr);


    public UpperCaseExpression createUpperCaseExpression(Expression expr);


    public ConvertStringToIntegerExpression createConvertStringToIntegerExpression(Expression expr);


    public ConcatExpression createConcatExpression(Expression lhs, Expression rhs);


    public ContainsExpression createContainsExpression(Expression source, Expression pattern);


    public StartsWithExpression createStartsWithExpression(Expression source, Expression pattern);


    public EndsWithExpression createEndsWithExpression(Expression source, Expression pattern);


    public CoalesceCalendarExpression createCoalesceCalendarExpression(List<GenericExpression<Calendar>> exprs);


    public SelectListExpression createSelectListExpression(List<? extends Expression> exprs);


    public <T> SelectExpression<T> createSelectExpression(
            SelectListExpression selectListExpression,
            EntityExpression<T> fromExpression,
            BooleanExpression whereExpression);
}
