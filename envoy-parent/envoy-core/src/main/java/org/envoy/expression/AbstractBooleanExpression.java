package org.envoy.expression;

import java.util.*;

abstract class AbstractBooleanExpression extends AbstractObjectInstanceExpression<Boolean, Boolean, AbstractBooleanExpression>
        implements
        BooleanExpression {
    public AbstractBooleanExpression(final ExpressionFactory expressionFactory) {
        super(expressionFactory);
    }


    @Override
    public final BooleanExpression and(final BooleanExpression expression) {
        return getExpressionFactory().createAndExpression(this, expression);
    }


    @Override
    public final BooleanExpression or(final BooleanExpression expression) {
        return getExpressionFactory().createOrExpression(this, expression);
    }


    @Override
    public final BooleanExpression not() {
        return getExpressionFactory().createNotExpression(this);
    }


    @Override
    protected AbstractBooleanExpression coalesceInternal(final List<GenericExpression<Boolean>> exprs) {
        final ArrayList<GenericExpression<Boolean>> allExprs = new ArrayList<GenericExpression<Boolean>>();
        allExprs.add(this);
        allExprs.addAll(exprs);

        return getExpressionFactory().createCoalesceBooleanExpression(allExprs);
    }


    @Override
    public Class<?> getType() {
        return Boolean.class;
    }
}
