package org.envoy.expression;

class DividedByIntegerExpression extends AbstractIntegerBinaryExpression {
    public DividedByIntegerExpression(final Expression lhs, final Expression rhs, final ExpressionFactory expressionFactory) {
        super(lhs, rhs, expressionFactory);
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitDividedBy(lhs, rhs);
    }
}
