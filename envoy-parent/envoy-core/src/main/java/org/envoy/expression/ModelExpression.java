package org.envoy.expression;

import java.beans.*;

/**
 * Created by thomas on 7/20/14.
 */
public interface ModelExpression extends Expression {
    public Object getFromRootEntity(Object root);


    public void setFromRootEntity(Object root, Object value);


    public String getTableAlias();


    public EntityExpression<?> getEntityExpression();


    public void setEntityExpression(EntityExpression<?> entityExpression);


    public FieldAliasCache getFieldAliasCache();

    public PropertyDescriptor getPropertyDescriptor();
}
