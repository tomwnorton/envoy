package org.envoy.expression;

import java.util.*;

public interface ObjectInstanceExpression<CompareType, T extends CompareType, ExpressionType extends ObjectInstanceExpression<CompareType, T, ExpressionType>>
        extends GenericExpression<T> {
    public BooleanExpression equalTo(ExpressionType expression);


    public BooleanExpression equalTo(T value);


    public BooleanExpression notEqualTo(ExpressionType expression);


    public BooleanExpression notEqualTo(T value);


    public ExpressionType coalesce(T value);


    public ExpressionType coalesce(List<GenericExpression<T>> exprs);


    @SuppressWarnings("unchecked")
    public ExpressionType coalesce(GenericExpression<T>... exprs);


    @SuppressWarnings("unchecked")
    public BooleanExpression in(T... exprs);
}
