package org.envoy.expression;

import java.math.*;

public interface IntegerExpression<T extends Number> extends NumericExpression<T> {
    IntegerExpression<Long> plus(IntegerExpression<?> expr);


    FloatingPointExpression<BigDecimal> plus(FloatingPointExpression<?> expr);


    IntegerExpression<Long> minus(IntegerExpression<?> expr);


    FloatingPointExpression<BigDecimal> minus(FloatingPointExpression<?> expr);


    IntegerExpression<Long> times(IntegerExpression<?> expr);


    FloatingPointExpression<BigDecimal> times(FloatingPointExpression<?> expr);


    IntegerExpression<Long> dividedBy(IntegerExpression<?> expr);


    FloatingPointExpression<BigDecimal> dividedBy(FloatingPointExpression<?> expr);


    IntegerExpression<Long> modulus(IntegerExpression<?> expr);


    IntegerExpression<T> negative();
}
