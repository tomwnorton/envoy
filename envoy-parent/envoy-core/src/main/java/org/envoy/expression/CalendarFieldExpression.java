package org.envoy.expression;

import java.beans.*;
import java.util.*;

public class CalendarFieldExpression extends AbstractCalendarExpression implements
        GenericFieldExpression<Calendar> {
    private final CommonFieldExpressionLogic commonFieldExpressionLogic;


    public CalendarFieldExpression(final ModelExpression parent) {
        this(parent, null);
    }


    public CalendarFieldExpression(final ModelExpression parent, final String fieldName) {
        super(ExpressionFactoryImpl.get());
        commonFieldExpressionLogic = new CommonFieldExpressionLogicImpl(parent, this, fieldName);
    }


    CalendarFieldExpression(
            final ExpressionFactory expressionFactory,
            final CommonFieldExpressionLogic commonFieldExpressionLogic) {
        super(expressionFactory);
        this.commonFieldExpressionLogic = commonFieldExpressionLogic;
    }


    @Override
    public Object getFromRootEntity(final Object root) {
        return commonFieldExpressionLogic.getFromRootEntity(root);
    }


    @Override
    public void setFromRootEntity(final Object root, final Object value) {
        commonFieldExpressionLogic.setFromRootEntity(root, value);
    }


    @Override
    public Class<Calendar> getType() {
        return Calendar.class;
    }


    @Override
    public String getTableAlias() {
        return commonFieldExpressionLogic.getTableAlias();
    }


    @Override
    public EntityExpression<?> getEntityExpression() {
        return commonFieldExpressionLogic.getEntityExpression();
    }


    @Override
    public void setEntityExpression(final EntityExpression<?> entityExpression) {
        commonFieldExpressionLogic.setEntityExpression(entityExpression);
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitField(this);
    }


    @Override
    public String getFieldName() {
        return commonFieldExpressionLogic.getFieldName();
    }


    @Override
    public void setFieldName(final String fieldName) {
        this.commonFieldExpressionLogic.setFieldName(fieldName);
    }


    @Override
    public FieldAliasCache getFieldAliasCache() {
        return commonFieldExpressionLogic.getFieldAliasCache();
    }

    @Override
    public PropertyDescriptor getPropertyDescriptor() {
        return commonFieldExpressionLogic.getPropertyDescriptor();
    }

    @Override
    public boolean isPrimaryKey() {
        return commonFieldExpressionLogic.isPrimaryKey();
    }

    public CalendarFieldExpression primaryKey() {
        commonFieldExpressionLogic.setPrimaryKey(true);
        return this;
    }

    @Override
    public boolean isAutoIncrement() {
        return false;
    }
}
