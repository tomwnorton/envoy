package org.envoy.expression;

class ContainsExpression extends AbstractBinaryStringExpression {
    public ContainsExpression(final Expression source, final Expression pattern, final ExpressionFactory expressionFactory) {
        super(source, pattern, expressionFactory);
    }


    @Override
    protected void onAccept(final Expression lhs, final Expression rhs, final ExpressionVisitor visitor) {
        visitor.visitContains(lhs, rhs);
    }
}
