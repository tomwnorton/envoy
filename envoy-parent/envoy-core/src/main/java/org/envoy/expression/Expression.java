package org.envoy.expression;

/**
 * Expressions allow developers to create complex database-agnostic queries.
 * <p/>
 * Expressions use the visitor pattern to separate the semantics of the
 * expression from the query-building process. For example:
 * <p/>
 * {@code box.getLength().equalTo(box.getWidth()).and(box.getLength().equalTo(box.getHeight()) }
 * <p/>
 * For most SQL databases, calling accept will perform the following pseudo
 * code:
 * <p/>
 * {@code
 * box.getLength().equalTo(box.getWidth()).accept(visitor);
 * write out " AND "
 * box.getLength().equalTo(box.getHeight()).accept(visitor);
 * }
 * <p/>
 * Calling {@code accept} on the other expressions in this example have a
 * similar effect, producing the following SQL:
 * <p/>
 * {@code BOX.LENGTH = BOX.WIDTH AND BOX.LENGTH = BOX.HEIGHT}
 *
 * @author thomas
 */
public interface Expression {
    public void accept(final ExpressionVisitor visitor);


    public Class<?> getType();
}
