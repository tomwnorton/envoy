package org.envoy.expression;

class UpperCaseExpression extends AbstractUnaryStringExpression {
    public UpperCaseExpression(final Expression operand, final ExpressionFactory expressionFactory) {
        super(operand, expressionFactory);
    }


    @Override
    protected void onAccept(final Expression operand, final ExpressionVisitor visitor) {
        visitor.visitUpperCase(operand);
    }
}
