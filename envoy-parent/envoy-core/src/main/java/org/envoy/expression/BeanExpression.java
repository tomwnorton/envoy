package org.envoy.expression;

import java.util.*;

/**
 * Created by thomas on 7/20/14.
 */
public interface BeanExpression extends ModelExpression {
    public List<FieldExpression> getFieldExpressions();

    public List<ModelExpression> getFlattenedModelExpressions();
}
