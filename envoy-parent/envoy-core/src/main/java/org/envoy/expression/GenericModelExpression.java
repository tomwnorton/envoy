package org.envoy.expression;

public interface GenericModelExpression<T> extends ModelExpression, GenericExpression<T> {
}
