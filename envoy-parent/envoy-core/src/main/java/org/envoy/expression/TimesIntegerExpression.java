package org.envoy.expression;

class TimesIntegerExpression extends AbstractIntegerBinaryExpression {
    public TimesIntegerExpression(final Expression lhs, final Expression rhs, final ExpressionFactory expressionFactory) {
        super(lhs, rhs, expressionFactory);
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitTimes(lhs, rhs);
    }
}
