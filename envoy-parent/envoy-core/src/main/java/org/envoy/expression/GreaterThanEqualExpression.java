package org.envoy.expression;

class GreaterThanEqualExpression extends AbstractBooleanBinaryExpression {
    public GreaterThanEqualExpression(final Expression lhs, final Expression rhs, final ExpressionFactory expressionFactory) {
        super(lhs, rhs, expressionFactory);
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitGreaterThanEqual(lhs, rhs);
    }
}
