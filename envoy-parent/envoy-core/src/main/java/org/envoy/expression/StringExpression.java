package org.envoy.expression;

public interface StringExpression extends ComparableExpression<String, String, StringExpression> {
    public StringExpression trim();


    public IntegerExpression<Long> length();


    public StringExpression toLowerCase();


    public StringExpression toUpperCase();


    public IntegerExpression<Long> toInteger();


    public StringExpression concat(StringExpression rhs);


    public StringExpression concat(Object rhs);


    public StringExpression contains(StringExpression pattern);


    public StringExpression contains(String pattern);


    public StringExpression startsWith(StringExpression pattern);


    public StringExpression startsWith(String pattern);


    public StringExpression endsWith(StringExpression pattern);


    public StringExpression endsWith(String pattern);
}
