package org.envoy.expression;

public class FloatFieldExpression extends AbstractFloatingPointFieldExpression<Float, FloatFieldExpression> {
    public FloatFieldExpression(final ModelExpression parent) {
        super(parent);
    }


    public FloatFieldExpression(final ModelExpression parent, final String fieldName) {
        super(parent, fieldName);
    }


    @Override
    public Class<Float> getType() {
        return Float.class;
    }
}
