package org.envoy.expression;

import java.util.*;

class InExpression<T> extends AbstractBooleanExpression {
    private final Expression expr;
    private final Collection<T> collection;


    public InExpression(final Expression expr, final Collection<T> collection, final ExpressionFactory expressionFactory) {
        super(expressionFactory);
        this.expr = expr;
        this.collection = collection;
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitIn(expr, collection);
    }
}
