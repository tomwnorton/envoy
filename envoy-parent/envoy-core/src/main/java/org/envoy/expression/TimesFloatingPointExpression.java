package org.envoy.expression;

class TimesFloatingPointExpression extends AbstractFloatingPointBinaryExpression {
    public TimesFloatingPointExpression(final Expression lhs, final Expression rhs, final ExpressionFactory expressionFactory) {
        super(lhs, rhs, expressionFactory);
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitTimes(lhs, rhs);
    }
}
