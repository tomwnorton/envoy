package org.envoy.expression;

class ConcatExpression extends AbstractStringExpression {
    private final Expression lhs;
    private final Expression rhs;


    public ConcatExpression(final Expression lhs, final Expression rhs, final ExpressionFactory expressionFactory) {
        super(expressionFactory);
        this.lhs = lhs;
        this.rhs = rhs;
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitConcat(lhs, rhs);
    }
}
