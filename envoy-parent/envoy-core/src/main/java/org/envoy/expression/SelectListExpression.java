package org.envoy.expression;

import java.util.*;

public class SelectListExpression implements Expression {
    private final List<? extends Expression> list;


    public SelectListExpression(final List<? extends Expression> list) {
        this.list = list;
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitSelectList(list);
    }


    @Override
    public Class<?> getType() {
        return null;
    }
}
