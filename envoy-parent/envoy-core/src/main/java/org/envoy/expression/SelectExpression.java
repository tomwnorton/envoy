package org.envoy.expression;

public class SelectExpression<T> implements GenericExpression<T> {
    private final SelectListExpression selectListExpression;
    private final EntityExpression<T> fromExpression;
    private final BooleanExpression whereExpression;


    public SelectExpression(
            final SelectListExpression selectListExpr,
            final EntityExpression<T> fromExpr,
            final BooleanExpression whereExpr) {
        selectListExpression = selectListExpr;
        fromExpression = fromExpr;
        whereExpression = whereExpr;
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitSelect(selectListExpression, fromExpression, whereExpression);
    }


    @Override
    public Class<?> getType() {
        return null;
    }
}
