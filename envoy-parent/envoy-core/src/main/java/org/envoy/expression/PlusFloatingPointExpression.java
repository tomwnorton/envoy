package org.envoy.expression;

class PlusFloatingPointExpression extends AbstractFloatingPointBinaryExpression {
    public PlusFloatingPointExpression(final Expression lhs, final Expression rhs, final ExpressionFactory expressionFactory) {
        super(lhs, rhs, expressionFactory);
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitPlus(lhs, rhs);
    }
}
