package org.envoy.expression;

public class ExpressionMethods {
    public static BooleanExpression not(final BooleanExpression expr) {
        return ExpressionFactoryImpl.get().createNotExpression(expr);
    }


    public static <T> ParameterExpression<T> asExpression(final T value) {
        return ExpressionFactoryImpl.get().createParameterExpression(value);
    }
}
