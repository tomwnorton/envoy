package org.envoy.expression;

abstract class AbstractNumericExpression<T extends Number> extends
        AbstractComparableExpression<Number, T, NumericExpression<T>> implements
        NumericExpression<T> {
    public AbstractNumericExpression(final ExpressionFactory expressionFactory) {
        super(expressionFactory);
    }


    @Override
    public StringExpression convertToString() {
        return getExpressionFactory().createConvertToStringExpression(this);
    }
}
