package org.envoy.expression;

import java.math.*;
import java.util.*;

class CoalesceFloatingPointExpression<T extends Number> extends AbstractFloatingPointExpression<T> {
    private final List<? extends Expression> exprs;


    public CoalesceFloatingPointExpression(final List<? extends Expression> exprs, final ExpressionFactory expressionFactory) {
        super(expressionFactory);
        this.exprs = exprs;
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitCoalesce(exprs);
    }


    @Override
    public Class<?> getType() {
        Class<?> resultType = Float.class;
        for (final Expression expr : exprs) {
            if (BigDecimal.class.equals(expr.getType())) {
                return BigDecimal.class;
            }
            if (Double.class.equals(expr.getType())) {
                resultType = Double.class;
            }
        }
        return resultType;
    }
}
