package org.envoy.expression;

import java.util.*;

public class ExpressionCollection extends ArrayList<BooleanExpression> {
    private static final long serialVersionUID = -5168940450502073869L;

    private final ExpressionFactory _expressionFactory;


    public ExpressionCollection(final ExpressionFactory expressionFactory) {
        this._expressionFactory = expressionFactory;
    }


    @Override
    public boolean add(final BooleanExpression e) {
        boolean collectionChanged = false;

        if (e instanceof BooleanFieldExpression) {
            collectionChanged = super.add(
                    this._expressionFactory.createEqualsExpression(
                            e,
                            this._expressionFactory.createBooleanConstantExpression(true)));
        } else {
            collectionChanged = super.add(e);
        }

        return collectionChanged;
    }
}
