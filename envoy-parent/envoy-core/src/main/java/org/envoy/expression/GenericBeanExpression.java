package org.envoy.expression;

/**
 * Created by thomas on 7/20/14.
 */
public interface GenericBeanExpression<T> extends GenericModelExpression<T>, BeanExpression {
}
