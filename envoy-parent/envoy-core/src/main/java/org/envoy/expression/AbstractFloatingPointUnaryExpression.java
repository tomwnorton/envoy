package org.envoy.expression;

abstract class AbstractFloatingPointUnaryExpression<T extends Number> extends AbstractFloatingPointExpression<T> {
    private final Expression operand;


    protected abstract void accept(ExpressionVisitor visitor, Expression operand);


    public AbstractFloatingPointUnaryExpression(final Expression operand, final ExpressionFactory expressionFactory) {
        super(expressionFactory);
        this.operand = operand;
    }


    @Override
    public final void accept(final ExpressionVisitor visitor) {
        accept(visitor, operand);
    }


    @Override
    public Class<?> getType() {
        return operand.getType();
    }
}
