package org.envoy.expression;

/**
 * Created by thomas on 7/20/14.
 */
public interface FieldExpression extends ModelExpression {
    public String getFieldName();


    public void setFieldName(String fieldName);

    public boolean isPrimaryKey();

    public boolean isAutoIncrement();
}
