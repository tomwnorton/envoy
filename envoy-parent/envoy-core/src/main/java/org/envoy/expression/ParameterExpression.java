package org.envoy.expression;

import org.apache.commons.lang3.*;

public class ParameterExpression<T> implements GenericExpression<T> {
    private final T value;


    public ParameterExpression(final T value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ParameterExpression)) {
            return false;
        }

        ParameterExpression<T> other = (ParameterExpression<T>) obj;
        return ObjectUtils.equals(value, other.value);
    }

    @Override
    public String toString() {
        return value == null ? "null" : value.toString();
    }

    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitParameter(value);
    }


    @Override
    public Class<?> getType() {
        return value.getClass();
    }
}
