package org.envoy.expression;

import org.apache.commons.lang3.*;

import java.util.*;

public class FieldAliasCacheImpl implements FieldAliasCache {
    private final HashMap<Expression, Integer> count = new HashMap<Expression, Integer>();
    private final HashMap<String, Integer> currentIndexForEntityExpressions = new HashMap<String, Integer>();
    private int currentIndexForCalculatedFields = 0;


    @Override
    public String getFieldAlias(final Expression expr) {
        if (expr instanceof FieldExpression) {
            final FieldExpression temp = (FieldExpression) expr;
            return getFieldAlias(((FieldExpression) expr).getEntityExpression()) + "_" + temp.getFieldName();
        }

        if (expr instanceof EntityExpression) {
            final EntityExpression<?> temp = (EntityExpression<?>) expr;
            if (!count.containsKey(temp)) {
                Integer currentIndex = currentIndexForEntityExpressions.get(temp.getTableName());
                if (currentIndex == null) {
                    currentIndex = 0;
                    currentIndexForEntityExpressions.put(temp.getTableName(), 0);
                }
                count.put(temp, currentIndex);
                currentIndexForEntityExpressions.put(temp.getTableName(), currentIndex + 1);
            }

            StringBuilder sb = new StringBuilder();
            if (StringUtils.isNotBlank(temp.getSchemaName())) {
                sb.append(temp.getSchemaName()).append("_");
            }
            return sb.append(temp.getTableName()).append("_").append(count.get(temp)).toString();
        }

        if (!count.containsKey(expr)) {
            count.put(expr, currentIndexForCalculatedFields++);
        }

        return "calculated_" + count.get(expr);
    }
}
