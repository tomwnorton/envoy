package org.envoy.expression;

class ModulusIntegerExpression extends AbstractIntegerBinaryExpression {
    public ModulusIntegerExpression(final Expression lhs, final Expression rhs, final ExpressionFactory expressionFactory) {
        super(lhs, rhs, expressionFactory);
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitModulus(lhs, rhs);
    }
}
