package org.envoy.expression;

class LessThanExpression extends AbstractBooleanBinaryExpression {
    public LessThanExpression(final Expression lhs, final Expression rhs, final ExpressionFactory expressionFactory) {
        super(lhs, rhs, expressionFactory);
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitLessThan(lhs, rhs);
    }
}
