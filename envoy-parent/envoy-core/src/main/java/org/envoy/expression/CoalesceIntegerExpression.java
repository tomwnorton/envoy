package org.envoy.expression;

import java.util.*;

class CoalesceIntegerExpression<T extends Number> extends AbstractIntegerExpression<T> {
    private final List<? extends Expression> exprs;


    public CoalesceIntegerExpression(final List<? extends Expression> exprs, final ExpressionFactory expressionFactory) {
        super(expressionFactory);
        this.exprs = exprs;
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitCoalesce(exprs);
    }


    @Override
    public Class<?> getType() {
        Class<?> type = Byte.class;

        for (final Expression expr : exprs) {
            if (Long.class.equals(expr.getType())) {
                return Long.class;
            }

            if (Integer.class.equals(expr.getType())) {
                type = Integer.class;
            } else if (!Integer.class.equals(type) && Short.class.equals(expr.getType())) {
                type = Short.class;
            }
        }

        return type;
    }
}
