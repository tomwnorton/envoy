package org.envoy.expression;

class NotExpression extends AbstractBooleanExpression {
    private final BooleanExpression expr;


    public NotExpression(final BooleanExpression expr, final ExpressionFactory expressionFactory) {
        super(expressionFactory);
        this.expr = expr;
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        if (expr instanceof BooleanFieldExpression) {
            visitor.visitEquals(expr, getExpressionFactory().createBooleanConstantExpression(false));
        } else {
            visitor.visitNot(expr);
        }
    }
}
