package org.envoy.expression;

import java.util.*;

abstract class AbstractStringExpression extends AbstractComparableExpression<String, String, StringExpression> implements
        StringExpression {
    AbstractStringExpression(final ExpressionFactory expressionFactory) {
        super(expressionFactory);
    }


    @Override
    protected StringExpression coalesceInternal(final List<GenericExpression<String>> exprs) {
        return getExpressionFactory().createCoalesceStringExpression(exprs);
    }


    @Override
    public StringExpression trim() {
        return getExpressionFactory().createTrimExpression(this);
    }


    @Override
    public IntegerExpression<Long> length() {
        return getExpressionFactory().createLengthExpression(this);
    }


    @Override
    public StringExpression toLowerCase() {
        return getExpressionFactory().createLowerCaseExpression(this);
    }


    @Override
    public StringExpression toUpperCase() {
        return getExpressionFactory().createUpperCaseExpression(this);
    }


    @Override
    public IntegerExpression<Long> toInteger() {
        return getExpressionFactory().createConvertStringToIntegerExpression(this);
    }


    @Override
    public StringExpression concat(final StringExpression rhs) {
        return getExpressionFactory().createConcatExpression(this, rhs);
    }


    @Override
    public StringExpression concat(final Object rhs) {
        return getExpressionFactory().createConcatExpression(this, getExpressionFactory().createParameterExpression(rhs));
    }


    @Override
    public StringExpression contains(final StringExpression pattern) {
        return getExpressionFactory().createContainsExpression(this, pattern);
    }


    @Override
    public StringExpression contains(final String pattern) {
        return getExpressionFactory().createContainsExpression(
                this,
                getExpressionFactory().createParameterExpression(pattern));
    }


    @Override
    public StringExpression startsWith(final StringExpression pattern) {
        return getExpressionFactory().createStartsWithExpression(this, pattern);
    }


    @Override
    public StringExpression startsWith(final String pattern) {
        return getExpressionFactory().createStartsWithExpression(
                this,
                getExpressionFactory().createParameterExpression(pattern));
    }


    @Override
    public StringExpression endsWith(final StringExpression pattern) {
        return getExpressionFactory().createEndsWithExpression(this, pattern);
    }


    @Override
    public StringExpression endsWith(final String pattern) {
        return getExpressionFactory().createEndsWithExpression(
                this,
                getExpressionFactory().createParameterExpression(pattern));
    }


    @Override
    public Class<?> getType() {
        return String.class;
    }
}
