package org.envoy.expression;

class LessThanEqualExpression extends AbstractBooleanBinaryExpression {
    public LessThanEqualExpression(final Expression lhs, final Expression rhs, final ExpressionFactory expressionFactory) {
        super(lhs, rhs, expressionFactory);
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitLessThanEqual(lhs, rhs);
    }
}
