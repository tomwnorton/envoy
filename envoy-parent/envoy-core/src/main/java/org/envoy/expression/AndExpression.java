package org.envoy.expression;

import org.apache.commons.lang3.*;

class AndExpression extends AbstractBooleanBinaryExpression {
    public AndExpression(final BooleanExpression lhs, final BooleanExpression rhs, final ExpressionFactory expressionFactory) {
        super(lhs, rhs, expressionFactory);
    }

    @Override
    public String toString() {
        return ObjectUtils.toString(lhs) + " AND " + ObjectUtils.toString(rhs);
    }

    @Override
    public void accept(final ExpressionVisitor visitor) {
        Expression left = this.lhs;
        Expression right = this.rhs;

        if (lhs instanceof BooleanFieldExpression) {
            left =
                    this.getExpressionFactory().createEqualsExpression(
                            lhs,
                            this.getExpressionFactory().createBooleanConstantExpression(true));
        }

        if (rhs instanceof BooleanFieldExpression) {
            right =
                    this.getExpressionFactory().createEqualsExpression(
                            rhs,
                            this.getExpressionFactory().createBooleanConstantExpression(true));
        }

        visitor.visitAnd(left, right);
    }
}
