package org.envoy.expression;

import java.math.*;
import java.util.*;

abstract class AbstractFloatingPointExpression<T extends Number> extends AbstractNumericExpression<T> implements
        FloatingPointExpression<T> {
    public AbstractFloatingPointExpression(final ExpressionFactory expressionFactory) {
        super(expressionFactory);
    }


    @Override
    public FloatingPointExpression<BigDecimal> plus(final NumericExpression<?> expr) {
        return getExpressionFactory().createPlusFloatingPointExpression(this, expr);
    }


    @Override
    public FloatingPointExpression<BigDecimal> minus(final NumericExpression<?> expr) {
        return getExpressionFactory().createMinusFloatingPointExpression(this, expr);
    }


    @Override
    public FloatingPointExpression<BigDecimal> times(final NumericExpression<?> expr) {
        return getExpressionFactory().createTimesFloatingPointExpression(this, expr);
    }


    @Override
    public FloatingPointExpression<BigDecimal> dividedBy(final NumericExpression<?> expr) {
        return getExpressionFactory().createDividedByFloatingPointExpression(this, expr);
    }


    @Override
    protected FloatingPointExpression<T> coalesceInternal(final List<GenericExpression<T>> exprs) {
        final ArrayList<GenericExpression<T>> allExprs = new ArrayList<GenericExpression<T>>();
        allExprs.add(this);
        allExprs.addAll(exprs);

        return getExpressionFactory().createCoalesceFloatingPointExpression(allExprs);
    }


    @Override
    public FloatingPointExpression<T> negative() {
        return getExpressionFactory().createNegativeFloatingPointExpression(this);
    }
}
