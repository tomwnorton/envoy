package org.envoy.expression;

public interface FieldAliasCache {
    public String getFieldAlias(Expression expr);
}
