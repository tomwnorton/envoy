package org.envoy.expression;

public class LongFieldExpression extends AbstractIntegerFieldExpression<Long, LongFieldExpression> {
    public LongFieldExpression(final ModelExpression parent) {
        super(parent);
    }


    public LongFieldExpression(final ModelExpression parent, final String fieldName) {
        super(parent, fieldName);
    }


    @Override
    public Class<Long> getType() {
        return Long.class;
    }
}
