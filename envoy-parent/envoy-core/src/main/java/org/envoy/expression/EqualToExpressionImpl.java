package org.envoy.expression;

import org.apache.commons.lang3.*;

/**
 * This class is not part of envoy's api. Do not use it.
 *
 * @author thomas
 */
public class EqualToExpressionImpl extends AbstractBooleanBinaryExpression {
    public EqualToExpressionImpl(final Expression lhs, final Expression rhs, final ExpressionFactory expressionFactory) {
        super(lhs, rhs, expressionFactory);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof EqualToExpressionImpl)) {
            return false;
        }

        EqualToExpressionImpl other = (EqualToExpressionImpl) obj;
        return ObjectUtils.equals(lhs, other.lhs) && ObjectUtils.equals(rhs, other.rhs);
    }

    @Override
    public String toString() {
        return ObjectUtils.toString(lhs) + " = (" + ObjectUtils.toString(rhs) + ")";
    }

    @Override
    public void accept(final ExpressionVisitor visitor) {
        if (rhs == null) {
            visitor.visitNull(lhs);
        } else {
            visitor.visitEquals(lhs, rhs);
        }
    }
}
