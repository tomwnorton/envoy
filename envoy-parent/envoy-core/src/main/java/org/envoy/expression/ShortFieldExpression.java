package org.envoy.expression;

public class ShortFieldExpression extends AbstractIntegerFieldExpression<Short, ShortFieldExpression> {
    public ShortFieldExpression(final ModelExpression parent) {
        super(parent);
    }


    public ShortFieldExpression(final ModelExpression parent, final String fieldName) {
        super(parent, fieldName);
    }


    @Override
    public Class<Short> getType() {
        return Short.class;
    }
}
