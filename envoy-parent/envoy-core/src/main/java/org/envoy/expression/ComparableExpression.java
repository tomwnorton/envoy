package org.envoy.expression;

public interface ComparableExpression<CompareType, T extends CompareType, ExpressionType extends ObjectInstanceExpression<CompareType, T, ExpressionType>>
        extends ObjectInstanceExpression<CompareType, T, ExpressionType> {

    public BooleanExpression lessThan(ExpressionType expression);


    public BooleanExpression lessThan(CompareType value);


    public BooleanExpression lessThanEqual(ExpressionType expression);


    public BooleanExpression lessThanEqual(CompareType value);


    public BooleanExpression greaterThan(ExpressionType expression);


    public BooleanExpression greaterThan(CompareType value);


    public BooleanExpression greaterThanEqual(ExpressionType expression);


    public BooleanExpression greaterThanEqual(CompareType value);


    public BooleanExpression between(ExpressionType lowerBound, ExpressionType upperBound);


    public BooleanExpression between(CompareType lowerBound, CompareType upperBound);
}
