package org.envoy.expression;

import org.apache.commons.lang3.*;
import org.envoy.*;
import org.envoy.exceptions.*;

import java.beans.*;
import java.lang.reflect.*;

class CommonFieldExpressionLogicImpl implements CommonFieldExpressionLogic {
    private EntityExpression<?> entityExpression;
    private String fieldName;
    private final ModelExpression parent;
    private final ModelExpression current;
    private PropertyDescriptor propertyDescriptor;
    private boolean readOnlyProperty;
    private boolean primaryKey;
    private boolean autoIncrement;


    public CommonFieldExpressionLogicImpl(final ModelExpression parent, final ModelExpression current) {
        this(parent, current, null);
    }


    public CommonFieldExpressionLogicImpl(final ModelExpression parent, final ModelExpression current, final String fieldName) {
        if (parent == null) {
            throw new EnvoyException("expression properties must be given the parent expression at construction.");
        }

        if (current == null) {
            throw new EnvoyException("There is a bug in envoy's expression classes.");
        }

        this.parent = parent;
        this.current = current;
        this.fieldName = fieldName;
    }


    @Override
    public EntityExpression<?> getEntityExpression() {
        return parent.getEntityExpression();
    }


    @Override
    public void setEntityExpression(final EntityExpression<?> entityExpression) {
        this.entityExpression = entityExpression;
    }


    @Override
    public String getTableAlias() {
        return entityExpression.getTableAlias();
    }


    @Override
    public String getFieldName() {
        return fieldName;
    }


    @Override
    public void setFieldName(final String fieldName) {
        this.fieldName = fieldName;
    }


    @SuppressWarnings("unchecked")
    @Override
    public Object getFromRootEntity(final Object root) {
        this.lazyLoadPropertyDescriptor();

        Object parentModel = parent.getFromRootEntity(root);
        if (parentModel != null) {
            try {
                return propertyDescriptor.getReadMethod().invoke(parentModel);
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new EnvoyReflectionException("Could not read value from entity property", e);
            }
        }
        return null;
    }


    private void lazyLoadPropertyDescriptor() {
        if (propertyDescriptor == null && !readOnlyProperty) {
            propertyDescriptor = findPropertyDescriptor();
            if (propertyDescriptor == null) {
                readOnlyProperty = true;
            }
        }
    }


    private PropertyDescriptor findPropertyDescriptor() {
        String propertyName;
        if (current.getType().equals(Boolean.class)) {
            propertyName = findBooleanExpressionPropertyName(parent.getClass());
        } else {
            propertyName = findExpressionPropertyDescriptor(parent.getClass()).getName();
        }
        PropertyDescriptor propertyDescriptor = findModelPropertyDescriptor(parent.getType(), propertyName);
        if (propertyDescriptor == null) {
            throw new EnvoyReflectionException(
                    "The property "
                    + propertyName
                    + " does not exists for class "
                    + parent.getClass().getCanonicalName());
        }
        return propertyDescriptor;
    }


    private String findBooleanExpressionPropertyName(Class<?> modelExpressionClass) {
        if (modelExpressionClass == null || !GenericModelExpression.class.isAssignableFrom(modelExpressionClass)) {
            throw new EnvoyReflectionException(
                    "Envoy field and component expressions must be public properties of envoy entity or component expressions.");
        }

        try {
            for (MethodDescriptor methodDescriptor : Introspector.getBeanInfo(modelExpressionClass).getMethodDescriptors()) {
                if (methodDescriptor.getMethod().getReturnType().isAssignableFrom(this.current.getClass())
                    && methodDescriptor.getMethod().getParameterTypes().length == 0
                    && methodDescriptor.getMethod().invoke(parent) == current) {
                    return getBooleanExpressionPropertyName(methodDescriptor.getName());
                }
            }
            return findBooleanExpressionPropertyName(modelExpressionClass.getSuperclass());
        } catch (IntrospectionException | InvocationTargetException | IllegalAccessException e) {
            throw new EnvoyReflectionException(e);
        }
    }


    private String getBooleanExpressionPropertyName(String methodName) {
        String methodNameWithoutPrefix = null;
        if (methodName.startsWith("get")) {
            methodNameWithoutPrefix = methodName.substring(3);
        } else if (methodName.startsWith("is")) {
            methodNameWithoutPrefix = methodName.substring(2);
        }
        if (StringUtils.isBlank(methodNameWithoutPrefix)) {
            throw new EnvoyReflectionException("The following is a bad name for a property: " + methodName);
        }
        if (Character.isLowerCase(methodNameWithoutPrefix.charAt(0))
            || Character.isUpperCase(methodNameWithoutPrefix.charAt(0))
               && methodNameWithoutPrefix.length() > 1 && Character.isUpperCase(methodNameWithoutPrefix.charAt(1))) {
            return methodNameWithoutPrefix;
        }
        if (methodNameWithoutPrefix.length() > 1) {
            return String.valueOf(methodNameWithoutPrefix.charAt(0)).toLowerCase() + methodNameWithoutPrefix.substring(1);
        }
        return String.valueOf(methodNameWithoutPrefix.charAt(0)).toLowerCase();
    }


    private PropertyDescriptor findExpressionPropertyDescriptor(Class<?> modelExpressionClass) {
        if (modelExpressionClass == null || !GenericModelExpression.class.isAssignableFrom(modelExpressionClass)) {
            throw new EnvoyReflectionException(
                    "Envoy field and component expressions must be public properties of envoy entity or component expressions.");
        }

        try {
            for (PropertyDescriptor propertyDescriptor : Introspector.getBeanInfo(modelExpressionClass).getPropertyDescriptors()) {
                if (propertyDescriptor.getPropertyType().isAssignableFrom(current.getClass())
                    && propertyDescriptor.getReadMethod() != null
                    && propertyDescriptor.getReadMethod().invoke(parent) == current) {
                    return propertyDescriptor;
                }
            }
            return findExpressionPropertyDescriptor(modelExpressionClass.getSuperclass());
        } catch (IntrospectionException | InvocationTargetException | IllegalAccessException e) {
            throw new EnvoyReflectionException(e);
        }
    }


    private PropertyDescriptor findModelPropertyDescriptor(Class<?> modelClazz, String propertyName) {
        if (modelClazz == null) {
            throw new EnvoyReflectionException("Null model given ");
        }

        try {
            for (PropertyDescriptor propertyDescriptor : Introspector.getBeanInfo(modelClazz).getPropertyDescriptors()) {
                if (propertyDescriptor.getPropertyType().isAssignableFrom(current.getType())
                    && propertyDescriptor.getName().equals(propertyName)) {
                    if (propertyDescriptor.getWriteMethod() == null) {
                        throw new EnvoyReflectionException(
                                "Field and model expressions MUST have a one-to-one mapping "
                                + "between expression and entity! This property is read-only: " + propertyName
                                + " on model: " + modelClazz.getCanonicalName());
                    }

                    //--The Java Beans API only allows is* read methods for primitive boolean properties.
                    if (propertyDescriptor.getReadMethod() == null
                        && propertyDescriptor.getPropertyType().equals(Boolean.class)) {
                        try {
                            String readMethodName = "is"
                                                    + String.valueOf(propertyName.charAt(0)).toUpperCase()
                                                    + propertyName.substring(1);
                            Method readMethod = modelClazz.getMethod(readMethodName);
                            return new PropertyDescriptor(propertyName, readMethod, propertyDescriptor.getWriteMethod());
                        } catch (NoSuchMethodException e) {
                            throw new EnvoyReflectionException(
                                    "Field and model expressions MUST have a one-to-one mapping "
                                    + "between expression and entity! This property is write-only: " + propertyName
                                    + " on model: " + modelClazz.getCanonicalName());
                        }
                    }

                    return propertyDescriptor;
                }
            }
            if (!AbstractBean.class.equals(modelClazz.getSuperclass())
                && !AbstractEntity.class.equals(modelClazz.getSuperclass())) {
                return findModelPropertyDescriptor(modelClazz.getSuperclass(), propertyName);
            }
            return null;
        } catch (IntrospectionException e) {
            throw new EnvoyReflectionException(e);
        }
    }


    @Override
    public void setFromRootEntity(final Object root, final Object value) {
        lazyLoadPropertyDescriptor();
        if (root != null) {
            try {
                Object parentModel = parent.getFromRootEntity(root);
                if (parentModel == null && value != null) {
                    parentModel = parent.getType().newInstance();
                    parent.setFromRootEntity(root, parentModel);
                }
                //--If this is the first expression on an entity or component, and this value is null, then
                //  there is no parent model yet, so don't bother setting null on a null parent.
                if (parentModel != null) {
                    propertyDescriptor.getWriteMethod().invoke(parentModel, value);
                }
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                throw new EnvoyReflectionException("Could not set property on entity.", e);
            }
        }
    }


    @Override
    public FieldAliasCache getFieldAliasCache() {
        return entityExpression.getFieldAliasCache();
    }

    public PropertyDescriptor getPropertyDescriptor() {
        lazyLoadPropertyDescriptor();
        return propertyDescriptor;
    }

    @Override
    public boolean isPrimaryKey() {
        return primaryKey;
    }

    @Override
    public void setPrimaryKey(boolean primaryKey) {
        this.primaryKey = primaryKey;
    }

    @Override
    public boolean isAutoIncrement() {
        return autoIncrement;
    }

    @Override
    public void setAutoIncrement(boolean autoIncrement) {
        this.autoIncrement = autoIncrement;
    }
}
