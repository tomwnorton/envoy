package org.envoy.expression;

public class BooleanExpressionBuilder {
    BooleanExpression expr;


    public BooleanExpressionBuilder and(final BooleanExpression expr) {
        if (this.expr != null) {
            this.expr = this.expr.and(expr);
        } else {
            this.expr = expr;
        }
        return this;
    }


    public BooleanExpressionBuilder or(final BooleanExpression expr) {
        if (this.expr != null) {
            this.expr = this.expr.or(expr);
        } else {
            this.expr = expr;
        }
        return this;
    }


    public BooleanExpression get() {
        return expr;
    }
}
