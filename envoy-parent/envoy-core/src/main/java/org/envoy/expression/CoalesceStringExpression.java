package org.envoy.expression;

import java.util.*;

class CoalesceStringExpression extends AbstractStringExpression {
    private final List<? extends Expression> exprs;


    CoalesceStringExpression(final List<? extends Expression> exprs, final ExpressionFactory expressionFactory) {
        super(expressionFactory);
        this.exprs = exprs;
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitCoalesce(exprs);
    }
}
