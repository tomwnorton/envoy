package org.envoy.expression;


import java.util.*;

public interface EntityExpression<EntityType> extends GenericBeanExpression<EntityType> {
    public String getTableName();


    public void setTableName(String tableName);


    @Override
    public String getTableAlias();


    public Class<? extends EntityType> getEntityClass();


    public void setFieldAliasCache(FieldAliasCache fieldAliasCache);

    public String getSchemaName();

    public void setSchemaName(String schemaName);

    public List<FieldExpression> getPrimaryKeys();
}
