package org.envoy.expression;

import java.util.*;

class CoalesceBooleanExpression extends AbstractBooleanExpression {
    private final List<? extends Expression> exprs;


    public CoalesceBooleanExpression(final List<? extends Expression> exprs, final ExpressionFactory expressionFactory) {
        super(expressionFactory);
        this.exprs = exprs;
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitCoalesce(exprs);
    }
}
