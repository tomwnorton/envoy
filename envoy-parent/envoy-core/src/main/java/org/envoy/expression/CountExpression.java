package org.envoy.expression;

public class CountExpression extends AbstractIntegerExpression<Long> implements AggregateExpression {
    private final Expression operand;


    public CountExpression(final Expression expr, final ExpressionFactory expressionFactory) {
        super(expressionFactory);
        this.operand = expr;
    }


    @Override
    public void accept(final ExpressionVisitor visitor) {
        visitor.visitCount(operand);
    }


    @Override
    public Class<?> getType() {
        return Long.class;
    }
}
