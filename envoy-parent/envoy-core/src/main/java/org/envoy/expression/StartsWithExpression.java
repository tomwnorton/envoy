package org.envoy.expression;

class StartsWithExpression extends AbstractBinaryStringExpression {
    public StartsWithExpression(final Expression source, final Expression pattern, final ExpressionFactory expressionFactory) {
        super(source, pattern, expressionFactory);
    }


    @Override
    protected void onAccept(final Expression source, final Expression pattern, final ExpressionVisitor visitor) {
        visitor.visitStartsWith(source, pattern);
    }
}
