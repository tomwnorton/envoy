package org.envoy;

import org.envoy.expression.*;

/**
 * Contains methods that get entity meta-data via reflection. Performs error
 * handling as well.
 * <p/>
 * Any reflection errors get wrapped in EnvoyExceptions
 */
public interface EntityFactory {
    /**
     * Creates an entity object using its public default constructor
     *
     * @param entityExpression
     * @return
     */
    public <EntityType> EntityType createEntity(EntityExpression<EntityType> entityExpression);
}