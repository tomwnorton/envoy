package org.envoy;

import org.envoy.exceptions.*;
import org.envoy.expression.*;

public class EntityFactoryImpl implements EntityFactory {
    @Override
    public <EntityType> EntityType createEntity(final EntityExpression<EntityType> entityExpression) {
        try {
            return entityExpression.getEntityClass().newInstance();
        } catch (final IllegalArgumentException
                | SecurityException
                | InstantiationException
                | IllegalAccessException
                | EnvoyReflectionException e) {
            throw new EnvoyException("Could not create entity object", e);
        }
    }
}
