package org.envoy;

/**
 * Created by thomas on 8/26/14.
 */
public class Property<T> {
    private T value;
    private boolean changed;

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
        changed = true;
    }

    public void initializeValue(T value) {
        this.value = value;
    }

    public boolean isChanged() {
        return changed;
    }

    public void flagAsUnchanged() {
        changed = false;
    }

    @Override
    public String toString() {
        return value == null ? "null" : value.toString();
    }
}
