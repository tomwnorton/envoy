package org.envoy;

import org.envoy.exceptions.*;
import org.envoy.expression.*;

import java.beans.*;
import java.lang.reflect.*;
import java.util.*;

/**
 * Created by thomas on 8/27/14.
 */
public abstract class AbstractBean {
    protected final Map<Method, Object> writeMethodsCalledSinceLastSave = new HashMap<>();

    protected void recordChange(Class<?> parameterType) {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        try {
            String callingMethodName = stackTraceElements[2].getMethodName();
            Method callingMethod = getClass().getMethod(callingMethodName, parameterType);
            Method getterMethod = getAccessorMethodFromSetterMethodName(callingMethodName);
            recordChange(callingMethod, getterMethod);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new EnvoyReflectionException(e);
        }
    }

    private Method getAccessorMethodFromSetterMethodName(String setterMethodName) throws NoSuchMethodException {
        try {
            return getAccessorMethodFromSetterMethodName(setterMethodName, "get");
        } catch (NoSuchMethodException getterMethodNotFoundException) {
            return getAccessorMethodFromSetterMethodName(setterMethodName, "is");
        }
    }

    private Method getAccessorMethodFromSetterMethodName(String setterMethodName, String accessorPrefix) throws
            NoSuchMethodException {
        return getClass().getMethod(setterMethodName.replaceFirst("set", accessorPrefix));
    }

    private void recordChange(Method writeMethod, Method getterMethod) throws
            InvocationTargetException,
            IllegalAccessException {
        Object oldValue = getterMethod.invoke(this);
        writeMethodsCalledSinceLastSave.put(writeMethod, oldValue);
    }

    protected void markAsClean() {
        writeMethodsCalledSinceLastSave.clear();
        try {
            markNestedBeansAsClean();
        } catch (IntrospectionException | InvocationTargetException | IllegalAccessException e) {
            throw new EnvoyReflectionException(e);
        }
    }

    private void markNestedBeansAsClean() throws IntrospectionException, InvocationTargetException, IllegalAccessException {
        for (AbstractBean nestedBean : getNestedBeansThatAreNotNull()) {
            nestedBean.markAsClean();
        }
    }

    private List<AbstractBean> getNestedBeansThatAreNotNull() {
        List<AbstractBean> nestedBeans = new ArrayList<>();
        try {
            for (PropertyDescriptor propertyDescriptor : Introspector.getBeanInfo(getClass()).getPropertyDescriptors()) {
                if (propertyDescriptor.getReadMethod() != null
                    && AbstractBean.class.isAssignableFrom(propertyDescriptor.getPropertyType())) {
                    AbstractBean nestedBean = (AbstractBean) propertyDescriptor.getReadMethod().invoke(this);
                    if (nestedBean != null) {
                        nestedBeans.add(nestedBean);
                    }
                }
            }
        } catch (IntrospectionException | InvocationTargetException | IllegalAccessException e) {
            throw new EnvoyReflectionException(e);
        }
        return nestedBeans;
    }

    protected boolean doesModelNeedToBeSaved(ModelExpression modelExpression) {
        boolean foundResult = writeMethodsCalledSinceLastSave.containsKey(modelExpression.getPropertyDescriptor().getWriteMethod());
        if (foundResult) {
            return true;
        }

        for (AbstractBean nestedBean : getNestedBeansThatAreNotNull()) {
            if (nestedBean.doesModelNeedToBeSaved(modelExpression)) {
                return true;
            }
        }

        return false;
    }

    protected boolean isFieldExpressionForUnchangedPrimaryKeyField(FieldExpression fieldExpression) {
        if (!fieldExpression.isPrimaryKey()
            || writeMethodsCalledSinceLastSave.get(fieldExpression.getPropertyDescriptor().getWriteMethod()) != null) {
            return false;
        }

        if (writeMethodsCalledSinceLastSave.containsKey(fieldExpression.getPropertyDescriptor().getWriteMethod())) {
            return true;
        }

        for (AbstractBean nestedBean : getNestedBeansThatAreNotNull()) {
            if (nestedBean.isFieldExpressionForUnchangedPrimaryKeyField(fieldExpression)) {
                return true;
            }
        }

        return false;
    }

    protected Object getOldValueForModelExpression(ModelExpression modelExpression) {
        Method writeMethod = modelExpression.getPropertyDescriptor().getWriteMethod();
        if (writeMethodsCalledSinceLastSave.containsKey(writeMethod)) {
            return writeMethodsCalledSinceLastSave.get(writeMethod);
        }

        for (AbstractBean nestedBean : getNestedBeansThatAreNotNull()) {
            if (nestedBean.doesModelNeedToBeSaved(modelExpression)) {
                return nestedBean.getOldValueForModelExpression(modelExpression);
            }
        }

        return null;
    }
}
