package org.envoy;

import org.envoy.expression.*;

import javax.sql.*;
import java.sql.*;
import java.util.*;

public interface SqlDatabaseMetaData extends DatabaseMetaData<Connection> {
    public DataSource getDataSource();


    public List<String> getReservedWords();


    /**
     * Used to determine if a database object name needs to be escaped
     *
     * @return
     */
    public String getOperatorsRegexClass();


    public String getStartForDatabaseObjectEscape();


    public String getEndForDatabaseObjectEscape();


    public String getEscapedObjectName(final String objectName);


    public FieldAliasCache getFieldAliasCache();

    public SqlExpressionVisitor createExpressionVisitor();
}
