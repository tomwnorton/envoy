package org.envoy.typepersisters;

import java.sql.*;

public abstract class AbstractNullableNumericTypePersister<T extends Number> extends AbstractTypePersister<T> {
    public AbstractNullableNumericTypePersister(final Class<T> clazz) {
        super(clazz);
    }


    protected abstract T toPrimitiveValue(Number value);


    @Override
    public final T get(final ResultSet rs, final String fieldName) throws SQLException {
        final Number temp = (Number) rs.getObject(fieldName);
        T value = null;
        if (temp != null) {
            value = this.toPrimitiveValue(temp);
        }
        return value;
    }


    @Override
    protected final void setImpl(final PreparedStatement ps, final int fieldIndex, final T value) throws SQLException {
        ps.setObject(fieldIndex, value);
    }
}
