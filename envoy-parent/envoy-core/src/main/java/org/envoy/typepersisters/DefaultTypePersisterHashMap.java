package org.envoy.typepersisters;

public class DefaultTypePersisterHashMap extends TypePersisterHashMap {
    private static final long serialVersionUID = 3817158769079934817L;


    public DefaultTypePersisterHashMap() {
        this.put(new BooleanTypePersister());
        this.put(new CharacterTypePersister());
        this.put(new ByteTypePersister());
        this.put(new ShortTypePersister());
        this.put(new IntegerTypePersister());
        this.put(new LongTypePersister());
        this.put(new DoubleTypePersister());
        this.put(new StringTypePersister());
        this.put(new CalendarTypePersister());
        this.put(new BigDecimalTypePersister());
    }
}
