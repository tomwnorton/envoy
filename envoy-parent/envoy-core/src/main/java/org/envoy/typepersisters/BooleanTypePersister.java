package org.envoy.typepersisters;

import java.sql.*;

public class BooleanTypePersister extends AbstractTypePersister<Boolean> {

    public BooleanTypePersister() {
        super(Boolean.class);
    }


    @Override
    public Boolean get(final ResultSet rs, final String fieldName) throws SQLException {
        Boolean result = rs.getBoolean(fieldName);

        if (rs.wasNull()) {
            result = null;
        }

        return result;
    }


    @Override
    protected void setImpl(final PreparedStatement ps, final int fieldIndex, final Boolean value) throws SQLException {
        ps.setObject(fieldIndex, value);
    }
}
