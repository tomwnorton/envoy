package org.envoy.typepersisters;

import java.sql.*;
import java.util.*;

public class CalendarTypePersister extends AbstractTypePersister<Calendar> {
    public CalendarTypePersister() {
        super(Calendar.class);
    }


    @Override
    public Calendar get(final ResultSet rs, final String fieldName) throws SQLException {
        final GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(rs.getTimestamp(fieldName));
        return gc;
    }


    @Override
    protected void setImpl(final PreparedStatement ps, final int fieldIndex, final Calendar value) throws SQLException {
        ps.setTimestamp(fieldIndex, new Timestamp(value.getTimeInMillis()));
    }
}
