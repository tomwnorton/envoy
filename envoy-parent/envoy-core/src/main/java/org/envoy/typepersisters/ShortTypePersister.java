package org.envoy.typepersisters;

public class ShortTypePersister extends AbstractNullableNumericTypePersister<Short> {
    public ShortTypePersister() {
        super(Short.class);
    }


    @Override
    protected Short toPrimitiveValue(final Number value) {
        return value.shortValue();
    }
}
