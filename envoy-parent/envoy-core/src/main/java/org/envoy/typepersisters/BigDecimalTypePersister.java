package org.envoy.typepersisters;

import java.math.*;
import java.sql.*;

public class BigDecimalTypePersister extends AbstractTypePersister<BigDecimal> {
    public BigDecimalTypePersister() {
        super(BigDecimal.class);
    }


    @Override
    public BigDecimal get(final ResultSet rs, final String fieldName) throws SQLException {
        return rs.getBigDecimal(fieldName);
    }


    @Override
    protected void setImpl(final PreparedStatement ps, final int fieldIndex, final BigDecimal value) throws SQLException {
        ps.setBigDecimal(fieldIndex, value);
    }
}
