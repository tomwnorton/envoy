package org.envoy.typepersisters;

import java.sql.*;

public interface TypePersister<T> {
    public T get(ResultSet rs, String fieldName) throws SQLException;


    public void set(PreparedStatement ps, int parameterIndex, Object value) throws SQLException;


    public Class<T> getJavaType();
}
