package org.envoy.typepersisters;

public class DoubleTypePersister extends AbstractNullableNumericTypePersister<Double> {
    public DoubleTypePersister() {
        super(Double.class);
    }


    @Override
    protected Double toPrimitiveValue(final Number value) {
        return value.doubleValue();
    }
}
