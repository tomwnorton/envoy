package org.envoy.typepersisters;

import java.sql.*;

public class StringTypePersister extends AbstractTypePersister<String> {
    public StringTypePersister() {
        super(String.class);
    }


    @Override
    public String get(final ResultSet rs, final String fieldName) throws SQLException {
        return rs.getString(fieldName);
    }


    @Override
    protected void setImpl(final PreparedStatement ps, final int fieldIndex, final String value) throws SQLException {
        ps.setString(fieldIndex, value);
    }
}
