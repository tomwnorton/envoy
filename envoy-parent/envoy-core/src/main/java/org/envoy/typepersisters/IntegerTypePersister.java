package org.envoy.typepersisters;

public class IntegerTypePersister extends AbstractNullableNumericTypePersister<Integer> {
    public IntegerTypePersister() {
        super(Integer.class);
    }


    @Override
    protected Integer toPrimitiveValue(final Number value) {
        return value.intValue();
    }
}
