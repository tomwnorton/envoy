package org.envoy.typepersisters;

public class LongTypePersister extends AbstractNullableNumericTypePersister<Long> {
    public LongTypePersister() {
        super(Long.class);
    }


    @Override
    protected Long toPrimitiveValue(final Number value) {
        return value.longValue();
    }
}
