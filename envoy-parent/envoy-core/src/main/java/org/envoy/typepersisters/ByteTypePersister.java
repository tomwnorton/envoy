package org.envoy.typepersisters;

public class ByteTypePersister extends AbstractNullableNumericTypePersister<Byte> {
    public ByteTypePersister() {
        super(Byte.class);
    }


    @Override
    protected Byte toPrimitiveValue(final Number value) {
        return value.byteValue();
    }
}
