package org.envoy.typepersisters;

import java.sql.*;

public class CharacterTypePersister extends AbstractTypePersister<Character> {

    public CharacterTypePersister() {
        super(Character.class);
    }


    @Override
    public Character get(final ResultSet rs, final String fieldName) throws SQLException {
        final String data = rs.getString(fieldName);
        Character result = null;
        if (data != null && !data.isEmpty()) {
            result = data.charAt(0);
        }
        return result;
    }


    @Override
    protected void setImpl(final PreparedStatement ps, final int fieldIndex, final Character value) throws SQLException {
        String data = null;
        if (value != null) {
            data = value.toString();
        }
        ps.setString(fieldIndex, data);
    }
}
