package org.envoy.typepersisters;

import java.util.*;

public interface TypePersisterMap extends Map<Class<?>, TypePersister<?>> {
    public void put(TypePersister<?> typePersister);
}
