package org.envoy.typepersisters;

import java.util.*;

public class TypePersisterHashMap extends HashMap<Class<?>, TypePersister<?>> implements
        TypePersisterMap {
    private static final long serialVersionUID = -6270980085747029816L;

    private final Map<Class<?>, Class<?>> primitiveTypeMap;


    public TypePersisterHashMap() {
        primitiveTypeMap = new HashMap<Class<?>, Class<?>>();
        primitiveTypeMap.put(Integer.class, int.class);
        primitiveTypeMap.put(Long.class, long.class);
        primitiveTypeMap.put(Short.class, short.class);
        primitiveTypeMap.put(Byte.class, byte.class);
        primitiveTypeMap.put(Double.class, double.class);
        primitiveTypeMap.put(Character.class, char.class);
    }


    @Override
    public void put(final TypePersister<?> typePersister) {
        put(typePersister.getJavaType(), typePersister);
    }


    @Override
    public TypePersister<?> put(
            final Class<?> key,
            final TypePersister<?> value) {
        TypePersister<?> result = null;
        result = super.put(key, value);

        if (primitiveTypeMap.containsKey(key)) {
            super.put(primitiveTypeMap.get(key), value);
        }

        return result;
    }
}
