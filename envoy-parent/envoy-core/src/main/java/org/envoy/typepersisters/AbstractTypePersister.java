package org.envoy.typepersisters;

import java.sql.*;

public abstract class AbstractTypePersister<T> implements TypePersister<T> {
    private final Class<T> clazz;


    protected abstract void setImpl(PreparedStatement ps, int fieldIndex, T value) throws SQLException;


    public AbstractTypePersister(final Class<T> clazz) {
        this.clazz = clazz;
    }


    @Override
    public Class<T> getJavaType() {
        return clazz;
    }


    @Override
    @SuppressWarnings("unchecked")
    public void set(final PreparedStatement ps, final int fieldIndex, final Object value) throws SQLException {
        setImpl(ps, fieldIndex, (T) value);
    }
}
