package org.envoy;

import org.envoy.exceptions.*;
import org.envoy.expression.*;
import org.envoy.query.*;
import org.envoy.typepersisters.*;

public abstract class AbstractDatabaseMetaData<ConnectionType extends AutoCloseable> implements DatabaseMetaData<ConnectionType> {
    private final EntityFactory entityFactory;
    private final TypePersisterMap typePersisterMap;
    private final ExceptionTransformer exceptionTransformer;
    private final ExpressionFactory expressionFactory;
    private final QueryBuilder queryBuilder;


    public AbstractDatabaseMetaData(final ExceptionTransformer exceptionTransformer) {
        entityFactory = new EntityFactoryImpl();
        typePersisterMap = new DefaultTypePersisterHashMap();
        expressionFactory = new ExpressionFactoryImpl();
        queryBuilder = new QueryBuilderImpl(this);
        this.exceptionTransformer = exceptionTransformer;
    }


    @Override
    public EntityFactory getEntityFactory() {
        return entityFactory;
    }


    @Override
    public TypePersisterMap getTypePersisterMap() {
        return typePersisterMap;
    }


    @Override
    public ExceptionTransformer getExceptionTransformer() {
        return exceptionTransformer;
    }


    @Override
    public ExpressionFactory getExpressionFactory() {
        return expressionFactory;
    }


    @Override
    public QueryBuilder getQueryBuilder() {
        return queryBuilder;
    }
}
