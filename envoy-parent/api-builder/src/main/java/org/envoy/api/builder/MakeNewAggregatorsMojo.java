package org.envoy.api.builder;

import org.apache.maven.plugin.*;
import org.apache.maven.plugins.annotations.*;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.project.*;
import org.apache.velocity.*;
import org.apache.velocity.app.*;
import org.apache.velocity.exception.*;
import org.apache.velocity.runtime.*;
import org.apache.velocity.runtime.resource.loader.*;

import java.io.*;
import java.nio.file.*;

@Mojo(name = "new-aggregators")
public class MakeNewAggregatorsMojo extends AbstractMojo {
    @Component
    private MavenProject project;

    @Parameter(property = "new-aggregators.numberOfColumns")
    private int numberOfColumns;


    @Override
    public void execute() throws MojoExecutionException {
        int currentNumberOfColumns = CountAggregatorsMojo.getAggregatorsCount(project.getBuild().getSourceDirectory());
        getLog().debug("Start after: " + currentNumberOfColumns);
        getLog().debug("End after: " + numberOfColumns);

        VelocityEngine engine = new VelocityEngine();
        engine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        engine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        engine.init();

        try (StringWriter stringWriter = new StringWriter()) {
            StringBuilder sbImportList = new StringBuilder();
            for (int i = currentNumberOfColumns + 1; i <= numberOfColumns; i++) {
                VelocityContext context = new VelocityContext();
                context.put("numberOfColumns", i);

                createJavaFile("ColumnAggregator" + i, context, engine, "column-aggregator-definition.vm");
                createJavaFile("AggregatorWrapper" + i, context, engine, "aggregator-wrapper-definition.vm");
                createJavaFile("AggregatorWrapper" + i + "Tests", context, engine, "aggregator-wrapper-tests-definition.vm");
                createJavaFile("AggregatedSelectQuery" + i, context, engine, "aggregated-select-query-definition.vm");
                createJavaFile(
                        "AggregatedSelectQuery" + i + "Tests",
                        context,
                        engine,
                        "aggregated-select-query-tests-definition.vm");
                addAggregateMethod(stringWriter, context, engine);
                sbImportList.append("import org.envoy.query.grouping.AggregatedSelectQuery" + i + ";\n");
            }

            Path pathToBasicListableSelectQueryImpl = Paths.get(
                    project.getBuild().getSourceDirectory(),
                    "org",
                    "envoy",
                    "query",
                    "BasicListableSelectQueryImpl.java");
            getLog().debug(pathToBasicListableSelectQueryImpl.toString());

            StringBuilder sbFileContents = new StringBuilder();
            try (BufferedReader reader = new BufferedReader(new FileReader(pathToBasicListableSelectQueryImpl.toFile()))) {
                String line = null;
                while ((line = reader.readLine()) != null) {
                    if (line.trim().startsWith("//")) {
                        if (line.trim().contains("@placeholder:import")) {
                            sbFileContents.append(sbImportList);
                            getLog().debug(sbImportList.toString());
                        }
                        if (line.trim().contains("@placeholder:aggregate()")) {
                            String result = stringWriter.toString();
                            sbFileContents.append(result);
                            getLog().debug(result);
                        }
                    }
                    sbFileContents.append(line).append("\n");
                    getLog().debug(line);
                }
            }

            try (FileWriter writer = new FileWriter(pathToBasicListableSelectQueryImpl.toFile())) {
                writer.write(sbFileContents.toString());
            }
        } catch (IOException e) {
            throw new MojoExecutionException("Could not generate aggregate methods for BasicListableSelectQueryImpl", e);
        }
    }


    private Path getGroupingPath(String className) {
        String sourceDir = project.getBuild().getSourceDirectory();
        if (className.endsWith("Tests")) {
            sourceDir = project.getBuild().getTestSourceDirectory();
        }
        return Paths.get(sourceDir, "org", "envoy", "query", "grouping", className + ".java");
    }


    private void createJavaFile(String className, VelocityContext context, VelocityEngine engine, String templatePath)
            throws MojoExecutionException {
        Path path = getGroupingPath(className);
        try (FileWriter fileWriter = new FileWriter(path.toString())) {
            engine.getTemplate("/org/envoy/api/builder/" + templatePath).merge(context, fileWriter);
            getLog().debug("Finished creating " + path.toString());
        } catch (IOException | ResourceNotFoundException | ParseErrorException e) {
            if (path.toFile().exists()) {
                try {
                    Files.delete(path);
                } catch (IOException e1) {
                    throw new MojoExecutionException(
                            "Could not cleanup " + path.getFileName().toString()
                            + " when faced with an error.  Tried cleanup because of: " + e.toString(),
                            e1);
                }
            }

            throw new MojoExecutionException("Could not create " + path.getFileName().toString(), e);
        }
    }


    private void addAggregateMethod(Writer stringWriter, VelocityContext context, VelocityEngine engine)
            throws MojoExecutionException {
        try {
            engine.getTemplate("/org/envoy/api/builder/aggregate-method-definition.vm").merge(context, stringWriter);
            getLog().debug("Added aggregate method.");
        } catch (ResourceNotFoundException | ParseErrorException e) {
            throw new MojoExecutionException("Could not create aggregate method", e);
        }
    }
}
