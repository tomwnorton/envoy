package org.envoy.api.builder;

import org.apache.maven.plugin.*;
import org.apache.maven.plugins.annotations.*;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.project.*;

import java.io.*;
import java.nio.file.*;
import java.nio.file.DirectoryStream.*;

@Mojo(name = "count-aggregators")
public class CountAggregatorsMojo extends AbstractMojo {
    @Component
    private MavenProject project;


    @Override
    public void execute() throws MojoExecutionException {
        try (
                DirectoryStream<Path> stream = CountAggregatorsMojo.getColumnAggregatorFiles(
                        project.getBuild()
                                .getSourceDirectory())) {
            getLog().info("count: " + getAggregatorsCount(project.getBuild().getSourceDirectory()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        getLog().info("SOURCE DIRECTORY: " + project.getBuild().getSourceDirectory());
        getLog().info("TEST DIRECTORY: " + project.getBuild().getTestSourceDirectory());
    }


    public static int getAggregatorsCount(String sourceDirectory) throws MojoExecutionException {
        try (DirectoryStream<Path> stream = getColumnAggregatorFiles(sourceDirectory)) {
            int max = 0;
            for (Path entry : stream) {
                Path fileName = entry.getFileName();
                String numberPart = fileName.toString().substring(
                        "ColumnAggregator".length(),
                        fileName.toString().length() - ".java".length());
                try {
                    int temp = Integer.parseInt(numberPart);
                    if (temp > max) {
                        max = temp;
                    }
                } catch (NumberFormatException e) {
                    throw new MojoExecutionException(
                            "The following is not a valid ColumnAggregator class: " + entry.toString());
                }
            }
            return max;
        } catch (IOException e) {
            throw new MojoExecutionException("Could not count the number of aggregators", e);
        }
    }


    private static DirectoryStream<Path> getColumnAggregatorFiles(String sourceDirectory) throws IOException {
        Path groupingSourcePath = Paths.get(sourceDirectory, "org", "envoy", "query", "grouping");
        return Files.newDirectoryStream(
                groupingSourcePath, new Filter<Path>() {
                    @Override
                    public boolean accept(Path entry) throws IOException {
                        return entry.getFileName().toString().startsWith("ColumnAggregator")
                               && entry.getFileName().toString().endsWith(".java");
                    }
                });
    }
}
